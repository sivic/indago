python>=3.9
numpy>=1.22.2
matplotlib>=3.6
scipy>=1.7
rich>=12.5
pillow>=10.0.1 # not directly required, pinned by Snyk to avoid a vulnerability
# Packages for building Sphinx documentation on ReadTheDocs 
#setuptools>=65.5.1
#myst-parser
#sphinx_rtd_theme
