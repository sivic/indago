# Indago

Indago is a Python 3 module for numerical optimization.

Indago contains several modern methods for real fitness function optimization over a real parameter domain. It was developed at the Department for Fluid Mechanics and Computational Engineering of the University of Rijeka, Faculty of Engineering, by Stefan Ivić, Siniša Družeta, Luka Grbčić, and others. 

Indago is developed for in-house research and teaching purposes and comes with no guarantees whatsoever. However, we use it regularly and it seems to be working fine. Hopefully you will find it useful as well.

**Important: After every Indago update please check documentation since Indago methods and API can undergo significant changes at any time.**

## Installation

You can install Indago via pip:
```
pip install indago
```
If you wish to update your existing Indago installation, just do:
```
pip install indago --upgrade
```

## Documentation

Full documentation is avaliable at [indago.readthedocs.io](https://indago.readthedocs.io).
