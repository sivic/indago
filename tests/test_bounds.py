import sys
sys.path.append('..')
sys.path.append('../..')
sys.path.append('../indago/')

import indago
import numpy as np


def f(x):
    assert np.all(np.abs(x) <= 100), f'OUT OF BOUNDS! {x=}'
    # return np.sum((100 + np.arange(x.size) - x) ** 2)
    # return - np.sum(x ** 2)
    return -np.sum((x - np.arange(x.size)) ** 2)

optimizers = indago.optimizers[10:]

def test_bounds():

    for optimizer_class in optimizers:

        print(f'Testing {optimizer_class.__name__}')
        optimizer = optimizer_class()
        optimizer.evaluation_function = f
        optimizer.dimensions = 10
        optimizer.lb = -100
        optimizer.ub = 100
        optimizer.X0 = np.random.uniform(optimizer.lb, optimizer.ub, [20, optimizer.dimensions])
        optimizer.max_evaluations = 20000
        # optimizer.params['xtol'] = 1e-6
        # optimizer.target_fitness = 285 + 1e-6
        optimizer.monitoring = 'none'
        # optimizer.convergence_log_file = f'test_bounds_{optimizer_class.__name__}.log'

        opt = optimizer.optimize()

        x_max = np.max(np.abs(opt.X))
        #print(f'{x_max}, {x_max < 100}, {x_max == 100}, {x_max > 100}, f={opt.f} in {optimizer.eval} evaluations')

        assert x_max <= 100, f'{optimizer_class.__name__} breached bounds!'
