# -*- coding: utf-8 -*-
import os
# need this for local (non-pip) install only
import sys
sys.path.append('..')

import pytest

import indago
from indagobench import CEC2014
import numpy as np

dims = 10
cec = CEC2014('F1', 10)
# np.random.seed(0)

optimizers = indago.optimizers

def run_optimizers(stop):

    for optimizer_class in optimizers:

        print(f'\nTesting {optimizer_class.__name__} with {stop} stopping criteria')
        optimizer = optimizer_class()
        optimizer.dimensions = dims
        optimizer.lb = -100
        optimizer.ub = 100
        optimizer.evaluation_function = cec
        # optimizer.monitoring = 'dashboard'
        optimizer.convergence_log_file = f'test_stopping_criteria_{optimizer_class.__name__}.log'

        if stop == 'none':
            pass
        elif stop == 'max_iterations':
            optimizer.max_iterations = 5
        elif stop == 'max_evaluations':
            optimizer.max_evaluations = 100
        elif stop == 'max_stalled_iterations':
            optimizer.max_stalled_iterations = 2
            if optimizer_class.__name__ in 'SSA MRFO GWO'.split():
                print('Skipping test')
                continue
        elif stop == 'max_stalled_evaluations':
            optimizer.max_stalled_evaluations = 5
            if optimizer_class.__name__ in 'SSA MRFO GWO'.split():
                print('Skipping test')
                continue
        elif stop == 'target_fitness':
            optimizer.target_fitness = 2.0e7
            if optimizer_class.__name__ in 'SSA MRFO GWO'.split():
                print('Skipping test')
                continue
        elif stop == 'max_elapsed_time':
            optimizer.max_elapsed_time = 5
        else:
            assert False, f'Unknown stopping criteria {stop}'

        optimizer.optimize(seed=0)

        log_file = open(optimizer.convergence_log_file, 'r')
        stop_log = log_file.readlines()[-4]
        log_file.close()
        os.remove(optimizer.convergence_log_file)
        status = str(optimizer.status).split('(')[0].strip()
        log = stop_log.split('(')[0].split('#')[-1].strip()
        # print(f'#{optimizer.status=}')
        # print(f'#{optimizer._err_msg=}')
        # print(f'#{stop_log=}')

        if stop == 'none':
            assert log == 'Optimization finished: maximum number of evaluations reached', \
                f'Unexpected stopping criteria: {log}'
            assert status == 'FINISHED: Optimization finished', \
                f'Unexpected Optimizer.status: {status}'
        elif stop == 'max_iterations':
            assert log == 'Optimization finished: maximum number of iterations reached', \
                f'Unexpected stopping criteria: {log}'
            assert status == 'FINISHED: Optimization finished', \
                f'Unexpected Optimizer.status: {status}'
        elif stop == 'max_evaluations':
            assert log == 'Optimization finished: maximum number of evaluations reached', \
                f'Unexpected stopping criteria: {log}'
            assert status == 'FINISHED: Optimization finished', \
                f'Unexpected Optimizer.status: {status}'
        elif stop == 'max_stalled_iterations':
            assert log == 'Optimization finished: maximum stalled iterations reached', \
                f'Unexpected stopping criteria: {log}'
            assert status == 'FINISHED: Optimization finished',\
                f'Unexpected Optimizer.status: {status}'
        elif stop == 'max_stalled_evaluations':
            stop_log = stop_log.split()
            del stop_log[5]
            assert log == 'Optimization finished: maximum stalled evaluations reached', \
                f'Unexpected stopping criteria: {log}'
            assert status == 'FINISHED: Optimization finished', \
                f'Unexpected Optimizer.status: {status}'
        elif stop == 'target_fitness':
            assert log == 'Optimization finished: target fitness achieved', \
                f'Unexpected stopping criteria: {log}'
            assert status == 'FINISHED: Optimization finished', \
                f'Unexpected Optimizer.status: {status}'
        elif stop == 'max_elapsed_time':
            assert log == 'Optimization finished: maximum elapsed time reached',\
                f'Unexpected stopping criteria: {log}'
            assert status == 'FINISHED: Optimization finished', \
                f'Unexpected Optimizer.status: {status}'
        else:
            assert False, f'Unknown stopping criteria {stop}'

        # os.remove(optimizer.convergence_log_file)
        del optimizer


def test_stopping_criteria_none():
    run_optimizers('none')

def test_stopping_criteria_max_iterations():
    run_optimizers('max_iterations')

def test_stopping_criteria_max_evaluations():
    run_optimizers('max_evaluations')

def test_stopping_criteria_max_stalled_iterations():
    run_optimizers('max_stalled_iterations')

def test_stopping_criteria_max_stalled_evaluations():
    run_optimizers('max_stalled_evaluations')

def test_stopping_criteria_max_elapsed_time():
    run_optimizers('max_elapsed_time')

def test_stopping_criteria_target_fitness():
    run_optimizers('target_fitness')
