# -*- coding: utf-8 -*-
"""
test inspect utility functions
"""

# need this for local (non-pip) install only
import sys

sys.path.append('..')

import numpy as np
from indagobench import CEC2014
from indago import inspect, inspect_optimizers
from indago import PSO, DE

MAXEVAL = 1000
RUNS = 15


def F(x):
    obj = np.sum(x ** 2)  # minimization objective
    constr1 = x[0] - x[1]  # constraint x_0 - x_1 <= 0
    constr2 = - np.sum(x)  # constraint sum x_i >= 0
    return obj, constr1, constr2


DIM = 8  # number of variables (i.e. size of design vector x)
LB = -10  # lower bound, given as scalar (equal for all variables)
UB = 10 + np.arange(DIM)


def test_inspect():
    res = inspect(F, LB, UB,
                  objectives=1, constraints=2,
                  evaluations=MAXEVAL,
                  optimizers_name_list=['PSO', 'FWA', 'MRFO', 'ABC'],
                  runs=RUNS,
                  optimize_seed=0,
                  printout=False)


def test_inspect_optimizers():

    opt1 = PSO()
    opt1.evaluation_function = F
    opt1.constraints = 2
    opt1.dimensions = DIM
    opt1.lb = LB
    opt1.ub = UB
    opt1.max_iterations = 10000
    opt1.max_evaluations = MAXEVAL
    opt1.variant = 'TVAC'

    opt2 = DE()
    opt2.evaluation_function = CEC2014('F3', 20)
    opt2.max_iterations = 10000
    opt2.max_evaluations = MAXEVAL
    opt2.variant = 'LSHADE'

    opt_dict = {'TVAC-PSO on constrained function': opt1,
                'LSHADE on 20d CEC F3': opt2}
    res = inspect_optimizers(opt_dict, runs=RUNS, seed=0, printout=False)


if __name__ == '__main__':
    test_inspect()
    test_inspect_optimizers()