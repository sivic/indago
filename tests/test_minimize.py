# -*- coding: utf-8 -*-
"""
test indago.minimize
"""

# need this for local (non-pip) install only
import sys
sys.path.append('..')

import numpy as np
from indagobench import CEC2014
from indago import minimize, minimize_exhaustive


DIM = 10
F = CEC2014('F3', DIM)
MAXEVAL = 1000
TOL = 1e-10

params = {'swarm_size': 10,
          'inertia': 0.6,
          'cognitive_rate': 2.0,
          'social_rate': 2.0
          }

def test_minimize():
    X, f = minimize(F, F.lb, F.ub,
                    'PSO',
                    0,  # seed for Optimizer.optimize() (needed for reproducibility)
                    variant='Vanilla',
                    max_evaluations=MAXEVAL,
                    params=params
                    )
    expected_result = 4.845127336717785
    result = np.log10(f)
    assert expected_result - TOL < result < expected_result + TOL, \
        f'TEST FAILED, result={result}'

def test_minimize_exhaustive():
    (X, f), optimal_params = \
        minimize_exhaustive(F, F.lb, F.ub,
                            'PSO',  # optimizer
                            {'cognitive_rate': [0, 2], 'social_rate': [0, 2]},
                            'FWA',  # hyper-optimizer
                            runs=10,  # optimizer runs
                            hyper_evaluations=3 * DIM,  # hyper-optimizer evaluations
                            optimize_seed=0,  # seed for Optimizer.optimize() (needed for reproducibility)
                            max_evaluations=MAXEVAL
                            )
    expected_result = 4.291164164179561
    result = np.log10(f)
    assert expected_result - TOL < result < expected_result + TOL, \
        f'TEST FAILED, result={result}'


# stand-alone testing
if __name__ == '__main__':
    test_minimize()
    test_minimize_exhaustive()
