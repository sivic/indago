.. Indago documentation master file, created by
   sphinx-quickstart on Mon Feb 13 12:45:01 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Indago 0.5.3 documentation!
======================================
.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   getting_started
   method_parameters
   api
   release_notes

   
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

.. * :ref:`modindex`
