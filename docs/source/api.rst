API
===

Utility functions
-----------------

.. autofunction:: indago.minimize
.. autofunction:: indago.minimize_exhaustive
.. autofunction:: indago.inspect
.. autofunction:: indago.inspect_optimizers
.. autofunction:: indago.unconstrain


Utility constants
-----------------

.. autodata:: indago.optimizers
   :annotation:
   
.. autodata:: indago.optimizers_name_list
   :annotation:

.. autodata:: indago.optimizers_dict
   :annotation:


Classes
-------

.. autoclass:: indago.Optimizer
    :members:
    :private-members:
    
.. autoclass:: indago.Candidate
    :members:
    :private-members:
    
.. autoclass:: indago.PSO
    :members:
    :private-members:
    
.. autoclass:: indago._pso.Particle
    :members:
    :private-members:

.. autoclass:: indago.FWA
    :members:
    :private-members: 

.. autoclass:: indago.SSA
    :members:
    :private-members: 

.. autoclass:: indago.DE
    :members:
    :private-members: 
    
.. autoclass:: indago._de.Solution
    :members:
    :private-members: 
    
.. autoclass:: indago.BA
    :members:
    :private-members: 
    
.. autoclass:: indago._ba.Bat
    :members:
    :private-members: 
    
.. autoclass:: indago.EFO
    :members:
    :private-members: 

.. autoclass:: indago.MRFO
    :members:
    :private-members: 

.. autoclass:: indago.ABC
    :members:
    :private-members: 

.. autoclass:: indago.NM
    :members:
    :private-members:
    
.. autoclass:: indago.MSGD
    :members:
    :private-members:
