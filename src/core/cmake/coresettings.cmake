
# compiler options
if(CMAKE_BUILD_TOOL MATCHES "(msdev|devenv|nmake)")
    add_definitions(/W4)
    add_definitions(/MP)
elseif (CMAKE_COMPILER_IS_GNUCXX)# OR CMAKE_COMPILER_IS_GNUC)
  add_definitions(-std=c++0x)
  add_definitions(-Wall)
  add_definitions(-Wextra)
  add_definitions(-pedantic)
  else (CMAKE_BUILD_TOOL MATCHES "(msdev|devenv|nmake)")
  message(STATUS "Unknown build tool, cannot set warning flags for your")
endif (CMAKE_BUILD_TOOL MATCHES "(msdev|devenv|nmake)")


macro(SetFolder pth)
    message("adding build files in folder :" ${pth})
    set(pth_FOLDER ${pth})
    include_directories(${pth_FOLDER})
    
    file(GLOB MODEL_SRCS ${pth_FOLDER}/[a-z]*.cpp)
    file(GLOB MODEL_HDRS ${pth_FOLDER}/[a-z]*.h)
    
    set(ALL_SRCS ${ALL_SRCS} ${MODEL_SRCS} )
    set(ALL_HDRS ${ALL_HDRS} ${MODEL_HDRS} )
endmacro()

# general folder
SetFolder(${PROJECT_SOURCE_DIR}/src/core/general)

# model folder
SetFolder(${PROJECT_SOURCE_DIR}/src/core/model)

    

# optimizers folder
SetFolder(${PROJECT_SOURCE_DIR}/src/core/optimizers)
SetFolder(${PROJECT_SOURCE_DIR}/src/core/optimizers/patternsearch)
SetFolder(${PROJECT_SOURCE_DIR}/src/core/optimizers/pso)
SetFolder(${PROJECT_SOURCE_DIR}/src/core/optimizers/geneticAlgo)


#paretofront folder
SetFolder(${PROJECT_SOURCE_DIR}/src/core/jobs)
SetFolder(${PROJECT_SOURCE_DIR}/src/core/jobs/paretofront)

# evaluator folder
SetFolder(${PROJECT_SOURCE_DIR}/src/core/evaluator)

# jobs folder
SetFolder(${PROJECT_SOURCE_DIR}/src/core/jobs)
