#ifndef IDHANDLING_H
#define IDHANDLING_H

#include <QString>
#include <QVector>

class IdHandling
{
public:
    static bool validateIdString(QString _id)
    {
        QString allowableChars = QString("0123456789") +
                                QString("ABCDEFGHIJKLMNOPQRSTUVWXYZ") + 
                                QString("abcdefghijklmnopqrstuvwxyz") +
                                QString("_");
        
        for(int i=0; i<_id.count(); i++)
        {
            if(!allowableChars.contains(_id.at(i))) return false;
        }
        return true;
    };
	
	template<class T>
	static bool checkIdUnique(const QString _id, QVector<T*> _t)
	{
		for (auto* prob : _t)
        {
            if(prob->id() == _id) return false;
        }
        return true;
	}
	
	template<class T>
	static QString generateUniqueId(const QString _prefix,
									const size_t _numberOfElements,
									QVector<T*> _t)
	{
		bool uniqueId = false;
		QString id = "";
		int index = _numberOfElements;
		while(!uniqueId)
		{
			id = _prefix + QString::number(index + 1);
			uniqueId = true;
			for (auto* p : _t)
			{
				if(p->id() == id)
				{
					uniqueId = false;
					break;
				}
			}   
			++index;
		}
		return id;
	}
	
    template<class T>
    static QString changedIdValue(QString _newId, QVector<T*> _t)
	{
        bool valid = IdHandling::validateIdString(_newId);
        bool uniqueId = true;
        for (auto* p : _t)
        {
            if(p->id() == _newId)
            {
                uniqueId = false;
                break;
            }
        }
		
		if(valid && uniqueId)
		{
            return _newId;
		}
        else return "";
    }
    
    template<class T>
    static bool idExists(QString _id, QVector<T*> _t)
	{
		for (T *p : _t)
		{
			if (p->id() == _id) return true;
		}
		return false;
	}
};
#endif // IDHANDLING_H
