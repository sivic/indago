#ifndef CONSOLEOUTPUT_H
#define CONSOLEOUTPUT_H

#include <QtCore/QObject>
#include <iostream>

class ConsoleOutput : public QObject
{
    Q_OBJECT

public:
    ConsoleOutput(QObject* _parrent = 0);
    
public slots:
    void onAppendToLog(QString _msg);
};

#endif // CONSOLEOUTPUT_H
