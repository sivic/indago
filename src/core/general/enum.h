#ifndef ENUM_H
#define ENUM_H

#include <QMetaEnum>
#include <QStringList>

template<typename def, typename inner = typename def::type>
class SafeEnum : public def
{
  typedef typename def::type type;
  inner val;

public:

  SafeEnum(type v) : val(v) {}
  inner underlying() const { return val; }

  bool operator == (const SafeEnum & s) const { return this->val == s.val; }
  bool operator != (const SafeEnum & s) const { return this->val != s.val; }
  bool operator <  (const SafeEnum & s) const { return this->val <  s.val; }
  bool operator <= (const SafeEnum & s) const { return this->val <= s.val; }
  bool operator >  (const SafeEnum & s) const { return this->val >  s.val; }
  bool operator >= (const SafeEnum & s) const { return this->val >= s.val; }
};

/*!
 * To use this macro include QMetaEnum
 */
#define ENUM_TO_STRING(CLASS_NAME, ENUM_NAME) \
static QString enumToString(ENUM_NAME _enumerator)\
{\
    const QMetaObject &mo = CLASS_NAME::staticMetaObject;\
    int index = mo.indexOfEnumerator(#ENUM_NAME);\
    QMetaEnum metaEnum = mo.enumerator(index);\
    return QString(metaEnum.key((int)_enumerator));\
}\

/*!
 * To use this macro include QMetaEnum
 */
#define STRING_TO_ENUM(CLASS_NAME, ENUM_NAME) \
static ENUM_NAME stringToEnum##ENUM_NAME(QString _enumerator)\
{\
    const QMetaObject &mo = CLASS_NAME::staticMetaObject;\
    int index = mo.indexOfEnumerator(#ENUM_NAME);\
    QMetaEnum metaEnum = mo.enumerator(index);\
    int nEnums = metaEnum.keyCount();\
    QStringList retLst;\
    for(int iEnum=0; iEnum<nEnums; iEnum++)\
        if(_enumerator == QString(metaEnum.key(iEnum))) return (ENUM_NAME)iEnum;\
    assert(false && "Invalid string to define CLASS_NAME##::##ENUM_NAME");\
}\


/*!
 * To use this macro include QMetaEnum
 */
#define ENUM_COUNT(CLASS_NAME, ENUM_NAME) \
static int enum ## ENUM_NAME ## Count(void)\
{\
    const QMetaObject &mo = CLASS_NAME::staticMetaObject;\
    int index = mo.indexOfEnumerator(#ENUM_NAME);\
    QMetaEnum metaEnum = mo.enumerator(index);\
    return metaEnum.keyCount();\
}\

/*!
 * To use this macro include QMetaEnum and QStringList
 */
#define ENUM_TO_STRINGLIST(CLASS_NAME, ENUM_NAME) \
static QStringList enum ## ENUM_NAME ## ToStringList(void)\
{\
    const QMetaObject &mo = CLASS_NAME::staticMetaObject;\
    int index = mo.indexOfEnumerator(#ENUM_NAME);\
    QMetaEnum metaEnum = mo.enumerator(index);\
    int nEnums = metaEnum.keyCount();\
    QStringList retLst;\
    for(int iEnum=0; iEnum<nEnums; iEnum++)\
        retLst.append(QString(metaEnum.key(iEnum)));\
    return retLst;\
}\

    /*
    const QMetaObject &mo = PatternSearch::staticMetaObject;
    int index = mo.indexOfEnumerator("Parameters"); // watch out during refactorings
    QMetaEnum metaEnum = mo.enumerator(index);
    metaEnum.key(0);
    */

#endif // ENUM_H