#ifndef RANDOMGENERATOR_H
#define RANDOMGENERATOR_H

#include <random>

class RandomGenerator
{

public:
RandomGenerator();
virtual ~RandomGenerator();

double randomDouble(double lowerBound,double upperBound);
int randomInt(int lowerBound,int upperBound);
char randomBit();

private:
  std::random_device rd;
  std::default_random_engine dre;
};

#endif // RANDOMGENERATOR_H
