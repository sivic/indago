#ifndef MYMATH_H
#define MYMATH_H
#include <cmath>
#ifdef _WINDOWS
	#ifndef M_PI
	namespace
	{
		const double M_PI = std::acos(-1.0);
	}
	#endif
#endif // _WINDOWS
#endif // MYMATH_H