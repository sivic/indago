#include "randomgenerator.h"

RandomGenerator::RandomGenerator()
{
    dre.seed(rd());
}

RandomGenerator::~RandomGenerator()
{

}

double RandomGenerator::randomDouble(double lowerBound, double upperBound)
{

  std::uniform_real_distribution<double> unif(lowerBound,upperBound);
  double rand=unif(dre);
  return rand;

}

int RandomGenerator::randomInt(int lowerBound, int upperBound)
{
  std::uniform_int_distribution<int> unif(lowerBound,upperBound);
  int rand=unif(dre);
  return rand;

}

char RandomGenerator::randomBit()
{
  std::uniform_int_distribution<int>unif (0,1);
  int rand=unif(dre);
  if(rand==0)
  {
    return '0';
  }
  else
  {
    return '1';
  }

}

