#ifndef EVALUATORRESULTS_H
#define EVALUATORRESULTS_H

class EvaluatorResults
{
public:
    EvaluatorResults(const double& val) : primaryValue(val){}
    double value() const {return primaryValue;}
private:
    double primaryValue;
};

#endif // EVALUATORRESULTS_H
