#ifndef EVALUATORFACTORY_H
#define EVALUATORFACTORY_H

#include "evaluator.h"
#include "builtinevaluator.h"
#include "pythonevaluator.h"
#include "evaluatordefinition.h"

class EvaluatorFactory
{
public:
    static Evaluator* createEvaluator(EvaluatorDefinition* _definition);
};

#endif // EVALUATORFACTORY_H
