#include "pythonevaluator.h"

PythonEvaluator::PythonEvaluator(void):
Evaluator(EvaluatorDefinition::EvaluatorType::python)
{

}

EvaluatorResults PythonEvaluator::evaluate(const QVector< QVariant > _x)
{
    return EvaluatorResults(_x.first().toDouble());
}
