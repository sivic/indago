#include "builtinevaluator.h"



BuiltinEvaluator::BuiltinEvaluator(BuiltInEvaluatorDefinition* _definition):
Evaluator(EvaluatorDefinition::EvaluatorType::builtIn)
{
    if(_definition->function() == BuiltInEvaluatorDefinition::TestFunction::Sphere)
    {
        m_function = sphere();
    }
    else if(_definition->function() == BuiltInEvaluatorDefinition::TestFunction::Rastrigin)
    {
        m_function = rastrigin();
    }
    else if(_definition->function() == BuiltInEvaluatorDefinition::TestFunction::Rosenbrock)
    {
        m_function = rosenbrock();
    }
    else if(_definition->function() == BuiltInEvaluatorDefinition::TestFunction::Schwefel)
    {
        m_function = schwefel();
    }
    else if(_definition->function() == BuiltInEvaluatorDefinition::TestFunction::Alpine)
    {
        m_function = alpine();
    }
    else if(_definition->function() == BuiltInEvaluatorDefinition::TestFunction::Ackley)
    {
        m_function = ackley();
    }
    else
    {
        m_function = nullptr;
    }
}

EvaluatorResults BuiltinEvaluator::evaluate(QVector< QVariant > _x)
{
    return m_function(_x);
}

