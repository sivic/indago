#include "evaluator.h"

Evaluator::Evaluator(EvaluatorDefinition::EvaluatorType _type):
m_type(_type)
{

}

EvaluatorResults Evaluator::evaluate(const QVector< double > _x)
{
    QVector<QVariant> x;
    for(int i=0; i<_x.count(); i++)
        x.append(QVariant(_x[i]));
    return evaluate(x);
}

EvaluatorResults Evaluator::evaluate(const QList< double > _x)
{
    QVector<QVariant> x;
    for(int i=0; i<_x.count(); i++)
        x.append(QVariant(_x[i]));
    return evaluate(x);
}

OptimizationProblem* Evaluator::optimizationProblem(void )
{
    return m_optimizationProblem;
}
