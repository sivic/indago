#include "evaluatorfactory.h"

Evaluator* EvaluatorFactory::createEvaluator(EvaluatorDefinition* _definition)
{
    if(_definition->type() == EvaluatorDefinition::EvaluatorType::builtIn)
    {
        return new BuiltinEvaluator((BuiltInEvaluatorDefinition*)_definition);
    }
    else if(_definition->type() == EvaluatorDefinition::EvaluatorType::python)
    {
        return new PythonEvaluator();
    }
    else
    {
        return nullptr;
    }    
}
