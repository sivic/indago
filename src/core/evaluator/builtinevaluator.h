#ifndef BUILTINEVALUATOR_H
#define BUILTINEVALUATOR_H

#define _USE_MATH_DEFINES
#include <cmath>
#include "mymath.h"

#include "evaluator.h"



/*!
 * Provides various built-in functions for optimization benchmark and testing.
 */
class BuiltinEvaluator: public Evaluator
{
public:
    /*!
     * \brief Constructor of built-in evaluator.
     * 
     * All neccessary parameters are automatically set based on given _definition. 
     */
    BuiltinEvaluator(BuiltInEvaluatorDefinition* _definition);
        
    /*!
     * \brief Function used for evaluation of given set of values.
     */
    EvaluatorResults evaluate(QVector<QVariant> _x);
    
private:
    /*!
     * \brief Pointer to built-in function that is used on calling evaluation function.
     */
    std::function<EvaluatorResults(QVector<QVariant>)> m_function;
    
};

/*!
* \brief Sphere test function.
* 
* \f$ f(x)= \sum _i^n x_i^2 \f$
*/
struct sphere
{
    EvaluatorResults operator()(QVector< QVariant > _x) const
    {
        double sum = 0.0;
        for(const auto& val : _x)
        {
            sum += std::pow(val.toDouble(), 2.);
        }
        return EvaluatorResults(sum);
    }
};

/*!
* \brief Rastrigin test function.
* 
* \f$ f(x)= 10n\sum _i^n (x_i^2 - 10 cos(2 \pi x_i)) \f$
*/
struct rastrigin
{
    EvaluatorResults operator()(QVector< QVariant > _x) const
    {
        double sum = 10.0 * _x.size();
        for(const auto& val : _x)
        {
            double x = val.toDouble();
            sum +=  std::pow(x, 2.0) - 10.0*std::cos(2.0*M_PI*x);
        }
        return EvaluatorResults(sum);
    }
};

/*!
* \brief Rosenbrock test function.
* 
* \f$ f(x)= \sum _i^n (1-x_i)^2 + 100(x_{i+1} - x_i^2)^2 \f$
*/
struct rosenbrock
{
    EvaluatorResults operator()(QVector< QVariant > _x) const
    {
        double sum = 0.0;
        double x0 = _x.first().toDouble();
        double x1, xx;
        for(int i=1; i<_x.count(); i++)
        {
            x1 = _x[i].toDouble();
            xx = 1.0 - x0;
            sum += xx * xx;
            xx = x1 - x0*x0;
            sum += 100.0 * xx *xx;
            x0 = x1;
        }
        return EvaluatorResults(sum);
    }
};

/*!
* \brief Schwefel test function.
* 
* \f$ f(x)= 418.9829 n \sum _i^n -x_i \sin \sqrt{\|x_i\|}  \f$
*/
struct schwefel
{
    EvaluatorResults operator()(QVector< QVariant > _x) const
    {
        double sum = 418.9829 * double(_x.count());
        for(int i=0; i<_x.count(); i++)
        {
            sum +=  - _x[i].toDouble() * std::sin(qAbs(_x[i].toDouble()));
        }
        return EvaluatorResults(sum);
    }
};

/*!
* \brief Alpine test function.
*/
struct alpine
{
    EvaluatorResults operator()(QVector< QVariant > _x) const
    {
        double sum = 0.0;
        double x;
        for(int i=0; i<_x.count(); i++)
        {
            x = _x[i].toDouble();
            sum += std::fabs(x*std::sin(x) + 0.1*x);
        }
        return EvaluatorResults(sum);
    }
};

/*!
* \brief Ackley test function.
*/
struct ackley
{
    EvaluatorResults operator()(QVector< QVariant > _x) const
    {            
        double sum1 = 0.0;
        double sum2 = 0.0;
        double x;
        double e = std::exp(1.0);
        for(int i=0; i<_x.count(); i++)
        {
            x = _x[i].toDouble();
            sum1 += x*x;
            sum2 += std::cos(2 * M_PI * x);
        }
        double sum = ( -20.0 * std::exp( -0.2 * sqrt( sum1 / double(_x.count()) ) ) - std::exp( sum2 / double(_x.count()) ) + 20.0 + e );
        return EvaluatorResults(sum);
    }
};

#endif // BUILTINEVALUATOR_H
