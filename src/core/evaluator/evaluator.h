#ifndef EVALUATOR_H
#define EVALUATOR_H

#include "optimizationproblem.h"
#include "evaluatorresults.h"
#include "evaluatordefinition.h"
#include <QVector>
#include <QVariant>


/*! 
 * \brief Base class for evaluators.
 */ 
class Evaluator
{
public:
    
    /*! 
     * \brief Constructor od Evaluator class. 
     */  
    Evaluator(EvaluatorDefinition::EvaluatorType _type);
    
    /*!
     * \brief Virtual function used for evaluation of given set of values.
     * 
     * This function is pure virtual function and it is used only to force its reimplementation in classes that inhertis Evaluator class.
     */
    virtual EvaluatorResults evaluate(const QVector<QVariant> _x) = 0;
    
    /*!
     * \brief Overloaded evaluation function which takes vector of floats.
     */
    EvaluatorResults evaluate(const QVector<double> _x);
    
    /*!
     * \brief Overloaded evaluation function which takes list of floats.
     */
    EvaluatorResults evaluate(const QList<double> _x);
    
    
    /*!
     * \brief Acces to optimization problem.
     */
    OptimizationProblem* optimizationProblem(void);
    
private:
    /*!
     * \param m_optimizationProblem contains a optimization problem definition.
     */
    OptimizationProblem* m_optimizationProblem;
    
    
    /*!
     * Evaluator type (builtin  or python) which is automatically set using BuiltinEvaluator or PythonEvaluator constructor.
     */
    EvaluatorDefinition::EvaluatorType m_type;
};

#endif // EVALUATOR_H
