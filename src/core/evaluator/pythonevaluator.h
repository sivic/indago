#ifndef PYTHONEVALUATOR_H
#define PYTHONEVALUATOR_H

#include "evaluator.h"
//#include "evaluatorresults.h"

class PythonEvaluator: public Evaluator
{
public:
    
    PythonEvaluator(void);
        
    EvaluatorResults evaluate(const QVector<QVariant> _x);
    
};

#endif // PYTHONEVALUATOR_H
