#include "pso/pso.h"
#include <limits>

PSO::PSO(void)
{
    setStatus(OptimizerStatus::notStarted);
    
    // Setting default parameters
    m_parameters[OptimizerDefinition::PsoParameters::SwarmSize] = 20;
    m_parameters[OptimizerDefinition::PsoParameters::InertiaFactor] = 0.72;
    m_parameters[OptimizerDefinition::PsoParameters::SocialFactor] = 1.0;
    m_parameters[OptimizerDefinition::PsoParameters::CognitiveFactor] = 1.0;
    m_parameters[OptimizerDefinition::PsoParameters::Topology] = QString("lbest");
    m_parameters[OptimizerDefinition::PsoParameters::PFIDI] = QString("none");
    
    
}

OptimizerSampleResults PSO::optimize(void)
{
    /*
    OptimizerSampleResults res;
    res.m_finalFitness = 1.0;
    res.m_numberOfEvaluations = 100;
    res.m_successful = true;
    return res;
    */
    
    //emit appentToLog("entering void PSO::optimize(void)");
    return pso_main();
    //emit appentToLog("leaving void PSO::optimize(void)");
    //emit finished();
}

bool PSO::validate(void)
{
	return true;    
}

bool PSO::init(void)
{
    int iDim = 0;
    for(uint iVar=0; iVar<m_optimizationProblem->numberOfVariableDefinitions(); iVar++)
    {
        ContinuousVariableDefinition* varDef = (ContinuousVariableDefinition*)(m_optimizationProblem->variableDefinition(iVar));
        for(int iVarCount=0; iVarCount<m_optimizationProblem->variableDefinition(iVar)->quantity();iVarCount++)
        {
            xmin[iDim] = varDef->lowerBound();
            xmax[iDim] = varDef->upperBound();
            iDim++;
        }
    }
    nDims = iDim;
    //qDebug() << "DIMENSIONS: " << nDims;
    
    X.resize(m_parameters[OptimizerDefinition::PsoParameters::SwarmSize].toInt());
    P.resize(m_parameters[OptimizerDefinition::PsoParameters::SwarmSize].toInt());
    
    for(int i=0.0; i<m_parameters[OptimizerDefinition::PsoParameters::SwarmSize].toInt(); i++)
    {
        //m_positions[i].m_position.resize(_nDims);
        //m_best[i].m_position.resize(_nDims);
    }
    return true;
}

void PSO::setParameter(OptimizerDefinition::PsoParameters _parameter, QVariant _value)
{
    //assert(m_parameters.contains(_parameter));
    m_parameters[_parameter]=_value;
}



// =================================================
OptimizerSampleResults PSO::pso_main ( void )
{
    int p_runs = 1;
    int p_maxeval = 10000;
    float p_eps = 1.0e-4;
    
    
    if (m_definition->stoppingCondition(StoppingConditionDefinition::StoppingCondition::MaximumNumberOfEvaluations)->enabled())
    {
        p_maxeval = m_definition->stoppingCondition(StoppingConditionDefinition::StoppingCondition::MaximumNumberOfEvaluations)->parameter(0).toInt();
    }
    if (m_definition->stoppingCondition(StoppingConditionDefinition::StoppingCondition::FitnessTarget)->enabled())
    {
        p_eps = m_optimizationProblem->fitnessTarget();
    }
    
    
    
    int p_topology = 1;
    if(m_parameters[OptimizerDefinition::PsoParameters::Topology].toString() == "lbest")
        p_topology = 1;
    if(m_parameters[OptimizerDefinition::PsoParameters::Topology].toString() == "gbest")
        p_topology = 0;
    
    int p_inerblocking = 0;
    if(m_parameters[OptimizerDefinition::PsoParameters::PFIDI].toString() == "none")
        p_inerblocking = 0;
    if(m_parameters[OptimizerDefinition::PsoParameters::PFIDI].toString() == "lpso")
        p_inerblocking = 1;
    if(m_parameters[OptimizerDefinition::PsoParameters::PFIDI].toString() == "gearbox")
        p_inerblocking = 2;
    
    
    init();
    
    /*
    printf("\nParameters:");
    
    //printf("\n- func: %d", p_func);
    printf("\n- dim: %d", nDims);
    //printf("\n- lb: %f", p_lb);
    //printf("\n- ub: %f", p_ub);
    printf("\n- eps: %f", p_eps);
    
    printf("\n- runs: %d", p_runs);
    printf("\n- swarm size: %d", m_parameters[OptimizerDefinition::PsoParameters::SwarmSize].toInt());
    printf("\n- w: %f", m_parameters[OptimizerDefinition::PsoParameters::InertiaFactor].toDouble());
    printf("\n- c_s: %f", m_parameters[OptimizerDefinition::PsoParameters::SocialFactor].toDouble());
    printf("\n- c_c: %f", m_parameters[OptimizerDefinition::PsoParameters::CognitiveFactor].toDouble());
    printf("\n- topology: %s", m_parameters[OptimizerDefinition::PsoParameters::Topology].toString().toStdString().c_str());
    printf("\n- inert. block.: %d", p_inerblocking);
    printf("\n- max func. eval.: %d", p_maxeval);
    printf("\n------------\n\n");
    */
    
    
    double c; // Second onfidence coefficient
    int d; // Current dimension
    double eps; // Admissible error
    double eps_mean; // Average error
    double error; // Error for a given position
    double error_prev; // Error after previous iteration
    int eval_max; // Max number of evaluations
    double eval_mean; // Mean number of evaluations
    int function; // Code of the objective function
    int g; // Rank of the best informant
    int init_links; // Flag to (re)init or not the information links
    int i;
    int K; // Max number of particles informed by a given one
    int m;
    double mean_best[R_max];
    double min; // Best result through several runs
    int n_exec, n_exec_max; // Nbs of executions
    int n_failure; // Number of failures
    int s; // Rank of the current particle
    //double t1, t2;
    double variance;
    double w; // First confidence coefficient
    
    srand(time(NULL));
    
    //f_run = fopen( "f_run.txt", "w" );
    E = exp( 1 );
    pi = acos( -1 );
    
    //----------------------------------------------- PROBLEM
    function = 10; //Function code
    /*
     * 0 Parabola (Sphere)
     * 1 De Jong' f4
     * 2 Griewank
     * 3 Rosenbrock (Banana)
     * 4 Step
     * 6 Foxholes 2D
     * 7 Polynomial fitting problem 9D
     * 8 Alpine
     * 9 Rastrigin
     * 10 Ackley
     * 13 Tripod 2D
     * 17 KrishnaKumar
     * 18 Eason 2D
     */
    
    //nDims = p_dim; // Search space dimension
    
    // D-cube data
    /*
     *    for ( d = 0; d < nDims; d++ )
     *    {
     *        xmin[d] = p_lb;
     *        xmax[d] = p_ub;
}
*/
    
    eps = p_eps; // Acceptable error
    f_min = 0; // Objective value
    n_exec_max = p_runs; // Numbers of runs
    eval_max = p_maxeval; // Max number of evaluations for each run
    
    
    
    if(n_exec_max>R_max) n_exec_max=R_max;
    //-----------------------------------------------------  PARAMETERS
    S = 10+( int )(2*sqrt(nDims)); if (S>S_max) S=S_max;
    K = 3;
    w = 1 / ( 2 * log( 2 ) ); c = 0.5 + log( 2 );
    
    S = m_parameters[OptimizerDefinition::PsoParameters::SwarmSize].toInt();
    w = m_parameters[OptimizerDefinition::PsoParameters::InertiaFactor].toDouble();
    c = m_parameters[OptimizerDefinition::PsoParameters::SocialFactor].toDouble();
    
    QVector<double> wstim;
    //wstim << 0.15 << 0.3 << 0.6 << 1.2;
    wstim << 0.0 << 0.4 << 0.78;
    //wstim << -0.3 << 0.3 << 0.6 << 1.2;
    //wstim << 0.0 << w << w + 0.2 << w + 0.4;
    //wstim << 0.0 << w;
    
    /*
    printf("\n Swarm size %i", S);
    printf("\n coefficients %f %f \n",w,c);
    */
    //----------------------------------------------------- INITIALISATION
    //t1 = clock(); // Init time
    // Initialisation of information variables
    n_exec = 0; eval_mean = 0; eps_mean = 0; n_failure = 0;
    
    //init:
    while( n_exec < n_exec_max )
    {
        n_exec = n_exec + 1;
        for ( s = 0; s < S; s++ )  // Positions and velocities
        {
            X[s].x.resize(nDims);
            V[s].size = nDims;
            
            for ( d = 0; d < nDims; d++ )
            {
                X[s].x[d] = alea( xmin[d], xmax[d] );
                V[s].v[d] = (alea( xmin[d], xmax[d] ) - X[s].x[d])/2; // Non uniform
                // V[s].v[d] = ( xmin[d]-xmax[d] )*(0.5-alea(0,1)); //Uniform. 2006-02-24
            }
        }
        
        // First evaluations
        nb_eval = 0;
        for ( s = 0; s < S; s++ )
        {
            X[s].f = fabs( perf( s, function ) - f_min );
            X[s].f_old = X[s].f;
            P[s] = X[s]; // Best position = current one
        }
        
        // Find the best
        best = 0;
        for ( s = 1; s < S; s++ )
            if ( P[s].f < P[best].f ) best = s;
            
            error =  P[best].f ; // Current min error
            if(n_exec==1) min=error;
            error_prev=error; // Previous min error
            
            init_links = 1; // So that information links will be initialized
            
            //---------------------------------------------- ITERATIONS
            //loop:
            while( error > eps && nb_eval < eval_max )
            {
                if ( init_links == 1 )
                {
                    // Who informs who, at random
                    for ( s = 0; s < S; s++ )
                    {
                        for ( m = 0; m < S; m++ )
                        {
                            if(p_topology == 0)
                                LINKS[m] [s] = 1; // Init to "all links"
                                else
                                    LINKS[m] [s] = 0; // Init to "no link"
                        }
                        LINKS[s] [s] = 1; // Each particle informs itself
                    }
                    
                    for ( m = 0; m < S; m++ ) // Other links
                    {
                        for ( i = 0; i < K; i++ )
                        {
                            s = alea_integer( 0, S - 1 );
                            LINKS[m] [s] = 1;
                        }
                    }
                }
                //printf( "\n%10.3e", P[best].f);
                
                // The swarm MOVES
                for ( s = 0; s < S; s++ ) // For each particle ...
                {
                    // .. find the best informant
                    g=s;
                    for ( m = 0; m < S; m++ )
                    {
                        if ( LINKS[m] [s] == 1 && P[m].f < P[g].f  ) g = m;
                    }
                    double www = w;
                    if(p_inerblocking == 1)
                    {
                        if(X[s].f > X[s].f_old){ www =0.;}
                    }
                    else if(p_inerblocking == 2)
                    {
                        if(X[s].f > X[s].f_old)
			{
			    X[s].gear = 0;
			}
			else
			{
			    if(X[s].gear < wstim.count() - 1)
			    {
				X[s].gear ++;
			    }
			}
			www = wstim[X[s].gear];
                    }
                    
                    
                    // ... compute the new velocity, and move
                    for ( d = 0; d < nDims; d++ )
                    {
                        V[s].v[d] = www * V[s].v[d] + alea( 0, c ) * ( P[s].x[d] - X[s].x[d] );
                        V[s].v[d] = V[s].v[d] + alea( 0, c ) * ( P[g].x[d] - X[s].x[d] );
                        X[s].x[d] = X[s].x[d] + V[s].v[d];
                    }
                    
                    
                    // ... interval confinement (keep in the box)
                    for ( d = 0; d < nDims; d++ )
                    {
                        if ( X[s].x[d] < xmin[d] )
                        {
                            X[s].x[d] = xmin[d]; V[s].v[d] = 0;
                        }
                        if ( X[s].x[d] > xmax[d] )
                        {
                            X[s].x[d] = xmax[d]; V[s].v[d] = 0;
                        }
                    }
                    
                    
                    // ... evaluate the new position
                    X[s].f_old= X[s].f;
                    X[s].f = fabs( perf( s, function ) - f_min );
                    
                    
                    // ... update the best previous position
                    if ( X[s].f<P[s].f )
                    {
                        P[s] = X[s];
                        // ... update the best of the bests
                        if (  P[s].f<P[best].f  ) best = s;
                    }
                }
                
                // Check if finished
                // If no improvement, information links will be reinitialized
                error=P[best].f;
                if ( error >= error_prev ) init_links = 1;
                else init_links = 0;
                error_prev = error;
            }
            //    if ( error > eps && nb_eval < eval_max ) goto loop;
            if ( error > eps ) n_failure = n_failure + 1;
            
            // Result display
            //printf( "\nExec %i Eval %i. Error %f ", n_exec, nb_eval, error );
            //printf( "\n Position :\n" );
            //for ( d = 0; d < D; d++ ) printf( " %f", P[best].x[d] );
            
            // Save result
            //fprintf( f_run, "\n%i %i %f ", n_exec, nb_eval,error );
            //fprintf( f_run, " Position: " );
            //for ( d = 0; d < D; d++ ) fprintf( f_run, " %f", P[best].x[d] );
            
            // Compute some statistical information
            if ( error < min ) min = error;
            if ( error <= eps ) eval_mean = eval_mean + nb_eval;
            eps_mean = eps_mean + error;
            mean_best[n_exec - 1] = error;
    }
    //    if ( n_exec < n_exec_max ) goto init;
    
    // END. Display some statistical information
    //t2 = clock();
    eval_mean = eval_mean / ( double )(n_exec - n_failure);
    if (n_exec == n_failure) eval_mean = std::numeric_limits<double>::infinity();
    eps_mean = eps_mean / ( double )n_exec;
    /*
    printf( "\n\n Total clocks %.0f", t2 - t1 );
    printf( "\n\n Eval. (mean)= %.2f", eval_mean );
    printf( "\n Error (mean) = %.2e", eps_mean );
    */
    
    OptimizerSampleResults res;
    res.m_finalFitness = P[best].f;
    res.m_numberOfEvaluations = nb_eval;
    res.m_successful = (P[best].f < m_optimizationProblem->fitnessTarget());
    return res;
    
    
    // Variance
    variance = 0;
    for ( d = 0; d < n_exec_max; d++ ) variance = variance + ( mean_best[d] - eps_mean ) * ( mean_best[d] - eps_mean );
    variance = sqrt( variance / n_exec_max );
    /*
    printf( "\n Std. dev. %.2e", variance );
    */
    
    // Success rate and minimum value
    /*
    printf( "\n Success rate = %.2f%%", 100 * ( 1 - n_failure / ( double )n_exec ) );
    if ( n_exec > 1 ) printf( "\n Best min value = %.2e", min );
    */
    
    /*
    printf( "\n%.0e", t2 - t1 );
    printf( "\n%.2e", eval_mean );
    printf( "\n%.2e", eps_mean );
    printf( "\n%.2e", variance );
    printf( "\n%.2e", 100 * ( 1 - n_failure / ( double )n_exec ) );
    printf( "\n%.2e", min );
    */
    
    //return 0;
}

//===========================================================
double PSO::alea( double a, double b )
{ // random number (uniform distribution) in [a b]
    double r;
    r=(double)rand(); r=r/RAND_MAX;
    return a + r * ( b - a );
}
//===========================================================
int PSO::alea_integer( int a, int b )
{ // Integer random number in [a b]
    int ir;
    double r;
    r = alea( 0, 1 ); ir = ( int )( a + r * ( b + 1 - a ) );
    if ( ir > b ) ir = b;
    return ir;
}


//===========================================================
double PSO::perf( int s, int function )
{ 
    // Evaluate the fitness value for the particle of rank s
    

    int d;
    int i, j, k;
    double f, p, xd, x1, x2;

    double sum1, sum2;
    double t0, tt, t1;
    struct position xs;
    
    
    
    
    // For Foxholes problem
    static int a[2] [25] =
    {
        {
            -32, -16, 0, 16, 32, -32, -16, 0, 16, 32, -32, -16, 0, 16, 32, -32, -16, 0, 16, 32, -32, -16, 0, 16, 32
        },
        {
            -32, -32, -32, -32, -32, -16, -16, -16, -16, -16, 16, 16, 16, 16, 16, 32, 32, 32, 32, 32
        }
    };
    // For polynomial fitting problem
    int const M = 60;
    double py, y = -1, dx = ( double )M;
    
    nb_eval = nb_eval + 1;
    for (int i=0; i<X[s].x.count(); i++)
    {
        xs.x[i] = X[s].x[i];
    }
    //    xs = m_best[s];
    
    
    QVector<double> newx;
    for ( d = 0; d < nDims; d++ )
    {
        newx.append( xs.x[d] );
    }
    double fitness = this->evaluator()->evaluate(newx).value();
    return fitness;
    
    
    switch ( function )
    {
        case 0: // Parabola (Sphere)
            f = 0;
            p = 0.; // Shift
            for ( d = 0; d < nDims; d++ )
            {
                xd = xs.x[d] - p;
                f = f + xd * xd;
            }
            break;
            
        case 1: // De Jong's f4
            f = 0;
            p = 0; // Shift
            for ( d = 0; d < nDims; d++ )
            {
                xd = xs.x[d] - p;
                f = f + (d+1)*xd*xd*xd*xd;
            }
            break;
            
            
        case 2: // Griewank
            f = 0;
            p = 1;
            for ( d = 0; d < nDims; d++ )
            {
                xd = xs.x[d];
                f = f + xd * xd;
                p = p * cos( xd / sqrt( d + 1 ) );
            }
            f = f / 4000 - p + 1;
            break;
            
        case 3: // Rosenbrock
            f = 0;
            t0 = xs.x[0];
            for ( d = 1; d < nDims; d++ )
            {
                t1 = xs.x[d];
                tt = 1 - t0;
                f += tt * tt;
                tt = t1 - t0 * t0;
                f += 100 * tt * tt;
                t0 = t1;
            }
            break;
            
        case 4: // Step
            f = 0;
            for ( d = 0; d < nDims; d++ ) f = f + ( int )xs.x[d];
            break;
            
            
        case 6: //Foxholes 2D
            f = 0;
            for ( j = 0; j < 25; j++ )
            {
                sum1 = 0;
                for ( d = 0; d < 2; d++ )
                {
                    sum1 = sum1 + pow( xs.x[d] - a[d] [j], 6 );
                }
                f = f + 1 / ( j + 1 + sum1 );
            }
            f = 1 / ( 0.002 + f );
            break;
            
        case 7: // Polynomial fitting problem
            // on [-100 100]^9
            f = 0;
            dx = 2 / dx;
            for ( i = 0; i <= M; i++ )
            {
                py = xs.x[0];
                for ( d = 1; d < nDims; d++ )
                {
                    py = y * py + xs.x[d];
                }
                if ( py < -1 || py > 1 ) f += ( 1 - py ) * ( 1 - py );
                y += dx;
            }
            py = xs.x[0];
            for ( d = 1; d < nDims; d++ ) py = 1.2 * py + xs.x[d];
            py = py - 72.661;
            if ( py < 0 ) f += py * py;
            py = xs.x[0];
            for ( d = 1; d < nDims; d++ ) py = -1.2 * py + xs.x[d];
            py = py - 72.661;
            if ( py < 0 ) f += py * py;
            break;
            
        case 8: // Clerc's f1, Alpine function, min 0
            f = 0;
            for ( d = 0; d < nDims; d++ )
            {
                xd = xs.x[d];
                f += fabs( xd * sin( xd ) + 0.1 * xd );
            }
            break;
            
        case 9: // Rastrigin. Minimum value 0. Solution (0,0 ...0)
            k = 10;
            f = 0;
            for ( d = 0; d < nDims; d++ )
            {
                xd = xs.x[d];
                f += xd * xd - k * cos( 2 * pi * xd );
            }
            f += nDims * k;
            break;
            
        case 10: // Ackley
            sum1 = 0;
            sum2 = 0;
            for ( d = 0; d < nDims; d++ )
            {
                xd = xs.x[d];
                sum1 += xd * xd;
                sum2 += cos( 2 * pi * xd );
            }
            y = nDims;
            f = ( -20 * exp( -0.2 * sqrt( sum1 / y ) ) - exp( sum2 / y ) + 20 + E );
            break;
            
            
        case 13: // 2D Tripod function (Louis Gacogne)
            // Search [-100, 100]^2. min 0 on (0  -50)
            x1 = xs.x[0];
            x2 = xs.x[1];
            if ( x2 < 0 )
            {
                f = fabs( x1 ) + fabs( x2 + 50 );
            }
            else
            {
                if ( x1 < 0 )
                    f = 1 + fabs( x1 + 50 ) + fabs( x2 - 50 );
                else
                    f = 2 + fabs( x1 - 50 ) + fabs( x2 - 50 );
            }
            break;
            
        case 17: // KrishnaKumar
            f = 0;
            for ( d = 0; d < nDims - 1; d++ )
            {
                f = f + sin( xs.x[d] + xs.x[d + 1] ) + sin( 2 * xs.x[d] * xs.x[d + 1] / 3 );
            }
            break;
            
        case 18: // Eason 2D (usually on [-100,100]
            // Minimum -1  on (pi,pi)
            x1 = xs.x[0]; x2 = xs.x[1];
            f = -cos( x1 ) * cos( x2 ) / exp( ( x1 - pi ) * ( x1 - pi ) + ( x2 - pi ) * ( x2 - pi ) );
            break;
    }
    
    
    return f;
}
