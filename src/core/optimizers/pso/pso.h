#ifndef PSO_H
#define PSO_H
/* Standard PSO version 2006

Motivation
Quite often some authors say they compare their PSO versions
to the "standard one" ... which is never the same!
So the idea is to define a real standard at least for one year, validated by some
researchers of the field, in particular James Kennedy and Maurice Clerc.
This PSO version does not intend to be the best one on the market (in particular there is
no adaptation of the swarm size nor of the coefficients) but simply very near of the
original version (1995) with just a few improvements based on some recent works.

So referring to "standard PSO 2006" would mean exactly this version with the default values
detailed below as,for example, referring to "standard PSO 2006 (w=0.8)" would mean almost
this version but with a non standard first cognitive/confidence coefficient.

Parameters
S := swarm size
K := maximum number of particles _informed_ by a given one
T := topology of the information links
w := first cognitive/confidence coefficient
c := second cognitive/confidence coefficient
R := random distribution of c
B := rule "to keep the particle in the box"

Equations
For each particle and each dimension
v(t+1) = w*v(t) + R(c)*(p(t)-x(t)) + R(c)*(g(t)-x(t))
x(t+1) = x(t) + v(t+1)
where
v(t) := velocity at time t
x(t) := position at time t
p(t) := best previous position of the particle
g(t) := best previous position of the informants of the particle

Default values
S = 10+2*sqrt(D) where D is the dimension of the search space
K = 3
T := randomly modified after each step if there has been no improvement
w = 1/(2*ln(2))
c = 0.5 + ln(2)
R = U(0..c), i.e. uniform distribution on [0, c]
B := set the position to the min. (max.) value and the velocity to zero

About information links topology
A lot of works have been done about this topic. The main result is there is no
"best" topology. Hence the random approach used here. Note that is does not mean that
each particle is informed by K ones: the number of particles that informs a given one
may be any value between 1 (for each particle informs itself) and S.

About initialisation
Initial positions are chosen at random inside the search space (which is
supposed to be a hyperparallelepid, and even often a hypercube), according to an uniform
distribution. This is not the best way, but the one of the original PSO.

Each initial velocity is simply defined as the difference of two random
positions. It is simple, and needs no additional parameter.
However, again, it is not the best approach. The resulting distribution is not even
uniform, as for any method that uses an uniform distribution independently for each
component. The mathematically correct approach needs to use an uniform
distribution inside a hypersphere. It is not that difficult, and indeed used in some PSO
versions, but quite different from the original one.

Some results with the standard values
You may want to recode this program in another language. Also you may want to modify it
for your own purposes. Here are some results on classical test functions to help you to
check your code.
Dimension D=30
Acceptable error eps=0
Objective value f_min=0
Number of runs n_exec_max=50
Number of evaluations for each run eval_max=30000

Problem                            Mean best value    Standard deviation
Parabola/Sphere on [-100, 100]^D        0                  0
Griewank on [-600, 600]^D               0.018              0.024
Rosenbrock/Banana on [-30, 30]^D       50.16              36.9
Rastrigin on [-5.12, 5.12]^D           48.35              14.43
Ackley on [-32, 32]^D                   1.12               0.85

Last updates
2006-02-27 Fixed a bug about minimal best value over several runs
2006-02-16 Fixed a bug (S_max for P, V, X, instead of D_max), thanks to Manfred Stickel
2006-02-16 replaced k by i x by xs (in perf()), because of possible confusion with K and X
2006-02-13  added De Jong's f4
*/
//#include "stdio.h"
#include "math.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "optimizer.h"
#include "particle.h"

#define	D_max 100  // Max number of dimensions of the search space
#define	S_max 100 // Max swarm size
#define R_max 2000 // Max number of runs


// Structures
struct velocity
{
    int size;
    double v[D_max];
};


struct position
{
    int size;
    double x[D_max];
    double f;
    double f_old;
};



class PSO: public Optimizer
{
    Q_OBJECT
    
public:
    
    PSO(void);
    
    
    
    
    /*!
     * \brief Method validates, initializes and starts patter search method.
     */
    OptimizerSampleResults optimize(void);
    bool validate(void);
    bool init(void);
    
    void setParameter(OptimizerDefinition::PsoParameters _parameter, QVariant _value);    

private:    
    
    // Sub-programs
    OptimizerSampleResults pso_main ( void );
    double alea( double a, double b );
    int alea_integer( int a, int b );
    double perf( int s, int function ); // Fitness evaluation

    // Global variables
    int best; // Best of the best position (rank in the swarm)
    int nDims; // Search space dimension
    double E; // exp(1). Useful for some test functions
    double f_min; // Objective(s) to reach
    int LINKS[S_max] [S_max]; // Information links
    int nb_eval; // Total number of evaluations
    double pi; // Useful for some test functions
    int S; // Swarm size
    struct velocity V[S_max]; // Velocities
    
    //struct position X[S_max]; // Positions
    QVector<Particle> X;
    
    //struct position P[S_max]; // Best positions found by each particle
    QVector<Particle> P;
    
    double xmin[D_max], xmax[D_max]; // Intervals defining the search space
};

// File(s)
//FILE * f_run;

#endif // PSO_H
