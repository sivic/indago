#ifndef PARTICLE_H
#define PARTICLE_H

#include <QVector>

/*!
 * \brief Particle class contains all particle data.
 */
class Particle
{
public:
    
    /*!
     * Particle constructor.
     */
    Particle();
    
    /*!
     * Vector of coordinates of a current position.
     */
    QVector<double> x;
    
    /*!
     * Current fitness.
     */
    double f;
    /*!
     * Old fitness.
     */
    double f_old;
    
    /*!
     * Current gear
     */
    int gear;
};

#endif // PARTICLE_H
