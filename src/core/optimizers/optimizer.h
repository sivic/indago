#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include "optimizerdefinition.h"
#include "evaluator.h"
#include "optimizationproblem.h"
#include "evaluatorfactory.h"
#include "optimizerresults.h"


#include <QThread>
#include <QMetaEnum>

/*!
 * \brief Base class for all optimizers (optimization methods).
 */
class Optimizer: public QThread
{
    Q_OBJECT
    Q_ENUMS(OptimizerStatus)

public:
    /*!
     * \brief Provides status flags for optimizers.
     */
    enum OptimizerStatus
    {
        /*! The Optimizer is not yet started. */
        notStarted,
        /*! The Optimizer is running; the optimization is in progress.*/
        running,     
        /*! The optimization is finished. If Optimizer is started in a new thread, the thread is finished.*/
        maxNumberOfEvaluationsReached, 
        /*! Optimizer was started but it was stopped due to validation fail.*/
        validationFailed    
    };
    ENUM_TO_STRING(Optimizer, OptimizerStatus)
    STRING_TO_ENUM(Optimizer, OptimizerStatus)
    ENUM_COUNT(Optimizer, OptimizerStatus)
    ENUM_TO_STRINGLIST(Optimizer, OptimizerStatus)
    
    /*!
     * \brief Optimizer destructor
     */
    // TODO virtual destructor?
    virtual ~Optimizer(void);
    
    /*!
     * \brief Sets optimizer definition.
     */
    void setOptimizerDefinition(OptimizerDefinition* _definition);
        
    /*!
     * \brief Sets an OptimizationProblem to be solved.
     */
    void setOptimizationProblem(OptimizationProblem* _optimizationProblem);
    
    /*!
     * \brief Pure virtual function for starting the optimization. Must be implemented in every derived class.
     */      
    virtual OptimizerSampleResults optimize(void) = 0;
    
    /*!
     * \brief Starts a new thread and calls optimize function (in a new thread).
     */
    void run(void);
        
    /*!
     * \brief Pure virtual function for validating the optimization problem and Optimizer parameters. Must be implemented in every derived class.
     */   
    virtual bool validate(void) = 0;
    
    /*!
     * \brief Current status of Optimizer.
     */
    OptimizerStatus status(void);
    
    
    void setParameter(int _parameter, QVariant _value){m_parameters[_parameter]=_value;};   
        
    
    
    
protected:
    /*!
     * \brief Shared pointer to evaluator associated on setOptimizationProblem call.
     */
    Evaluator* evaluator(void);
    
    /*!
     * \brief Return associated optimization problem.
     */
    OptimizationProblem* optimizationProblem(void);
    
    /*!
     * \brief Sets/changes a current status of optimizer.
     */ 
    void setStatus(OptimizerStatus _status);
    
protected:
    
    /*!
     * \brief Optimizer definition.
     */
    OptimizerDefinition* m_definition;
    
    /*!
     * \brief Associated evaluator.
     */
    Evaluator* m_evaluator;
    
    /*!
     * \brief Associated optimization problem.
     */
    OptimizationProblem* m_optimizationProblem;
    
    
    /*!
     * \brief Current optimizer status.
     */
    OptimizerStatus m_status;

protected:
    
    /*!
     * \brief Optimizer parameters.
     */
    QHash<int, QVariant> m_parameters;
    
    /*!
     * \brief Stopping conditions.
     */
    // TODO what about this?
    //QVector<StoppingConditionDefinition*> m_stoppingConditions;
    
signals:
    void appentToLog(QString);
    
    void finished(void);
};

#endif // OPTIMIZER_H
