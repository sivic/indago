#include "optimizer.h"

Optimizer::~Optimizer(void)
{
    if(m_evaluator)
        delete m_evaluator;
    
    m_parameters.clear();
}

Evaluator* Optimizer::evaluator(void )
{
    return m_evaluator;
}

void Optimizer::setOptimizationProblem(OptimizationProblem* _optimizationProblem)
{
    m_optimizationProblem = _optimizationProblem;
        
    m_evaluator = EvaluatorFactory::createEvaluator(_optimizationProblem->evaluatorDefinition());
}

OptimizationProblem* Optimizer::optimizationProblem(void )
{
    return m_optimizationProblem;
}

void Optimizer::run(void )
{
    optimize();
}

void Optimizer::setStatus(Optimizer::OptimizerStatus _status)
{
    m_status = _status;
}

Optimizer::OptimizerStatus Optimizer::status(void )
{
    return m_status;
}

void Optimizer::setOptimizerDefinition(OptimizerDefinition* _definition)
{
    m_definition = _definition;
    
    for(int iParam = 0; iParam < _definition->parametersDefinitions(_definition->method()).count(); iParam++)
    {
        setParameter(iParam, _definition->parameter(iParam));
    }
}

