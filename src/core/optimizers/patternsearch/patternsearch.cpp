#include "patternsearch/patternsearch.h"
#include <qvarlengtharray.h>

PatternSearch::PatternSearch(void )
{
    setStatus(OptimizerStatus::notStarted);
    
    // Setting default parameters
    m_parameters[OptimizerDefinition::PatternSearchParameter::rho] = 0.5;
    m_parameters[OptimizerDefinition::PatternSearchParameter::epsilon] = 1.0e-3;
    m_parameters[OptimizerDefinition::PatternSearchParameter::maxIterations] = 100;
}

bool PatternSearch::validate(void )
{
    int nVars = optimizationProblem()->numberOfVariableDefinitions();
    for(int iVar=0; iVar<nVars; iVar++)
    {
        if(optimizationProblem()->variableDefinition(iVar)->type() != VariableDefinition::VariableType::continuous)
            return false;
    }
    
    return true;
}

OptimizerSampleResults PatternSearch::optimize(void )
{
    // Validate
    if(!validate())
    {
        setStatus(OptimizerStatus::validationFailed);
        return OptimizerSampleResults();
    }
    setStatus(OptimizerStatus::running);
    
    // Initialize parameters
    
    double p_rho = m_parameters[OptimizerDefinition::PatternSearchParameter::rho].toDouble();
    
    //QList<QVariant> vals = m_parameters[OptimizerDefinition::PatternSearchParameter::startPoint].toList();
    int nDim = m_optimizationProblem->numberOfVariableDefinitions();
    QVector<double> p_startPoint;
    int iDim = 0;
    if(m_parameters[OptimizerDefinition::PatternSearchParameter::startPoint].toString() == "random")
    {
        RandomGenerator rg;
        for(int iVar=0;iVar<nDim;iVar++)
        {
            ContinuousVariableDefinition* var = (ContinuousVariableDefinition*)m_optimizationProblem->variableDefinition(iVar);
            for(int iVarCount=0; iVarCount<m_optimizationProblem->variableDefinition(iVar)->quantity();iVarCount++)
            {
                p_startPoint.append( rg.randomDouble(var->upperBound(), var->lowerBound()) );
            }
        }
    }
    else
    {
        for(int iVar=0;iVar<nDim;iVar++)
        {
            ContinuousVariableDefinition* var = (ContinuousVariableDefinition*)m_optimizationProblem->variableDefinition(iVar);
            for(int iVarCount=0; iVarCount<m_optimizationProblem->variableDefinition(iVar)->quantity();iVarCount++)
            {
                p_startPoint.append(var->defaultValue());
            }
        }
    }
    nDim = iDim;
    
    double p_epsilon = m_parameters[OptimizerDefinition::PatternSearchParameter::epsilon].toDouble();
    double p_maxIterations = m_parameters[OptimizerDefinition::PatternSearchParameter::maxIterations].toInt();
        
    qDebug() << p_startPoint;   
    OptimizerSampleResults res = hooke(p_startPoint, p_rho, p_epsilon, p_maxIterations);
    //qDebug() << res;
    return res;
}


void PatternSearch::setParameter(OptimizerDefinition::PatternSearchParameter _parameter, QVariant _value)
{
    //assert(m_parameters.contains(_parameter));
    m_parameters[_parameter]=_value;
    
}


double PatternSearch::best_nearby(QVector< double > delta, QVector< double >& point, double prevbest)
{
    int nvars = point.count();
    QVector<double> z(nvars);

    double minf, ftmp;
    minf = prevbest;

    for (int i = 0; i < nvars; i++)
        z[i] = point[i];

    for (int i = 0; i < nvars; i++)
    {
        z[i] = point[i] + delta[i];
        ftmp = this->evaluator()->evaluate(z).value();
        if (ftmp < minf)
        {
            minf = ftmp;
        }
        else // why else?
        {
            delta[i] = 0.0 - delta[i];
            z[i] = point[i] + delta[i];
            ftmp = this->evaluator()->evaluate(z).value();
            if (ftmp < minf)
            {
                minf = ftmp;
            }
            else
            {
                z[i] = point[i];
            }
        }
    }
    for (int i = 0; i < nvars; i++)
        point[i] = z[i];
    return (minf);
}

OptimizerSampleResults PatternSearch::hooke(QVector< double > startpt, double rho, double epsilon, int itermax)
{
    int nvars = startpt.size();
    QVector<double> newx(nvars), xbefore(nvars), delta(nvars), endpt(nvars);
    for (int i = 0; i < nvars; i++)
    {
        newx[i] = xbefore[i] = startpt[i];
        //std::cout << startpt[i] << " ";
        delta[i] = fabs(startpt[i] * rho);
        if (delta[i] == 0.0)
            delta[i] = rho;
        
        // temporary
        int sum = 0;
        int var_def = -1;
        do
        {
            var_def ++;
            sum += optimizationProblem()->variableDefinition(var_def)->quantity();
        }while(sum < i);
        
        ContinuousVariableDefinition* var = (ContinuousVariableDefinition*)(optimizationProblem()->variableDefinition(var_def));
        delta[i] = (var->upperBound() - var->lowerBound())*0.5*rho;
    }
    //std::cout << std::endl;

    
    
    int iadj = 0;
    double steplength = rho;
    int iters = 0;

    double fbefore = this->evaluator()->evaluate(newx).value();

    double newf = fbefore;


    while ((iters < itermax) && (steplength > epsilon))
    {
        iters++;
        iadj++;

        //printf("\nAfter %5d funevals, f(x) =  %.4le at\n", funevals, fbefore);
        //for (j = 0; j < nvars; j++)
        //    printf("   x[%2d] = %.4le\n", j, xbefore[j]);

        /* find best new point, one coord at a time */
        for (int i = 0; i < nvars; i++)
        {
            newx[i] = xbefore[i];
        }
        newf = best_nearby(delta, newx, fbefore);
        
    
        /* if we made some improvements, pursue that direction */
        int keep = 1;
        while ((newf < fbefore) && (keep == 1))
        {
            iadj = 0;
            for (int i = 0; i < nvars; i++)
            {
                /* firstly, arrange the sign of delta[] */
                if (newx[i] <= xbefore[i])
                {
                    delta[i] = 0.0 - fabs(delta[i]);
                }
                else
                {
                    delta[i] = fabs(delta[i]);
                }
                /* now, move further in this direction */
                double tmp = xbefore[i];
                xbefore[i] = newx[i];
                newx[i] = newx[i] + newx[i] - tmp;
            }
            fbefore = newf;
            /*
            for (int i = 0; i < nvars; i++)
            {
                std::cout << xbefore[i] << " ";
            }
            std::cout << std::endl;
            */

            newf = best_nearby(delta, newx, fbefore);

            /* if the further (optimistic) move was bad.... */
            if (newf >= fbefore)
            {
                break;
            }

            /* make sure that the differences between the new */
            /* and the old points are due to actual */
            /* displacements; beware of roundoff errors that */
            /* might cause newf < fbefore */
            keep = 0;
            for (int i = 0; i < nvars; i++)
            {
                keep = 1;
                if (fabs(newx[i] - xbefore[i]) > (0.5 * fabs(delta[i])))
                    break;
                else
                    keep = 0;
            }
        }
        if ((steplength >= epsilon) && (newf >= fbefore))
        {
            steplength = steplength * rho;
            for (int i = 0; i < nvars; i++)
            {
                delta[i] *= rho;
            }
        }
    }
    /*
    for (int i = 0; i < nvars; i++)
    {
        endpt[i] = xbefore[i];
        std::cout << endpt[i] << " ";
    }
    std::cout << std::endl << "f=" << newf;
    */
    
    setStatus(OptimizerStatus::maxNumberOfEvaluationsReached);
    // endpt is result
    
    OptimizerSampleResults res;
    res.m_finalFitness = newf;
    res.m_numberOfEvaluations = iters;
    res.m_successful = (newf < m_optimizationProblem->fitnessTarget());
    return res;
}