#ifndef PATTERNSEARCH_H
#define PATTERNSEARCH_H

#include "optimizer.h"
#include "randomgenerator.h"
#include <QHash>



class PatternSearch: public Optimizer
{
    Q_OBJECT
    
public:
    
    PatternSearch(void);
    
    /*!
     * \brief Method validates, initializes and starts patter search method.
     */
    OptimizerSampleResults optimize(void);
    bool validate(void);
    
    void setParameter(OptimizerDefinition::PatternSearchParameter _parameter, QVariant _value);    

private:
    double best_nearby(QVector<double> delta, QVector<double>& point, double prevbest);    
    OptimizerSampleResults hooke(QVector<double> startpt, double rho, double epsilon, int itermax);
    
};

#endif // PATTERNSEARCH_H
