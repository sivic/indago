#ifndef POPULATION_H
#define POPULATION_H
#include <vector>
#include <memory>
#include "individual.h"
#include "evaluator.h"
#include "optimizationproblem.h"

/**
 * < Responsible for creating and holding individuals>
 */
class Population
{

public:
  Population();
  virtual ~Population();
  
  /**
   * < creates Individuals >
   * @noOfBits binary
   * @popSize number of individuals
   */
  void createPopulation(int noOfBits,int _popSize,int noOfVariables);
  
  /**
   *< calculates fitness for every individual in population>
   */
  void calculatePopulationFitness(Evaluator *eval,std::vector<std::array<double,2>> &bounds);
  
  
  /**
   * < selection,crossover and mutation will be done in separate class >
   * @return If number of rejected duplicates is greater than popSize*alpha we should stop GA
   */
  bool createNewGeneration(const double& crossoverRate,const double& mutationRate,const bool& elitism,const bool& eliminateDuplicates,const int& tournamentSize,const double& alpha);
  
  /**
   * < for testing purposes >
   */
  void printPopulationInformation();
  
   /**
   * < We'll need individual with smallest fitness for elitism>
   */
  Individual findBestIndividual();


private:
  std::vector<Individual> pop;
  
  

};

#endif // POPULATION_H
