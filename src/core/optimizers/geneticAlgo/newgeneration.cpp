#include "newgeneration.h"
#include <iostream>
#include <qvarlengtharray.h>
#include "randomgenerator.h"

NewGeneration::NewGeneration():noOfDuplicatesEliminated(0)
{

}

NewGeneration::~NewGeneration()
{

}

bool NewGeneration::start(const std::vector< Individual >& oldGeneration,const double& crossoverRate,const double& mutationRate,const bool& m_elitism, Individual worstIndividual,const bool& eliminateDuplicates,const int& tournamentSize,const double& alpha)
{
  elitism(m_elitism,worstIndividual);
  Individual parent1,parent2;

  
  while(m_newGeneration.size()<oldGeneration.size())
  {
    //select 2 parents,do recombination and mutation
    parent1=tournamentSelection(oldGeneration,tournamentSize);
    parent2=tournamentSelection(oldGeneration,tournamentSize);
    
    onePointCrossover(crossoverRate,parent1,parent2);
    simpleMutation(mutationRate,parent1);
    simpleMutation(mutationRate,parent2);
        
    insertChild(parent1,eliminateDuplicates);
    if(m_newGeneration.size()<oldGeneration.size())
    {
      insertChild(parent2,eliminateDuplicates);
    }
  }
  
  //check if number of duplicates rejected is greater than popSize*alpha->stopping condition for GA
  return noOfDuplicatesEliminated>oldGeneration.size()*alpha==true?true:false;
  
}

Individual NewGeneration::tournamentSelection(const std::vector< Individual >& oldGeneration,const int& tournamentSize)
{
    /*
  Individual selectedIndividual;
  RandomGenerator rg;
  //select the first one
  selectedIndividual=oldGeneration.at(rg.randomInt(0,(oldGeneration.size()-1)));
  
  //select tournamentSize individuals-1,because one is already randomly selected
  for(int i=0;i<tournamentSize-1;i++)
  {
    //select random individual
    int rand=rg.randomInt(1,oldGeneration.size());
    if(selectedIndividual.fitness()>oldGeneration.at(rand-1).fitness())
    {
      selectedIndividual=oldGeneration.at(rand-1);
    }
  }
  return selectedIndividual;
  */
    
    // More efficient code
    
    RandomGenerator rg;    
    int bestInd = rg.randomInt(0,oldGeneration.size()-1);
    for(int i=1; i<tournamentSize; i++)
    {
        int nextInd =  rg.randomInt(0,oldGeneration.size()-1);
        if (oldGeneration.at(nextInd).fitness() < oldGeneration.at(bestInd).fitness())
        {
            bestInd = nextInd;
        }
    }
    return oldGeneration.at(bestInd);
    
}



void NewGeneration::onePointCrossover(const double& crossoverRate,Individual& parent1,Individual& parent2)
{
    RandomGenerator rg;
    double randNum=rg.randomDouble(0,1);
    //std::cout<<"random crossover probability: "<<randNum<<std::endl;
    if(randNum<=crossoverRate)
    {
        //do crossover
        //get binary vectors
        std::vector<std::vector<char>> &parrent1BV=parent1.binaryVectorsRef();
        std::vector<std::vector<char>> &parrent2BV=parent2.binaryVectorsRef();
        
        // More efficient crossover implementation
        int crossoverPoint = rg.randomInt(0, parrent1BV.size()-1);        
        for(int i=0; i<(int)parrent1BV.size(); i++)
        {
            if(i >= crossoverPoint)
            {
                int xPoint = 0;
                if(i == crossoverPoint)
                {
                    xPoint = rg.randomInt(0, parrent1BV.at(i).size()-1);
                }
                for(int j=xPoint; j<parrent1BV.at(i).size(); j++)
                {
                    char temp = parrent1BV[i][j];
                    parrent1BV[i][j] = parrent2BV[i][j];
                    parrent2BV[i][j] = temp;
                }
            }
        }        
        
        //swap bits after randomPoint
        /*
        for(auto vec1=parrent1BV.begin(),vec2=parrent2BV.begin();vec1!=parrent1BV.end();vec1++,vec2++)
        {
            double randomPoint=rg.randomInt(1,parrent1BV.at(0).size()-1);
            int i=0;
            for(auto ch1=vec1->begin(),ch2=vec2->begin();ch1!=vec1->end();ch1++,ch2++)
            {
                if(i>=randomPoint)
                {
                    char temp;
                    temp=*ch1;
                    *ch1=*ch2;
                    *ch2=temp;
                }
                i++;
            }      
        }
        */
    }
}

void NewGeneration::simpleMutation(const double& mutationRate,Individual& child)
{
  //if random number <= mutationRate do mutation
  RandomGenerator rg;
  
  //get binary vectors
  std::vector<std::vector<char>> &childBV=child.binaryVectorsRef();
  
  //we can mutate max 2 bits
  if(rg.randomDouble(0,1)<=mutationRate)
  {
    //mutate some random bit in random vector
    int randomVec=rg.randomInt(0,childBV.size()-1);
    int randomBit=rg.randomInt(0,childBV.at(0).size()-1);   
    childBV.at(randomVec).at(randomBit)=='1'?childBV.at(randomVec).at(randomBit)='0':childBV.at(randomVec).at(randomBit)='1';
    
    if(rg.randomDouble(0,1)<=mutationRate)
    {
      randomVec=rg.randomInt(0,childBV.size()-1);
      randomBit=rg.randomInt(0,childBV.at(0).size()-1);      
      childBV.at(randomVec).at(randomBit)=='1'?childBV.at(randomVec).at(randomBit)='0':childBV.at(randomVec).at(randomBit)='1';      
    }
  }
  
}

std::vector< Individual > NewGeneration::newGeneration()
{
  return m_newGeneration;
}

bool NewGeneration::isDuplicate(Individual& newChild)
{
  for(Individual &ind : m_newGeneration)
  {
    if(newChild==ind)
    {
      noOfDuplicatesEliminated++;
      return true;
    }
  }
  return false;
}

void NewGeneration::elitism(bool m_elitism, Individual& worstIndividual)
{
  if(m_elitism==true)
  {
    m_newGeneration.push_back(worstIndividual);
  }
}

void NewGeneration::insertChild(Individual& child, bool eliminateDuplicates)
{
  //first check if we should eliminate duplicates
  if(eliminateDuplicates)
  {
    if(!isDuplicate(child))
    {
      m_newGeneration.push_back(child);
    }
  }
  else
  {
    m_newGeneration.push_back(child);
  } 
}


