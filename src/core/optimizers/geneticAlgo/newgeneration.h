#ifndef NEWGENERATION_H
#define NEWGENERATION_H

#include <vector>
#include "individual.h"

/**
 * < Responsible for creating new generations with the help of selection and GA operators(crossover,mutation) >
 */

class NewGeneration
{

public:
  NewGeneration();
  virtual ~NewGeneration();
  
  /**
   * < do selection,crossover and mutation>
   */
  bool start(const std::vector<Individual>&oldGeneration,const double& crossoverRate,const double& mutationRate,const bool& elitism,Individual worstIndividual,const bool& eliminateDuplicates,const int& tournamentSize,const double& alpha);
  
  /**
   * < returns m_newGeneration, so we can replace old generation with new generation>
   */
  std::vector<Individual> newGeneration();

private:
  std::vector<Individual> m_newGeneration;
  int noOfDuplicatesEliminated;
  
  
   /**
   * < select 2 individuals for recombination >
   */
  Individual tournamentSelection(const std::vector<Individual> &oldGeneration,const int& tournamentSize);
  
   /**
   * < most simple crossover, swap bits after random bit
   */
  void onePointCrossover(const double& crossoverRate,Individual& parent1,Individual& parent2);
  
  /**
   * < There can be maximum 2 mutations in one Individual. >
   */
  void simpleMutation(const double& mutationRate,Individual& child);
  
  /**
   * @return returns true if individual already exists in new generation
   */
  bool isDuplicate(Individual& newChild);
  
  /**
   * < if elitism is on, insert individual with worst fitness>
   */
  void elitism(bool m_elitism,Individual &worstIndividual);
  
  /**
   * < Inserts generated child into new generation>
   */
  void insertChild(Individual &child,bool eliminateDuplicates);
};

#endif // NEWGENERATION_H
