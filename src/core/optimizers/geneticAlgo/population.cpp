#include "population.h"
#include <iostream>
#include "newgeneration.h"

Population::Population()
{
}


Population::~Population()
{
}

void Population::createPopulation(int noOfBits,int popSize,int noOfVariables)
{
  for(int i=0;i<popSize;i++)
  {
    Individual ind;
    ind.initializeIndividual(noOfBits,noOfVariables);
    pop.push_back(ind);
  }
}

void Population::calculatePopulationFitness(Evaluator *eval,std::vector<std::array<double,2>> &bounds)
{
  
  for(Individual &ind : pop)
  { 
    //pass on boundaries from variables for decoding
    ind.decode(bounds);
    
    //calculate fitness
    ind.setFitness(eval->evaluate(ind.decodedVectors()).value());
  }
}


Individual Population::findBestIndividual()
{
    /*
Individual worstIndividual=pop.at(0);

for(Individual &ind:pop)
{
    if(ind.fitness()<worstIndividual.fitness())
    {
    worstIndividual=ind;
    }
}
return worstIndividual;
*/
    
    // This should be more efficient (although not significantly)
    int bestIndex = 0;
    double bestFit = pop.at(0).fitness();
    
    for(int i=1;i<pop.size(); i++)
    {
        if(pop.at(i).fitness() < bestFit)
        {
            bestFit = pop.at(i).fitness();
            bestIndex = i;
        }
    }
    return pop.at(bestIndex);
}


bool Population::createNewGeneration(const double& crossoverRate,const double& mutationRate,const bool& elitism,const bool& eliminateDuplicates,const int& tournamentSize,const double& alpha)
{
  bool DuplicatesStoppingCondition;
  NewGeneration ng;
  DuplicatesStoppingCondition=ng.start(pop,crossoverRate,mutationRate,elitism,findBestIndividual(),eliminateDuplicates,tournamentSize,alpha);
  
  pop=ng.newGeneration();
  
  return DuplicatesStoppingCondition;
}

void Population::printPopulationInformation()
{
  int i=1;
  for(Individual &ind:pop)
  {
    std::cout<<"*****Individual "<<i<<"******"<<std::endl;
    ind.printBinaryVectors();
    ind.printDecodedVectors();
    ind.printFitness();
    i++;
    std::cout<<std::endl;
  }

}



