#include "geneticalgorithm.h"

GeneticAlgorithm::GeneticAlgorithm()
{
  //default
  m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::NoOfBits]=16;
  m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::PopulationSize]=30;
  m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::CrossoverRate]=0.80;
  m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::MutationRate]=0.001;
  m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::TournamentSize]=2;
  m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::Alpha]=1;
  m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::Elitism]=true;
  m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::EliminateDuplicates]=true;
}

OptimizerSampleResults GeneticAlgorithm::optimize(void )
{
  initialize();
  return start();
}

bool GeneticAlgorithm::validate(void )
{
	return false;
}

void GeneticAlgorithm::initialize()
{
  //get bounds
  for(int i=0;i<(int)optimizationProblem()->numberOfVariableDefinitions();i++)
  {
    ContinuousVariableDefinition *con=static_cast<ContinuousVariableDefinition*>(optimizationProblem()->variableDefinition(i));
    for(int i=0;i<con->quantity();i++)
    {
      std::array<double,2> x;
      x[0]=con->lowerBound();
      x[1]=con->upperBound();
      bounds.push_back(x);
    }
  }
  
  //create initial population  
  population.createPopulation(m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::NoOfBits].toInt(),m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::PopulationSize].toInt(),bounds.size());
  population.calculatePopulationFitness(evaluator(),bounds);
}

OptimizerSampleResults GeneticAlgorithm::start()
{
  int noOfGenerations=200;
  double fitnessTarget=-1;
  int maxeval;
  bool DuplicatesStoppingCondition;
  
  
  //get stopping conditions from gui
  if (m_definition->stoppingCondition(StoppingConditionDefinition::StoppingCondition::MaximumNumberOfEvaluations)->enabled())
  {
      maxeval = m_definition->stoppingCondition(StoppingConditionDefinition::StoppingCondition::MaximumNumberOfEvaluations)->parameter(0).toInt();
      noOfGenerations=maxeval/m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::PopulationSize].toInt();
  }
  if (m_definition->stoppingCondition(StoppingConditionDefinition::StoppingCondition::FitnessTarget)->enabled())
  {
      fitnessTarget = m_optimizationProblem->fitnessTarget();
  }
  
  //start the algorithm
  int iGen = 0;
  for(iGen=1;iGen<=noOfGenerations;iGen++)
  { 
      
    //selection,crossover and mutation
    DuplicatesStoppingCondition=population.createNewGeneration(m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::CrossoverRate].toDouble(),m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::MutationRate].toDouble(),m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::Elitism].toBool(),m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::EliminateDuplicates].toBool(),m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::TournamentSize].toInt(),m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::Alpha].toDouble());
  
    population.calculatePopulationFitness(evaluator(),bounds);
    
    //std::cout<<"GENERATION "<<iGen<<" : worst fitness:"<<population.findWorstIndividual().getFitness()<<std::endl;
    
    //check if fitness target reached
    if(population.findBestIndividual().fitness()<=fitnessTarget)
    {
      break;
    }
    
    //check if numberOfDuplicates>populationSize*alpha
    if(DuplicatesStoppingCondition)
    {
      break;
    }
  }
  
  OptimizerSampleResults osr;
  osr.m_finalFitness=population.findBestIndividual().fitness();
  osr.m_numberOfEvaluations=iGen*m_parameters[OptimizerDefinition::GeneticAlgorithmParameters::PopulationSize].toInt();
  osr.m_successful=(population.findBestIndividual().fitness() < m_optimizationProblem->fitnessTarget());
  return osr;
}


void GeneticAlgorithm::setParameter(OptimizerDefinition::GeneticAlgorithmParameters _parameter, QVariant _value)
{
  m_parameters[_parameter]=_value;
}



