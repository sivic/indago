#ifndef GENETICALGORITHM_H
#define GENETICALGORITHM_H

#include <memory>
#include "population.h"
#include "optimizer.h"

/**
 * < This class inherits from Optimizer and uses it's evaluator, optimizationProblem,...>
 * < For starting and stopping the algorithm >
 */

class GeneticAlgorithm : public Optimizer
{

public:
  
  GeneticAlgorithm();

  /**
  * < virtual from Optimizer class, it must return optimization results>
  */
  OptimizerSampleResults optimize(void);

  /**
  * < virtual from Optimizer class >
  */
  bool validate(void);

  /**
  * < for setting the parameters of GA >
  */
  void setParameter(OptimizerDefinition::GeneticAlgorithmParameters _parameter, QVariant _value);


private:
  /**
   * < GA starts optimizatin here >
   */
  OptimizerSampleResults start();
  
  /**
   * < get bounds and create initial population>
   */
  void initialize();
  
  //ga parameters
  Population population;
  std::vector< std::array< double, 2 > > bounds;
  
};

#endif // GENETICALGORITHM_H
