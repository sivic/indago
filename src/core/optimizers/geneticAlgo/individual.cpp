#define _USE_MATH_DEFINES
#include <cmath>
#include "individual.h"
#include <cstdlib>
#include <iostream>
//#include <math.h>
#include <array>
#include "randomgenerator.h"


Individual::Individual()
{

}

Individual::~Individual()
{

}


void Individual::initializeIndividual(int noOfBits, int noOfDimensions)
{

  for(int i=0;i<noOfDimensions;i++)
  {
    m_binaryVectors.push_back(randomBinaryVector(noOfBits));    
  }
    
}

std::vector<char> Individual::randomBinaryVector(int noOfBits)
{
  RandomGenerator rg;
  std::vector<char> binaryVector;
  for(int i=0;i<noOfBits;i++)
  {
    binaryVector.push_back(rg.randomBit());    
  }
  return binaryVector;
}


void Individual::decode(std::vector< std::array< double, 2 > > bounds)
{
  m_decodedVectors.clear();//because we want an empty vector when we create a new generation
  int i=0;
  double maxDecVal = double(pow(2,m_binaryVectors.at(0).size())-1);
  for(std::vector<char> &binVect : m_binaryVectors)
  {   
    double realNum=bounds.at(i).at(0) + double(binToDec(binVect))/maxDecVal*(bounds.at(i).at(1)-bounds.at(i).at(0));
    m_decodedVectors.push_back(realNum);
    i++;    
  }
}

long int Individual::binToDec(std::vector< char > &binary)
{
  long num=0;
  int j;
  int i;
  for(i=binary.size()-1,j=0;i>=0;i--,j++)
  {
    int bit;
    bit=binary.at(i)=='0' ? 0 : 1;
    num+=pow(2,j)*bit;
  }
  
  return num; 
}


void Individual::setFitness(double f)
{
  m_fitness=f;
}

QVector< double > Individual::decodedVectors()
{
  return m_decodedVectors;
}

double Individual::fitness() const
{
  return m_fitness;
}

void Individual::printBinaryVectors()
{
  std::cout<<"BINARY VECTORS"<<std::endl;
  int i=1;
  for(std::vector<char> &vect:m_binaryVectors)
  {
    std::cout<<"Variable "<<i<<": ";
    for(char &ch:vect)
    {
      std::cout<<ch;
    }
    std::cout<<std::endl;
    i++;
  }

}

void Individual::printDecodedVectors()
{
  std::cout<<"DECODED VECTORS"<<std::endl;
  int i=1;
  for(double &dec:m_decodedVectors)
  {
    std::cout<<"Variable "<<i<<": "<<dec<<std::endl;
    i++;
  }

}

void Individual::printFitness()
{
  std::cout<<"FITNESS: "<<m_fitness<<std::endl;
}

std::vector< std::vector< char > >& Individual::binaryVectorsRef()
{
  return m_binaryVectors;
}

bool operator==(const Individual& lhs, const Individual& rhs)
{
  return (lhs.m_binaryVectors==rhs.m_binaryVectors);
}

bool operator!=(const Individual& lhs, const Individual& rhs)
{
  return !(lhs.m_binaryVectors==rhs.m_binaryVectors);
}



