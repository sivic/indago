#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include <string.h>
#include <vector>
#include <array>
#include <QVector>

/**
 * < This class holds information about individual-fitness,binaryVectors >
 * < we also have methods here for decoding binary vectors to real numbers >
 */

class Individual
{

public:
  
  /**
   * < we need it when we're searching for duplicates, compares binary vectors of 2 individuals>
   */
  friend bool operator==(const Individual &lhs,const Individual &rhs);
  friend bool operator!=(const Individual &lhs,const Individual &rhs);
  
  Individual();
  virtual ~Individual();
  
  /**
   * < Creates a new individual with random vectors >
  * @param noOfBits binary representation-how many bits
  * @param noOfDimensions how much dimensions does fitness function have
  */
  void initializeIndividual(int noOfBits,int noOfDimensions);
  
  /**
   * < calculate real values from binary >
   * @param bounds boundaries for every dimension of fitness function
   */
  void decode(std::vector< std::array< double, 2 > > bounds);
  
  /**
   * < set fitness of individual >
   */
  void setFitness(double f);
  
  /**
   * < get fitness of individual >
   */
  double fitness() const;
  
  /**
   * < return decoded vectors >
   */
  QVector<double> decodedVectors();
  
  /**
   * < for testing purposes >
   */
  void printBinaryVectors();
  
  /**
   * < for testing purposes >
   */
  void printDecodedVectors();
  
  /**
   * < for testing purposes >
   */
  void printFitness();
  
  /**
   * < return reference to binary vectors, we need it when we're doing crossover and mutation >
   */
  std::vector<std::vector<char>> &binaryVectorsRef();


private:
  std::vector<std::vector<char>> m_binaryVectors;
  QVector<double> m_decodedVectors;//decoded binary 
  double m_fitness;
  
  /**
   * < create a random binary vector >
   */
  std::vector<char> randomBinaryVector(int);
  
  /**
   * < decode a binary vector to a decimal number >
   */
  long int binToDec(std::vector< char > &binary);
  
};


#endif // INDIVIDUAL_H
