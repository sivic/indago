#include "optimizerresults.h"
#include <limits>
OptimizerSampleResults::OptimizerSampleResults(void)
{

}

OptimizerSampleResults::OptimizerSampleResults(int _numberOfEvaluations, double _finalFitness)
{
    m_numberOfEvaluations = _numberOfEvaluations;
    m_finalFitness = _finalFitness;
}

void OptimizerStatistics::addSample(OptimizerSampleResults _sample)
{
    m_samples.append(_sample);
}

int OptimizerStatistics::numberOfSamples(void)
{
    return m_samples.count();
}

double OptimizerStatistics::averageNumberOfEvaluations(void)
{
    int nEvals = 0;
    int succesfulSamples = 0;
    for(int i=0; i<m_samples.size(); i++)
    {
        if(m_samples[i].m_successful)
        {
            nEvals += m_samples[i].m_numberOfEvaluations;
            succesfulSamples ++;
        }
    }
    qDebug() << m_samples.size();
    return double(nEvals)/double(succesfulSamples);
}

double OptimizerStatistics::successRate(void)
{
    int nSucc = 0;
    for(int i=0; i<m_samples.size(); i++)
    {   
        if(m_samples[i].m_successful)
            nSucc++;
    }
    return double(nSucc)/double(m_samples.size());
}

double OptimizerStatistics::averageFitness(void)
{
    double avgf = std::numeric_limits<double>::quiet_NaN();
    if(m_samples.size()>0) avgf = 0.0;
    for(int i=0; i<m_samples.size(); i++)
    {
        avgf += m_samples[i].m_finalFitness;
    }
    return avgf/double(m_samples.size());
}

double OptimizerStatistics::maxFitness(void)
{
    double maxf = std::numeric_limits<double>::quiet_NaN();
    if(m_samples.size()>0) maxf = m_samples.first().m_finalFitness;
    for(int i=0; i<m_samples.size(); i++)
    {
        maxf = std::max(maxf, m_samples[i].m_finalFitness);
    }
    return maxf;
}

double OptimizerStatistics::minFitness(void)
{
    double minf = std::numeric_limits<double>::quiet_NaN();
    if(m_samples.size()>0) minf = m_samples.first().m_finalFitness;
    for(int i=0; i<m_samples.size(); i++)
    {
        minf = std::min(minf, m_samples[i].m_finalFitness);
    }
    return minf;
}

double OptimizerStatistics::medianFitness(void)
{
    QVector<double> allSamplesFitness;
    foreach (const OptimizerSampleResults &sample, m_samples)
        allSamplesFitness.append(sample.m_finalFitness);
    qSort(allSamplesFitness);
    /*
    foreach(const double &s, allSamplesFitness)
        qDebug() << s;
    */
    int nSamples = m_samples.count();
    if(nSamples % 2 == 0)
    {
        int iMedian1 = int(floor(double(nSamples - 1) * 0.5));
        int iMedian2 = int(ceil(double(nSamples - 1) * 0.5));
        
        return (allSamplesFitness[iMedian1] + allSamplesFitness[iMedian2]) * 0.5;
    }
    else
    {
        int iMedian = (nSamples - 1) / 2;
        
        return allSamplesFitness[iMedian];
    }
}

