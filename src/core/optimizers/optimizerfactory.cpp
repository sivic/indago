#include "optimizerfactory.h"

Optimizer* OptimizerFactory::createOptimizer(OptimizerDefinition* _optimizerDefinition)
{
    Optimizer* newOptimizer = nullptr;
    
    switch(_optimizerDefinition->method())
    {
        case OptimizerDefinition::Method::PatternSearch:
        {
            newOptimizer = new PatternSearch();
            break;
        }
        case OptimizerDefinition::Method::PSO:
        {
            newOptimizer = new PSO();
            break;
        }
        case OptimizerDefinition::Method::GeneticAlgorithm:
        {
            newOptimizer = new GeneticAlgorithm();
            break;
        }
    }
    /*
    if(_optimizerDefinition->method() == OptimizerDefinition::Method::PSO)
    {
        newOptimizer = new PSO();
    }
    else if(_optimizerDefinition->method()==OptimizerDefinition::Method::GeneticAlgorithm)
    {
	newOptimizer = new GeneticAlgorithm();
    }
    */
    //QVector<ParameterDefinition*> parametersDefinitions = _optimizerDefinition->parametersDefinitions(_optimizerDefinition->method());
    
    newOptimizer->setOptimizerDefinition(_optimizerDefinition);
    /*
    for(int iParam = 0; iParam < parametersDefinitions.count(); iParam++)
    {
        newOptimizer->setParameter(iParam, _optimizerDefinition->parameter(iParam));
    }
    */
    
    return newOptimizer;
}
