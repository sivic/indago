#ifndef OPTIMIZERRESULTS_H
#define OPTIMIZERRESULTS_H

#include <QVector>
#include <QVariant>
#include <QString>
#include <QDebug>
#include "optimizationproblem.h"


/*!
 * \brief Base class for optimization results.
 */
class OptimizerSampleResults
{
public:
    
    OptimizerSampleResults(void);
    OptimizerSampleResults(int _numberOfEvaluations, double _finalFitness);

//private: // TODO: change to private and add access methods.
    int m_numberOfEvaluations;
    double m_finalFitness;
    bool m_successful;
};

/*!
 * \brief Container of stochastic optimizer results based on multiple runs (Monte Carlo Simulation).
 */
class OptimizerStatistics
{
public:
    /*!
     * \brief Adds a sample of optimization task results to the optimizer statistics.
     */
    void addSample(OptimizerSampleResults _sample);
    
    /*!
     * \brief Number of added samples.
     */
    int numberOfSamples(void);
    
    /*!
     * \brief Calculates average evaluations of all successful samples.
     */
    double averageNumberOfEvaluations(void);
    
    /*!
     * \brief Calculates succes rate of all samples.
     * \return A value between 0 (all unsuccessful) and 1 (all successful).
     */
    double successRate(void);
    
    /*!
     * \brief Calculates average achieved fitness for all samples.
     */
    double averageFitness(void);
    
    /*!
     * \brief Calculates minimal achieved fitness for all samples.
     */
    double minFitness(void);
    
    /*!
     * \brief Calculates maximal achieved fitness for all samples.
     */
    double maxFitness(void);
    
    /*!
     * \brief Calculates median of achieved fitness for all samples.
     */
    double medianFitness(void);

//    double fitnessStdDev(void); // TODO:
    
private:
    /*!
     * Container of optimization samples results.
     */
    QVector<OptimizerSampleResults> m_samples;

};

#endif // OPTIMIZERRESULTS_H
