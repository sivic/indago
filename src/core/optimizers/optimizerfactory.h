#ifndef OPTIMIZERFACTORY_H
#define OPTIMIZERFACTORY_H

#include "optimizer.h"
#include "patternsearch.h"
#include "pso.h"
#include "geneticalgorithm.h"

class OptimizerFactory
{
public:
    static Optimizer* createOptimizer(OptimizerDefinition* _optimizerDefinition);    
};

#endif // OPTIMIZERFACTORY_H
