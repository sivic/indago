#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include "population.h"
//#include <math.h>
#include <array>
#include "geneticalgorithm.h"
#include "builtinevaluatordefinition.h"
#include "evaluator.h"
#include "evaluatordefinition.h"


int TestGeneticAlgorithm(int,char *[])
{
  
  //optimization problem
  EvaluatorDefinition *evaluatorDef=new BuiltInEvaluatorDefinition(BuiltInEvaluatorDefinition::Sphere);
  
  OptimizationProblem *optimizationProblem=new OptimizationProblem(nullptr, QString("Name"), QString("Description"), evaluatorDef);
  VariableDefinition *var=new ContinuousVariableDefinition("x", -100, 100, 100, 30);
  optimizationProblem->addVariableDefinition(var);
  optimizationProblem->setFitnessTarget(-1);
  
  GeneticAlgorithm ga;
  ga.setOptimizationProblem(optimizationProblem);
  ga.setParameter(OptimizerDefinition::GeneticAlgorithmParameters::PopulationSize,QVariant(30));
  ga.setParameter(OptimizerDefinition::GeneticAlgorithmParameters::NoOfBits,QVariant(16));
  ga.setParameter(OptimizerDefinition::GeneticAlgorithmParameters::CrossoverRate,QVariant(0.8));
  ga.setParameter(OptimizerDefinition::GeneticAlgorithmParameters::MutationRate,QVariant(0.6));
  ga.setParameter(OptimizerDefinition::GeneticAlgorithmParameters::TournamentSize,QVariant(3));
  ga.setParameter(OptimizerDefinition::GeneticAlgorithmParameters::Elitism,QVariant(true));
  ga.setParameter(OptimizerDefinition::GeneticAlgorithmParameters::EliminateDuplicates,QVariant(true));
  ga.setParameter(OptimizerDefinition::GeneticAlgorithmParameters::Alpha,QVariant(1));
  
  ga.optimize();
  

  
  return 0;
}

