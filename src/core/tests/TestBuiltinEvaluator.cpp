
#include <iostream>
#include "builtinevaluatordefinition.h"
#include "evaluator.h"
#include "evaluatordefinition.h"
#include "evaluatorfactory.h"

int TestBuiltinEvaluator(int, char *[])
{
    int numErr = 0;

    std::cout << "Built-in evaluator test" << std::endl;
    
    BuiltInEvaluatorDefinition* definition = 
	    new BuiltInEvaluatorDefinition(BuiltInEvaluatorDefinition::Sphere);
    Evaluator* evaluator = EvaluatorFactory::createEvaluator(definition);
    
    QVector<QVariant> x;
    double res;
    
    x.fill(QVariant(10.0), 30);
    res = evaluator->evaluate(x).value();
    std::cout << "sphere(10.0 x 30d) = " << res << std::endl;
    if(std::abs(res - 3000.0) > 1.0e-8) ++numErr;
    
    x.clear();
    for(int i=0; i<30; i++)
        x.append(double(i));
    res = evaluator->evaluate(x).value();
    std::cout << "sphere(1, 2, ..., 30) = " << res << std::endl;
    if(std::abs(res - 8555.0) > 1.0e-8) ++numErr;
    
    return numErr;
}
