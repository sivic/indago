#include <stdlib.h>
#include <iostream>
#include <QVector>
#include <fstream>
#include <string>
#include <algorithm>
#include <time.h>
#include "datapoint.h"
#include "pathconfig.h"
#include "algorithm.h"
#include "quickalgorithm.h"
#include "paretoalgorithm.h"
#include <chrono>
using namespace std;

int TestParetoFront_auto(int, char *[])
{
  cout<<"\n\t---Pareto Front Test---\n"<<endl;
  //size_t K = 2; //hardoced dimension, for now, 2D
  
  
  
  QVector<Datapoint*>dataset; //vektor objekata Datapoint
    
    ifstream infile( filePathBuilder::testDataPath("points").toStdString() );
    if(!infile){
      cerr << "Ne mogu otvoriti file sa točkama." << endl;
      exit(1);
    }
    
    size_t id = 0;
    double a,b; //a is for first objective -> first column in file, and b is for second objective -> second column in file
    //hardoced 2D problem - dimension
    double m_1 = 1; //maximize is better -> for first objective
    double m_2 = 1; //maximize is better -> for second objective
    
    while( infile >> a >> b ){ // !infile.eof()
      //if( i == 0 ){
	Datapoint *d = new Datapoint(id);
	dataset.push_back(d);
      //}
      dataset[id]->addNumbers(m_1 * a);
      dataset[id]->addNumbers(m_2 * b);
      ++id;
    }
    
    infile.close(); 
  //unutar jednog file-a se nalaze točke koje određuju pareto frontu : 1 - pareto, 0 - nije pareto.
  //taj file ucitavam u vektor kako bih mogao vidjeti da li je test prošao ili pao.
  QVector<int> pfresult;
  ifstream in(filePathBuilder::testDataPath("pfresult").toStdString());
  double number;
  while( in >> number ){ 
    pfresult.push_back(number);
  }

  auto start = chrono::steady_clock::now();
  //make instance of algorithm and compute pareto (input will be dataset)
  ParetoAlgorithm *algo = new Algorithm();
  int numberOfPareto = algo -> computeParetoFrontier(dataset);  
  
  auto end = chrono::steady_clock::now(); 
  auto diff = end - start;

  cout << "Pareto points: # " << numberOfPareto << " of total " << dataset.size() << "\n" << endl;
  

  cout << "Proteklo vrijeme u mikro sec: "<<chrono::duration <double, micro> (diff).count()<<endl;
  
  //clear all data in dumpFile before wiriting new points into the file
  ofstream outf;
  outf.open(filePathBuilder::testDataPath("dumpFile").toStdString(), std::ofstream::trunc);
  
  QVector<int> paretoStatus;
  
  for(int d = 0; d < dataset.size(); ++d)
  {
    dataset[d] -> printResult();
    //adding point to the file but only that one that belong pareto fronta
    if(dataset[d] ->getParetoStaus() == 1 ){
      dataset[d] ->dumpToFile();
      paretoStatus.push_back(1);
    }else paretoStatus.push_back(0);
  }
  cout << endl;   
  //check if test is passed or not
  cout << "\t---Conclusion--- "<< endl;
  if( pfresult == paretoStatus ){
    cout << "\t  Test passed" << endl;
  }else cout << "\t  Test is failed" << endl;
  
  return 0;
}