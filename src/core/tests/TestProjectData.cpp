#include "projectdata.h"
#include <iostream>

#include "pathconfig.h"

int TestProjectData(int, char *[])
{
    int numErr = 0;
    // path builder example \todo to remove once everyone start using this...
    std::cout << filePathBuilder::testDataPath("filename.txt").toStdString() << std::endl;

    ProjectData project;
    project.setName("Testno ime");
    project.setDescription("Kratki opis projekta. I još jedna linija.");

     project.addOptimizationProblem(new OptimizationProblem(&project, "Rastrigin", "Testni problem 1", 
                                                       new BuiltInEvaluatorDefinition()
                                  ));
     project.addOptimizationProblem(new OptimizationProblem(&project, "Rasenbrock", "Testni problem 2", 
                                                       new BuiltInEvaluatorDefinition()
                                  ));
     project.addOptimizationProblem(new OptimizationProblem(& project, "Ackley", "Novi test", 
                                                       new BuiltInEvaluatorDefinition()
                                  ));
    project.saveToFile("TestProjectData.indago");
    std::cout<<"neki test"<<std::endl;
    return numErr;
}
