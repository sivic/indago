#include <iostream>
#include "projectdata.h"
#include "optimizerfactory.h"
#include "builtinevaluatordefinition.h"
#include "evaluator.h"
#include "evaluatordefinition.h"

#include "pso.h"
#include <QTime>
//#include <QCoreApplication>

#include "consoleoutput.h"
#include "pathconfig.h"

int test_pso_1(void);
int test_pso_2(void);
int test_pso_3(void);
    


int TestPSO(int argc, char *argv[])
{
    int numErr=0;
    std::cout << "TestPSO started" << std::endl;
    
    //numErr += test_pso_1();
    //numErr += test_pso_2();
    numErr += test_pso_3();
    
    std::cout << "TestPSO finished" << std::endl;
    
    return numErr;
}
int test_pso_1(void)
{
    int numErr = 0;
    QTime myTimer;
    myTimer.start();

    std::cout << "PSO search test" << std::endl;
    
    /***********************************************************************************************
     * Preparing optimization problem definition (usually should be constructed 
     * during reading the project file or trough GUI) 
     ***********************************************************************************************/
    EvaluatorDefinition* evaluatorDef = 
    new BuiltInEvaluatorDefinition(BuiltInEvaluatorDefinition::Sphere);
    
        
    OptimizationProblem* optimizationProblem =
    new OptimizationProblem(nullptr, QString("Name"), QString("Description"), evaluatorDef);
        
   VariableDefinition* var = new ContinuousVariableDefinition("x", -100.0, 100.0, 100.0, 30);
   optimizationProblem->addVariableDefinition(var);
   /***********************************************************************************************/
    
    std::cout << "- Optimization problem defined" << std::endl;
    
    
    
    

    
    std::cout << "- Starting the optimization" << std::endl;
   
    /***********************************************************************************************
     * Creating, passing parameters and running Pattern search                                     *
     ***********************************************************************************************/
    PSO pso; // Use OptimizerFactory?
    pso.setOptimizationProblem(optimizationProblem);
    pso.setParameter(OptimizerDefinition::PsoParameters::SwarmSize, QVariant(20));
    pso.setParameter(OptimizerDefinition::PsoParameters::InertiaFactor, QVariant(0.72));
    pso.setParameter(OptimizerDefinition::PsoParameters::CognitiveFactor, QVariant(1.0));
    pso.setParameter(OptimizerDefinition::PsoParameters::SocialFactor, QVariant(1.0));
    pso.setParameter(OptimizerDefinition::PsoParameters::Topology, QVariant("lbest"));
    
    // Same thread
    pso.optimize();
    /***********************************************************************************************/
    
    std::cout << "- Optimization finished" << std::endl;
    //std::cout << "- Final status: " << PatternSearch::enumToString(p.status()).toStdString() << std::endl;
    
    int nMilliseconds = myTimer.elapsed();
    
    std::cout << "PSO test finished in " << nMilliseconds << " ms." << std::endl;
    
    return numErr;
}

int test_pso_2(void)
{
    int numErr = 0;
    
    ProjectData project;
    //project.readFromFile("../../projects/test_pso_01.indago");
    project.readFromFile(filePathBuilder::testDataPath("test_pso_01.indago"));
    //project.optimizer(0)->setParameter(OptimizerDefinition::PsoParameters::PFIDI, QString("lpso"));
    
    Optimizer* pso1 = OptimizerFactory::createOptimizer(project.optimizer(0)); 
    pso1->setOptimizationProblem(project.optimizationProblem(0));
    pso1->optimize();
        
    /*
    project.optimizer(0)->setParameter(OptimizerDefinition::PsoParameters::PFIDI, QString("lpso"));
    Optimizer* pso2 = OptimizerFactory::createOptimizer(project.optimizer(0)); 
    pso2->setOptimizationProblem(project.optimizationProblem(0));
    pso2->optimize();
    */
    
    return numErr;
}

int test_pso_3(void)
{
    std::cout << "-----------------------------" << std::endl;
    std::cout << "test_pso_3 started" << std::endl;
    //QCoreApplication a(argc, argv);    
    //QTextStream qout(stdout);
    ConsoleOutput co;
    
    ProjectData project;
    //project.readFromFile("../../projects/test_pso_01.indago");    
    project.readFromFile(filePathBuilder::testDataPath("test_pso_01.indago"));
    
    Optimizer* pso1 = OptimizerFactory::createOptimizer(project.optimizer(0)); 
    pso1->setOptimizationProblem(project.optimizationProblem(0));
    QObject::connect(pso1, SIGNAL(appentToLog(QString)), &co, SLOT(onAppendToLog(QString)));
    //QObject::connect(pso1, SIGNAL(finished()), &a, SLOT(quit()));
    pso1->optimize();
    std::cout << "test_pso_3 finished" << std::endl;
    std::cout << "-----------------------------" << std::endl;
        
    //return a.exec();
    return 0;
}

