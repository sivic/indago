#include <iostream>
#include "builtinevaluatordefinition.h"
#include "evaluator.h"
#include "evaluatordefinition.h"
#include "patternsearch.h"

    
int TestPatternSearch(int, char *[])
{
    int numErr = 0;

    std::cout << "Pattern search test" << std::endl;
    
    /***********************************************************************************************
     * Preparing optimization problem definition (usually should be constructed 
     * during reading the project file or trough GUI) 
     ***********************************************************************************************/
    EvaluatorDefinition* evaluatorDef = 
    new BuiltInEvaluatorDefinition(BuiltInEvaluatorDefinition::Sphere);
    
        
    OptimizationProblem* optimizationProblem =
    new OptimizationProblem(nullptr, QString("Name"), QString("Description"), evaluatorDef);
        
    for(int iVar = 0; iVar<30; iVar++)
    {
        VariableDefinition* var = new ContinuousVariableDefinition("var"+QString::number(iVar+1), -100.0, 100.0, 100.0);
        optimizationProblem->addVariableDefinition(var);
    }
    /***********************************************************************************************/
    
    std::cout << "- Optimization problem defined" << std::endl;
    
    
    
    
    // Start point needed for Pattern search
    QList<QVariant> x0;
    for(int i=0; i<30; i++)
        x0.append(QVariant(double(i)*1.1 + 0.02));
    

    
    std::cout << "- Starting the optimization" << std::endl;
   
    /***********************************************************************************************
     * Creating, passing parameters and running Pattern search                                     *
     ***********************************************************************************************/
    PatternSearch p; // Use OptimizerFactory?
    p.setOptimizationProblem(optimizationProblem);
//    p.setParameter(PatternSearch::startPoint, QVariant(x0));
//    p.setParameter(PatternSearch::rho, 0.5);
    
    // Same thread
    p.optimize();
    
    // New Thread
    p.start();
    p.wait();
    /***********************************************************************************************/
    
    std::cout << "- Optimization finished" << std::endl;
    std::cout << "- Final status: " << PatternSearch::enumToString(p.status()).toStdString() << std::endl;
    
    return numErr;
}
