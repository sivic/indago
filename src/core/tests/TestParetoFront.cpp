#include <stdlib.h>
#include <iostream>
#include <QVector>
#include <fstream>
#include <vector>
#include <string>
#include <time.h>
#include "paretoalgorithm.h"
#include "quickalgorithm.h"
#include "algorithm.h"
#include "datapoint.h"
#include "pathconfig.h"
#include <chrono>
using namespace std;

int TestParetoFront(int, char *[])
{
  cout<<"\n\tPareto Front Test\n\n"<<endl;
  
  int Kdim;
  
  double m_1 = 1; //maximize is better
  double m_2 = 1; //minimize is better
  
  cout<<"Problem in dimension(2,3 or higher): ";
  cin >> Kdim;
  
    
  //vector<pair<string, int> > files;
  //for(vector<string>::const_iterator it = p.beginVecFile(); it != p.endVecFile(); ++it)
  //{
  //  files.push_back(make_pair(*it, m));
  //}
  //files.push_back( make_pair(filePathBuilder::testDataPath("first_set").toStdString(), 1) );
  //files.push_back(make_pair(filePathBuilder::testDataPath("second_set").toStdString(), 1) );
  
  //check if Kdim is >= 2
  if(Kdim <2){
    cerr << "[error] nedovoljan broj dimenzija (potrebno 2 ili više ulaznih fileova podataka). " << endl;
  }
  
  QVector<Datapoint*> dataset;
  
  //pr. ako je korisnik unesao 2D problem, 2 file-a, postojati će dva zapisa tipa Datapoint u vektoru dataset
  //for(size_t x = 0; x < files.size(); ++x)
  //{
    ifstream inputfile(filePathBuilder::testDataPath("points").toStdString());
    
    if(!inputfile){
      cerr<<"\nCan not open file with points. " <<endl;
      exit(1);
    }
    
    size_t id = 0;	
    double num1;
    double num2;
    
    //return 1 or -1, where is 1-> maximize is better, and -1 ->minimize is better
    //double f = files[x].second; 
    
    while(inputfile >> num1 >> num2){
      
      //instaciramo objekt klase Datapoint i dodajemo brojeve u vector, uz to, nakon svakog dodanog, povećamo id za svaku točku
      //if( x == 0){
	//instanciram objekt tipa Datapoint i dodajem u dataset
	Datapoint *dp = new Datapoint(id);
	dataset.push_back(dp);
      //}
      //if insert 1, it's be maximize objective, or minimize for f= -1
      dataset[id] -> addNumbers(num1*m_1);
      dataset[id] -> addNumbers(num2*m_2);
      ++id;
    }
    
  //} //end-of for loop
  auto start = chrono::steady_clock::now();
  //dodao točke u dataset, sada treba izračunati frontier...
  ParetoAlgorithm *algo = new quickAlgorithm();
  int numberOfPareto = algo -> computeParetoFrontier(dataset);
  
  auto end = chrono::steady_clock::now();
  auto diff = end - start;
 
  
  cout << "Proteklo vrijeme u mikro sec: "<<chrono::duration <double, micro> (diff).count()<<endl;
  
  //clear all data in dumpFile before wiriting new points into the file
  ofstream outf;
  outf.open(filePathBuilder::testDataPath("dumpFile").toStdString(), std::ofstream::trunc);
  
  cout << "\nTotal pareto: " << numberOfPareto << "of # " << dataset.size() << endl;
  for(size_t d = 0; d < (size_t)dataset.size(); ++d)
  {
    if(dataset[d] ->getParetoStaus() == 1 ) dataset[d] ->dumpToFile();
  }
  
  return 0;
}