#include <iostream>
#include "projectdata.h"
#include "optimizerfactory.h"
#include "builtinevaluatordefinition.h"
#include "evaluator.h"
#include "evaluatordefinition.h"
#include "jobmanager.h"
#include "pso.h"
#include <QTime>
#include "pathconfig.h"

int test_simple_runs(void);
    
int TestSimpleRuns(int, char *[])
{
    int numErr=0;
    
    numErr += test_simple_runs();
    
    return numErr;
}

int test_simple_runs(void)
{
    std::cout << "-----------------------------" << std::endl;
    std::cout << "test_simple_runs started" << std::endl;
    int numErr = 0;
    
    ProjectData project;
    //project.readFromFile("../../projects/test_pso_03.indago");
    project.readFromFile(filePathBuilder::testDataPath("test_pso_03.indago"));
    
    qDebug() << "Number of problems: " << project.numberOfOptimizationProblems();
    qDebug() << "Number of optimizers: " << project.numberOfOptimizers();
    qDebug() << "Number of jobs: " << project.numberOfJobs();

    JobManager jobMng;
    jobMng.addJob(&project, 0);
    jobMng.addJob(&project, 1);
    jobMng.addJob(&project, 2);
    jobMng.run();
    
    std::cout << "test_simple_runs finished" << std::endl;
    std::cout << "-----------------------------" << std::endl;

    return numErr;
}
