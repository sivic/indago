#ifndef PYTHONEVALUATORDEFINITION_H
#define PYTHONEVALUATORDEFINITION_H

#include <QString>
#include "evaluatordefinition.h"

class PythonEvaluatorDefinition: public EvaluatorDefinition
{
public:
    PythonEvaluatorDefinition(void);
    
    YAML::Node toYamlNode(void);
    void fromYamlNode(YAML::Node _node);
    
    QString pythonFile;
    QString functionName;
};

#endif // PYTHONEVALUATORDEFINITION_H
