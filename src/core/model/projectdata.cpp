#include "projectdata.h"

YAML::Node ProjectData::toYamlNode(void )
{
    YAML::Node project;
    project["name"] = m_name.toStdString();
    project["description"] = m_description.toStdString();
    
    for(auto& problem : m_optimizationProblems)
    {
        project["optimization problems"].push_back(problem->toYamlNode());
    }
    for(auto& method : m_optimizers)
    {
        project["optimizers"].push_back(method->toYamlNode());
    }
    for(auto& jobitem : m_jobs)
    {
        project["jobs"].push_back(jobitem->toYamlNode());
    }
    return project;
}

void ProjectData::fromYamlNode(YAML::Node _node)
{
    //qDebug() << _node.size();

    if(_node["name"])
    {
        m_name = QString::fromUtf8(_node["name"].as<std::string>().c_str());
    }
    if(_node["description"])
    {
        m_description = QString::fromUtf8(_node["description"].as<std::string>().c_str());
    }
    if(_node["optimization problems"])
    {
        for(size_t i=0; i<_node["optimization problems"].size(); i++)
        {
            OptimizationProblem* op = new OptimizationProblem(this);
            op->fromYamlNode(_node["optimization problems"][i]);
            m_optimizationProblems.append(op);
        }
    }
    if(_node["optimizers"])
    {
        for(size_t i=0; i<_node["optimizers"].size(); i++)
        {
            OptimizerDefinition* op = new OptimizerDefinition(this);
            op->fromYamlNode(_node["optimizers"][i]);
            m_optimizers.append(op);
        }
    }
    if (_node["jobs"])
    {
        for (size_t i = 0; i < _node["jobs"].size(); ++i)
        {
            JobDefinition* job = new JobDefinition(this);
            job->fromYamlNode(_node["jobs"][i], this);
            m_jobs.append(job);
        }
    }
}

OptimizationProblem* ProjectData::addOptimizationProblem(OptimizationProblem *_problem, QString _id)
{
    bool uniqueId = true;
    if(_id == "")
    {
        _id = IdHandling::generateUniqueId<OptimizationProblem>("optprob_",
                                                                numberOfOptimizationProblems(),
                                                                m_optimizationProblems);
    }
    else
    {
        uniqueId = IdHandling::checkIdUnique(_id, m_optimizationProblems);
    }

    bool valid = IdHandling::validateIdString(_id);

    if(uniqueId && valid)
    {
        _problem->setId(_id);
        m_optimizationProblems.push_back(_problem);
        return m_optimizationProblems.last();
    }
    else
    {
        return nullptr;
    }
}

bool ProjectData::changeOptimizationProblemId(int _index, QString _newId)
{
    QString currentId = optimizationProblem(_index)->id();
    if(currentId == _newId)
        return true;
    
    QString changedValue = IdHandling::changedIdValue<OptimizationProblem>(
                                                _newId, m_optimizationProblems);
    

    if(changedValue != "")
    {
        for (JobDefinition *job : m_jobs)
        {
            job->changeProblemId(currentId, _newId);
        }
        optimizationProblem(_index)->setId(_newId);
        return true;
    }
    else return false;
}

OptimizationProblem *ProjectData::optimizationProblem(const int &_at)
{ assert(_at<m_optimizationProblems.size());return m_optimizationProblems[_at]; }

OptimizationProblem *ProjectData::optimizationProblem(const QString &_id)
{
    for (OptimizationProblem *op : m_optimizationProblems)
    {
        if (op->id() == _id) return op;
    }
    return nullptr;
}

bool ProjectData::optimizationProblemExists(QString _id)
{
    return IdHandling::idExists<OptimizationProblem>(_id, m_optimizationProblems);
}

void ProjectData::removeOptimizationProblemAt(const int &_index)
{
    assert(_index<m_optimizationProblems.size());
    
    for (JobDefinition *job : m_jobs)
    {
        job->removeProblem(m_optimizationProblems[_index]->id());
    }
    delete m_optimizationProblems[_index];
    m_optimizationProblems.remove(_index);
}

OptimizerDefinition *ProjectData::addOptimizer(OptimizerDefinition *_optimizer, QString _id)
{
    bool uniqueId = true;
    if(_id == "")
    {
        _id = IdHandling::generateUniqueId<OptimizerDefinition>("optdef_",
                                                                numberOfOptimizers(),
                                                                m_optimizers);
    }
    else
    {
        uniqueId = IdHandling::checkIdUnique(_id, m_optimizers);
    }

    bool valid = IdHandling::validateIdString(_id);

    if(uniqueId && valid)
    {
        _optimizer->setId(_id);
        m_optimizers.push_back(_optimizer);
        return m_optimizers.last();
    }
    else
    {
        return nullptr;
    }
}

bool ProjectData::changeOptimizerId(int _index, QString _newId)
{
    QString currentId = optimizer(_index)->id();
    if(currentId == _newId)
        return true;

    QString changedValue = IdHandling::changedIdValue<OptimizerDefinition>(
                                                            _newId, m_optimizers);


    if(changedValue != "")
    {
        for (JobDefinition *job : m_jobs)
        {
            job->changeOptimizerId(currentId, _newId);
        }
        optimizer(_index)->setId(_newId);
        return true;
    }
    else return false;
}

OptimizerDefinition *ProjectData::optimizer(const int &_at)
{ assert(_at<m_optimizers.size());return m_optimizers[_at]; }

OptimizerDefinition *ProjectData::optimizer(const QString &_id)
{
    for (OptimizerDefinition *od : m_optimizers)
    {
        if (od->id() == _id) return od;
    }
    return nullptr;
}

size_t ProjectData::numberOfJobs() const
{
    return m_jobs.size();
}

bool ProjectData::optimizerExists(QString _id)
{
    return IdHandling::idExists<OptimizerDefinition>(_id, m_optimizers);
}

void ProjectData::removeOptimizer(int _index)
{
    assert(_index<m_optimizers.size());
    
    for (JobDefinition *job : m_jobs)
    {
        job->removeOptimizer(m_optimizers[_index]->id());
    }
    delete m_optimizers[_index];
    m_optimizers.remove(_index);
}

JobDefinition* ProjectData::addJob(JobDefinition *_job, QString _id)
{
    bool uniqueId = true;
    if(_id == "")
    {
        _id = IdHandling::generateUniqueId<JobDefinition>("jobdef_",
                                                          numberOfJobs(),
                                                          m_jobs);
    }
    else
    {
        uniqueId = IdHandling::checkIdUnique(_id, m_jobs);
    }

    bool valid = IdHandling::validateIdString(_id);

    if(uniqueId && valid)
    {
        _job->setId(_id);
        m_jobs.push_back(_job);
        return m_jobs.last();
    }
    else
    {
        return nullptr;
    }
}

bool ProjectData::changeJobId(int _index, QString _newId)
{
    if(job(_index)->id() == _newId)
        return true;

    QString changedValue = IdHandling::changedIdValue<JobDefinition>(
                                                            _newId, m_jobs);

    if(changedValue != "")
    {
        job(_index)->setId(_newId);
        return true;
    }
    else return false;
}

JobDefinition* ProjectData::job(const int& _at)
{
    assert(_at<m_jobs.size());
    return m_jobs[_at];
}

JobDefinition *ProjectData::job(const QString &_id)
{
    for (JobDefinition *jd : m_jobs)
    {
        if (jd->id() == _id) return jd;
    }
    return nullptr;
}

void ProjectData::removeJob(int _at)
{
    assert(_at<m_jobs.size());

    delete m_jobs[_at];
    m_jobs.remove(_at);
}

void ProjectData::saveToFile(QString _filename)
{
    m_filename = _filename;
    std::ofstream pf(_filename.toUtf8());
    pf << toYamlNode();
}

void ProjectData::readFromFile(QString _filename)
{
    if(!QFile::exists(_filename))
    {	qDebug() << "ERROR: " << _filename<< " doesn't exist!"; }
    
    m_filename = _filename;
    YAML::Node node = YAML::LoadFile(_filename.toStdString());
    //qDebug() << "number of optimizers: " << m_optimizers.count();

    fromYamlNode(node);
}
