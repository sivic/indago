#ifndef PROJECTDATA_H
#define PROJECTDATA_H

#include <QString>
#include "yaml-cpp/yaml.h"
#include <fstream>
#include <QDebug>
#include <QFile>
#include "optimizationproblem.h"
#include "jobdefinition.h"
#include "optimizerdefinition.h"
#include <QVector>
#include "idhandling.h"

class ProjectData: public QObject
{
    Q_OBJECT
    
public:
    /*! \brief Returns the file name where the project is saved.
     *  
     * If project is not (yet) saved, the filename is empty QString.
     */    
    QString filename() const{return m_filename;}
    
    QString name() const {return m_name;}
    void setName(const QString& _n){m_name = _n;}
    
    QString description() const {return m_description;}
    void setDescription(const QString& _d){m_description = _d;}
    
    
    //! \brief removes optimization problem at _index
    //! \param _index optimization problem index to be removed
    //! \precondition _index should be less than \var m_optimizationProblems 's size
    void removeOptimizationProblemAt(const int& _index);
    
    /*! \brief Returns a project's optimization problem for given index.
     * \param _index must be valid index of optimization problem.
     */
    OptimizationProblem* optimizationProblem(const int& _index);
    OptimizationProblem* optimizationProblem(const QString &_id);
    OptimizationProblem* addOptimizationProblem(OptimizationProblem *_problem, QString _id = "");
    bool changeOptimizationProblemId(int _index, QString _newId);
    size_t numberOfOptimizationProblems() const {return m_optimizationProblems.size();}
    bool optimizationProblemExists(QString _id);
    
    OptimizerDefinition* optimizer(const int& _index);
    OptimizerDefinition* optimizer(const QString &_id);
    OptimizerDefinition* addOptimizer(OptimizerDefinition *_optimizer, QString _id = "");
    bool changeOptimizerId(int _index, QString _newId);
    size_t numberOfOptimizers() const {return m_optimizers.size();}
    bool optimizerExists(QString _id);
    void removeOptimizer(int _index);
    
    
    /*!
     * \brief Returns number of jobs in project.
     */
    size_t numberOfJobs() const;
    
    /*!
     * \brief Returns the job at position _at in the list of project's jobs.
     * \arg _index must be a valid index (0 <= _at < numberOfJobs()).
     */
    JobDefinition* job(const int& _index);
    JobDefinition *job(const QString &_id);
    
    /*!
     * \brief Appends a job to list of project's jobs.
     * \arg _job is pointer to job that is beeing added.
     * \return Returns a pointer to added job.
     */
    JobDefinition* addJob(JobDefinition *_job, QString _id = "");
    
    /*!
     * \brief Changes name of the job.
     * \arg _index is job position in vector and _newId is a new job string
     * \return Returns true if id is changed, false otherwise
     */
    bool changeJobId(int _index, QString _newId);

    /*!
     * \brief Removes job at given index.
     * \arg _index must be a valid index (0 <= _at < numberOfJobs()).
     */
    void removeJob(int _index);
    
    /*!
     * \brief saves project data to file
     * \precondition _filename is a valid filename
     * \sideeffect, \var m_filename gets to be \param _filename  
     */
    void saveToFile(QString _filename);
    void readFromFile(QString _filename);
        
private:
    //! \brief list of problems 
    QVector<OptimizationProblem*> m_optimizationProblems;
    //! \brief list of jobs 
    QVector<OptimizerDefinition*> m_optimizers;
    //! \brief list of jobs 
    QVector<JobDefinition*> m_jobs;
    //! \brief project name
    QString m_name;
    //! \brief project description
    QString m_description;
    //! \brief full file path where the project description is saved
    QString m_filename;
    
    YAML::Node toYamlNode(void);
    void fromYamlNode(YAML::Node _node);
};

#endif // PROJECTDATA_H
