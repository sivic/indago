#ifndef STOPPINGCONDITIONDEFINITION_H
#define STOPPINGCONDITIONDEFINITION_H

#include "enum.h"
#include "parameterdefinition.h"
#include "yaml-cpp/yaml.h"
#include <QHash>
#include <QObject>
#include <QVariant>
#include <QVector>

#include <QDebug>

/*! 
 * \brief Stopping condition
 */
class StoppingConditionDefinition: public QObject
{
    Q_OBJECT
    Q_ENUMS(StoppingCondition)
   
public:
    /*
    OptimizerDefinition(QObject* _parent);
    */
    YAML::Node toYamlNode(void);
    void fromYamlNode(YAML::Node _node);
    
    enum StoppingCondition
    {
        FitnessTarget,
        MaximumNumberOfEvaluations,
    };
    ENUM_TO_STRING(StoppingConditionDefinition, StoppingCondition)
    STRING_TO_ENUM(StoppingConditionDefinition, StoppingCondition)
    ENUM_COUNT(StoppingConditionDefinition, StoppingCondition)
    ENUM_TO_STRINGLIST(StoppingConditionDefinition, StoppingCondition)
    
    StoppingConditionDefinition(StoppingCondition _stoppingCondition, QObject* parent = 0);
    
    QString name(void){return m_name;}
    //void setName(QString _name){m_name = _name;}
    QString description(void){return m_description;}
    //void setDescription(QString _description){m_description = _description;}
    bool enabled(void){return m_enabled;}
    void setEnabled(bool _enabled){m_enabled = _enabled;}
    
    size_t numberOfParameters(void){return m_parameters.size();};
    QVariant parameter(int _index){return m_parameters[_index];};
    void setParameter(int _index, QVariant _value);
    
    StoppingCondition type(void){return m_type;};
    
    static QVector<ParameterDefinition*> parametersDefinitions(StoppingCondition _stoppingCondition);
    
private:
    QString m_name;
    QString m_description;
    bool m_enabled;
    StoppingCondition m_type;
    
    QVector<QVariant> m_parameters;
};

#endif //STOPPINGCONDITIONDEFINITION_H
