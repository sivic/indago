#ifndef DISCRETEVARIABLEDEFINITION_H
#define DISCRETEVARIABLEDEFINITION_H

#include "yaml-cpp/yaml.h"
#include "variabledefinition.h"


class DiscreteVariableDefinition: public VariableDefinition
{
public:
    DiscreteVariableDefinition();
    DiscreteVariableDefinition(const QString n, int _quantity = 1);
    
    YAML::Node toYamlNode(void);
    void fromYamlNode(YAML::Node _node);
};

#endif // DISCRETEVARIABLEDEFINITION_H
