#ifndef EVALUATORDEFINITION_H
#define EVALUATORDEFINITION_H

#include "enum.h"
#include "yaml-cpp/yaml.h"
#include <QObject>


class EvaluatorDefinition: public QObject
{
    Q_OBJECT
    Q_ENUMS(EvaluatorType)
    
public:
    enum EvaluatorType
    {
        builtIn,
        python
    };
    ENUM_TO_STRING(EvaluatorDefinition, EvaluatorType)
    STRING_TO_ENUM(EvaluatorDefinition, EvaluatorType)
    ENUM_COUNT(EvaluatorDefinition, EvaluatorType)
    ENUM_TO_STRINGLIST(EvaluatorDefinition, EvaluatorType)
    
    EvaluatorDefinition(const EvaluatorType t);
    EvaluatorType type()const {return  m_type;}

private:
    const EvaluatorType m_type;
    
public:
    virtual YAML::Node toYamlNode(void) = 0;
    virtual void fromYamlNode(YAML::Node _node) = 0;

};

#endif // EVALUATORDEFINITION_H
