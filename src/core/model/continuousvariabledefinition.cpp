#include "continuousvariabledefinition.h"

ContinuousVariableDefinition::ContinuousVariableDefinition() :
VariableDefinition(VariableType::continuous)
{
}

ContinuousVariableDefinition::ContinuousVariableDefinition(const QString n, const double lb, const double ub, const double dv, int _quantity) : 
VariableDefinition(n,VariableType::continuous, _quantity), 
m_lowerBound(lb), 
m_upperBound(ub), 
m_defaultValue(dv)
{}

YAML::Node ContinuousVariableDefinition::toYamlNode(void )
{
    YAML::Node node;
    node["name"] = name().toStdString();
    node["type"] = ContinuousVariableDefinition::enumToString(continuous).toStdString();
    node["lower bound"] = QString::number(m_lowerBound).toStdString();
    node["upper bound"] = QString::number(m_upperBound).toStdString();
    node["default value"] = QString::number(m_defaultValue).toStdString();
    node["quantity"] = QString::number(quantity()).toStdString();
    return node;
}

void ContinuousVariableDefinition::fromYamlNode(YAML::Node _node)
{
    if(_node["name"])
    {
        setName(QString::fromUtf8(_node["name"].as<std::string>().c_str()));
    }
    if(_node["lower bound"])
    {
        m_lowerBound = QString::fromUtf8(_node["lower bound"].as<std::string>().c_str()).toDouble();
    }
    if(_node["upper bound"])
    {
        m_upperBound = QString::fromUtf8(_node["upper bound"].as<std::string>().c_str()).toDouble();
    }
    if(_node["default value"])
    {
        m_defaultValue = QString::fromUtf8(_node["default value"].as<std::string>().c_str()).toDouble();
    }
    if(_node["quantity"])
    {
        setQuantity( QString::fromUtf8(_node["quantity"].as<std::string>().c_str()).toInt() );
    }
}
