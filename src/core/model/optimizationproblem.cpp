#include "optimizationproblem.h"




OptimizationProblem::OptimizationProblem(QObject* /*_parent*/, const QString &n, const QString &d, EvaluatorDefinition* eval, QVector<VariableDefinition*> vars) :
m_id(""),
m_name(n), 
m_description(d), 
m_evaluatorDefinition(eval), 
m_variables(vars)
{
    
}

YAML::Node OptimizationProblem::toYamlNode(void )
{
    YAML::Node node;
    node["name"] = m_name.toStdString();
    node["description"] = m_description.toStdString();
    node["fitness target"] = QString::number(m_fitnessTarget, 'e', 3).toStdString();
    node["id"] = m_id.toStdString();
    
    node["evaluator"] = m_evaluatorDefinition->toYamlNode();
            
    for(auto& var : m_variables)
    {
        node["variables"].push_back(var->toYamlNode());
    }
    return node;
}

void OptimizationProblem::fromYamlNode(YAML::Node _node)
{
    if(_node["name"])
    {
        m_name = QString::fromUtf8(_node["name"].as<std::string>().c_str());
    }
    if(_node["description"])
    {
        m_description = QString::fromUtf8(_node["description"].as<std::string>().c_str());
    }
    if (_node["fitness target"])
    {
        m_fitnessTarget = QString::fromUtf8(_node["fitness target"].as<std::string>().c_str()).toDouble();
    }
    if (_node["id"])
    {
        m_id = QString::fromUtf8(_node["id"].as<std::string>().c_str());
    }
    if(_node["evaluator"])
    {
        if(_node["evaluator"]["type"])
        {
            QString typeString = QString::fromUtf8(_node["evaluator"]["type"].as<std::string>().c_str());
            delete m_evaluatorDefinition;
            if(typeString == EvaluatorDefinition::enumToString(EvaluatorDefinition::builtIn))
            {
                m_evaluatorDefinition = new BuiltInEvaluatorDefinition();
                m_evaluatorDefinition->fromYamlNode(_node["evaluator"]);
            }
            else if(typeString == EvaluatorDefinition::enumToString(EvaluatorDefinition::python))
            {
                m_evaluatorDefinition = new PythonEvaluatorDefinition();
                m_evaluatorDefinition->fromYamlNode(_node["evaluator"]);
            }                
        }
    }
    if(_node["variables"])
    {
        for(uint iVar=0; iVar<_node["variables"].size(); iVar++)
        {
            if(_node["variables"][iVar]["type"])
            {
                QString typeString = QString::fromUtf8(_node["variables"][iVar]["type"].as<std::string>().c_str());
                if(typeString == VariableDefinition::enumToString(VariableDefinition::continuous))
                {
                    ContinuousVariableDefinition* var = new ContinuousVariableDefinition;
                    var->fromYamlNode(_node["variables"][iVar]);
                    m_variables.append(var);
                }
                else if(typeString == VariableDefinition::enumToString(VariableDefinition::discrete))
                {
                    /*
                    std::shared_ptr<DiscreteVariableDefinition> var = std::make_shared<ContinuousVariableDefinition>() ;
                    var->fromYamlNode(_node["variables"][iVar]);
                    m_variables.append(var);
                    */
                }
            }
        }
    }      
}

size_t OptimizationProblem::numberOfVariableDefinitions() const
{
    return m_variables.count();
}

void OptimizationProblem::addVariableDefinition(VariableDefinition* v)
{
    m_variables.push_back(v);
}

QString OptimizationProblem::variableDefinitionNameAt(const int& _at)
{
    assert(_at<m_variables.size());
    return m_variables[_at]->name();
}

void OptimizationProblem::changeVariableDefinitionType(int _index, VariableDefinition::VariableType _type)
{
    if(m_variables[_index]->type() != _type)
    {
        QString varName = m_variables[_index]->name();
        int varCount = m_variables[_index]->quantity();
        delete m_variables[_index];
        if(_type == VariableDefinition::VariableType::continuous)
            m_variables[_index] = new ContinuousVariableDefinition(varName, 0.0, 1.0, 0.5, varCount);
        
        if(_type == VariableDefinition::VariableType::discrete)
            m_variables[_index] = new DiscreteVariableDefinition(varName, varCount);
        
    }
}

VariableDefinition* OptimizationProblem::variableDefinition(int _index)
{
    return m_variables[_index];
}

void OptimizationProblem::removeVariableDefinition(int _index)
{
    assert(_index < m_variables.size());
    
    delete m_variables[_index];
    m_variables.remove(_index);
}

