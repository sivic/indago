#include "jobdefinition.h"
//#include "optimizerdefinition.h"
#include "projectdata.h"
#include <QFile>
#include <QDebug>

JobDefinition::JobDefinition(/*const EvaluatorType t = EvaluatorTypeEnum::builtIn*/)//: m_type(t)
{
    m_numberOfSamples = 1;
}

YAML::Node JobDefinition::toYamlNode(void)
{
    YAML::Node node;
    node["name"] = m_name.toStdString();
    node["description"] = m_description.toStdString();
    node["id"] = m_id.toStdString();
    node["number of samples"] = QString::number(m_numberOfSamples).toStdString();
    
    for (int iOpt = 0; iOpt < m_optimizersIds.count(); iOpt++)
    {
        node["optimizers"][iOpt] = m_optimizersIds[iOpt].toStdString();
    }
    
    for (int iProb = 0; iProb < m_problemsIds.count(); iProb++)
    {
        node["problems"][iProb] = m_problemsIds[iProb].toStdString();
    }
      
    
    /*
    node["method"] = enumToString(m_method).toStdString();
    
    QVector< ParameterDefinition*> paramsDefs = parametersDefinitions(method());
    for(int iParam=0; iParam<m_parameters.count(); iParam++)
    {
        QString sValue = m_parameters[iParam].toString();
        node["parameters"][paramsDefs[iParam]->name().toStdString()] = sValue.toStdString();    
    }
    */
    return node;
}


void JobDefinition::fromYamlNode(YAML::Node _node, ProjectData *_pr)
{
    if (_node["name"])
    {
        m_name = QString::fromUtf8(_node["name"].as<std::string>().c_str());
    }
    if (_node["description"])
    {
        m_description = QString::fromUtf8(_node["description"].as<std::string>().c_str());
    }
    if (_node["id"])
    {
        m_id = QString::fromUtf8(_node["id"].as<std::string>().c_str());
    }
    if (_node["number of samples"])
    {
        m_numberOfSamples = QString::fromUtf8(_node["number of samples"].as<std::string>().c_str()).toInt();
    }
    if (_node["optimizers"])
    {
        for (size_t i = 0; i < _node["optimizers"].size(); ++i)
        {
            QString tmp = QString::fromUtf8(_node["optimizers"][i].as<std::string>().c_str());
            //qDebug() << "Optimizer ==> " << tmp;
            addOptimizer(tmp, _pr);
        }
    }
    if (_node["problems"])
    {
        for (size_t i = 0; i < _node["problems"].size(); ++i)
        {
            QString tmp = QString::fromUtf8(_node["problems"][i].as<std::string>().c_str());
            //qDebug() << "Problem ==> " << tmp;
            addProblem(tmp, _pr);
        }
    }

}

QVector<QString> JobDefinition::optimizersIds()
{
    return m_optimizersIds;
}

QVector<QString> JobDefinition::problemsIds()
{
    return m_problemsIds;
}

bool JobDefinition::addOptimizer(const QString _o, ProjectData *_pr)
{
    if (_pr->optimizerExists(_o))
    {
        m_optimizersIds.append(_o);
        return true;
    }
    return false;
}

bool JobDefinition::addProblem(const QString _p, ProjectData *_pr)
{
    if (_pr->optimizationProblemExists(_p))
    {
        m_problemsIds.append(_p);
        return true;
    }
    return false;
}

void JobDefinition::removeOptimizer(const QString _id)
{
    for(int i=0; i<(int)numberOfOptimizers();i++)
    {
        if(m_optimizersIds[i] == _id)
        {
            m_optimizersIds.remove(i);
            break;
        }
    }
}

void JobDefinition::removeProblem(const QString _id)
{
    for(int i=0; i<(int)numberOfProblems();i++)
    {
        if(m_problemsIds[i] == _id)
        {
            m_problemsIds.remove(i);
            break;
        }
    }
}

bool JobDefinition::changeOptimizerId(QString _oldId, QString _newId)
{
    for (QString &element : m_optimizersIds)
    {
        if (element == _oldId)
        {
            element = _newId;
            return true;
        }
    }
    return false;
}

bool JobDefinition::changeProblemId(QString _oldId, QString _newId)
{
    for (QString &element : m_problemsIds)
    {
        if (element == _oldId)
        {
            element = _newId;
            return true;
        }
    }
    return false;
}

int JobDefinition::numberOfSamples(void)
{
    return m_numberOfSamples;
}
void JobDefinition::setNumberOfSamples(int _numberOfSamples)
{
    assert(_numberOfSamples>0);
    m_numberOfSamples = _numberOfSamples;
}

