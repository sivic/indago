#ifndef VARIABLEDEFINITION_H
#define VARIABLEDEFINITION_H

#include "yaml-cpp/yaml.h"
#include <QString>
#include <QObject>
#include "enum.h"


class VariableDefinition: public QObject
{
    Q_OBJECT
    Q_ENUMS(VariableType)
    
    
public:
    /**
    * @brief Enum for variable type.
    */
    enum VariableType
    {
        continuous,
        discrete
    };
    ENUM_TO_STRING(VariableDefinition, VariableType)
    STRING_TO_ENUM(VariableDefinition, VariableType)
    ENUM_COUNT(VariableDefinition, VariableType)
    ENUM_TO_STRINGLIST(VariableDefinition, VariableType)
    
    VariableDefinition(VariableType t);
    VariableDefinition(QString n, VariableType t, int _quantity=0);
    QString name() const{return m_name;}
    void setName(const QString& n){m_name = n;}
    VariableType type() const{return m_type;}
    void setQuantity(const int& val){m_quantity = val;}
    double quantity()const{return m_quantity;}

private:
    QString m_name;
    VariableType m_type;   
    int m_quantity;
public:
    virtual YAML::Node toYamlNode(void) = 0;
    virtual void fromYamlNode(YAML::Node _node) = 0;
};

#endif // VARIABLEDEFINITION_H
