#ifndef OPTIMIZATIONPROBLEM_H
#define OPTIMIZATIONPROBLEM_H

#include <QString>
#include <QVector>
#include <QObject>
#include "yaml-cpp/yaml.h"
#include "evaluatordefinition.h"
#include "builtinevaluatordefinition.h"
#include "pythonevaluatordefinition.h"
#include "variabledefinition.h"
#include "continuousvariabledefinition.h"
#include "discretevariabledefinition.h"


class OptimizationProblem: public QObject
{
    Q_OBJECT
    
public:
    OptimizationProblem(QObject* _parent, const QString &n = "", const QString &d = "", EvaluatorDefinition* eval = new BuiltInEvaluatorDefinition(),
    QVector<VariableDefinition*> vars = QVector<VariableDefinition*>());
    YAML::Node toYamlNode(void);
    void fromYamlNode(YAML::Node _node);

    QString name() const { return m_name; }
    void setName(const QString& _n) { m_name = _n; }

    QString description() const { return m_description; }
    void setDescription(const QString& _d) { m_description = _d; }

    double fitnessTarget() const { return m_fitnessTarget; }
    void setFitnessTarget(const double& _d) { m_fitnessTarget = _d; }
    
    QString id() const { return m_id; }

    void setEvaluatorDefinition(EvaluatorDefinition* _evaluatorDefinition){m_evaluatorDefinition=_evaluatorDefinition;}
    bool hasEvaluatorDefinition() const {return m_evaluatorDefinition ? true : false;}
    EvaluatorDefinition::EvaluatorType evaluatorDefinitionType() const{return m_evaluatorDefinition->type();}
    EvaluatorDefinition* evaluatorDefinition() {return m_evaluatorDefinition;}
    
    /*!
     * \brief Returns number of variables.
     */
    size_t numberOfVariableDefinitions()const;
    
    /*!
     * \brief Adds a variable definition to optimization problem.
     */
    void addVariableDefinition(VariableDefinition* v);
    
    /*!
     * \brief Access a variable definition name at given index.
     */
    QString variableDefinitionNameAt(const int& _at);
    
    /*!
     * \brief Access a variable definition at given index.
     */
    VariableDefinition* variableDefinition(int _index);
    
    /*!
     * \brief Changes variable definition type.
     */
    void changeVariableDefinitionType(int _index, VariableDefinition::VariableType _type);
    
    /*!
     * \brief Removes variable definition at given index.
     */
    void removeVariableDefinition(int _index);
    
private:
    QString m_id;
    QString m_name;
    QString m_description;
    
    double m_fitnessTarget;
    
    EvaluatorDefinition* m_evaluatorDefinition;
    QVector<VariableDefinition*> m_variables;
    
    void setId(const QString& _id) { m_id = _id; }
    
    
    friend class ProjectData;
};

#endif // OPTIMIZATIONPROBLEM_H
