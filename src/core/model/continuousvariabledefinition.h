#ifndef CONTINUOUSVARIABLEDEFINITION_H
#define CONTINUOUSVARIABLEDEFINITION_H

#include "yaml-cpp/yaml.h"
#include "variabledefinition.h"

//continuous
class ContinuousVariableDefinition: public VariableDefinition
{
public:
    ContinuousVariableDefinition();
    ContinuousVariableDefinition(const QString n, const double lb, const double ub, const double dv, int _quantity = 1);
    
    YAML::Node toYamlNode(void);
    void fromYamlNode(YAML::Node _node);
    void setLowerBound(const double& val){m_lowerBound = val;}
    void setUpperBound(const double& val){m_upperBound = val;}
    void setDefaultValue(const double& val){m_defaultValue = val;}
    double lowerBound()const{return m_lowerBound;}
    double upperBound()const{return m_upperBound;}
    double defaultValue()const{return m_defaultValue;}
private:
    double m_lowerBound;
    double m_upperBound;
    double m_defaultValue; 
};

#endif // CONTINUOUSVARIABLEDEFINITION_H
