#include "builtinevaluatordefinition.h"


BuiltInEvaluatorDefinition::BuiltInEvaluatorDefinition(TestFunction f) : 
EvaluatorDefinition(EvaluatorType::builtIn),m_function(f)
{
}

YAML::Node BuiltInEvaluatorDefinition::toYamlNode(void )
{
    YAML::Node node;
    node["type"] = EvaluatorDefinition::enumToString(type()).toStdString();
    node["test function"] = enumToString(function()).toStdString();
    return node;
}

void BuiltInEvaluatorDefinition::fromYamlNode(YAML::Node _node)
{    
    if(_node["test function"])
    {
        QString funstring = QString::fromUtf8(_node["test function"].as<std::string>().c_str());
        m_function = stringToEnumTestFunction(funstring);
    }
}
