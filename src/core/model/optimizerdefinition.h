#ifndef OPTIMIZERDEFINITION_H
#define OPTIMIZERDEFINITION_H

#include "enum.h"
#include "parameterdefinition.h"
#include "stoppingconditiondefinition.h"

#include "yaml-cpp/yaml.h"
#include <QHash>
#include <QObject>
#include <QVariant>
#include <QVector>

#include <QDebug>

/*! 
 * \brief Optimization method definition 
 */
class OptimizerDefinition: public QObject
{
    Q_OBJECT
    Q_ENUMS(Method)
    Q_ENUMS(PatternSearchParameter)
    Q_ENUMS(PsoParameters)
    Q_ENUMS(GeneticAlgorithmParameters)
   
public: 
    OptimizerDefinition(QObject* _parent);
    YAML::Node toYamlNode(void);
    void fromYamlNode(YAML::Node _node);
    
    enum Method
    {
        PatternSearch,
        PSO,
        GeneticAlgorithm,
    };
    ENUM_TO_STRING(OptimizerDefinition, Method)
    STRING_TO_ENUM(OptimizerDefinition, Method)
    ENUM_COUNT(OptimizerDefinition, Method)
    ENUM_TO_STRINGLIST(OptimizerDefinition, Method)
    
    
    enum PatternSearchParameter
    {
        startPoint,
        rho,
        epsilon,
        maxIterations
    };
    ENUM_TO_STRING(OptimizerDefinition, PatternSearchParameter)
    STRING_TO_ENUM(OptimizerDefinition, PatternSearchParameter)
    ENUM_COUNT(OptimizerDefinition, PatternSearchParameter)
    ENUM_TO_STRINGLIST(OptimizerDefinition, PatternSearchParameter)
    
    enum PsoParameters
    {
        SwarmSize,
        InertiaFactor,
        CognitiveFactor,
        SocialFactor,
        Topology,
        PFIDI
    };
    ENUM_TO_STRING(OptimizerDefinition, PsoParameters)
    STRING_TO_ENUM(OptimizerDefinition, PsoParameters)
    ENUM_COUNT(OptimizerDefinition, PsoParameters)
    ENUM_TO_STRINGLIST(OptimizerDefinition, PsoParameters)
    
    enum GeneticAlgorithmParameters
    {
        PopulationSize,
        NoOfBits,
        CrossoverRate,
        MutationRate,
        TournamentSize,
        Alpha,
        Elitism,
        EliminateDuplicates,
    };
    ENUM_TO_STRING(OptimizerDefinition, GeneticAlgorithmParameters)
    STRING_TO_ENUM(OptimizerDefinition, GeneticAlgorithmParameters)
    ENUM_COUNT(OptimizerDefinition, GeneticAlgorithmParameters)
    ENUM_TO_STRINGLIST(OptimizerDefinition, GeneticAlgorithmParameters)
    
    
    QString name(void){return m_name;}
    void setName(QString _name){m_name = _name;}
    QString description(void){return m_description;}
    void setDescription(QString _description){m_description = _description;}
    QString id(void){return m_id;}
    void setId(QString _id){m_id = _id;}
    Method method(void){return m_method;}
    void setMethod(Method _method);
    
    QVariant parameter(int _index){return m_parameters[_index];};
    void setParameter(int _index, QVariant _value);
    
    static QVector<ParameterDefinition*> parametersDefinitions(Method _method);
    
    size_t numberOfStoppingConditions(void){return m_stoppingConditions.size();};
    StoppingConditionDefinition* stoppingCondition(int _index){return m_stoppingConditions[_index];};
private:
    QString m_name;
    QString m_description;
    QString m_id;
    Method m_method;
    
    
    QVector<QVariant> m_parameters;
    QVector<StoppingConditionDefinition*> m_stoppingConditions;
};

#endif //OPTIMIZERDEFINITION_H
