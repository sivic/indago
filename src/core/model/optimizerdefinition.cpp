#include "optimizerdefinition.h"


OptimizerDefinition::OptimizerDefinition(QObject* _parent): QObject(_parent)
{
    for(int iStpCond=0; iStpCond<StoppingConditionDefinition::enumStoppingConditionCount(); iStpCond++)
    {
        m_stoppingConditions.append(new StoppingConditionDefinition(StoppingConditionDefinition::StoppingCondition(iStpCond)));
    }
}


YAML::Node OptimizerDefinition::toYamlNode(void)
{
    YAML::Node node;
    node["name"] = m_name.toStdString();
    node["description"] = m_description.toStdString();
    node["id"] = m_id.toStdString();
    node["method"] = enumToString(m_method).toStdString();
    
    QVector< ParameterDefinition*> paramsDefs = parametersDefinitions(method());
    for(int iParam=0; iParam<m_parameters.count(); iParam++)
    {
        if(parametersDefinitions(method())[iParam]->type() == ParameterDefinition::ParameterType::boolean)
        {
            if(m_parameters[iParam].toBool())
                node["parameters"][paramsDefs[iParam]->name().toStdString()] = "yes";
            else
                node["parameters"][paramsDefs[iParam]->name().toStdString()] = "no";  
        }
        else
        {
            QString sValue = m_parameters[iParam].toString();
            node["parameters"][paramsDefs[iParam]->name().toStdString()] = sValue.toStdString();    
        }
    }
    for(uint iStpCond=0; iStpCond<numberOfStoppingConditions(); iStpCond++)
    {
        node["stopping conditions"][stoppingCondition(iStpCond)->name().toStdString()] = stoppingCondition(iStpCond)->toYamlNode();
    }
    
    return node;
}

void OptimizerDefinition::setMethod(OptimizerDefinition::Method _method)
{
    m_method = _method;
    
    QVector< ParameterDefinition*> paramsDefs = parametersDefinitions(method());
    int nDefParams = paramsDefs.count();
    m_parameters.resize(nDefParams);


    for(int iDefParam=0; iDefParam<nDefParams; iDefParam++)
    {
        if(paramsDefs[iDefParam]->type() == ParameterDefinition::ParameterType::decimal)
            m_parameters[iDefParam] = ((DecimalParameterDefinition*)paramsDefs[iDefParam])->defaultValue;
        
        if(paramsDefs[iDefParam]->type() == ParameterDefinition::ParameterType::integer)
            m_parameters[iDefParam] = ((IntegerParameterDefinition*)paramsDefs[iDefParam])->defaultValue;
        
        if(paramsDefs[iDefParam]->type() == ParameterDefinition::ParameterType::boolean)
            m_parameters[iDefParam] = ((BooleanParameterDefinition*)paramsDefs[iDefParam])->defaultValue;
        
        if(paramsDefs[iDefParam]->type() == ParameterDefinition::ParameterType::list)
            m_parameters[iDefParam] = ((ListParameterDefinition*)paramsDefs[iDefParam])->defaultValue;
    }
}


void OptimizerDefinition::fromYamlNode(YAML::Node _node)
{
    if(_node["name"])
    {
        m_name = QString::fromUtf8(_node["name"].as<std::string>().c_str());
    }
    if(_node["description"])
    {
        m_description = QString::fromUtf8(_node["description"].as<std::string>().c_str());
    }
    if (_node["id"])
    {
        m_id = QString::fromUtf8(_node["id"].as<std::string>().c_str());
    }
    if(_node["method"])
    {
        setMethod(stringToEnumMethod(QString::fromUtf8(_node["method"].as<std::string>().c_str())));
    }
    if(_node["parameters"])
    {
        QVector< ParameterDefinition*> paramsDefs = parametersDefinitions(method());
        int nDefParams = paramsDefs.count();

        for(int iDefParam=0; iDefParam<nDefParams; iDefParam++)
        {            
            if(_node["parameters"][paramsDefs[iDefParam]->name().toStdString()])
            {
                //qDebug() << "Parameter " << paramsDefs[iDefParam]->name() << " ==> FOUND";
                
                QString stringValue = QString::fromUtf8(_node["parameters"][paramsDefs[iDefParam]->name().toStdString()].as<std::string>().c_str());
                bool valid = false;
                
                if(paramsDefs[iDefParam]->type() == ParameterDefinition::ParameterType::decimal)
                {
                    double doubleVal = stringValue.toDouble(&valid);
                    if(valid)
                        m_parameters[iDefParam] = doubleVal;
                }
                else if(paramsDefs[iDefParam]->type() == ParameterDefinition::ParameterType::integer)
                {
                    int intVal = stringValue.toInt(&valid);
                    if(valid)
                        m_parameters[iDefParam] = intVal;
                }
                else if(paramsDefs[iDefParam]->type() == ParameterDefinition::ParameterType::boolean)
                {
                    if(stringValue == "yes")
                    {
                        m_parameters[iDefParam] = true;
                        valid = true;
                    }
                    else if(stringValue == "no")
                    {
                        m_parameters[iDefParam] = false;
                        valid = true;
                    }                       
                }
                else if(paramsDefs[iDefParam]->type() == ParameterDefinition::ParameterType::list)
                {
                    m_parameters[iDefParam] = stringValue;
                    valid = true;
                }
                
                /*
                if(!valid)
                {
                    qDebug() << "Parameter " << paramsDefs[iDefParam]->name() << " ==> INVALID VALUE"; 
                }
                */
            }
            /*
            else
            {
                qDebug() << "Parameter " << paramsDefs[iDefParam]->name() << " ==> NOT FOUND";
            }
            */
        }
    }
    if(_node["stopping conditions"])
    {        
        for(int iStpCond=0; iStpCond<StoppingConditionDefinition::enumStoppingConditionCount(); iStpCond++)
        {
            if(_node["stopping conditions"][stoppingCondition(iStpCond)->name().toStdString()])
            {
                stoppingCondition(iStpCond)->fromYamlNode(_node["stopping conditions"][stoppingCondition(iStpCond)->name().toStdString()]);
            }
        }
    }
}

QVector< ParameterDefinition* > OptimizerDefinition::parametersDefinitions(OptimizerDefinition::Method _method)
{
    QVector<ParameterDefinition*> params;
    
    if(_method == Method::PSO)
    {
        int nParams = enumPsoParametersCount();
        params.resize(nParams);
        params[PsoParameters::InertiaFactor] = new DecimalParameterDefinition("w", "inertia factor", 0.0, 2.0, 2, 0.72);
        params[PsoParameters::CognitiveFactor] = new DecimalParameterDefinition("c_c", "cognitive factor", 0.0, 2.0, 2, 1.0);
        params[PsoParameters::SocialFactor] = new DecimalParameterDefinition("c_s", "social factor", 0.0, 2.0, 2, 1.0);
        params[PsoParameters::SwarmSize] = new IntegerParameterDefinition("s", "swarm size", 10, 100, 20);
        
        params[PsoParameters::Topology] = new ListParameterDefinition("topology", "a way of swarm linkage", QVector<QVariant>() << "lbest" << "gbest", 0);
        params[PsoParameters::PFIDI] = new ListParameterDefinition("PFIDI", "Personal fitness improvement dependant inertiality", QVector<QVariant>() << "none" << "lpso" << "gearbox", 0);
    }
    else if(_method == Method::PatternSearch)
    {
        QVector<QVariant> startPoint;
        startPoint.append(QVariant("from default values"));
        startPoint.append(QVariant("random"));
        /*
        params.append(new DecimalParameterDefinition("rho", "rho", 0.0, 2.0, 2, 0.6));
        params.append(new ListParameterDefinition("x0", "start point", startPoint, 0));
        params.append(new DecimalParameterDefinition("eps", "epsilon", 0.0, 2.0, 2, 0.6));
        params.append(new IntegerParameterDefinition("rho", "rho", 0, 10e6, 1000));
        */
        int nParams = enumPatternSearchParameterCount();
        params.resize(nParams);
        params[PatternSearchParameter::rho] = new DecimalParameterDefinition("rho", "rho", 0.0, 2.0, 2, 0.6);
        params[PatternSearchParameter::startPoint] = new ListParameterDefinition("x0", "start point", startPoint, 0);
        params[PatternSearchParameter::epsilon] = new DecimalParameterDefinition("eps", "epsilon", 0.0, 2.0, 9, 0.6);
        params[PatternSearchParameter::maxIterations] = new IntegerParameterDefinition("max_iter", "max_iter", 0, 1e6, 1000);
    }
    else if(_method == Method::GeneticAlgorithm)
    {
        int nParams = enumGeneticAlgorithmParametersCount();
        params.resize(nParams);
        params[GeneticAlgorithmParameters::PopulationSize] = new IntegerParameterDefinition("pop","population size",2,100,30);
        params[GeneticAlgorithmParameters::NoOfBits] = new IntegerParameterDefinition("nob","number of bits",4,64,16);
        params[GeneticAlgorithmParameters::CrossoverRate]=new DecimalParameterDefinition("cr","crossover rate",0.0,1.0,2,0.80);
        params[GeneticAlgorithmParameters::MutationRate]=new DecimalParameterDefinition("mr","mutation rate",0.0,1.0,2,0.6);
        params[GeneticAlgorithmParameters::TournamentSize]=new IntegerParameterDefinition("ts","tournament size",2,100,5);
        params[GeneticAlgorithmParameters::Alpha]=new DecimalParameterDefinition("alpha","DuplicatesStoppingCondition coefficient",0.0,5.0,2,1);
        params[GeneticAlgorithmParameters::Elitism]=new BooleanParameterDefinition("el","elitism",true);
        params[GeneticAlgorithmParameters::EliminateDuplicates]=new BooleanParameterDefinition("ed","eliminate duplicates",true);
      
    }
    
    return params;
}

void OptimizerDefinition::setParameter(int _index, QVariant _value)
{
    m_parameters[_index] = _value;
    
    for(int i=0; i<m_parameters.count(); i++)
        qDebug() << m_parameters[i] << " ";
}


