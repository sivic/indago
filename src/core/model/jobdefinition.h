#ifndef JOBDEFINITION_H
#define JOBDEFINITION_H

//#include "enum.h"
#include "yaml-cpp/yaml.h"
#include <QString>
#include <QVector>
#include <QVariant>
class ProjectData;

class JobDefinition : public QObject
{
    Q_OBJECT
public:
    JobDefinition(/*const EvaluatorType t*/);
    JobDefinition(QObject *_parent) : QObject(_parent) {}
    //EvaluatorType type()const {return  m_type;}

private:
    //const EvaluatorType m_type;
    //QString m_filename;
    QString m_name;
    QString m_description;
    QString m_id;
    QVector<QString> m_optimizersIds;
    QVector<QString> m_problemsIds;
    int m_numberOfSamples;
    bool changeOptimizerId(QString _oldId, QString _newId);
    bool changeProblemId(QString _oldId, QString _newId);
    friend class ProjectData;
    
public:
    YAML::Node toYamlNode(void);
    void fromYamlNode(YAML::Node _node, ProjectData *_pr);
    void setName(QString _n) { m_name = _n; }
    QString name(void) { return m_name; }
    void setDescription(QString _d) { m_description = _d; }
    QString description(void) { return m_description; }
    void setId(QString _id) { m_id = _id; }
    QString id(void) { return m_id; }
    size_t numberOfOptimizers() const { return m_optimizersIds.size(); }
    size_t numberOfProblems() const { return m_problemsIds.size(); }
    QVector<QString> optimizersIds();
    QVector<QString> problemsIds();
    bool addOptimizer(const QString _o, ProjectData *);
    bool addProblem(const QString _p, ProjectData *);
    int numberOfSamples(void);
    void setNumberOfSamples(int _numberOfSamples);
    
    /*!
     * \brief Removes optimizer with given id from list of selected job's optimizers, if given id exists.
     * \param _id is id of optimizer to be removed.
     */
    void removeOptimizer(const QString _id);
    
    /*!
     * \brief Removes problem with given id from list of selected job's problems, if given id exists.
     * \param _id is id of problem to be removed.
     */
    void removeProblem(const QString _id);
};

#endif // JOBDEFINITION_H
