#include "stoppingconditiondefinition.h"

StoppingConditionDefinition::StoppingConditionDefinition(StoppingConditionDefinition::StoppingCondition _stoppingCondition, QObject* parent): QObject(parent)
{
    m_type = _stoppingCondition;
    switch(_stoppingCondition)
    {
        case StoppingCondition::FitnessTarget:
        {
            m_name = "fitness target";
            m_description = "Stop when fitness target is reached";
            setEnabled(true);
            m_parameters.clear();
            break;
        }
        case StoppingCondition::MaximumNumberOfEvaluations:
        {
            m_name = "max evaluations";
            m_description = "Limit the number of problem evaluations";
            setEnabled(true);
            m_parameters.resize(parametersDefinitions(_stoppingCondition).size());
            m_parameters[0] = 1000;
            break;
        }
    }
}


YAML::Node StoppingConditionDefinition::toYamlNode(void)
{
    YAML::Node node;
    //node["name"] = m_name.toStdString();
    //node["description"] = m_description.toStdString();
    if(m_enabled)
        node["enabled"] = "yes";
    else
        node["enabled"] = "no";
    
    
    QVector< ParameterDefinition*> paramsDefs = parametersDefinitions(m_type);
    for(int iParam=0; iParam<m_parameters.count(); iParam++)
    {
        if(paramsDefs[iParam]->type() == ParameterDefinition::ParameterType::boolean)
        {
            if(m_parameters[iParam].toBool())
                node[paramsDefs[iParam]->name().toStdString()] = "yes";
            else
                node[paramsDefs[iParam]->name().toStdString()] = "no";  
        }
        else if(paramsDefs[iParam]->type() == ParameterDefinition::ParameterType::integer)
        {
            node[paramsDefs[iParam]->name().toStdString()] = m_parameters[iParam].toInt();    
        }
    }
    return node;
}

void StoppingConditionDefinition::fromYamlNode(YAML::Node _node)
{
    if(_node["enabled"])
    {
        if(QString::fromUtf8(_node["enabled"].as<std::string>().c_str()) == "yes")
            m_enabled = true;
        else
            m_enabled = false;
    }
    QVector< ParameterDefinition*> paramsDefs = parametersDefinitions(m_type);
    for(int iParam=0; iParam<paramsDefs.count(); iParam++)
    {
        if(_node[paramsDefs[iParam]->name().toStdString()])
        {
            QString stringValue = QString::fromUtf8(_node[paramsDefs[iParam]->name().toStdString()].as<std::string>().c_str());
            if(paramsDefs[iParam]->type() == ParameterDefinition::ParameterType::boolean)
            {
                if(stringValue == "yes")
                    m_parameters[iParam].setValue(true);
                else
                    m_parameters[iParam].setValue(false);
            }
            if(paramsDefs[iParam]->type() == ParameterDefinition::ParameterType::integer)
            {
                m_parameters[iParam].setValue(stringValue.toInt());
            }
        }
    }
}

QVector< ParameterDefinition* > StoppingConditionDefinition::parametersDefinitions(StoppingCondition _stoppingCondition)
{
    QVector<ParameterDefinition*> params;
    
    if(_stoppingCondition == StoppingCondition::FitnessTarget)
    {
        params.clear();
    }
    else if(_stoppingCondition == StoppingCondition::MaximumNumberOfEvaluations)
    {
        params.append(new IntegerParameterDefinition("max_eval", "Maximum number of evaluations", 0, 10e6, 1000));
    }
    
    return params;
}

void StoppingConditionDefinition::setParameter(int _index, QVariant _value)
{
    m_parameters[_index] = _value;
}
