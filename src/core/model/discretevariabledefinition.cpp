#include "discretevariabledefinition.h"

DiscreteVariableDefinition::DiscreteVariableDefinition(): 
VariableDefinition(VariableType::discrete)
{

}

DiscreteVariableDefinition::DiscreteVariableDefinition(const QString n, int _quantity):
VariableDefinition(n, VariableType::discrete, _quantity)
{

}

YAML::Node DiscreteVariableDefinition::toYamlNode(void )
{
    YAML::Node node;
    node["name"] = name().toStdString();
    node["type"] = DiscreteVariableDefinition::enumToString(discrete).toStdString();
    node["quantity"] = QString::number(quantity()).toStdString();
    return node;
}

void DiscreteVariableDefinition::fromYamlNode(YAML::Node _node)
{
    if(_node["name"])
    {
        setName(QString::fromUtf8(_node["name"].as<std::string>().c_str()));
    }
    if(_node["quantity"])
    {
        setQuantity( QString::fromUtf8(_node["quantity"].as<std::string>().c_str()).toInt() );
    }
}
