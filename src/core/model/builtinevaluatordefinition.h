/*
 *
 */

#ifndef BUILTINEVALUATORDEFINITION_H
#define BUILTINEVALUATORDEFINITION_H

#include "evaluatordefinition.h"


class BuiltInEvaluatorDefinition: public EvaluatorDefinition
{
    Q_OBJECT
    Q_ENUMS(TestFunction)
    
public:
    enum TestFunction
    {
        Sphere,
        Rastrigin,
        Rosenbrock,
        Schwefel,
        Alpine,
        Ackley
    };
    ENUM_TO_STRING(BuiltInEvaluatorDefinition, TestFunction)
    STRING_TO_ENUM(BuiltInEvaluatorDefinition, TestFunction)
    ENUM_COUNT(BuiltInEvaluatorDefinition, TestFunction)
    ENUM_TO_STRINGLIST(BuiltInEvaluatorDefinition, TestFunction)
    
    BuiltInEvaluatorDefinition(TestFunction f=TestFunction::Sphere);
    void setFunction(TestFunction f){m_function = f;}
    TestFunction function()const {return m_function;}
    
    
    YAML::Node toYamlNode(void);
    void fromYamlNode(YAML::Node _node);
private:
    TestFunction m_function;
};

#endif // BUILTINEVALUATORDEFINITION_H
