#include "pythonevaluatordefinition.h"

PythonEvaluatorDefinition::PythonEvaluatorDefinition(void) : EvaluatorDefinition(EvaluatorType::python)
{
}

YAML::Node PythonEvaluatorDefinition::toYamlNode(void )
{
    YAML::Node node;
    node["type"] = "python";
    node["file"] = pythonFile.toStdString();
    node["function"] = functionName.toStdString();
    return node;
}

void PythonEvaluatorDefinition::fromYamlNode(YAML::Node _node)
{    
    if(_node["file"])
    {
        pythonFile = QString::fromUtf8(_node["file"].as<std::string>().c_str());
    }
    if(_node["function"])
    {
        functionName = QString::fromUtf8(_node["function"].as<std::string>().c_str());
    }
}
