#include "variabledefinition.h"

VariableDefinition::VariableDefinition(VariableType t=VariableType::continuous): 
QObject(),
m_type(t)
{
}
VariableDefinition::VariableDefinition(QString n, VariableType t=VariableType::continuous, int _quantity): 
m_name(n),
m_type(t),
m_quantity(_quantity)
{
}
