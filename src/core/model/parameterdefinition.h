#ifndef PARAMETERDEFINITION_H
#define PARAMETERDEFINITION_H


#include <QObject>
#include <QVariant>
#include <QVector>


class ParameterDefinition
{
public:
    
    enum ParameterType
    {
        decimal,
        integer,
        boolean,
        list
    };
    
public:
    ParameterType type(){return m_type;};
    QString name(){return m_name;};
    QString description(){return m_description;};
protected:
    ParameterType m_type;
    QString m_name;
    QString m_description;
};

class DecimalParameterDefinition: public ParameterDefinition
{
public:
    DecimalParameterDefinition(QString _name, QString _description, double _lowerBound, double _upperBound, int _decimals, double _defaultValue)
    {
        m_name = _name;
        m_description = _description;
        m_type = ParameterType::decimal;
        lowerBound = _lowerBound; 
        upperBound = _upperBound; 
        decimals = _decimals;
        defaultValue = _defaultValue;        
    }
    double lowerBound;
    double upperBound;
    int decimals;
    double defaultValue;
};

class IntegerParameterDefinition: public ParameterDefinition
{
public:
    IntegerParameterDefinition(QString _name, QString _description, int _lowerBound, int _upperBound, int _defaultValue)
    {
        m_name = _name;
        m_description = _description;
        m_type = ParameterType::integer;
        lowerBound = _lowerBound; 
        upperBound = _upperBound; 
        defaultValue = _defaultValue;
        
    }
    int lowerBound;
    int upperBound;
    int defaultValue;
};

class BooleanParameterDefinition: public ParameterDefinition
{
public:
    BooleanParameterDefinition(QString _name, QString _description, bool _defaultValue)
    {
        m_name = _name;
        m_description = _description;
        m_type = ParameterType::boolean;
        defaultValue = _defaultValue;
        
    }
    bool defaultValue;
};

class ListParameterDefinition: public ParameterDefinition
{
public:
    ListParameterDefinition(QString _name, QString _description, QVector<QVariant> _avaliableValues, int _defaultValue)
    {
        m_name = _name;
        m_description = _description;
        m_type = ParameterType::list;
        avaliableValues = _avaliableValues;
        defaultValue = _defaultValue;
    }
    QVector<QVariant> avaliableValues;
    int defaultValue;
};


#endif //OPTIMIZERDEFINITION_H