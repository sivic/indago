#include "simplerun.h"
#include "optimizer.h"
#include <QDebug>

SimpleRun::SimpleRun(QString _optimizerId, QString _problemId):
m_stop(false)
{
    m_optimizerId = _optimizerId;
    m_problemId = _problemId;
    setAutoDelete(false);
}

SimpleRun::~SimpleRun()
{
    for(Optimizer* opt: m_optimizers)
        delete opt;
    
    m_optimizers.clear();
}


void SimpleRun::run()
{
    OptimizerStatistics stats;
    
    for (Optimizer* opt : m_optimizers)
    {
        if(m_stop) break;
        OptimizerSampleResults res = opt->optimize();
        stats.addSample(res);
        emit progress();
    }
            
    emit finished(stats, m_optimizerId, m_problemId);
}

void SimpleRun::addOptimizer(Optimizer *_op)
{
    m_optimizers.append(_op);
}

void SimpleRun::stop(void)
{
    m_stop = true;
}
