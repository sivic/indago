#ifndef JOBMANAGER_H
#define JOBMANAGER_H

#include <QVector>
#include <QThreadPool>
#include <QTime>
#include "optimizerresults.h"

class ProjectData;
class SimpleRun;
class OptimizerDefinition;
class OptimizationProblem;


/*!
 * \brief A class that manages job execution.
 */
class JobManager : public QThread
{
    Q_OBJECT

signals:
    void status(int _status);
//    void finished();
    
    /*!
     * \brief Signal used to report current progress of a job.
     * \param _progress percentage of concluded tasks.
     */
    void progress(int _progress);
    
    /*!
     * \brief Signal used to send a log message.
     */
    void appendToLog(QString _message);

public:
    /*!
     * \brief JobManager constructor
     */
    JobManager();
    
    /*!
     * \brief JobManager destructor
     */
    ~JobManager();
    
    /*!
     * \brief Make simple runs from job.
     * \arg _project is a pointer to ProjectData and _job is an ID of job
     * \return void
     */
    void addJob(ProjectData *_project, int _job);
    
    /*!
     * \brief Start previously scheduled job.
     * \arg no arguments
     * \return void
     */
    void run(void);
    
    /*!
     * \brief Get current status of job processing
     * \arg no args
     * \return Percentage of status
     */
    int status() {return m_status;}
    
    
private slots:    
    //void onTaskFinished(void);
    void onTaskFinished(OptimizerStatistics stats, QString _optimizerId, QString _problemId);
    
    void onTaskProgress(void);
    
public slots:
    /*!
     * \brief Stops all tasks and finally the job itself.
     */
    void stopRequested(void);
    
private:
    int m_numberOfFinishedTasks;
    int m_numberOfFinishedSamples;
    int m_numberOfSamples;
    QVector<SimpleRun*> m_simpleruns;
    int m_status;
    
    /*!
     * \brief QTime object used for measuring job execution time.
     */
    QTime m_timer;
};
#endif
