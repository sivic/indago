#ifndef QUICKALGORITHM_H
#define QUICKALGORITHM_H

#include "paretoalgorithm.h"

/*! \brief klasa za računanje pareto frontier
 *
 * Nasljeđuje public od abstraktne klase i implementira
 * computeParetoFrontier metodu
 *
 */
class quickAlgorithm : public ParetoAlgorithm
{
  virtual int computeParetoFrontier(QVector<Datapoint*>&);
};

#endif // QUICKALGORITHM_H
