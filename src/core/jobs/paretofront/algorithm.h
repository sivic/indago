#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "paretoalgorithm.h"
#include "datapoint.h"
#include <QVector>

/*! \brief klasa za računanje pareto frontier
 *
 * Nasljeđuje public od abstraktne klase i implementira
 * computeParetoFrontier metodu
 *
 */
class Algorithm : public ParetoAlgorithm
{
  virtual int computeParetoFrontier(QVector<Datapoint*>&);
};

#endif // ALGORITHM_H
