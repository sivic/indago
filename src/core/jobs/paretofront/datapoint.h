#ifndef DATAPOINT_H
#define DATAPOINT_H

#include <QVector>
#include <iostream>
#include <functional>
#include <stdlib.h>

/*! \brief klasa za N K-dim točke
 *
 * Hold method for set pareto status (0 -> not pareto, 1-> pareto,
 * and for example, -1 -> don't know)
 * For now, only 2D points
 *
 */
class Datapoint
{
  /*Defines domination in k-dimension. For this reason, i need to redefine
   relation operators( '>' and '<'). I will compare two object of Datapoint class*/
  friend bool operator<(const Datapoint&, const Datapoint&);
  friend bool operator>(const Datapoint&, const Datapoint&);
  
public:
  Datapoint(size_t id);
  void setParetoStatus(int);
  int getParetoStaus() const;
  void printResult() const;
  void addNumbers(double);
  size_t vecSize() const;
  double getNumberAtDim(size_t d) const;
  void incrementDominatedPoint();
  void decrementDominatedPoint();
  int numOfDominated() const; //returns number of dominated point
  void dumpToFile() const;
private:
  //private data members
  size_t id;
  int m_paretostatus;
  QVector<double> coordinates;
  size_t dominatedPoint; //točke koje dominiraju - broj točaka
};

#endif // DATAPOINT_H
