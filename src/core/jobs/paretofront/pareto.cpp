#include "pareto.h"
#include "datapoint.h"
//#include "algorithm.h"
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

pareto::pareto() : Kdim(0)
{

}

void pareto::setFilePath(string file)
{
  files.push_back(file);
}

string pareto::getFilePath(int i) const
{
  return files[i];
}

void pareto::setDim(int dim)
{
  Kdim = dim;
}

int pareto::getDim() const
{
  return Kdim;
}

vector< string >::const_iterator pareto::beginVecFile()
{
  return files.begin();
}

vector< string >::const_iterator pareto::endVecFile()
{
  return files.end();
}
