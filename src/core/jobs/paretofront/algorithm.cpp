#include <stdlib.h>
#include <iostream>
#include <iterator>
#include <QVector>
#include "algorithm.h"
#include "datapoint.h"

using namespace std;

int Algorithm::computeParetoFrontier(QVector<Datapoint*>& dataset)
{
  //varijabla koja će brojati pareto optimal solucija, pr.: 8 pareto od max. 20 točaka
  int paretoOptimal = 0;
  
  /* After research on the Internet, i found a way how to solve this -> Redefine/overload relational operator.
   * For now, i use classic for loop, because in this way work, when redefining relational operator ( '>' and '<' ) */
  
  for(int i = 0; i < dataset.size(); ++i)
  {
    for(int j=0; j < dataset.size(); ++j)
    {
      if(*dataset[i] < *dataset[j]) //*dataset[i] < *dataset[j]
      {
	//increment dominated number
	dataset[i]->incrementDominatedPoint();
      }
    }
  }
  //sada za svaki dataset[iter_x] provjeriti ako je nonDominated (efficient)
  //ako je, postavi pareto status u 1 -> znači da je pareto optimalna
  for(int i = 0; i < dataset.size(); ++i)
  {
    //if this point is non-dominated, set pareto status -> 1. This point belong to pareto front
    if( dataset[i] -> numOfDominated() == 0 ){
	dataset[i] -> setParetoStatus(1); //because, 1 is pareto, 0 - not pareto
	paretoOptimal++;//and increment number of points which are pareto
    }else{
      //just set paretoStatus to 0
      dataset[i] -> setParetoStatus(0);
    }
  }
  // return # pareto points
  return paretoOptimal;
}