#include <iostream>
#include <QVector>
#include "quickalgorithm.h"
#include "datapoint.h"

using namespace std;

int quickAlgorithm::computeParetoFrontier(QVector< Datapoint* >& dataset )
{
  //copy original dataset - keep original dataset in original order
  QVector<Datapoint*> copyOfDataset = dataset;
  //qCopy(dataset.begin(), dataset.end(), copyOfDataset.begin());
  
  //idea is to sort by first dimension - found this solution after some research - this work only with C++11 if I'm wright ? :)
  sort(copyOfDataset.begin(), copyOfDataset.end(), [](const Datapoint* a, const Datapoint* b){ return a -> getNumberAtDim(0) > b -> getNumberAtDim(0); });
  
  //if data is sorted, set pareto point to 1 -> because first one shourly belongs pareto fronti
  int paretoOptimal=1;
  copyOfDataset[0] -> setParetoStatus(1); //this one belong to pareto front
  
  Datapoint *current = copyOfDataset[0];
  //iterate through all dataset
  for(int d = 1; d < copyOfDataset.size(); ++d){
    //if current points is dominated by other points, this one is not pareto
    if(*copyOfDataset[d] < *current){
      copyOfDataset[d] -> setParetoStatus(0);
    }
    else{
      //otherwise
      copyOfDataset[d] -> setParetoStatus(1);
      paretoOptimal++;
      current = copyOfDataset[d];
    }
  }
  
  return paretoOptimal;
}


