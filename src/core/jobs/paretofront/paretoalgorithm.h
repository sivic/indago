#ifndef PARETOALGORITHM_H
#define PARETOALGORITHM_H

#include "datapoint.h"

/*! \brief Abstraktna bazna klasa za pareto frontier algorithm
 *
 * Sadrži pure virtual funkciju za računanje pareto frontier
 *
 */
class ParetoAlgorithm
{
public:
  virtual int computeParetoFrontier(QVector<Datapoint*>&) = 0;
  virtual ~ParetoAlgorithm(){}
};

#endif // PARETOALGORITHM_H
