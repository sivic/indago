#ifndef PARETO_H
#define PARETO_H

#include "datapoint.h"
//#include "algorithm.h"

using namespace std;

/*! \brief klasa primarno za tests
 *
 * Hold vector of file paths to the data point
 * and variable for N-dimension problem
 *
 */
class pareto
{
public:
  pareto();
  void setFilePath(string);
  string getFilePath(int) const;
  void setDim(int);
  int getDim() const;
  vector<string>::const_iterator beginVecFile();
  vector<string>::const_iterator endVecFile();
private:
  vector<string> files;
  int Kdim;
};

#endif // PARETO_H
