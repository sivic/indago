#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <QVector>
#include <iterator>
#include <functional>
#include "datapoint.h"
#include "pathconfig.h"

using namespace std;

Datapoint::Datapoint(size_t id) : id(id), m_paretostatus(-1), dominatedPoint(0)
{
}

void Datapoint::setParetoStatus(int m_status)
{
  m_paretostatus = m_status;
}


int Datapoint::getParetoStaus() const
{
  return m_paretostatus;
}

void Datapoint::printResult() const
{
  cout << id << " " << m_paretostatus << " : ";
  for(QVector<double>::const_iterator it = coordinates.begin(); it != coordinates.end(); ++it){
    cout << *it << "  ";
  }
  cout << endl;
}

void Datapoint::addNumbers(double num)
{
  coordinates.push_back(num);
}

size_t Datapoint::vecSize() const
{
  return coordinates.size();
}

double Datapoint::getNumberAtDim(size_t d) const
{
  return coordinates[d];
}

void Datapoint::incrementDominatedPoint()
{
  dominatedPoint++;
}

void Datapoint::decrementDominatedPoint()
{
  dominatedPoint--;
}

int Datapoint::numOfDominated() const
{
  return dominatedPoint;
}

void Datapoint::dumpToFile() const
{
  ofstream outfile;
  outfile.open(filePathBuilder::testDataPath("dumpFile").toStdString(), std::ofstream::out | std::ofstream::app);
  
  if( !outfile ){
     cerr << "Can not open/find any file." << endl;
     exit(1);
  }
  for(QVector<double>::const_iterator it = coordinates.begin(); it != coordinates.end(); ++it){
    outfile << *it << " ";
  }
  outfile << "\n";
}

/*From the C++ Primer (fourth edition) book, is best to define like 
  non-member function. For that reason i define operator using friend
  and there is no need for scope-'::' operator */
bool operator<(const Datapoint& lhs, const Datapoint& rhs)
{
  /* From pareto definition: If A[i]<=B[i] for all i and A[i] < B[i] for at least one i, 
   * we say: A is dominated by B*/
  
  size_t le = 0; //hold lhs[i] <= rhs[i]
  size_t l = 0; //hold lhs[i] < rhs[i]
  size_t dimension = lhs.vecSize();
  for(size_t i = 0; i < dimension; ++i){
    if( lhs.coordinates[i] <= rhs.coordinates[i] ){
      le++;
    }
    if(lhs.coordinates[i] < rhs.coordinates[i]){
      l++;
    }
  }
  return ((le == dimension) && (l > 0)); //jer 'le' treba biti jednak onom broju koliko je velicina vektora -> A[i] < B[i] za sve i, a 'l' treba biti > 0, jer 
					     //barem jedan A[i] mora biti < B[i]
}

bool operator>(const Datapoint& lhs, const Datapoint& rhs)
{
   /* From pareto definition: If A[i]>=B[i] for all i and A[i] > B[i] for at least one i , 
   * we say: A is dominated by B*/
  size_t ge = 0; //greather or equal
  size_t g = 0;
  size_t dimension = lhs.vecSize();
  for(size_t i=0; i < dimension; ++i){
    if(lhs.coordinates[i] >= rhs.coordinates[i]){
      ge++;
    }
    if(lhs.coordinates[i] > rhs.coordinates[i]){
      g++;
    }
  }
  return ( (ge == dimension) && (g > 0) );
}