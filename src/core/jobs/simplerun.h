#ifndef SIMPLERUN_H
#define SIMPLERUN_H 

#include <QRunnable>
#include <QObject>
//#include <QMutex>

#include "optimizerresults.h"

class Optimizer;

class SimpleRun : public QObject, public QRunnable
{
    Q_OBJECT
private:
    
    QVector<Optimizer*> m_optimizers;
    QString m_optimizerId;
    QString m_problemId;
    //QMutex m_mutex;
    bool m_stop;
    
public:
    /*!
     * \brief SimpleRun constructor
     */
    SimpleRun(QString _optimizerId, QString _problemId);
    
    /*!
     * \brief Simple run destructor
     */
    ~SimpleRun();
    
    
    void addOptimizer(Optimizer *_op);
    void run();
    
signals:
    //void finished(void);
    void finished(OptimizerStatistics, QString, QString);
    void progress(void);
    
public slots:
    void stop(void);
};
#endif //SIMPLERUN_H 
