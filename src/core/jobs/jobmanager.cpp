#include "jobmanager.h"
#include "projectdata.h"
#include "simplerun.h"
#include "optimizerfactory.h"
#include <qvarlengtharray.h>

JobManager::JobManager()
{
    m_status = 0;
    qRegisterMetaType<OptimizerStatistics>("OptimizerStatistics");
}

JobManager::~JobManager()
{
    //QThread instances are autodestruced (by garbage collector)
    
    for (SimpleRun* task : m_simpleruns)
    {
        if(task)
            delete task;
    }
    
    m_simpleruns.clear();
}


void JobManager::addJob(ProjectData *_project, int _job)
{
    JobDefinition *job = _project->job(_job);

    for (QString optId : job->optimizersIds())
    {
        for (QString prbId : job->problemsIds())
        {
            SimpleRun *task = new SimpleRun(optId, prbId);
            
            for(int iSample = 0; iSample < job->numberOfSamples(); iSample++)
            {
                Optimizer *opt = OptimizerFactory::createOptimizer(_project->optimizer(optId));
                opt->setOptimizationProblem(_project->optimizationProblem(prbId));
                task->addOptimizer(opt);
            }
            
            connect(task, SIGNAL(finished(OptimizerStatistics, QString, QString)), this, SLOT(onTaskFinished(OptimizerStatistics, QString, QString)));  
            connect(task, SIGNAL(progress(void)), this, SLOT(onTaskProgress(void)));    
            m_simpleruns.append(task);
        }
    }
    m_numberOfSamples = job->numberOfSamples();
}

void JobManager::run(void)
{
    qDebug() << "void JobManager::run(void)";
    m_timer.start();
    m_numberOfFinishedTasks = 0;
    m_numberOfFinishedSamples = 0;
    emit progress(0);
    QString str;
    str.sprintf("%7s %10s %10s %10s %10s %10s %10s %10s %10s", 
                "task",
                "optimizer",
                "problem",
                "min",
                "avg",
                "max",
                "succes",
                "evals",
                "median"
               );
    emit appendToLog(str);
    
    for (SimpleRun *task : m_simpleruns)
    {
        QThreadPool::globalInstance()->start(task);
    }
    QThreadPool::globalInstance()->waitForDone();
    
    exit();
}


void JobManager::onTaskFinished(OptimizerStatistics stats, QString _optimizerId, QString _problemId)
{
    m_numberOfFinishedTasks ++;
    //emit progress(int(100*m_numberOfFinishedTasks/m_simpleruns.count()));
    QString str;
    str.sprintf("%3d/%3d %10s %10s %10.2e %10.2e %10.2e %10.4f %10.1f %10.2e", 
                m_numberOfFinishedTasks, 
                m_simpleruns.count(),
                _optimizerId.toStdString().c_str(),
                _problemId.toStdString().c_str(),
                stats.minFitness(),
                stats.averageFitness(),
                stats.maxFitness(),
                stats.successRate(),
                stats.averageNumberOfEvaluations(),
                stats.medianFitness()
               );
    emit appendToLog(str);

    if(m_numberOfFinishedTasks == m_simpleruns.count())
    {
        int nMilliseconds = m_timer.elapsed();
        QString str;
        str.sprintf("Job finished. Elapsed time: %.1f seconds.", double(nMilliseconds)/1000.0);
        emit appendToLog(str);
    }
}

void JobManager::onTaskProgress(void)
{
    m_numberOfFinishedSamples ++;
    int totalProgress = int(100.0*
        double(m_numberOfFinishedSamples)/
        double(m_simpleruns.count() * m_numberOfSamples)
    );
    emit progress(totalProgress);
}

void JobManager::stopRequested(void)
{
    emit appendToLog("Stopping all tasks...");
    for (SimpleRun *task : m_simpleruns)
    {
        task->stop();
    }
}

