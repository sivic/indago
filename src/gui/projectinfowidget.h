
#ifndef PROJECTINFOWIDGET_H
#define PROJECTINFOWIDGET_H

#include <QtWidgets/QWidget>
#include "projectdata.h"

namespace Ui
{
class ProjectInfoWidget;
}

class ProjectInfoWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ProjectInfoWidget(ProjectData* _project, QWidget *_parent = 0);
  
public slots:
    void on_txtProjectName_textChanged(void);
    void on_txtProjectDescription_textChanged(void);
    
private:
    Ui::ProjectInfoWidget* ui;
    
    ProjectData* project;
    
    void refresh(void);

};

#endif // PROJECTINFOWIDGET_H
