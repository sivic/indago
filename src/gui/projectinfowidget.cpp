
#include "projectinfowidget.h"
#include "ui_projectinfowidget.h"

ProjectInfoWidget::ProjectInfoWidget(ProjectData* _project, QWidget* _parent): 
    QWidget(_parent),
    ui(new Ui::ProjectInfoWidget)
{
    ui->setupUi(this);
    project = _project;
    
    refresh();
}

void ProjectInfoWidget::refresh(void )
{
    ui->txtProjectName->setText(project->name());
    ui->txtProjectDescription->setPlainText(project->description());
}

void ProjectInfoWidget::on_txtProjectName_textChanged(void)
{
    project->setName(ui->txtProjectName->text());
}

void ProjectInfoWidget::on_txtProjectDescription_textChanged(void)
{
    project->setDescription(ui->txtProjectDescription->toPlainText());
}
