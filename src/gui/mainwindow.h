#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QFileDialog>

#include "projectdata.h"
#include "projectinfowidget.h"
#include "optimizationproblemwidget.h"
#include "optimizerwidget.h"
#include "jobwidget.h"
#include <memory>

// Job manager
#include "jobmanager.h"

// Temporary
#include "optimizerfactory.h"


namespace Ui {
class MainWindow;
}



class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
    
public slots:
    // Automatic connection: void on_<widget name="">_<signal name="">(<signal parameters="">);

    // Indago menu
    void on_actionOpenProject_triggered(void);
    void on_actionSaveProjectAs_triggered(void);
    void on_actionSaveProject_triggered(void);
    void on_actionExit_triggered(void);
    
    // Log
    void on_pushShowHideLog_clicked(void);
    
    // Optimization problem menu
    void on_actionOptimizationProblemAddNew_triggered(void);
    void on_actionOptimizationProblemRemove_triggered(void);
    
    // Optimizer menu
    void on_actionOptimizerAddNew_triggered(void);
    void on_actionOptimizerRemove_triggered(void);
    
    // Job menu
    void on_actionJobAddNew_triggered(void);
    void on_actionJobRemove_triggered(void);
    void on_actionJobRun_triggered(void);
    
    void on_actionTestA_triggered(void);
    
    void on_projectTree_itemSelectionChanged(void);
    
    // Refresh slots
    void refreshProjectTree(void);
    void refreshProjectTreeOptimizationProblemItem(int _index);
    void refreshProjectTreeOptimizerItem(int _index);
    void refreshProjectTreeJobItem(int _index);
    
    void appendToLog(QString _message);
private:
    Ui::MainWindow *ui;
    
    std::shared_ptr<ProjectData> project;
    
    
    void clearMainWidget(void);
    void refreshProjectInfoWidget(void);
    void refreshOptimizationProblemWidget(int _optimizationProblemIndex);
    void refreshOptimizerWidget(int _optimizerIndex);
    void refreshJobWidget(int _jobIndex);
  
    bool m_freezeWorkspace;
    void freezeWorkspace(bool _freeze);
    
    JobManager* m_jobManager;
    
    QProgressBar* m_progressBar;
    QPushButton* m_stopButton;
    
private slots:
    void onJobManagerFinished(void);
};

#endif // MAINWINDOW_H
