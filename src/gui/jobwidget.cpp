
#include "jobwidget.h"
#include "ui_jobwidget.h"

JobWidget::JobWidget(ProjectData* _project, int _jobIndex, QWidget* _parent): 
    QWidget(_parent),
    ui(new Ui::JobWidget)
{
    ui->setupUi(this);
    project = _project;
    jobIndex = _jobIndex;
    
    
    ui->treeOptimizers->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->treeOptimizers->expandAll();
    ui->treeOptimizers->setItemsExpandable(false);
    ui->treeOptimizers->header()->setSectionResizeMode(QHeaderView::Fixed);
    ui->treeOptimizers->header()->setStretchLastSection(true);
    ui->treeOptimizers->header()->resizeSection(0,80);
    ui->treeOptimizers->setStyleSheet("QTreeWidget::item{ height: 26px;}");
    
    ui->treeOptimizationProblems->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->treeOptimizationProblems->expandAll();
    ui->treeOptimizationProblems->setItemsExpandable(false);
    ui->treeOptimizationProblems->header()->setSectionResizeMode(QHeaderView::Fixed);
    ui->treeOptimizationProblems->header()->setStretchLastSection(true);
    ui->treeOptimizationProblems->header()->resizeSection(0,80);
    ui->treeOptimizationProblems->setStyleSheet("QTreeWidget::item{ height: 26px;}");
    
    
    initialize();
    refresh();
        
}

void JobWidget::initialize(void)
{
    optimizersTableCheckBoxSignalMapper= new QSignalMapper(this);
    connect(optimizersTableCheckBoxSignalMapper, SIGNAL(mapped(int)), this, SLOT(on_optimizersTableCheckBox_valueChanged(int)));
    
    problemsTableCheckBoxSignalMapper= new QSignalMapper(this);
    connect(problemsTableCheckBoxSignalMapper, SIGNAL(mapped(int)), this, SLOT(on_problemsTableCheckBox_valueChanged(int)));
}


void JobWidget::refresh(void )
{
    ui->txtJobName->setText(project->job(jobIndex)->name());
    ui->txtJobId->setText(project->job(jobIndex)->id());
    ui->txtJobDescription->setPlainText(project->job(jobIndex)->description());
    ui->spinNumberOfSamples->setValue(project->job(jobIndex)->numberOfSamples());
        
    for(int iOptimizer = 0; iOptimizer < (int)project->numberOfOptimizers(); iOptimizer++)
    {
        QFont itemFont;
        itemFont.setBold(true);
        QTreeWidgetItem * optimizerItem = new QTreeWidgetItem;
        optimizerItem->setText(1,project->optimizer(iOptimizer)->id());
        optimizerItem->setFont(1,itemFont);
        ui->treeOptimizers->addTopLevelItem(optimizerItem);
        
        bool includeOpt = false;
        QVector<QString> optIds = project->job(jobIndex)->optimizersIds();
        for(int i=0; i<optIds.size(); i++)
        {
            if(optIds[i] == project->optimizer(iOptimizer)->id())
                includeOpt = true;
        }       
        
        QCheckBox* checkBox = new QCheckBox(this);
        checkBox->setText("");
        checkBox->setChecked(includeOpt);
                
        connect(checkBox, SIGNAL(stateChanged(int)), optimizersTableCheckBoxSignalMapper, SLOT(map()));
        optimizersTableCheckBoxSignalMapper->setMapping(checkBox, iOptimizer);
                    
        ui->treeOptimizers->setItemWidget(optimizerItem, 0, checkBox);
    }
    
    
    for(int iProblem = 0; iProblem < (int)project->numberOfOptimizationProblems(); iProblem++)
    {
        QFont itemFont;
        itemFont.setBold(true);
        QTreeWidgetItem * problemItem = new QTreeWidgetItem;
        problemItem->setText(1,project->optimizationProblem(iProblem)->id());
        problemItem->setFont(1,itemFont);
        ui->treeOptimizationProblems->addTopLevelItem(problemItem);
        
        bool includeProb = false;
        QVector<QString> probIds = project->job(jobIndex)->problemsIds();
        for(int i=0; i<probIds.size(); i++)
        {
            if(probIds[i] == project->optimizationProblem(iProblem)->id())
                includeProb = true;
        } 
        
        QCheckBox* checkBox = new QCheckBox(this);
        checkBox->setText("");
        checkBox->setChecked(includeProb);
                
        connect(checkBox, SIGNAL(stateChanged(int)), problemsTableCheckBoxSignalMapper, SLOT(map()));
        problemsTableCheckBoxSignalMapper->setMapping(checkBox, iProblem);
                    
        ui->treeOptimizationProblems->setItemWidget(problemItem, 0, checkBox);
       
    }
    
}

void JobWidget::on_txtJobName_textChanged(void)
{
    project->job(jobIndex)->setName(ui->txtJobName->text());
    emit requestProjectTreeJobRefresh(jobIndex);
}

void JobWidget::on_pushChangeJobId_clicked(void)
{
    QString oldId = project->job(jobIndex)->id();
    QString newId = ui->txtJobId->text();
    
    if(!project->changeJobId(jobIndex, newId))
    {
        emit appendToLog("Failed to change job ID (" + oldId + " to " + newId + ")");
        ui->txtJobId->setText(oldId);
    }
    else
    {
        emit appendToLog("Job ID changed (" + oldId + " to " + newId + ")");        
    }
}

void JobWidget::on_txtJobDescription_textChanged(void)
{
    project->job(jobIndex)->setDescription(ui->txtJobDescription->toPlainText());
}

void JobWidget::on_spinNumberOfSamples_valueChanged(int _value)
{
    project->job(jobIndex)->setNumberOfSamples(_value);
}

void JobWidget::on_optimizersTableCheckBox_valueChanged(int _index)
{
    QCheckBox* checkBox = static_cast<QCheckBox*>(ui->treeOptimizers->itemWidget(ui->treeOptimizers->topLevelItem(_index), 0));
    if(checkBox->isChecked())
    {
        project->job(jobIndex)->addOptimizer(project->optimizer(_index)->id(), project);
    }
    else
    {
        project->job(jobIndex)->removeOptimizer(project->optimizer(_index)->id());
    }
}

void JobWidget::on_problemsTableCheckBox_valueChanged(int _index)
{
    QCheckBox* checkBox = static_cast<QCheckBox*>(ui->treeOptimizationProblems->itemWidget(ui->treeOptimizationProblems->topLevelItem(_index), 0));
    if(checkBox->isChecked())
    {
        project->job(jobIndex)->addProblem(project->optimizationProblem(_index)->id(), project);
    }
    else
    {
        project->job(jobIndex)->removeProblem(project->optimizationProblem(_index)->id());
    }
}

