
#include "optimizerwidget.h"
#include "ui_optimizerwidget.h"

OptimizerWidget::OptimizerWidget(ProjectData* _project, int _optimizerIndex, QWidget* _parent): 
    QWidget(_parent),
    ui(new Ui::OptimizerWidget)
{
    project = _project;
    optimizerIndex = _optimizerIndex;
    optimizerDefinition = project->optimizer(optimizerIndex);
    
    ui->setupUi(this);
    
    ui->tableOptimizerParameters->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableOptimizerParameters->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->tableOptimizerParameters->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableOptimizerParameters->verticalHeader()->hide();
    //ui->tableVariables->setContextMenuPolicy(Qt::CustomContextMenu);

    
    ui->treeStoppingConditions->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->treeStoppingConditions->expandAll();
    ui->treeStoppingConditions->setItemsExpandable(false);
    ui->treeStoppingConditions->header()->setStretchLastSection(false);
    ui->treeStoppingConditions->header()->setSectionResizeMode(QHeaderView::Fixed);
    ui->treeStoppingConditions->header()->resizeSection(0,150);
    ui->treeStoppingConditions->header()->resizeSection(1,75);
    ui->treeStoppingConditions->header()->resizeSection(3,150);
    ui->treeStoppingConditions->header()->setSectionResizeMode(2,QHeaderView::Stretch);
    ui->treeStoppingConditions->setStyleSheet("QTreeWidget::item{ height: 26px;}");
    
    initialize();
    refresh();
}

void OptimizerWidget::initialize(void )
{
    // Add items to optimizer type combo box.
    ui->comboOptimizerType->clear();
    QStringList optimizerTypes = OptimizerDefinition::enumMethodToStringList();
    for(const auto& optimizerType: optimizerTypes)
    {
        ui->comboOptimizerType->addItem(optimizerType);
    }
    
    tableComboBoxSignalMapper = new QSignalMapper(this);
    connect(tableComboBoxSignalMapper, SIGNAL(mapped(int)), this, SLOT(on_tableComboBox_itemChanged(int)));
    
    tableSpinBoxSignalMapper = new QSignalMapper(this);
    connect(tableSpinBoxSignalMapper, SIGNAL(mapped(int)), this, SLOT(on_tableSpinBox_valueChanged(int)));
    
    tableDoubleSpinBoxSignalMapper = new QSignalMapper(this);
    connect(tableDoubleSpinBoxSignalMapper, SIGNAL(mapped(int)), this, SLOT(on_tableDoubleSpinBox_valueChanged(int)));
    
    tableCheckBoxSignalMapper= new QSignalMapper(this);
    connect(tableCheckBoxSignalMapper, SIGNAL(mapped(int)), this, SLOT(on_tableCheckBox_valueChanged(int)));
    
    // Stoping conditions
    scEnableCheckBoxSignalMapper = new QSignalMapper(this);
    connect(scEnableCheckBoxSignalMapper, SIGNAL(mapped(int)), this, SLOT(stoppingCondition_checked(int)));

    scParamSpinBoxSignalMapper = new QSignalMapper(this);
    connect(scParamSpinBoxSignalMapper, SIGNAL(mapped(int)), this, SLOT(stoppingConditionSpinParam_changed(int)));
    
}


void OptimizerWidget::refresh(void )
{
    ui->txtOptimizerName->setText(optimizerDefinition->name());
    ui->txtOptimizerId->setText(optimizerDefinition->id());
    ui->txtOptimizerDescription->setPlainText(optimizerDefinition->description());
    
    ui->comboOptimizerType->setCurrentIndex((int)optimizerDefinition->method());

    refreshParametersTable();
    refreshStoppingConditionsTable();
}

void OptimizerWidget::refreshParametersTable(void )
{
    QVector<ParameterDefinition*> parametersDefs = OptimizerDefinition::parametersDefinitions(optimizerDefinition->method());
    int nParams = parametersDefs.count();
    ui->tableOptimizerParameters->setRowCount(nParams);
    qDebug() << "Number of parameters: " << nParams;
    
    for(int iParam=0; iParam<nParams; iParam++)
    {
        QTableWidgetItem *nameItem = new QTableWidgetItem(parametersDefs[iParam]->name());
        QTableWidgetItem *descriptionItem = new QTableWidgetItem(parametersDefs[iParam]->description()); 
        nameItem->setFlags(nameItem->flags() ^ Qt::ItemIsEditable);
        descriptionItem->setFlags(descriptionItem->flags() ^ Qt::ItemIsEditable);
        ui->tableOptimizerParameters->setItem(iParam, 0, nameItem);
        ui->tableOptimizerParameters->setItem(iParam, 1, descriptionItem);
        
        if(parametersDefs[iParam]->type() == ParameterDefinition::ParameterType::decimal)
        {            
            DecimalParameterDefinition* paramDefinition = ((DecimalParameterDefinition*)parametersDefs[iParam]);
            
            QDoubleSpinBox* spinBox = new QDoubleSpinBox(this);
            spinBox->setMinimum(paramDefinition->lowerBound);
            spinBox->setMaximum(paramDefinition->upperBound);
            spinBox->setDecimals(paramDefinition->decimals);
            spinBox->setSingleStep(pow(10, -paramDefinition->decimals));
            
            spinBox->setValue( optimizerDefinition->parameter(iParam).toDouble() );
            
            connect(spinBox, SIGNAL(valueChanged(double)), tableDoubleSpinBoxSignalMapper, SLOT(map()));
            tableDoubleSpinBoxSignalMapper->setMapping(spinBox, iParam);
            
            ui->tableOptimizerParameters->setCellWidget(iParam, 2, spinBox);
        }
        else if(parametersDefs[iParam]->type() == ParameterDefinition::ParameterType::integer)
        {            
            IntegerParameterDefinition* paramDefinition = ((IntegerParameterDefinition*)parametersDefs[iParam]);
            
            QSpinBox* spinBox = new QSpinBox(this);
            spinBox->setMinimum(paramDefinition->lowerBound);
            spinBox->setMaximum(paramDefinition->upperBound);
            spinBox->setSingleStep(1);
            
            spinBox->setValue( optimizerDefinition->parameter(iParam).toInt() );
            
            connect(spinBox, SIGNAL(valueChanged(int)), tableSpinBoxSignalMapper, SLOT(map()));
            tableSpinBoxSignalMapper->setMapping(spinBox, iParam);
            
            ui->tableOptimizerParameters->setCellWidget(iParam, 2, spinBox);
        }
        else if(parametersDefs[iParam]->type() == ParameterDefinition::ParameterType::boolean)
        {            
            //BooleanParameterDefinition* paramDefinition = ((BooleanParameterDefinition*)parametersDefs[iParam]);
            
            QCheckBox* chekcBox = new QCheckBox(this);
            chekcBox->setText("");
            chekcBox->setChecked(optimizerDefinition->parameter(iParam).toBool());
            
            qDebug() << optimizerDefinition->parameter(iParam).toString();
            
            connect(chekcBox, SIGNAL(stateChanged(int)), tableCheckBoxSignalMapper, SLOT(map()));
            tableCheckBoxSignalMapper->setMapping(chekcBox, iParam);
                        
            ui->tableOptimizerParameters->setCellWidget(iParam, 2, chekcBox);
        }
        else if(parametersDefs[iParam]->type() == ParameterDefinition::ParameterType::list)
        {    
            QComboBox* comboBox = new QComboBox(this);
            QVector<QVariant> comboItems = ((ListParameterDefinition*)parametersDefs[iParam])->avaliableValues;
            int currentIndex = 0;
            for(int iItem=0; iItem<comboItems.count();iItem++)
            {
                comboBox->addItem(comboItems[iItem].toString());
                if(optimizerDefinition->parameter(iParam).toString() == comboItems[iItem].toString())
                    currentIndex = iItem;
            }
            comboBox->setCurrentIndex(currentIndex);
            
            connect(comboBox, SIGNAL(currentIndexChanged(int)), tableComboBoxSignalMapper, SLOT(map()));
            tableComboBoxSignalMapper->setMapping(comboBox, iParam);
            
            ui->tableOptimizerParameters->setCellWidget(iParam, 2, comboBox);
        }
    }
}

void OptimizerWidget::refreshStoppingConditionsTable(void)
{
    int nStpCond = optimizerDefinition->numberOfStoppingConditions();
    //ui->tableStoppingConditions->setRowCount(nStpCond);
    int paramCumulative = 0;
    for(int iStpCond =0; iStpCond<nStpCond; iStpCond++)
    {
        qDebug() << optimizerDefinition->stoppingCondition(iStpCond)->name() << " : " << optimizerDefinition->stoppingCondition(iStpCond)->enabled();
        QFont itemFont;
        itemFont.setBold(true);
        QTreeWidgetItem * stoppingConditionItem = new QTreeWidgetItem;
        stoppingConditionItem->setText(0,optimizerDefinition->stoppingCondition(iStpCond)->name());
        stoppingConditionItem->setFont(0,itemFont);
        stoppingConditionItem->setText(2,optimizerDefinition->stoppingCondition(iStpCond)->description());
        ui->treeStoppingConditions->addTopLevelItem(stoppingConditionItem);
        
        
        QCheckBox* checkBox = new QCheckBox(this);
        checkBox->setText("");
        checkBox->setChecked(optimizerDefinition->stoppingCondition(iStpCond)->enabled());
                
        connect(checkBox, SIGNAL(stateChanged(int)), scEnableCheckBoxSignalMapper, SLOT(map()));
        scEnableCheckBoxSignalMapper->setMapping(checkBox, iStpCond);
                    
        ui->treeStoppingConditions->setItemWidget(stoppingConditionItem, 1, checkBox);
        
        QVector<ParameterDefinition*> paramDefs = 
            StoppingConditionDefinition::parametersDefinitions(StoppingConditionDefinition::StoppingCondition(iStpCond));
        for(int iParam=0; iParam<paramDefs.count();iParam++)
        {
            QTreeWidgetItem * paramItem = new QTreeWidgetItem;
            paramItem->setText(0,paramDefs[iParam]->name());
            paramItem->setText(2,paramDefs[iParam]->description());
            stoppingConditionItem->addChild(paramItem);
            
            if(paramDefs[iParam]->type() == ParameterDefinition::ParameterType::integer)
            {
                QSpinBox* spinBox = new QSpinBox(this);
                spinBox->setMinimum(((IntegerParameterDefinition*)paramDefs[iParam])->lowerBound);
                spinBox->setMaximum(((IntegerParameterDefinition*)paramDefs[iParam])->upperBound);
                spinBox->setValue(optimizerDefinition->stoppingCondition(iStpCond)->parameter(iParam).toInt());
                ui->treeStoppingConditions->setItemWidget(paramItem, 3, spinBox);
                                
                connect(spinBox, SIGNAL(valueChanged(int)), scParamSpinBoxSignalMapper, SLOT(map()));
                scParamSpinBoxSignalMapper->setMapping(spinBox, paramCumulative);
            }
            paramCumulative++;
        }
    }
    ui->treeStoppingConditions->expandAll();
}

void OptimizerWidget::on_tableComboBox_itemChanged(int _index)
{
    qDebug() << "index: " << _index;
    
    QComboBox* comboBox = static_cast<QComboBox *>(ui->tableOptimizerParameters->cellWidget(_index, 2));
    optimizerDefinition->setParameter(_index, QVariant(comboBox->currentText()));
}

void OptimizerWidget::on_tableCheckBox_valueChanged(int _index)
{
    qDebug() << "index: " << _index;
    
    QCheckBox* checkBox = static_cast<QCheckBox*>(ui->tableOptimizerParameters->cellWidget(_index, 2));
    if(checkBox->isChecked())
        optimizerDefinition->setParameter(_index, QVariant(true));
    else
        optimizerDefinition->setParameter(_index, QVariant(false));
}

void OptimizerWidget::on_tableDoubleSpinBox_valueChanged(int _index)
{
    QDoubleSpinBox* spinBox = static_cast<QDoubleSpinBox*>(ui->tableOptimizerParameters->cellWidget(_index, 2));
    optimizerDefinition->setParameter(_index, QVariant(spinBox->value()));
}

void OptimizerWidget::on_tableSpinBox_valueChanged(int _index)
{
    QSpinBox* spinBox = static_cast<QSpinBox*>(ui->tableOptimizerParameters->cellWidget(_index, 2));
    optimizerDefinition->setParameter(_index, QVariant(spinBox->value()));
}

void OptimizerWidget::stoppingCondition_checked(int _index)
{
    QCheckBox* checkBox = static_cast<QCheckBox*>(ui->treeStoppingConditions->itemWidget(
        ui->treeStoppingConditions->topLevelItem(_index), 1));
    optimizerDefinition->stoppingCondition(_index)->setEnabled(checkBox->isChecked());
}

void OptimizerWidget::stoppingConditionSpinParam_changed(int _index)
{
    int cumParam = 0;
    for(int iSc=0; iSc<ui->treeStoppingConditions->topLevelItemCount(); iSc++)
    {
        int nParams = ui->treeStoppingConditions->topLevelItem(iSc)->childCount();
        for(int iParam=0;iParam<nParams;iParam++)
        {
            if(cumParam == _index)
            {
                QSpinBox* spinBox = static_cast<QSpinBox*>(ui->treeStoppingConditions->itemWidget(
                    ui->treeStoppingConditions->topLevelItem(iSc)->child(iParam), 3));
                optimizerDefinition->stoppingCondition(iSc)->setParameter(iParam, spinBox->value());
            }
            cumParam ++;
        }
    }
}

void OptimizerWidget::on_txtOptimizerName_textChanged(void )
{
    optimizerDefinition->setName(ui->txtOptimizerName->text());
    emit requestProjectTreeOptimizerRefresh(optimizerIndex);
}

void OptimizerWidget::on_pushChangeOptimizerId_clicked(void)
{
    QString oldId = optimizerDefinition->id();
    QString newId = ui->txtOptimizerId->text();
    
    if(!project->changeOptimizerId(optimizerIndex, newId))
    {
        emit appendToLog("Failed to change optimizer ID (" + oldId + " to " + newId + ")");
        ui->txtOptimizerId->setText(oldId);
    }
    else
    {
        emit appendToLog("Optimizer ID changed (" + oldId + " to " + newId + ")");        
    }
}

void OptimizerWidget::on_txtOptimizerDescription_textChanged(void )
{
    optimizerDefinition->setDescription(ui->txtOptimizerDescription->toPlainText());
}

void OptimizerWidget::on_comboOptimizerType_activated(int _index)
{
    optimizerDefinition->setMethod((OptimizerDefinition::Method)_index);
    
    refreshParametersTable();
}

