
#ifndef OPTIMIZERWIDGET_H
#define OPTIMIZERWIDGET_H

#include <QtWidgets/QWidget>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QTableWidgetItem>
#include <QtCore/QDebug>
#include <QtCore/QSignalMapper>
#include "projectdata.h"

namespace Ui
{
class OptimizerWidget;
}

class OptimizerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OptimizerWidget(ProjectData* _project, int _optimizerIndex, QWidget *_parent = 0);
    
    
public slots:
    void on_txtOptimizerName_textChanged(void);
    void on_pushChangeOptimizerId_clicked(void);
    void on_txtOptimizerDescription_textChanged(void);
    void on_comboOptimizerType_activated(int _index);
    
    //void on_tableOptimizerParameters_itemChanged(QTableWidgetItem* _item);
    
    void on_tableComboBox_itemChanged(int _index);
    void on_tableSpinBox_valueChanged(int _index);
    void on_tableDoubleSpinBox_valueChanged(int _index);
    void on_tableCheckBox_valueChanged(int _index);
    
    void stoppingCondition_checked(int _index);
    void stoppingConditionSpinParam_changed(int _index);
signals:
    void requestProjectTreeOptimizerRefresh(int _index);
    void appendToLog(QString _message);

private:
    Ui::OptimizerWidget* ui;
    ProjectData* project;
    OptimizerDefinition* optimizerDefinition;
    int optimizerIndex;
        
    QSignalMapper* tableComboBoxSignalMapper;
    QSignalMapper* tableSpinBoxSignalMapper;
    QSignalMapper* tableDoubleSpinBoxSignalMapper;
    QSignalMapper* tableCheckBoxSignalMapper;
    
    // Stopping condition tree widget
    QSignalMapper* scEnableCheckBoxSignalMapper;
    QSignalMapper* scParamSpinBoxSignalMapper;
    
    void initialize(void);
    void refresh(void);
    void refreshParametersTable(void);
    void refreshStoppingConditionsTable(void);

};

#endif // OPTIMIZERWIDGET_H
