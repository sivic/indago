/*
 *
 */

#ifndef OPTIMIZATIONPROBLEMWIDGET_H
#define OPTIMIZATIONPROBLEMWIDGET_H

#include <QtWidgets/QWidget>
#include <QtWidgets/QMenu>
#include <QtWidgets/QTableWidgetItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHeaderView>
#include <QtCore/QDebug>
#include <QtCore/QSignalMapper>

#include "projectdata.h"

namespace Ui
{
class OptimizationProblemWidget;
}

class OptimizationProblemWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OptimizationProblemWidget(ProjectData* _project, int _problemIndex, QWidget *_parent = 0);


public slots:
    void on_txtProblemName_textChanged(void);
    void on_pushChangeProblemId_clicked(void);
    void on_txtProblemDescription_textChanged(void);
    void on_txtFitnessTarget_editingFinished(void);
    void on_comboEvaluatorType_activated(int _index);
    void on_comboEvaluatorType_currentIndexChanged(int _index);
    void on_comboTestFunction_activated(int _index);
    
    
    void on_tableVariables_currentCellChanged(int _currentRow, int _currentCol, int _prevRow, int _prevCol);
    void on_tableVariables_customContextMenuRequested(QPoint _position);
    void on_tableVariables_itemChanged(QTableWidgetItem* _item);

    void on_actionCreateNewVariable_triggered(void);
    void on_actionRemoveVariable_triggered(void);
    /*
    void on_txtVariableName_editingFinished(void);
    void on_comboVariableType_activated(int _index);
    */
    
    void on_tableComboBox_itemChanged(int _index);
    void on_tableSpinBox_valueChanged(int _index);
    
    void on_txtLowerBound_editingFinished(void);
    void on_txtUpperBound_editingFinished(void);
    void on_txtDefaultValue_editingFinished(void);
signals:

    void requestProjectTreeOptimizationProblemRefresh(int _index);
    void appendToLog(QString _message);

private:
    Ui::OptimizationProblemWidget* ui;
    ProjectData* project;
    OptimizationProblem* optimizationProblem;
    int problemIndex;
    QSignalMapper* tableComboBoxSignalMapper;
    QSignalMapper* tableSpinBoxSignalMapper;

    void initialize(void);
    void refresh(void);    
    void refreshOptVariableWidget(int _varIndex);

    QMenu tableVariablesContextMenu;
};

#endif // OPTIMIZATIONPROBLEMWIDGET_H
