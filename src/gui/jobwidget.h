
#ifndef JOBWIDGET_H
#define JOBWIDGET_H

#include <QtWidgets/QWidget>
#include <QtWidgets/QCheckBox>
#include <QtCore/QSignalMapper>

#include "projectdata.h"

namespace Ui
{
class JobWidget;
}

class JobWidget : public QWidget
{
    Q_OBJECT

public:
    explicit JobWidget(ProjectData* _project, int _jobIndex, QWidget *_parent = 0);
    
    
public slots:
    void on_txtJobName_textChanged(void);
    void on_pushChangeJobId_clicked(void);
    void on_txtJobDescription_textChanged(void);
    void on_spinNumberOfSamples_valueChanged(int _value);
    
    void on_optimizersTableCheckBox_valueChanged(int _index);
    void on_problemsTableCheckBox_valueChanged(int _index);
    
signals:
    void requestProjectTreeJobRefresh(int _index);
    void appendToLog(QString _message);

private:
    Ui::JobWidget* ui;
    
    ProjectData* project;
    int jobIndex;
    
    QSignalMapper* optimizersTableCheckBoxSignalMapper;
    QSignalMapper* problemsTableCheckBoxSignalMapper;
    
    void refresh(void);
    void initialize(void);

};

#endif // JOBWIDGET_H
