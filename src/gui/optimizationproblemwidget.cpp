/*
 *
 */

#include "optimizationproblemwidget.h"
#include "ui_optimizationproblemwidget.h"

OptimizationProblemWidget::OptimizationProblemWidget(ProjectData* _project, int _problemIndex, QWidget *_parent): 
    QWidget(_parent),
    ui(new Ui::OptimizationProblemWidget)
{
    project = _project;
    problemIndex = _problemIndex;
    optimizationProblem = project->optimizationProblem(problemIndex);
    
    ui->setupUi(this);
    
    ui->tableVariables->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableVariables->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->tableVariables->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableVariables->verticalHeader()->hide();
    
    initialize();
    
    refresh();
}

void OptimizationProblemWidget::initialize(void )
{
    tableComboBoxSignalMapper = new QSignalMapper(this);
    connect(tableComboBoxSignalMapper, SIGNAL(mapped(int)), this, SLOT(on_tableComboBox_itemChanged(int)));
    
    tableSpinBoxSignalMapper = new QSignalMapper(this);
    connect(tableSpinBoxSignalMapper, SIGNAL(mapped(int)), this, SLOT(on_tableSpinBox_valueChanged(int)));
    
    // Add items to evaluator type combo box.
    ui->comboEvaluatorType->clear();
    QStringList evaluatorTypes = EvaluatorDefinition::enumEvaluatorTypeToStringList();
    for(const auto& evaluatorType: evaluatorTypes)
    {
        ui->comboEvaluatorType->addItem(evaluatorType);
    }
    
    // Add items to test function combo box.
    ui->comboTestFunction->clear();
    QStringList functions = BuiltInEvaluatorDefinition::enumTestFunctionToStringList();
    for(const auto& function: functions)
    {
        ui->comboTestFunction->addItem(function);
    }
        
    // Table variables context menu
    // Defined in ui
    tableVariablesContextMenu.addAction(ui->actionCreateNewVariable);
    tableVariablesContextMenu.addAction(ui->actionRemoveVariable);
}


void OptimizationProblemWidget::refresh(void )
{
    ui->txtProblemName->setText(optimizationProblem->name());
    ui->txtProblemId->setText(optimizationProblem->id());
    ui->txtProblemDescription->setPlainText(optimizationProblem->description());
    ui->txtFitnessTarget->setText(QString::number(optimizationProblem->fitnessTarget(), 'e', 3));
    
    assert(optimizationProblem->hasEvaluatorDefinition() && "Optimization problem has no evaluator defined!");

    if(optimizationProblem->evaluatorDefinitionType() == EvaluatorDefinition::EvaluatorType::builtIn)
    {
        ui->comboEvaluatorType->setCurrentIndex((int)EvaluatorDefinition::EvaluatorType::builtIn);
        
        BuiltInEvaluatorDefinition* evaluatorDef = 
            (BuiltInEvaluatorDefinition*)(optimizationProblem->evaluatorDefinition());
        int funIndex = (int)evaluatorDef->function();
        ui->comboTestFunction->setCurrentIndex(funIndex);
    }
    else if(optimizationProblem->evaluatorDefinitionType() == EvaluatorDefinition::EvaluatorType::python)
    {
        ui->comboEvaluatorType->setCurrentIndex((int)EvaluatorDefinition::EvaluatorType::python);
        PythonEvaluatorDefinition* pyed = (PythonEvaluatorDefinition*)(optimizationProblem->evaluatorDefinition());
        assert(pyed!=nullptr);
        ui->txtPythonFile->setText(pyed->pythonFile);
        ui->txtPythonFunctionName->setText(pyed->functionName);
    }
    
    int previouslySelectedVar = ui->tableVariables->currentRow();
    int nVar = optimizationProblem->numberOfVariableDefinitions();
    ui->tableVariables->setRowCount(nVar);
    for(int iVar=0; iVar<nVar; iVar++)
    {
        QTableWidgetItem *nameItem = new QTableWidgetItem(optimizationProblem->variableDefinitionNameAt(iVar));
        //QTableWidgetItem *typeItem = new QTableWidgetItem(VariableDefinition::enumToString(optimizationProblem->variableDefinition(iVar)->type()));
        //QTableWidgetItem *countItem = new QTableWidgetItem(QString::number(optimizationProblem->variableDefinition(iVar)->quantity()));
        //nameItem->setFlags(nameItem->flags() ^ Qt::ItemIsEditable);
        //typeItem->setFlags(typeItem->flags() ^ Qt::ItemIsEditable);
        ui->tableVariables->setItem(iVar, 0, nameItem);
        //ui->tableVariables->setItem(iVar, 1, typeItem);
        //ui->tableVariables->setItem(iVar, 2, countItem);
         
        QComboBox* comboBox = new QComboBox(this);
        QStringList comboItems = VariableDefinition::enumVariableTypeToStringList();
        int currentIndex = 0;
        for(int iItem=0; iItem<comboItems.count();iItem++)
        {
            comboBox->addItem(comboItems[iItem]);
        }
        if(optimizationProblem->variableDefinition(iVar)->type() == VariableDefinition::continuous)
            comboBox->setCurrentIndex(0);
        if(optimizationProblem->variableDefinition(iVar)->type() == VariableDefinition::discrete)
            comboBox->setCurrentIndex(1);
        
        connect(comboBox, SIGNAL(currentIndexChanged(int)), tableComboBoxSignalMapper, SLOT(map()));
        tableComboBoxSignalMapper->setMapping(comboBox, iVar);
        
        ui->tableVariables->setCellWidget(iVar, 1, comboBox);
        
        
        QSpinBox* spinBox = new QSpinBox(this);
        spinBox->setMinimum(0);
        spinBox->setMaximum(1000);
        spinBox->setSingleStep(1);
        
        spinBox->setValue( optimizationProblem->variableDefinition(iVar)->quantity() );
        
        connect(spinBox, SIGNAL(valueChanged(int)), tableSpinBoxSignalMapper, SLOT(map()));
        tableSpinBoxSignalMapper->setMapping(spinBox, iVar);
        
        ui->tableVariables->setCellWidget(iVar, 2, spinBox);
    }
    
    if(previouslySelectedVar >= 0 && previouslySelectedVar < nVar)
    {
        ui->tableVariables->setCurrentCell(previouslySelectedVar,0);        
    }
    
    if(ui->tableVariables->currentRow() < 0)
    {
        ui->groupContinuousVariable->setEnabled(false);
        ui->actionRemoveVariable->setEnabled(false);
    }
    ui->groupDiscreteVariable->hide();
}

void OptimizationProblemWidget::on_txtProblemName_textChanged(void)
{
    optimizationProblem->setName(ui->txtProblemName->text()); 
    
    emit requestProjectTreeOptimizationProblemRefresh(problemIndex);
}

void OptimizationProblemWidget::on_pushChangeProblemId_clicked(void)
{
    QString oldId = optimizationProblem->id();
    QString newId = ui->txtProblemId->text();
    
    if(!project->changeOptimizationProblemId(problemIndex, newId))
    {
        emit appendToLog("Failed to change optimization problem ID (" + oldId + " to " + newId + ")");
        ui->txtProblemId->setText(oldId);
    }
    else
    {
        emit appendToLog("Optimization problem ID changed (" + oldId + " to " + newId + ")");        
    }
}


void OptimizationProblemWidget::on_txtProblemDescription_textChanged(void )
{
    optimizationProblem->setDescription(ui->txtProblemDescription->toPlainText());
}

void OptimizationProblemWidget::on_txtFitnessTarget_editingFinished(void)
{
    bool isValid = false;
    double fitTarget = ui->txtFitnessTarget->text().toDouble(&isValid);
    if(isValid)
    {
        ui->txtFitnessTarget->setText(QString::number(fitTarget, 'e', 3));
        optimizationProblem->setFitnessTarget(fitTarget);
    }
    else
    {
        ui->txtFitnessTarget->setText(QString::number(optimizationProblem->fitnessTarget(), 'e', 3));
    }
}


void OptimizationProblemWidget::on_tableVariables_itemChanged(QTableWidgetItem* _item)
{
    //qDebug() << _item->text();
    if(_item->column()==0)
        optimizationProblem->variableDefinition(_item->row())->setName(_item->text());
}

void OptimizationProblemWidget::on_tableComboBox_itemChanged(int _index)
{
    QComboBox* comboBox = static_cast<QComboBox *>(ui->tableVariables->cellWidget(_index, 1));
    optimizationProblem->changeVariableDefinitionType(_index,(VariableDefinition::VariableType)comboBox->currentIndex());

    refreshOptVariableWidget(_index);
}

void OptimizationProblemWidget::on_tableSpinBox_valueChanged(int _index)
{
    QSpinBox* spinBox = static_cast<QSpinBox*>(ui->tableVariables->cellWidget(_index, 2));
    optimizationProblem->variableDefinition(_index)->setQuantity(spinBox->value());
}

void OptimizationProblemWidget::on_comboEvaluatorType_activated(int _index)
{    
    switch(_index)
    {
        case (int)EvaluatorDefinition::builtIn:
        {
            optimizationProblem->setEvaluatorDefinition(new BuiltInEvaluatorDefinition());
            break;
        }
        case (int)EvaluatorDefinition::python:
        {
            optimizationProblem->setEvaluatorDefinition(new PythonEvaluatorDefinition());
            break;
        }
    }    
}

void OptimizationProblemWidget::on_comboEvaluatorType_currentIndexChanged(int _index)
{
    ui->groupBuiltIn->hide();
    ui->groupPython->hide();
    
    switch(_index)
    {
        case (int)EvaluatorDefinition::builtIn:
        {
            ui->groupBuiltIn->show();
            break;
        }
        case (int)EvaluatorDefinition::python:
        {
            ui->groupPython->show();
            break;
        }
    }  
}

void OptimizationProblemWidget::on_comboTestFunction_activated(int _index)
{
    if(optimizationProblem->evaluatorDefinitionType()!=EvaluatorDefinition::builtIn)
        return;
    
    BuiltInEvaluatorDefinition* evaluatorDef = (BuiltInEvaluatorDefinition*)(optimizationProblem->evaluatorDefinition());
    
    evaluatorDef->setFunction((BuiltInEvaluatorDefinition::TestFunction)_index);
}


void OptimizationProblemWidget::on_tableVariables_currentCellChanged(int _currentRow, int _currentCol, int _prevRow, int _prevCol)
{  
    if(ui->tableVariables->currentRow() < 0)
    {
        ui->groupContinuousVariable->setEnabled(false);
        ui->groupContinuousVariable->setVisible(true);
    }
    else if(_currentRow != _prevRow)
    {
        refreshOptVariableWidget(_currentRow);
        ui->actionRemoveVariable->setEnabled(true);
    }
}

void OptimizationProblemWidget::refreshOptVariableWidget(int _varIndex)
{
    ui->groupContinuousVariable->setEnabled(true);
    ui->groupContinuousVariable->setVisible(true);
    
    if(optimizationProblem->variableDefinition(_varIndex)->type() == VariableDefinition::continuous)
    {
        ContinuousVariableDefinition* var = (ContinuousVariableDefinition*)(optimizationProblem->variableDefinition(_varIndex));
        ui->txtLowerBound->setText(QString::number(var->lowerBound()));
        ui->txtUpperBound->setText(QString::number(var->upperBound()));
        ui->txtDefaultValue->setText(QString::number(var->defaultValue()));
    }
}

void OptimizationProblemWidget::on_tableVariables_customContextMenuRequested(QPoint _position)
{
    tableVariablesContextMenu.exec(ui->tableVariables->mapToGlobal(_position));
}

void OptimizationProblemWidget::on_actionCreateNewVariable_triggered(void)
{
    qDebug("OptimizationProblemWidget::on_actionCreateNewVariable_triggered(void)");
    optimizationProblem->addVariableDefinition(new ContinuousVariableDefinition("new variable", 0.0, 0.0, 0.0, 1));
    refresh();
}

void OptimizationProblemWidget::on_actionRemoveVariable_triggered(void)
{
    qDebug("OptimizationProblemWidget::on_actionRemoveVariable_triggered(void)");
    
    int varIndex = ui->tableVariables->currentRow();
    qDebug() << varIndex;
    project->optimizationProblem(problemIndex)->removeVariableDefinition(varIndex);
    refresh();
}

void OptimizationProblemWidget::on_txtLowerBound_editingFinished(void)
{
    qDebug() << "OptimizationProblemWidget::on_txtLowerBound_editingFinished(void)";
    int varIndex = ui->tableVariables->currentRow();

    ContinuousVariableDefinition* varDef = (ContinuousVariableDefinition*)optimizationProblem->variableDefinition(varIndex);
    varDef->setLowerBound(ui->txtLowerBound->text().toDouble());
}

void OptimizationProblemWidget::on_txtUpperBound_editingFinished(void)
{
    qDebug() << "OptimizationProblemWidget::on_txtUpperBound_editingFinished(void)";
    int varIndex = ui->tableVariables->currentRow();
    
    ContinuousVariableDefinition* varDef = (ContinuousVariableDefinition*)optimizationProblem->variableDefinition(varIndex);
    varDef->setUpperBound(ui->txtUpperBound->text().toDouble());
}

void OptimizationProblemWidget::on_txtDefaultValue_editingFinished(void)
{
    qDebug() << "OptimizationProblemWidget::on_txtUpperBound_editingFinished(void)";
    int varIndex = ui->tableVariables->currentRow();
    
    ContinuousVariableDefinition* varDef = (ContinuousVariableDefinition*)optimizationProblem->variableDefinition(varIndex);
    varDef->setDefaultValue(ui->txtDefaultValue->text().toDouble());
}
