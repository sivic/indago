# -*- coding: utf-8 -*-
"""
Tutorial code given in the readme.md
"""

# need this for local (non-pip) install only
import sys
sys.path.append('..')

import numpy as np
from copy import deepcopy

from indagobench import CEC2014


if __name__ == '__main__':
       
    ### Differential Evolution
    
    from indago import DE

    optimizers = []

    # shade = DE()
    # shade.variant = 'SHADE'
    # shade.dimensions = 10
    # shade.max_evaluations = 1000 * shade.dimensions
    # shade.params = {'pop_init': 5 * 10}  # 5*d (default 18*d)
    # optimizers.append(shade)

    lshade = DE()
    lshade.variant = 'LSHADE'
    lshade.dimensions = 10
    lshade.max_evaluations = 1000 * lshade.dimensions
    lshade.params = {'pop_init': 10 * lshade.dimensions}  # (default 18*d)
    optimizers.append(lshade)
       
    ### TEST
    
    for optimizer in optimizers:

        optimizer.lb = -100
        optimizer.ub = 100

        print(optimizer)

        runs = 100
        
        test_results_all = []
        for r in range(runs):
            test_results = []
            for fname in CEC2014.case_definitions:
                f = CEC2014(problem=fname, dimensions=optimizer.dimensions)
                optimizer.evaluation_function = f
                test_results.append(optimizer.optimize().f)
            print(f'run #{r}: {np.median(np.log10(np.array(test_results)))}')
            test_results_all.append(np.median(np.log10(np.array(test_results))))
        
        print(f'median score on {runs} runs: {np.median(np.array(test_results_all))}')


"""
SCORES:
LSHADE (popsize=18*d) 1.12
LSHADE (popsize=10*d) 0.56
LSHADE (popsize=5*d) 0.50
LSHADE (popsize=3*d) 0.78
"""
