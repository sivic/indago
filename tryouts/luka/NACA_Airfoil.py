import numpy as np
import matplotlib.pyplot as plt 
from indago import PSO, DE


def generateNACA(m, p, t):
    
    x = np.linspace(0, 1, 160)
        
    yc = np.where(x < p, 
                    m / (p ** 2) * (2 * p * x - x ** 2), 
                    m / ((1 - p) ** 2) * ((1 - 2 * p) + 2 * p * x - x ** 2))
    
    dyc_dx = np.where(x < p, 
                        2 * m / (p ** 2) * (p - x), 
                        2 * m / ((1 - p) ** 2) * (p - x))
    
    theta = np.arctan(dyc_dx)
    
    yt = 5 * t * (0.2969 * np.sqrt(x) - 0.1260 * x - 0.3516 * x ** 2 + 0.2843 * x ** 3 - 0.1015 * x ** 4)
    
    xu = x - yt * np.sin(theta)
    yu = yc + yt * np.cos(theta)
    
    xl = x + yt * np.sin(theta)
    yl = yc - yt * np.cos(theta)
    
    return xu, yu, xl, yl


def XFOIL(data, airfoil_label):
        
        np.savetxt(f'tmp/{airfoil_label}/airfoil_{airfoil_label}.dat', data, header=f'{airfoil_label}', comments='')

        file = open(f'tmp/{airfoil_label}/parameters_{airfoil_label}.txt', 'w')      
        file.write(f'load airfoil_{airfoil_label}.dat\n')
        file.write('GDES\n')
        file.write('FILT\n')  # Apply smoothing filter
        file.write('\n')  # Exit FILT
        file.write('EXEC\n')  # Execute changes
        file.write('\n')  # Exit GDES
        file.write('PPAR\n')
        file.write('N 600\n')  # Adjust the number of panels as needed
        # file.write('P1 0.1\n')  # Panel bunching parameter at the leading edge
        # file.write('P2 1.0\n')  # Panel bunching parameter at the trailing edge
        # file.write('T 1.0\n')  # Trailing edge panel spacing factor
        file.write('\n\n')
        file.write('PANE\n')
        file.write(f'OPER\n')
        file.write(f'ITER 500\n')
        file.write(f'VISC {self.Re}\n')
        file.write(f'ALFA {self.AoA}\n')
        file.write(f'CPWR testcp.txt\n')
        file.close()
        
        
        command = f"cd tmp/{airfoil_label} && xfoil < parameters_{airfoil_label}.txt"        
                
        # command = f"Xvfb :99 & export DISPLAY=:99; cd tmp/{airfoil_label} && xfoil < parameters_{airfoil_label}.txt; killall Xvfb"        
        process = subprocess.Popen(
        [command],  shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
        )
        stdout, stderr = process.communicate()
        coeffs = stdout[-300:].decode('utf-8')
        # coeffs = stdout.decode('utf-8')
        # print (coeffs)

        process = subprocess.Popen(
        ['killall xfoil'],  shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
        )
        
        cl_indx = np.where(np.array(coeffs.split()) == 'CL')[0]
        cd_indx = np.where(np.array(coeffs.split()) == 'CD')[0]
        failed = np.where(np.array(coeffs.split()) == 'failed')[0]

        if len(cl_indx) == 0 or len(cd_indx) == 0  or len(failed) > 0:
            """in case there is no solution / divergence"""
            cl_value = -10
            cd_value = 10
            
        else: 
                    
            # x_, cp_values = np.loadtxt(f'tmp/{airfoil_label}/testcp.txt', unpack=True)
            cl_value = float(coeffs.split()[int(cl_indx[0]+2)])
            cd_value = float(coeffs.split()[int(cd_indx[0]+2)])
            
        command = f"rm -rf tmp/{airfoil_label}"   
        deletion = subprocess.run(command, shell=True, executable='/bin/bash', capture_output=True)
        
        return cl_value, cd_value


        
def evaluation(x):
    
    m = x[0]
    p = x[1]
    t = x[2]
    
    xu, yu, xl, yl = ng.naca4Generator().generate(m, p, t)        
    xy_upper = np.hstack((xu[::-1].reshape(-1,1), yu[::-1].reshape(-1,1)))
    xy_lower = np.hstack((xl.reshape(-1,1), yl.reshape(-1,1)))
    xy = np.vstack((xy_upper[:-1,:], xy_lower))
    
    
    strings = string.ascii_letters + string.digits
    airfoil_label = ''.join(random.choice(strings) for _ in range(8))
    folder_path = f'tmp/{airfoil_label}'

    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    
    cl, cd = XFOIL(xy, airfoil_label)
            
    return cl, cd

        
lb = np.array([0.02, 0.2, 0.06])
ub = np.array([0.09, 0.7, 0.15])

def f(x):
        
    cl, cd = evaluation(x) #returns lift and draf coeff

    return -cl #maximize lift 
    
optimizer = PSO()
optimizer.lb = lb
optimizer.ub = ub
optimizer.evaluation_function = f
optimizer.max_evaluations = 1500
optimizer.processes = 1
optimizer.monitoring = 'basic'
result = optimizer.optimize()
x = result.X


  
xu, yu, xl, yl = generate(x[0], x[1], x[2])         
plt.figure(figsize=(10, 5))
plt.plot(xu, yu, 'b', xl, yl, 'b', linewidth=3)
plt.axis('equal')
plt.xlabel('x')
plt.ylabel('y')
plt.title('Optimized Result')




