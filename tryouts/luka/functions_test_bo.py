# -*- coding: utf-8 -*-
"""
Tutorial code given in the readme.md
"""

import sys
sys.path.append('..')
sys.path.append('../..')

import numpy as np

from indagobench import CEC2014
from indagobench import EmpReg
from scipy.optimize import rosen

"""need cma module by hansen / pip install cma"""
import cma

from indago import RS, PSO, BO, RBS



# DIM = 2


def f(x):
    # x = x + 1
    return rosen(x)

lb = -2.048
ub = 2.048


# # lb = -5
# # ub = 10


# # def f(x):
# #     # print (x)
# #     return cma.ff.sphere(x)

# # lb = -5.12
# # ub = 5.12


# """schwefel"""
def f(x):
    # print (x)
    return 418.9829*len(x) - np.sum(x*np.sin(np.sqrt(np.abs(x))))

lb = -500
ub = 500



# def f(x):
#     # print (x)
#     return cma.ff.rastrigin(x)

# lb = -5.12
# ub = 5.12



def f(x):
    
    a=20
    b=0.2 
    c=2*np.pi
    n = len(x)
    sum_sq = np.sum(x**2)
    sum_cos = np.sum(np.cos(c * x))
    term1 = -a * np.exp(-b * np.sqrt(sum_sq / n))
    term2 = -np.exp(sum_cos / n)
    
    return term1 + term2 + a + np.exp(1)

# # lb = -32
# # ub = 32

# # """ Ackley 200D with irregular domain (-5, 10) like in https://arxiv.org/pdf/1910.01739.pdf
# # RBS performs better, TURBO on 10k evaluations is around f ~ 5
# # """
lb = -5
ub = 10



# # DIM = 10
# f = CEC2014('F1', 10)
# lb = -100
# ub = 100

# def f(X):
#     return np.sin(3*X) - X**2 + 0.7*X
# lb = -1
# ub = 2

# PROBLEM = 'drag2'
# # PROBLEM = 'rab'
PROBLEM = 'drag2'

# # DragCoefficientRegression functions preparing
# # dims = np.array([3, 4, 6, 9]) # 2D regression 
# # dims = np.array([3, 5, 7]) # 1D regression
dim = 10
# f = EmpReg(problem='drag2', dimensions=dim)
# print (f.lb)
# 
dim = dim
max_eval = 200
# lb = -100
# ub = 100

bo = BO()
bo.params['init_size'] = 10
bo.params['batch_size'] = 1
bo.params['model_type'] = 'GP'
bo.params['opt_cycles'] = 5
bo.params['acquisition_function'] = 'EI'
bo.params['xi'] = 0.01
# bo.params['kappa'] = 2
bo.dimensions = dim 
bo.max_evaluations = max_eval
bo.evaluation_function = f
bo.monitoring = 'basic'
bo.lb = np.ones(dim) * lb
bo.ub = np.ones(dim) * ub
# bo.lb = f.lb
# bo.ub = f.ub
result_bo = bo.optimize()
print ('BO', result_bo.f)

   
pso = PSO()
pso.dimensions = dim 
pso.max_evaluations = max_eval
pso.evaluation_function = f
# pso.lb = f.lb
# pso.ub = f.ub
pso.lb = np.ones(dim) * lb
pso.ub = np.ones(dim) * ub
result_pso = pso.optimize()
print ('PSO', result_pso.f)

rs = RS()
rs.dimensions = dim 
rs.max_evaluations = max_eval
rs.evaluation_function = f
# rs.lb = f.lb
# rs.ub = f.ub
rs.lb = np.ones(dim) * lb
rs.ub = np.ones(dim) * ub
result_rs = rs.optimize()
print ('RS', result_rs.f)


# rs = RBS()
# rs.dimensions = dim 
# rs.params['batch_size'] = 10
# rs.max_evaluations = max_eval
# rs.evaluation_function = f
# rs.lb = f.lb
# rs.ub = f.ub
# rs.lb = np.ones(dim) * lb
# rs.ub = np.ones(dim) * ub
# result_rs = rs.optimize()
# print ('RBS', result_rs.f)


