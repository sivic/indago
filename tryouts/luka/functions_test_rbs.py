# -*- coding: utf-8 -*-
"""
Tutorial code given in the readme.md
"""

import sys
sys.path.append('..')
sys.path.append('../..')

import numpy as np

from indagobench import CEC2014
from indagobench import EmpReg
from scipy.optimize import rosen

"""need cma module by hansen / pip install cma"""
import cma



from indago import RS, RBS, PSO


"""
Test on functions from :
https://www.sfu.ca/~ssurjano/optimization.html
(use the same lb,ub)
"""
DIM = 200


def f(x):
    # x = x + 1
    return rosen(x)

lb = -2.048
ub = 2.048


# lb = -5
# ub = 10


def f(x):
    # print (x)
    return cma.ff.sphere(x)

lb = -5.12
ub = 5.12


"""schwefel"""
def f(x):
    # print (x)
    return 418.9829*len(x) - np.sum(x*np.sin(np.sqrt(np.abs(x))))

lb = -500
ub = 500



def f(x):
    # print (x)
    return cma.ff.rastrigin(x)

lb = -5.12
ub = 5.12


#ackley
def f(x):
    
    a=20
    b=0.2 
    c=2*np.pi
    n = len(x)
    sum_sq = np.sum(x**2)
    sum_cos = np.sum(np.cos(c * x))
    term1 = -a * np.exp(-b * np.sqrt(sum_sq / n))
    term2 = -np.exp(sum_cos / n)
    
    return term1 + term2 + a + np.exp(1)

# lb = -32
# ub = 32

""" Ackley 200D with irregular domain (-5, 10) like in https://arxiv.org/pdf/1910.01739.pdf
RBS performs better, TURBO on 10k evaluations is around f ~ 5
"""
lb = -5
ub = 10



# DIM = 10
# f = CEC2014('F16', DIM)
# lb = -100
# ub = 100




dim = DIM
max_eval = 10000

#this is the vanilla version of RBS, no added stuff that makes it even better
rbs = RBS()
rbs.params['batch_size'] = 20 #batch size affects the result
rbs.dimensions = dim 
rbs.max_evaluations = max_eval
rbs.evaluation_function = f
rbs.lb = np.ones(dim) * lb
rbs.ub = np.ones(dim) * ub
result_rbs = rbs.optimize()
print ('RBS', result_rbs.f)

   
pso = PSO()
pso.dimensions = dim 
pso.max_evaluations = max_eval
pso.evaluation_function = f
pso.lb = np.ones(dim) * lb
pso.ub = np.ones(dim) * ub
result_pso = pso.optimize()
print ('PSO', result_pso.f)

rs = RS()
rs.dimensions = dim 
rs.max_evaluations = max_eval
rs.evaluation_function = f
rs.lb = np.ones(dim) * lb
rs.ub = np.ones(dim) * ub
result_rs = rs.optimize()
print ('Random search', result_rs.f)




