# -*- coding: utf-8 -*-
"""
Tutorial code given in the readme.md
"""

# need this for local (non-pip) install only
import sys
sys.path.append('..')

import numpy as np
from indagobench import CEC2014


if __name__ == '__main__':
    
    from indago import CMAES
    cmaes = CMAES()
    cmaes.dimensions = 10
    cmaes.max_evaluations = 1000 * cmaes.dimensions
    # cmaes.monitoring = 'basic'
       
    ### TEST
    
    for optimizer in [cmaes]:

        optimizer.lb = np.ones(optimizer.dimensions) * -100
        optimizer.ub = np.ones(optimizer.dimensions) * 100
        runs = 100
        
        test_results_all = []
        for r in range(runs):
            test_results = []
            for fname in CEC2014.case_definitions:
                f = CEC2014(problem=fname, dimensions=optimizer.dimensions)
                optimizer.evaluation_function = f
                test_results.append(optimizer.optimize().f)
            with np.errstate(divide='ignore'):
                meanlogres = np.mean(np.log10(np.array(test_results)))
                # meanlogres = np.median(np.log10(np.array(test_results)))  # zeros in test_results
            print(f'run #{r}: {meanlogres}')
            test_results_all.append(meanlogres)
        
        print(f'median score on {runs} runs: {np.median(np.array(test_results_all))}')


"""
scores:
-------
PSO 2.02
FWA 2.33
SHADE 1.85
LSHADE 0.95
BA 2.28
GWO 2.83
CMAES 0.55
"""
