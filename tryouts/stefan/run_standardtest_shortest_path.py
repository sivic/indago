import sys
sys.path.append('..')

import indago.benchmarks as indago_benchmarks
import local_paths
import os

if __name__ == '__main__':


    # Rename CEC results
    # for dims in [10]:  # , 20, 50 ,100]:
    #     for f in range(1, 31):
    #         os.rename(f'{local_paths.st24_results_dir}/CEC2014_{dims}D_F{f}.npz',
    #                   f'{local_paths.st24_results_dir}/CEC2014_F{f}_{dims}D.npz')

    supertest = indago_benchmarks.StandardTest(local_paths.st24_results_dir)
    supertest.optimizers = indago_benchmarks.st24_optimizers_list
    supertest.benchmarks = indago_benchmarks.shortest_path_function_dict_list
    # supertest.single_run(supertest.optimizers[0], supertest.benchmarks[0])
    supertest.run_all()


