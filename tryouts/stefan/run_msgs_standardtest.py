import sys
sys.path.append('..')
sys.path.append('../..')

import indago
import indago.benchmarks as indago_benchmarks
import local_paths


if __name__ == '__main__':

    supertest = indago_benchmarks.StandardTest(local_paths.st24_results_dir)
    supertest.optimizers = indago_benchmarks.st24_optimizers_list[:-1]
    supertest.optimizers = (indago_benchmarks.st24_optimizers_list[:2] +
                            indago_benchmarks.st24_optimizers_list[-3:-1])
    supertest.optimizers.append({'optimizer_class': indago.MSGS,
                             'variant': 'Vanilla',
                             'params': {'zoom_in': 8},  # Default parameters
                             'label': f'MSGS zi8n10'})
    supertest.optimizers.append({'optimizer_class': indago.MSGS,
                             'variant': 'Vanilla',
                             'params': {'zoom_in': 4},  # Default parameters
                             'label': f'MSGS zi4n10'})
    supertest.optimizers.append({'optimizer_class': indago.MSGS,
                             'variant': 'Vanilla',
                             'params': {'zoom_in': 4, 'n': 5},  # Default parameters
                             'label': f'MSGS zi4n5'})
    supertest.optimizers.append({'optimizer_class': indago.MSGS,
                             'variant': 'Vanilla',
                             'params': {'zoom_in': 4, 'n': 20},  # Default parameters
                             'label': f'MSGS zi4n20'})
    supertest.optimizers.append({'optimizer_class': indago.MSGS,
                             'variant': 'Vanilla',
                             'params': {'zoom_in': 8, 'n': 20},  # Default parameters
                             'label': f'MSGS zi8n20'})

    supertest.benchmarks = indago_benchmarks.st24_function_dict_list
    # supertest.single_run(supertest.optimizers[0], supertest.benchmarks[0])
    supertest.run_all()


