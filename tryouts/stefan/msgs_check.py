# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 14:53:52 2018

@author: Stefan
"""


import sys
sys.path.append('../..')
import time
import numpy as np
import matplotlib.pyplot as plt
from indagobench import ShortestPath, CEC2014
from indago import MSGS
import scipy.optimize as spo

dim = 10


if __name__ == '__main__':

    f = ShortestPath(problem='zigzag2', dimensions=10)
    msgs = MSGS()

    # Define optimization problem
    msgs.evaluation_function = f
    msgs.dimensions = dim
    msgs.lb = f.lb
    msgs.ub = f.ub
    msgs.max_evaluations = 10000
    # msgs.params['n'] = 20
    # msgs.params['zoom_in'] = 4
    # msgs.params['zoom_out'] = 2

    msgs.monitoring = 'basic'
    msgs.convergence_log_file = f'msgs.log'

    x0 = np.random.uniform(msgs.lb, msgs.ub, msgs.dimensions)
    msgs.X0 = x0
    opt = msgs.optimize()
    print(f'MSGS: {opt.f} in {msgs.history["eval"][-1, 0]} evaluations')
    msgs.plot_history()

    fig, ax = plt.subplots()
    f.draw_obstacles(ax)
    f.draw_path(opt.X, ax, label=f'MSGS f={opt.f:.5f} ({msgs.eval})')

    #print(opt)
    # r = spo.minimize(lambda x: np.sum((x - np.arange(np.size(x))) ** 2), ggs.results.cHistory[0][2].X)
    r = spo.minimize(f, x0,
                     method='L-BFGS-B',
                     bounds=zip(f.lb, f.ub),
                     tol=1e-20,
                     options={'eps': 0,
                              #'finite_diff_rel_step': msgs.params['xtol'],
                              'maxiter': msgs.max_evaluations,
                              'maxfun': msgs.max_evaluations,
                              # 'maxls': 100,
                              })
    #print(r)

    print(f'scipy.minimize: {r.fun} in {r.nfev} evaluations')

    f.draw_path(r.x, ax, label=f'L-BFGS-B f={r.fun:.5f} ({r.nfev})')
    ax.axis('equal')
    ax.set_ylim(-300, 300)
    ax.legend()
    fig.savefig('zigzag2.png')