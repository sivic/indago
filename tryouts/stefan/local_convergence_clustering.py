# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 14:53:52 2018

@author: Stefan
"""
import os.path
import sys

sys.path.append('../..')
import time
import numpy as np
import matplotlib.pyplot as plt
from indagobench import ShortestPath, CEC2014
import indago

cec = CEC2014(10)
dims, lb, ub, max_evals = 10, -100, 100, 10_000
functions = [(f.__name__, f) for f in cec.functions]

# sp1 = ShortestPath('zigzag3')
# sp2 = ShortestPath('trap3')
# dims, lb, ub, max_evals = 20, -30, 30, 40_000
# functions = [('zigzag3', sp1.penalty),
#              ('trap3', sp2.penalty)]

method = 'L-BFGS-B'
# method = 'TNC'


if __name__ == '__main__':

    from scipy.optimize import minimize

    runs = 200
    for label, f in functions:

        print(f'{label:10s} ', end='')
        res_file = f'{label}_{method}.npz'
        X = np.full([runs, dims + 1], np.nan)
        if os.path.exists(res_file):
            _X = np.load(res_file)['X']
            X[:_X.shape[0], :] = _X
            # X = _X[:runs, :]
            print('+' * _X.shape[0], end='')

        for run in range(runs):
            if not np.isnan(X[run, -1]):
                continue
            if method == 'L-BFGS-B':
                res = minimize(f, np.random.uniform(lb, ub, dims),
                               method=method,
                               # bounds=[(-100, 100)] * 10,
                               bounds=[(lb, ub)] * dims,
                               options={'maxfun': max_evals,
                                        'tol': 0,
                                        'ftol': 0,
                                        'gtol': 0,
                                        'eps': 0})
            elif method == 'TNC':
                res = minimize(f, np.random.uniform(lb, ub, dims),
                               method=method,
                               # bounds=[(-100, 100)] * 10,
                               bounds=[(lb, ub)] * dims,
                               options={'maxfun': max_evals,
                                        'xtol': 0,
                                        'ftol': 0,
                                        'gtol': 0,
                                        'eps': 1e-9})
            X[run, :-1] = res.x
            X[run, -1] = res.fun
            print('-', end='')
        else:
            print()
        np.savez_compressed(res_file, X=X)

    print(f'Optimization method: {method}, runs: {runs}')
    print(f'{"Function":10s} {"Unique/rnd":>10s} {"LC 1%":>10s} {"RLC 1%":>10s} {"f median":>10s} {"f std. dev.":>10s} {"C":>10s}')
    for label, f in functions:
        res_file = f'{label}_{method}.npz'
        if os.path.exists(res_file):
            X = np.load(res_file)['X']
        else:
            print('No results.')
            continue

        _, nlocal = np.unique(X.round(decimals=0), axis=0, return_counts=True)

        LC = []
        for run in range(runs):
            x = X[run, :-1].reshape(1, -1)
            for lc in LC:
                diff = np.abs(np.mean(lc, axis=0) - x)
                #print(f'{diff=}')
                if np.all(diff < (ub - lb) * 1e-2):
                    lc = np.append(lc, x, axis=0)
                    break
            else:
                LC.append(x)



        RLC = []
        for run in range(runs):
            rx = (X[run, :-1] - lb) / (ub - lb)
            for ir, rlc in enumerate(RLC):
                diff = np.linalg.norm(rlc - rx)
                if diff < 1e-2 * np.sqrt(10):
                    RLC[ir] = np.append(rlc, rx.reshape(1, -1), axis=0)
                    break
            else:
                RLC.append(rx.reshape(1, -1))

        clusters = []
        for run in range(runs):
            rx = (X[run, :-1] - lb) / (ub - lb)
            rxf = np.append(rx, X[run, -1]).reshape(1, -1)
            # print(f'{rx.shape=}, {rxf.shape=}')
            for ic, cluster in enumerate(clusters):
                # print(f'{cluster.shape=}, {rx.shape=}')
                diff = np.linalg.norm(cluster[:, :-1] - rx)
                if diff < 1e-2 * np.sqrt(10):
                    clusters[ic] = np.append(cluster, rxf, axis=0)
                    break
            else:
                clusters.append(rxf)


        N = np.zeros(len(clusters))
        F = np.zeros(len(clusters))

        # print(len(RLC))
        for ic, cluster in enumerate(clusters):

            N[ic] = len(cluster)
            F[ic] = np.mean(cluster[:, -1])


        SRT = np.argsort(F)
        N = N[SRT]
        F = F[SRT]
        # print(f'{N[:10]=}')
        # print(f'{F=}')

        nc1 = np.dot(N, np.arange(1, N.size + 1)) / np.sum(N)
        nc2 = np.dot(N, np.arange(1, N.size + 1)) / np.sum(np.arange(1, N.size + 1)) / N.sum()
        # nc2 = 100 / nc2
        # print()
        print(f'{label:10s} {nlocal.size:10d} {len(LC):10d} {len(RLC):10d} {np.median(X[:, -1]):10.3e} {np.std(X[:, -1]):10.3e} {nc1:10.2f} {nc2:10.2f}')

        # print(f'{SRT=}')
        # input('?')