# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 14:53:52 2018

@author: Stefan
"""


import sys
sys.path.append('../..')
import time
import numpy as np
import matplotlib.pyplot as plt
from indagobench import ShortestPath, CEC2014
from indago import GGS
import scipy.optimize as spo

def ackley( x, a=20, b=0.2, c=2*np.pi ):
    x = np.asarray_chkfinite(x)  # ValueError if any NaN or Inf
    n = np.size(x)
    s1 = np.sum( x**2 )
    s2 = np.sum( np.cos( c * x ))
    return -a*np.exp( -b*np.sqrt( s1 / n )) - np.exp( s2 / n ) + a + np.exp(1)
def rosenbrock( x ):  # rosen.m
    """ http://en.wikipedia.org/wiki/Rosenbrock_function """
        # a sum of squares, so LevMar (scipy.optimize.leastsq) is pretty good
    x = np.asarray_chkfinite(x)
    x0 = x[:-1]
    x1 = x[1:]
    return (sum( (1 - x0) **2 )
        + 100 * sum( (x1 - x0**2) **2 ))

cec = CEC2014(100)

if __name__ == '__main__':
        
    f = cec.f5
    #np.random.seed()
    ggs = GGS()
    
    # Define optimization problem
    # ggs.evaluation_function = lambda x: np.sum((x - np.arange(np.size(x))) ** 2)
    # ggs.evaluation_function = lambda x: np.sqrt(np.sum((x - np.arange(np.size(x))) ** 2) + 1)
    ggs.evaluation_function = f
    ggs.dimensions = 100
    ggs.lb = np.ones(ggs.dimensions) * -100
    ggs.ub = np.ones(ggs.dimensions) *  100
    ggs.objectives = 1
    ggs.iterations = 10000
    ggs.params['n'] = 2001
    # ggs.params['n'] = np.array([int(i)*10 for i in np.linspace(101, 201, ggs.dimensions)])
    ggs.params['k_max'] = 2
    ggs.monitoring = 'basic'

    ggs.X0 = np.random.uniform(ggs.lb, ggs.ub)
    opt = ggs.optimize()
        
    
    print(opt)
    # r = spo.minimize(lambda x: np.sum((x - np.arange(np.size(x))) ** 2), ggs.results.cHistory[0][2].X)
    # r = spo.minimize(f, ggs.results.cHistory[0][2].X)
    #print(r)
    
    # print()
    # print(f'GGS: {opt.f} in {ggs.results.cHistory[-1][1]} evaluations')
    # print(f'scipy.minimize: {r.fun} in {r.nfev} evaluations')
    # print(r.x)
    # print(f.__doc__)
    
    # sp = ShortestPath('case_2.4')
    # ggs = GGS()
    # ggs.evaluation_function = sp.obj_cnstr
    # # ggs.processes = 4
    # ggs.dimensions = 100
    # ggs.lb = np.ones(ggs.dimensions) * -20
    # ggs.ub = np.ones(ggs.dimensions) *  20
    # ggs.objectives = 1
    # ggs.objective_labels = ['Length']
    # ggs.constraints = 1
    # ggs.constraint_labels = ['Obstacles intersection length']
    # ggs.iterations = 10000
    # ggs.maximum_evaluations = ggs.iterations * ggs.dimensions
    # ggs.params['n'] = 8001
    # ggs.monitoring = 'dashboard'
    # opt = ggs.optimize()
    # print(opt)
    # ggs.results.plot_convergence()
    
    # plt.figure()
    # plt.title('GGS')
    # ax = plt.gca()
    # sp.draw_obstacles(ax)
    # sp.draw_path(opt.X, ax, f'Optimal GGS ({opt.O}, {opt.C}) in {ggs.results.cHistory[-1][1]}')
    
    
    # r = spo.minimize(sp.penalty, ggs.results.cHistory[0][2].X)    
    # sp.draw_path(r.x, ax, f'Optimal scipy.minimize({r.fun} in {r.nfev}')
    # print(r.fun, r.nfev)
    # plt.legend()