
import sys
sys.path.append('..')
sys.path.append('../..')


import indago
from indagobench import  HydraulicNetwork


if __name__ == '__main__':

    hm = HydraulicNetwork(working_dir='hn', problem='31-1-5.1',
                          lb=0, ub=0.4)
    # x_rnd = 0.4 * np.random.uniform(size=hm.wn.num_pipes)
    # x_rnd[5] = 0
    # hm.run_simulation(x_rnd, plot=True, unique_str='rnd')
    # hm.run_simulation(np.full(104, 0.2), plot=True, unique_str='mid')
    # hm.save_case(case='31-1-5.1')

    # hm.run_simulation(hm.lb, plot=True, unique_str='lb')

    optimizer = indago.DE()
    optimizer.dimensions = hm.base_wn.num_pipes
    optimizer.lb = hm.lb
    optimizer.ub = hm.ub
    optimizer.evaluation_function = hm.penalty
    # optimizer.objectives = 2
    # optimizer.constraints = 3
    # optimizer.evaluation_function = hm.run_simulation
    optimizer.monitoring = 'dashboard'
    optimizer.max_evaluations = 40_000
    optimizer.processes = 'max'
    optimizer.forward_unique_str = True
    optimizer.convergence_log_file = 'history.log'

    r = optimizer.optimize()
    optimizer.plot_history(filename='conv.png')
    hm.run_simulation(r.X, plot=True, case_name='best')