import sys

sys.path.append('../..')

import indago
import numpy as np
class Evaluator():
    def __call__(self, *args, **kwargs):
        X = args[0]
        return np.sum(X ** 2)

pso = indago.PSO()
pso.evaluation_function = Evaluator()
pso.lb = -100
pso.ub = 100
pso.dimensions = 10
pso.monitoring = 'basic'
pso.optimize()