
import sys

import numpy as np

sys.path.append('../..')

import indago
import indagobench

def f(x):
    return np.sum((x - np.arange(x.size))**2)

optimizer = indago._esc.ESC()
optimizer.evaluation_function = f
optimizer.lb = -100
optimizer.ub = 100
optimizer.dimensions = 12
optimizer.monitoring = 'basic'
optimizer.max_iterations = 10

np.random.seed(0)
optimizer.optimize(seed=0)
