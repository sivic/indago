# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 14:53:52 2018

@author: Stefan
"""


import sys
sys.path.append('../..')
import os
import indago
import numpy as np

def f(x):
    o1 = np.sum(x**2)
    o2 = np.sum(np.sin(x))
    c1 = np.prod(x)
    return o1, o2, c1

if __name__ == '__main__':

    optimizer = indago.PSO()
    optimizer.dimensions = 10
    optimizer.lb = -10
    optimizer.ub = 10
    optimizer.objectives = 2
    optimizer.constraints = 1
    optimizer.evaluation_function = f
    optimizer.evals_db = 'all_evals.npy'
    optimizer.optimize()

    print(f'{optimizer.eval=}')

    out = indago.read_evals_db(optimizer.evals_db)
    print(f'{out.shape=}')