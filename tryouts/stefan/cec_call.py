#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 16:32:29 2023

@author: stefan
"""

import numpy as np
import json

def load_cases_dict(filename):
    with open(filename) as jsonfile:
        cases_dict = json.load(jsonfile)
    return cases_dict
    
class Problem:   
    
    cases = {}
    def __init__(self):
        self.f_call = None
    def __call__(self, x):
        return self.f_call(x)  
    
class CEC(Problem):
    
    cases = load_cases_dict('cec2014_definitions.txt')
    
    def __init__(self, case, dim=0):
        Problem.__init__(self)
        self.functions = [self.F1, self.F2, self.F3, self.F4, self.F5]

        assert len(self.functions) == len(CEC.cases), f'{self.__class__.__name__}.functions mismatch {self.__class__.__name__}.cases'

        for f in self.functions:
            if f.__name__ == case:
                self.f_call = f
                break
        else:
            assert False, f'Case {case} is not defined in {self.__class__.__name__} class'
       
        dims = CEC.cases[case] 
        if dims == 0:
            assert dim != 0, 'Dimension needs to be provided for this case'                
        if type(dims) == list:
            if dim != 0:
                assert dim in dims, f'''Dimension are not matching for this 
                case, available dims: {dims}'''
                
        
    def F1(self, x):
        return np.sum(np.abs(x))
    def F2(self, x):
        return np.sum(x**2)
    def F3(self, x):
        return np.sum((x + np.arange(x.size))**2)
    def F4(self, x):
        return np.sum((x + np.arange(x.size))**2)
    def F5(self, x):
        return np.sum((x + np.arange(x.size))**2)
        

for case, dims in CEC.cases.items():
    print('-' * 50)
    print(f'{case=}, {dims=}')
    
    if dims == 0:
        print('Any D is possible!')
        dim = 987
        f = CEC(case, dim)
        x = np.arange(dim)
        print(f'D{dim}, {f(x)=}')
        
    else:
        for dim in dims:
            f = CEC(case, dim)
            x = np.arange(dim)
            print(f'D{dim}, {f(x)=}')