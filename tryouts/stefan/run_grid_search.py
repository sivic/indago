# import _local_paths
# import sys
import numpy as np

import indago
import indagobench
def f(x):
    # return np.sum(x ** 2)
    return np.sum((x - np.arange(x.size)-10.23456789) ** 2)
f.dimensions = 10
f.lb=-100
f.ub=100
f_dict = indagobench.cec2014_10d_function_dict_list[1]
f = f_dict['class'](f_dict['problem'], f_dict['dimensions'])

optimizer = indago.MSGD()
optimizer.evaluation_function = f
# optimizer.dimensions = f.dimensions
# optimizer.lb = f.lb
# optimizer.ub = f.ub
optimizer.params['divisions'] = 10
optimizer.params['base'] = 4
optimizer.params['max_scale'] = 15
optimizer.monitoring = 'basic'
optimizer.max_evaluations = 10_000
# optimizer.max_iterations = 10
optimizer.convergence_log_file = 'grid_search.log'
optimizer.optimize()
print(optimizer.best)
optimizer.plot_history('grid_search.png')
