import sys
sys.path.append('..')
sys.path.append('../..')

import indagobench
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':



    standard_test = indagobench.StandardTest(indagobench._local_paths.st24_results_dir,
                                             convergence_window=10, eps_max=0.1, runs_min=10,
                                             )
    standard_test.optimizers = [indagobench.st24_optimizers_list[0],
                                indagobench.st24_optimizers_list[1],
                                indagobench._standard_test_optimizers.mads_dict,
                                indagobench._standard_test_optimizers.nomadlib_dict,
                                indagobench._standard_test_optimizers.nomadbatch_dict,
                                ]

    standard_test.benchmarks = []
    #standard_test.benchmarks.extend(indagobench._cec2014.cec2014_10d_function_dict_list)
    # standard_test.benchmarks.extend(indagobench.AEP_function_dict_list)
    standard_test.benchmarks.extend(indagobench._empirical_regression.EmpReg_function_dict_list[:2])
    # standard_test.benchmarks.extend(indagobench._ill_posed_problems.IPP_all_function_dict_list)
    # standard_test.benchmarks.extend(indagobench._shortest_path.shortest_path_function_dict_list)
    # standard_test.benchmarks.extend(indagobench._hydraulic_network.hydraulic_network_function_dict_list)
    # standard_test.benchmarks.extend(indagobench._structural_frame.structural_frame_function_dict_list)

    #standard_test.single_run(standard_test.benchmarks[0], standard_test.optimizers[0], 'ST_test')
    standard_test.run_all()

    standard_test.matrix_plots(var='t_cpu')
    standard_test.matrix_plots(var='runs')