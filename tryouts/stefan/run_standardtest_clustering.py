import sys
sys.path.append('..')
sys.path.append('../..')

import indago
import indago.benchmarks as indago_benchmarks
import local_paths


if __name__ == '__main__':

    supertest = indago_benchmarks.StandardTest(local_paths.st24_results_dir)
    supertest.optimizers = indago_benchmarks.st24_optimizers_list
    supertest.benchmarks = indago_benchmarks.st24_function_dict_list

    # supertest.single_run(supertest.optimizers[0], supertest.benchmarks[0])
    # supertest.run_all()

    for benchmark in supertest.benchmarks:
        supertest._benchmark_convx_clustering(benchmark)

