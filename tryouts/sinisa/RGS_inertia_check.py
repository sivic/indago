# -*- coding: utf-8 -*-
"""
test Risky Giant Steps inertia
@author: Sinisa
"""

import sys
sys.path.append('..\..')
import indago
from indagobench import CEC2014
import numpy as np
import time


# cec funs optimization parameters
cecD = 20
maxevals = 10 * cecD**2
CEC = CEC2014(cecD)
lb = np.ones(cecD) * -100
ub = np.ones(cecD) * 100
runs = 1000

# used optimizers
mtds = {
        'Vanilla PSO': {'method': 'PSO', 'variant': 'Vanilla', 'params': {}},
        'Vanilla LDIW PSO': {'method': 'PSO', 'variant': 'Vanilla', 'params': {'inertia': 'LDIW'}},
        'Vanilla HSIW PSO': {'method': 'PSO', 'variant': 'Vanilla', 'params': {'inertia': 'HSIW'}},
        }


##############################################################################


if __name__ == '__main__':
    
    print(f'd={cecD}, maxevals={maxevals}, runs={runs}\n')

    for method, desc in mtds.items():
        
        print(f'# {method}')
    
        for f in CEC.functions[:3]: # only unimodal functions
            
            fits = np.full(runs, np.nan)
            startt = time.time() 
            for i in range(runs):
                _, fits[i] = indago.minimize(f, lb, ub, desc['method'], 
                                             variant=desc['variant'], params=desc['params'], 
                                             max_evaluations=maxevals)#, processes='max')
            stopt = time.time()
            # dim, f, median, mean, std, elapsed time
            print(f'{cecD}D, {f.__name__}, {np.median(fits):e}, {np.mean(fits):e}, {np.std(fits):e}, {(stopt-startt)/3600:.3f}')

