# -*- coding: utf-8 -*-
"""
test tuned PSO vs tuned PSO-like methods
@author: Sinisa
"""

import sys
sys.path.append('..\..')
from indagobench import CEC2014
from indago import minimize_exhaustive
import numpy as np
import time


# cec funs optimization parameters
cecD = 10
maxevals = 1000 * cecD
CEC = CEC2014(cecD)
lb = np.ones(cecD) * -100
ub = np.ones(cecD) * 100

# PSO-like methods
methods = ['BA', 'MRFO', 'SSA']

##############################################################################

# pre-computed
file = np.loadtxt('pso_vs_offspring_tuned.txt')
res = {}
for i, method in enumerate(['PSO'] + methods):
    res[method] = {f'F{int(F)}': val for (F, val) \
                   in zip(file[:,0], file[:,i+1])}
    keys_to_pop = []
    for key, val in res[method].items():
        if val == -1:
            keys_to_pop.append(key)
    for key in keys_to_pop:
        res[method].pop(key)

# run through
for f in CEC.functions[11:]:
    
    print('\n*** CEC function:', f.__name__)
    
    print('\ncomputing PSO')
    if f.__name__ not in res['PSO']:
        startt = time.time()       
        (_, f_PSO), _ = minimize_exhaustive(f, lb, ub, 'PSO', 
                                            hyper_optimizer_name='FWA', 
                                            epochs=20, 
                                            iterations=int(1e10),
                                            maximum_evaluations=maxevals)
        stopt = time.time()
        print(f'elapsed time [hr]: {(stopt-startt)/3600:.3f}')
    else:
        print('...skipping')
        f_PSO = res['PSO'][f.__name__]
    print('PSO done, f =', f_PSO)
    
    for method in methods:
        
        print('\ncomputing', method)
        
        if f.__name__ not in res[method]:
            startt = time.time()           
            (_, f_method), _ = minimize_exhaustive(f, lb, ub, method, 
                                                   hyper_optimizer_name='FWA', 
                                                   epochs=20, 
                                                   iterations=int(1e10),
                                                   maximum_evaluations=maxevals)
            res[method][f.__name__] = f_method
            stopt = time.time()
            print(f'elapsed time [hr]: {(stopt-startt)/3600:.3f}')
        else:
            print('...skipping')
            f_method = res[method][f.__name__]
        print(method, 'done, f =', f_method)

        # COMPUTE LOG-SCORE
        omega = np.log10(f_method/f_PSO)
        print(f'omega [oom]: {-omega:6.3f}')
        
        # COMPUTE ALPHA-SCORE
        alpha = 2 * (f_PSO - f_method)/(f_PSO + f_method)
        print(f'alpha [-]: {alpha:6.3f}')     
