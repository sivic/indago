# -*- coding: utf-8 -*-
"""
test stall criterion
"""

# need this for local (non-pip) install only
import sys
sys.path.append('..')

import numpy as np

def F(X):
    return 1


if __name__ == '__main__': 

    ### Particle Swarm Algorithm
    
    from indago import PSO, EFO, DE, BA, FWA, MRFO, SSA, EEEO
    # np.random.seed(0)
    opt = MRFO()
    opt.dimensions = 10
    opt.max_iterations = 100000
    opt.max_evaluations = 1000 * opt.dimensions 
    opt.lb = np.ones(opt.dimensions) * -5
    opt.ub = np.ones(opt.dimensions) * 5    
    opt.evaluation_function = F
    opt.monitoring = 'dashboard'
    opt.processes = 1
    opt.convergence_log_file = 'test_stall.log'
    
    opt.max_stalled_iterations = 10
    opt.max_stalled_evaluations = 44
    
    opt.optimize()
