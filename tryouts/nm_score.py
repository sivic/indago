# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 14:53:52 2018

@author: Stefan
"""


import sys
sys.path.append('..')
import time
import numpy as np
import matplotlib.pyplot as plt
import indago
from indagobench import CEC2014


dim = 10
cec = CEC2014(dim)
X0 = np.random.uniform(-100, 100, dim)
# FWA instance
for variant in 'Vanilla GaoHan'.split():

    optimizer = indago.NM()
    # Define optimization problem
    optimizer.evaluation_function = cec.F1
    optimizer.dimensions = dim
    optimizer.lb = np.full(optimizer.dimensions, -100)
    optimizer.ub = np.full(optimizer.dimensions, 100)
    optimizer.objectives = 1
    optimizer.monitoring = 'dashboard'
    optimizer.variant = variant
    optimizer.X0 = X0
    print(optimizer.__class__.__name__)

    opt = optimizer.optimize()
    print(opt)

