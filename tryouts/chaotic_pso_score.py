# -*- coding: utf-8 -*-

# need this for local (non-pip) install only
import sys
sys.path.append('..')

import numpy as np
from copy import deepcopy

from indagobench import CEC2014


if __name__ == '__main__': 

    ### Particle Swarm Algorithm
    
    from indago import PSO
    pso = PSO()
    pso.variant = 'Chaotic' 
    pso.params['chaotic_elite'] = 0.2
    pso.dimensions = 10 # number of variables in the design vector (x)
    pso.max_evaluations = 1000 * pso.dimensions
    
    ### TEST
    
    for optimizer in [pso]:
    
        test = CEC2014(optimizer.dimensions)
        optimizer.lb = np.ones(optimizer.dimensions) * -100
        optimizer.ub = np.ones(optimizer.dimensions) * 100
        runs = 100
        optimizer.processes = 'max'
        
        test_results_all = []
        for r in range(runs):
            test_results = []
            for f in test.functions:
                opt = deepcopy(optimizer)
                opt.evaluation_function = f
                test_results.append(opt.optimize().f)
            print(f'run #{r}: {np.mean(np.log10(np.array(test_results)))}')
            test_results_all.append(np.mean(np.log10(np.array(test_results))))
        
        print(f'median score on {runs} runs: {np.median(np.array(test_results_all))}')


"""
SCORES:
PSO 2.02
FWA 2.33
BA 2.28
MRFO 2.51
SHADE 1.85
LSHADE 0.95
Chaotic-PSO chaotic_elite=0.2 2.55
Chaotic-PSO chaotic_elite=0.5 2.30
Chaotic-PSO chaotic_elite=1.0 2.08
"""
