import sys

sys.path.append('../..')
import indago
import numpy as np


DIM = 3
X0 = np.ones(DIM) * 10
XMIN = np.arange(DIM)

def f(x):
    return np.sum((x - XMIN) ** 2)

optimizers = indago.optimizers

for optimizer in optimizers:
    print(f'Testing {optimizer.__name__}')
    opt = optimizer()
    opt.evaluation_function = f
    opt.dimensions = DIM
    # opt.lb = -np.inf
    # opt.ub = np.inf
    opt.max_evaluations = 100_000
    opt.monitoring = 'none'
    opt.X0 = X0

    try:
        res = opt.optimize()
    except:
        print('...method STOPPED')
        continue
    if (res.X == X0).all():
        print('...method failed to move from X0')
    else:
        print(f'...method works, result = {res.X}')
