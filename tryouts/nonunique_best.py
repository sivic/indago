import sys

sys.path.append('../..')
import indago
import numpy as np


def f(x):
    return np.sum((x + 1e-6 * np.arange(np.size(x))) ** 2)
def fr(x):
    fit = np.round(f(x))
    # print(x, fit)
    return fit


optimizers = indago.optimizers
# optimizers = [indago.DE]

for evalf in [f, fr]:
    for optimizer_class in optimizers:
        print(f'Testing {optimizer_class.__name__} with {evalf.__name__}')
        optimizer = optimizer_class()
        optimizer.evaluation_function = evalf
        optimizer.dimensions = 10
        optimizer.lb = -100
        optimizer.ub = 100
        optimizer.max_evaluations = 10000
        optimizer.monitoring = 'none'
        optimizer.X0 = np.random.uniform(optimizer.lb, optimizer.ub, [20, optimizer.dimensions])

        optimizer.convergence_log_file = f'test_nonunique_best_{optimizer_class.__name__}.log'

        opt = optimizer.optimize()

        log_file = open(optimizer.convergence_log_file, 'r')
        log = log_file.read()
        log_file.close()
        r = log.find('#  Warning: nonunique optimum; multiple best candidates with same fitness but different X.')
        print(f'{optimizer.convergence_log_file}, {r=}')
        if (evalf.__name__ == 'f' and r == -1) or (evalf.__name__ == 'fr' and r > -1):
            print(f'OK')
        else:
            print(f'###  F A I L ! ###')



    # assert x_max <= 100, f'{optimizer_class.__name__} breached bounds!'
