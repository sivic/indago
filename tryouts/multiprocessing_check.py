#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 15:47:13 2020

@author: stefan
"""

import sys
sys.path.append('..')
import indago
from indago import PSO
from indago import Optimizer
from indagobench import CEC2014, ShortestPath
import numpy as np

if __name__ == '__main__':
    print(f'Indago version: {indago.__version__}')

    seed = 1
    dim = 200
    # use cpus = 'maximum' for employing all available processors/cores
    # cpus = 6
    cpus = 'maximum'
    neval = 1000

    # cec = CEC2014(100)
    # def cec_sum(X):
    #     return np.sum([fun(X) for fun in cec.functions])
    sp = ShortestPath('case_6.1')

    pso1 = PSO()
    pso2 = PSO()
    pso2.processes = cpus

    for pso in [pso1, pso2]:
        #pso.evaluation_function = cec.F27
        #pso.evaluation_function = cec_sum
        #pso.evaluation_function = sp.penalty
        pso.evaluation_function = sp.obj_cnstr
        pso.constraints = 1

        pso.dimensions = dim
        pso.lb = np.ones(dim) * -100
        pso.ub = np.ones(dim) *  100
        pso.params['swarm_size'] = 400
        pso.max_iterations = 20
        pso.variant = 'Vanilla'
        pso.params['inertia'] = 0.65
        pso.params['cognitive_rate'] = 1.0
        pso.params['social_rate'] = 1.0
        pso.monitoring = 'dashboard'

    pso1.tic()
    sol1 = pso1.optimize(seed=seed)
    pso1.toc(msg='Singleprocess PSO elapsed time:')
    print(f'Candidate 0 fitness: {sol1.f}')

    pso2.tic()
    sol2 = pso2.optimize(seed=seed)
    pso2.toc(msg='Multiprocess PSO elapsed time:')
    print(f'Candidate 0 fitness: {sol2.f}')