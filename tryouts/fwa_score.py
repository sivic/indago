# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 14:53:52 2018

@author: Stefan
"""


import sys
sys.path.append('..')
import time
import numpy as np
import matplotlib.pyplot as plt
from indagobench import ShortestPath
from indago import FWA

if __name__ == '__main__':

    runs = 10  # Number of runs
    OPT1 = []
    print('\nRANK')
    for run in range(runs):
        # FWA instance
        fw = FWA()

        # Define optimization problem
        sp = ShortestPath('case_3.1')
        fw.evaluation_function = sp.obj_cnstr
        fw.processes = 4
        fw.dimensions = 50
        fw.lb = np.ones(fw.dimensions) * -20
        fw.ub = np.ones(fw.dimensions) *  20
        fw.objectives = 1
        fw.objective_labels = ['Length']
        fw.constraints = 1
        fw.constraint_labels = ['Obstacles intersection length']

        fw.max_iterations = 500
        fw.params['n'] = 20
        fw.params['m1'] = 10
        fw.params['m2'] = 10

        fw.variant = 'Rank'
        opt = fw.optimize()
        OPT1.append(opt)
        print(f'Run {run + 1} fitness: {opt.f}')
    print(f'Average fitness: {np.average([opt.f for opt in OPT1])}')
    print(f'Median fitness: {np.median([opt.f for opt in OPT1])}')
    
    print('\nVANILLA')
    OPT2 = []
    for run in range(runs):
        fw.variant = 'Vanilla'
        fw.evaluation_function = sp.penalty
        fw.constraints = 0
        fw.constraint_labels = None
        opt = fw.optimize()
        OPT2.append(opt)
        print(f'Run {run + 1} fitness: {opt.f}')
    print(f'Average fitness: {np.average([opt.f for opt in OPT2])}')
    print(f'Median fitness: {np.median([opt.f for opt in OPT2])}')

    
    # plt.figure()
    # plt.title('FWA')
    # ax = plt.gca()
    # sp.draw_obstacles(ax)
    # for run, opt in enumerate(OPT1):
    #     sp.draw_path(opt.X, ax, f'(Rank) Run {run + 1}, f:{opt.f:.2f}', c='b', a=0.1)
    # for run, opt in enumerate(OPT2):
    #     sp.draw_path(opt.X, ax, f'(Vanilla) Run {run + 1}, f:{opt.f:.2f}', c='r', a=0.1)
    # sp.draw_path(best.X, ax, f'Best, f:{opt.f:.2f}', c='k', a=1)
    # #plt.legend()
    # print(f'RANK vs VANILLA: {np.average([opt.f for opt in OPT1])} vs {np.average([opt.f for opt in OPT2])}')
    # plt.show()
