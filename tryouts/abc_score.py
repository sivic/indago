# -*- coding: utf-8 -*-
"""
test ABC
"""

# need this for local (non-pip) install only
import sys
sys.path.append('..')

import numpy as np
from copy import deepcopy
from indagobench import CEC2014


if __name__ == '__main__': 

    
    from indago import ABC
    abc = ABC()
    abc.variant = 'Vanilla'
    abc.dimensions = 10
    abc.max_evaluations = 1000 * abc.dimensions
       
    ### TEST
    
    for optimizer in [abc]:
    
        test = CEC2014(optimizer.dimensions)
        optimizer.lb = np.ones(optimizer.dimensions) * -100
        optimizer.ub = np.ones(optimizer.dimensions) * 100
        runs = 100
        # optimizer.processes = 'max'
        
        test_results_all = []
        for r in range(runs):
            test_results = []
            for f in test.functions:
                opt = deepcopy(optimizer)
                opt.evaluation_function = f
                test_results.append(opt.optimize().f)
            print(f'run #{r}: {np.mean(np.log10(np.array(test_results)))}')
            test_results_all.append(np.mean(np.log10(np.array(test_results))))
        
        print(f'median score on {runs} runs: {np.median(np.array(test_results_all))}')


"""
SCORES:
PSO 2.02
FWA 2.33
SHADE 1.85
LSHADE 0.95
BA 2.28
MRFO 2.51
ABC 1.79
"""
