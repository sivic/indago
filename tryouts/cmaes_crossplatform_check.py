# -*- coding: utf-8 -*-
"""
comparing CMAES results across different platforms (Linux, Windows)
"""

# need this for local (non-pip) install only
import sys
sys.path.append('..')

import numpy as np
from indagobench import CEC2014
from indago import CMAES
import numpy as np
np.set_printoptions(precision=8)
np.set_printoptions(edgeitems=4, linewidth=100,
    formatter=dict(float=lambda x: "%10.8g" % x))

MAXEVAL = 1000
DIM = 10
F = CEC2014(problem='F3', dimensions=DIM)

cmaes = CMAES()
cmaes.evaluation_function = F
cmaes.dimensions = F.dimensions
cmaes.lb = F.lb
cmaes.ub = F.ub
cmaes.max_evaluations = MAXEVAL
cmaes.convergence_log_file = 'cmaes_crossplatform_check.log'

cmaes.optimize(seed=0)
