# -*- coding: utf-8 -*-
"""
test optimizer reset
"""

# need this for local (non-pip) install only
import sys
sys.path.append('..')

import numpy as np

def F(X):
    return np.sum(X**2)


# if __name__ == '__main__':

from indago import SSA

opt = SSA()
opt.dimensions = 10
opt.max_iterations = 100
# opt.max_evaluations = 1000 * opt.dimensions
opt.lb = np.ones(opt.dimensions) * -5
opt.ub = np.ones(opt.dimensions) * 5
opt.evaluation_function = F
opt.monitoring = 'basic'
# opt.processes = 1
opt.convergence_log_file = 'resume_check.log'

# start
opt.optimize()
print(opt)

# resume
opt.max_iterations = 110
opt.optimize(resume=True)
print(opt)

# restart
opt.optimize()
print(opt)
