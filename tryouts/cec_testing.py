# -*- coding: utf-8 -*-
"""
test Indago CEC implementation

@author: Sinisa
"""

import sys
sys.path.append('..')
from indagobench import CEC2014
import numpy as np

fname_prefix = 'cec2014_benchmark_data'
N_SAMPLES = 10

funs_not_good = []

for d in [10, 20, 50, 100]:
    print("\nDimension: %d\n" % d)

    # Generating samples
    # np.savetxt('%s/samples_%dd.txt' % (fname_prefix, d),
    #           np.random.uniform(-100, 100, [N_SAMPLES, d]),
    #           fmt='%.5f')
    # continue

    X = np.loadtxt('%s/samples_%dd.txt' % (fname_prefix, d))
    F = np.loadtxt('%s/results_%dd.txt' % (fname_prefix, d)).T
    # X = np.loadtxt(fname_prefix + str(d) + 'd_x.txt')[:N_SAMPLES,:]
    # F = np.loadtxt(fname_prefix + str(d) + 'd_f.txt')[:N_SAMPLES]

    for i, x in enumerate(X):
        for j, fun_str in enumerate(CEC2014.case_definitions):
            fun = CEC2014(problem=fun_str, dimensions=d)
            f = fun(x)
            f = float(f'{f:.10f}')
            err = np.abs(f - F[i, j]) / F[i, j]

            # print("%3s: %.10e %.10e %.18e" % (fun.__name__, f, F[i,j], err)

            if err > 1e-8:
                if fun_str not in funs_not_good:
                    funs_not_good.append(fun_str)
                    print(f'{fun_str}, {d}d -> this: {fun(x):.10e}, C++: {F[i, j]:.10e}, rel_err: {err:.3e}')

if funs_not_good:
    print('\nfunctions not in agreement with CEC C++ code:')
    print(*sorted(funs_not_good), sep=', ')
else:
    print('all functions OK')
