import sys

sys.path.append('../..')
import indago
import numpy as np
from indagobench import CEC2014

DIM = 10

cec = CEC2014(DIM)
for optimizer_class in indago.optimizers + [indago.SA]:
    print(f'Testing {optimizer_class.__name__}')
    optimizer = optimizer_class()
    optimizer.evaluation_function = cec.f14
    optimizer.dimensions = DIM
    optimizer.lb = -100
    optimizer.ub = 100
    optimizer.max_iterations = 2000
    optimizer.max_evaluations = 1
    optimizer.monitoring = 'none'
    optimizer.X0 = 100

    optimizer.convergence_log_file = f'test_X0_{optimizer_class.__name__}.log'

    opt = optimizer.optimize()

    if optimizer_class == indago.PSO or optimizer_class == indago.SSA:
        n = optimizer.params['swarm_size']

    print(f' - Evaluations {optimizer.eval} = init {optimizer.X0} + pop {n}')

    print(f' - params: {optimizer.params}')
