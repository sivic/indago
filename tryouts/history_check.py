# -*- coding: utf-8 -*-
"""
@author: Stefan
"""

import sys
sys.path.append('..')
import indago
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

print(f'{indago.__version__=}')

e = 1
def f(x):
    global e
    if e < 1500:
        e += 1.
    # return -1, -e, 500 - e, -400
    return 1/e**0.01, \
           1 + np.sin(e/20)**2 / np.log(e), \
           np.cos(e/10) + 200/e, \
           np.sin(e/15) + 1/e * 1e3

optimizers = indago.optimizers
optimizers.remove(indago.DE)
# optimizers = [indago.PSO]

fig, ax_f = plt.subplots(figsize=(12, 6), nrows=1, sharex=True, constrained_layout=True)
ax_f.set_title('History test')
colors = plt.cm.rainbow(np.linspace(0, 1, len(optimizers)))

convergence_points = np.zeros([0, 2])
for optimizer, c in zip(optimizers, colors):
    print(f'Testing {optimizer.__name__} history')
    e = 1
    opt = optimizer()
    opt.evaluation_function = f
    opt.dimensions = 10
    opt.lb = -100
    opt.ub = 100
    opt.objectives = 2
    opt.constraints = 2
    opt.monitoring = 'none'
    opt.convergence_log_file = f'test_history_{optimizer.__name__}.log'
    opt.max_evaluations = 2000
    opt.optimize()

    convergence_points = np.append(convergence_points,
                                   [[opt.history['eval'][-1, 0], opt.history['f'][-1, 0]]], axis=0)
    # opt.plot_history(axes=[None, None, None, ax_f])
    # ax_f.lines[-1].set_color(c)
    # ax_f.lines[-1].set_linestyle(':')
    # ax_f.lines[-1].set_label(optimizer.__name__)
    # ax_f.plot(opt.history['eval'][-1, 0], opt.history['f'][-1, 0], 'o', c=c)
    # ax_f.collections[-1].set_alpha(0.1)

    opt.plot_history(filename=f'test_history_{optimizer.__name__}.png',
                     title=f'{optimizer.__name__} history test')
    # plt.savefig()
    # plt.close()

    print(f'{opt.it=}')
    for k in 'eval X O C f'.split():
        print(f'{k}.shape: {opt.history[k].shape}')
        assert opt.it + 1 == opt.history[k].shape[0], \
            f'History ndarray shape for {optimizer.__name__} does not match number of iterations'
    print()

# srt = np.argsort(convergence_points[:, 0])[::-1]
# convergence_points = convergence_points[srt, :]
#
# for i in range(convergence_points.shape[0]):
#     print(convergence_points[i, :])
#     ax_f.annotate(f'{optimizers[srt[i]].__name__}',
#                   xy=convergence_points[i, :], xycoords='data',
#                   xytext=(0.7 - 0.05 * i, 0.8), textcoords='axes fraction',
#                   arrowprops=dict(arrowstyle="->",
#                                 connectionstyle="arc3"),
#                 )
# plt.legend()
# plt.savefig(f'test_history.png')
