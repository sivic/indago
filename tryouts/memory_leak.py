# -*- coding: utf-8 -*-

# need this for local (non-pip) install only
import sys
sys.path.append('..')

import indago
from indagobench import CEC2014
import numpy as np

dims = 10
cec = CEC2014(dims)

from memory_profiler import profile
import gc

# instantiating the decorator
# @profile
def run_optimization(optimizer_class, runs):
    print(f'Testing {optimizer_class.__name__} ({runs=})')
    for i in range(runs):
        # np.random.seed(0)
        # gc.set_debug(gc.DEBUG_STATS)
        optimizer = optimizer_class()
        optimizer.dimensions = dims
        optimizer.lb = -100
        optimizer.ub = 100
        optimizer.evaluation_function = cec.F1
        optimizer.monitoring = 'none'
        optimizer.optimize()
        # del optimizer
        print('.', end='')
        # print(gc.get_stats())
    print(' done!')


for optimizer_class in indago.optimizers:
    run_optimization(optimizer_class, 1000)
    # run_optimization(optimizer_class, 5)
