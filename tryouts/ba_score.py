# -*- coding: utf-8 -*-
"""
Tutorial code given in the readme.md
"""

# need this for local (non-pip) install only
import sys
sys.path.append('..')

import numpy as np
from copy import deepcopy

from indagobench import CEC2014


if __name__ == '__main__': 

    ### Particle Swarm Algorithm
    
    from indago import PSO
    pso = PSO()
    pso.variant = 'Vanilla' # we will use standard PSO, the other available option is 'TVAC' [1]
    pso.dimensions = 10 # number of variables in the design vector (x)
    pso.max_evaluations = 1000 * pso.dimensions
    pso.monitoring = 'none'
        
    ### Bat Algorithm
    
    from indago import BA
    ba = BA()
    ba.variant = 'Vanilla'
    ba.dimensions = pso.dimensions
    ba.max_iterations = pso.max_iterations
    ba.max_evaluations = pso.max_evaluations
    ba.monitoring = pso.monitoring
       
    ### TEST
    
    for optimizer in [ba]:

        optimizer.lb = np.ones(optimizer.dimensions) * -100
        optimizer.ub = np.ones(optimizer.dimensions) * 100
        runs = 100
        optimizer.processes = 'max'
        
        test_results_all = []
        for r in range(runs):
            test_results = []
            for fname in CEC2014.case_definitions:
                f = CEC2014(problem=fname, dimensions=optimizer.dimensions)
                optimizer.evaluation_function = f
                test_results.append(optimizer.optimize().f)
            print(f'run #{r}: {np.mean(np.log10(np.array(test_results)))}')
            test_results_all.append(np.mean(np.log10(np.array(test_results))))
        
        print(f'median score on {runs} runs: {np.median(np.array(test_results_all))}')


"""
SCORES:
PSO 2.02
FWA 2.33
SHADE 1.85
BA 2.28
LSHADE 0.95
"""
