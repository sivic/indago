# -*- coding: utf-8 -*-
"""
test EEEO
"""

# need this for local (non-pip) install only
import sys
sys.path.append('..')

import numpy as np
from indagobench import CEC2014


if __name__ == '__main__':
    
    from indago import EEEO
    eeeo = EEEO()  # default
    eeeo.dimensions = 10
    eeeo.max_evaluations = 1000 * eeeo.dimensions

    print(eeeo.methods)

    eeeo.lb = -100
    eeeo.ub = 100

    runs = 100

    test_results_all = []
    for r in range(runs):
        test_results = []
        for fname in CEC2014.case_definitions:
            f = CEC2014(problem=fname, dimensions=eeeo.dimensions)
            eeeo.evaluation_function = f
            test_results.append(eeeo.optimize().f)
        with np.errstate(divide='ignore'):
            meanlogres = np.mean(np.log10(np.array(test_results)))
            # meanlogres = np.median(np.log10(np.array(test_results)))  # zeros in test_results
        print(f'run #{r}: {meanlogres}')
        test_results_all.append(meanlogres)

    print(f'median score on {runs} runs: {np.median(np.array(test_results_all))}')


"""
scores:
-------
PSO 2.02
FWA 2.33
SHADE(pop_init=18*d) 1.85
LSHADE(pop_init=18*d) 0.95
BA 2.28
GWO 2.83
CMAES 0.55
EEEO PSO+FWA 1.91
EEEO FWA+GWO 2.02
EEEO CMAES+LSHADE(pop_init=18*d) 1.26
EEEO PSO+LSHADE(pop_init=5*d) 0.86
"""
