import sys
sys.path.append('..')
sys.path.append('../..')
sys.path.append('../../..')


import numpy as np
import indago.benchmarks as indago_benchmarks
from indago.benchmarks import *
import inspect


# supertest = indago_benchmarks.StandardTest('./results')
# supertest.optimizers = indago_benchmarks.st24_optimizers_list
# supertest.benchmarks = indago_benchmarks.st24_function_dict_list    

    
class nomad_f_generator:

    
    def __init__(self, dict_value):
               
        
        self.dict_value = dict_value
        
    
    def get_values(self):
       
        
        funcname =  self.dict_value['label']
        gen_fun = f"""
import sys
sys.path.append('..')
sys.path.append('../..')
sys.path.append('../../..')

import numpy as np
import indago.benchmarks as indago_benchmarks
function = indago_benchmarks.st24_function_dict_list    

indx = [i for i in range(len(function)) if function[i]['label'] == '{funcname}']
f = function[indx[0]]['gen'](function[indx[0]]['label'])
print (f(np.loadtxt(sys.argv[1])))

"""
        
        f = self.dict_value['gen'](self.dict_value['label'])
        lb = f.lb
        ub = f.ub
        D = len(f.ub)
        evals = self.dict_value['max_evaluations']

        return gen_fun, int(evals), int(D), lb, ub
        

# d = supertest.benchmarks[1]
# print (supertest.benchmarks)

# indx = [i for i in range(len(supertest.benchmarks)) if supertest.benchmarks[i]['label'] == supertest.benchmarks[1]['label']]
# print (indx[0])


#gen_fun, evals, D, lb, ub = nomad_f_generator(supertest.benchmarks[1]).get_values()

# import indago.benchmarks as indago_benchmarks
# from indago.benchmarks import *


# supertest = indago_benchmarks.StandardTest('./results')
# # supertest.optimizers = indago_benchmarks.st24_optimizers_list
# supertest.benchmarks = indago_benchmarks.st24_function_dict_list   

# gen_fun = f'import sys\nsys.path.append("../")\nsys.path.append("../../")\n
# from indago.benchmarks import *\nimport numpy as np\nprint({B}({D}).{F}(np.loadtxt(sys.argv[1])))'




# f = supertest.benchmarks[1]['gen'](supertest.benchmarks[1]['label'])
# print (supertest.benchmarks[1]['max_evaluations'])
# print (f.lb)
# print (f(x))


# from indago import PSO

# optimizer = PSO()
                
# optimizer.lb = f.lb
# optimizer.ub = f.ub
   
# optimizer.evaluation_function = f

# optimizer.params['swarm_size'] = 10
# optimizer.max_evaluations = 10000
# optimizer.objectives = 1  
# optimizer.monitoring = 'basic'
# result = optimizer.optimize()
# 		
# min_f = result.f 
# min_x = result.X       



