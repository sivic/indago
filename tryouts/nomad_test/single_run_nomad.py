import sys
import os

sys.path.append('..')
sys.path.append('../..')
sys.path.append('../../..')

import shutil
import glob
import nomad_wrapper_parallel as nw
import nomad_f_generator as n_gen
import indago.benchmarks as indago_benchmarks
from indago.benchmarks import StandardTest

for case_dir in glob.glob('case_*'):
    shutil.rmtree(case_dir)

supertest = indago_benchmarks.StandardTest('./results')
supertest.optimizers = indago_benchmarks.st24_optimizers_list
supertest.benchmarks = indago_benchmarks.st24_function_dict_list    


f, evals, D, lb, ub = n_gen.nomad_f_generator(supertest.benchmarks[1]).get_values()

test = nw.nomad(f, D, lb, ub, 100, n_jobs=1)
results = test.run()


for tmp in glob.glob('nomadtmp'):
    os.remove(tmp)
    
for case_dir in glob.glob('case_*'):
    shutil.rmtree(case_dir)


# def single_run_nomad_(x):
    
#     import nomad_wrapper_parallel as nw
#     import nomad_f_generator as n_gen
    
#     f_x = []
#     for function in x:
#         print (function)
#         f, evals, D, lb, ub = n_gen.nomad_f_generator(function).get_values()
#         test = nw.nomad(f, D, lb, ub, 10, n_jobs=1)
#         results = test.run()
#         f_x.append((results[0][0], results[0][1]))
    
#     return f_x[0], f_x[1]
# # single_run_nomad_(supertest.benchmarks[:2])

# nomad_optimizer = []
# nomad_optimizer.append({'run': single_run_nomad_,
#                          'label': f'NOMAD'})

# cecf1_dict = indago_benchmarks.st24_function_dict_list[:5]

# print (single_run_nomad_(cecf1_dict))

# # supertest = indago_benchmarks.StandardTest('./results')
# # supertest.optimizers = indago_benchmarks.st24_optimizers_list
# # supertest.optimizers.extend(nomad_optimizer)
# # supertest.benchmarks = indago_benchmarks.st24_function_dict_list
# # # # supertest.single_run(supertest.optimizers[0], supertest.benchmarks[0])
# # supertest.run_all()






