

# need this for local (non-pip) install only
import sys
sys.path.append('..')
from indagobench import ShortestPath
from indago import PSO, FWA
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

print(f'{matplotlib.__version__=}')

test = ShortestPath('zigzag3', dimensions=20)
optimizer = PSO()
optimizer.evaluation_function = test.obj_cnstr
optimizer.dimensions = 20
optimizer.lb = -10
optimizer.ub = 10
optimizer.objectives = 1
#optimizer.objective_labels = ['Length']
optimizer.constraints = 1
optimizer.constraint_labels = ['Obstacle collision length']
optimizer.max_iterations = 20000
optimizer.max_evaluations = 20000
optimizer.params['swarm_size'] = 5
optimizer.convergence_log_file = 'convergence.log'
optimizer.monitoring = 'dashboard'
optimizer.optimize()
optimizer.plot_history()
plt.savefig('convergence.png')
plt.show()

data = np.loadtxt('convergence.log')
fig, axes = plt.subplots(constrained_layout=True, nrows=3, sharex=True)
I = data[:, 0]
E = data[:, 1]
X = data[:, 2:2+optimizer.dimensions]
O = data[:, 2+optimizer.dimensions]
C = data[:, 3+optimizer.dimensions]
for i in range(optimizer.dimensions):
    axes[0].plot(E, X[:, i], label=f'$x_{i + 1}$')
axes[1].plot(E, O, label=optimizer.objective_labels[0])
axes[2].plot(E, C, label=optimizer.constraint_labels[0])
axes[2].set_xlabel('Evaluations')
[ax.legend() for ax in axes]
plt.show()