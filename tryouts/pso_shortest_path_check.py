# -*- coding: utf-8 -*-
"""
Created on Fri Mar 30 21:52:32 2018

@author: Stefan
"""
import sys
sys.path.append('..')
from indago import PSO
from indagobench import ShortestPath
import numpy as np
import time
import matplotlib.pyplot as plt
from copy import deepcopy



# NewPSO instance
pso = PSO()
runs = 5 # Number of runs

# Define optimization problem
sp = ShortestPath('bubbles64')
pso.evaluation_function = sp.obj_cnstr
pso.processes = 1
pso.dimensions = 50
pso.lb = np.ones(pso.dimensions) * -5
pso.ub = np.ones(pso.dimensions) *  5
pso.objectives = 1
pso.objective_labels = ['Length']
pso.constraints = 1
pso.constraint_labels = ['Obstacles intersection length']

# Define PSO parameters
pso.max_evaluations = 20_000
pso.monitoring = 'dashboard'
pso.convergence_log_file = 'log.txt'
# print(pso)


pso.optimize()
pso.plot_history(filename='pso_sp.png')