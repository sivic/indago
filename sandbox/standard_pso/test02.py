# -*- coding: utf-8 -*-
"""
Created on Fri Jul 12 15:12:41 2013

@author: stefan
"""


import pso2006 as pso
from pylab import *

from multiprocessing import Pool
import copy
import sys
import time

  
    

def f(opt): 
    
    r = pso.run(opt, opt['uniqueID'])  
    pso.printOpt(opt)
    pso.printRes(r)
    return r

if __name__ == '__main__':
    
    options={
        'func': 'rosenbrock',
        'dim': 10,
        'lb': -5.12,
        'ub': 5.12,
        'eps': 0.01,
        'runs': 1000,
        'swarmsize': 25,
        'w': 0.8,
        'c': 1.0,
        'lbest': 0,
        'inertblock': 1,
        'maxeval': 100000,
        'uniqueID': -1
        }   
    

    
    opt=[] 
    for i in range(3):
        o = copy.deepcopy(options)
        o['uniqueID'] = i
        opt.append(o)    
    

    opt[0]['inertblock'] = 0
    opt[0]['lbest'] = 0
    
    opt[1]['inertblock'] = 1
    opt[1]['lbest'] = 0
    
    opt[2]['inertblock'] = 2
    opt[2]['lbest'] = 0
    
    """
    opt[2]['inertblock'] = 1
    opt[2]['lbest'] = 0
    
    opt[3]['inertblock'] = 1
    opt[3]['lbest'] = 1 
    
    opt[4]['inertblock'] = 2
    opt[4]['lbest'] = 0
    
    opt[5]['inertblock'] = 2
    opt[5]['lbest'] = 1
    """
    
    print "number of tasks: %d" % (len(opt))
  
    pool = Pool(processes=3)      # start 4 worker processes
    
    start_time = time.time()
    R = pool.map(f, opt)          # izvrsi f sa argumentima iz liste opt
    end_time = time.time()
    print 'Elapsed time: %.1f s' % (end_time - start_time)
    """
    for i in range(len(opt)):
        pso.printOpt(opt[i])
        pso.printRes(R[i])
    """  