# -*- coding: utf-8 -*-
"""
Created on Fri Jul 12 15:12:41 2013

@author: stefan
"""


import pso2006 as pso
from pylab import *

from multiprocessing import Pool
import copy
import sys
import time

  
    

def f(opt): 
        
    r = pso.run(opt, opt['uniqueID'])   
    return r

if __name__ == '__main__':
    
    options={
        'func': 'dejong',
        'dim': 10,
        'lb': -100.0,
        'ub': 100.0,
        'eps': 0.01,
        'runs': 1000,
        'swarmsize': 25,
        'w': 0.8,
        'c': 1.0,
        'lbest': 0,
        'inertblock': 1,
        'maxeval': 50000,
        'uniqueID': -1
        }   
    
    print f(options)
    
    SS = linspace(20,40,5)
    W = linspace(0.6,0.9,7)
    
    print "number of tasks: %d" % (len(SS)*len(W))
    
    ID = 0
    opt=[]    
    for i in range(len(SS)):
        for j in range(len(W)):
            ID = ID + 1
            o = copy.deepcopy(options)
            o['swarmsize'] = SS[i]    
            o['w'] = W[j]
            o['uniqueID'] = ID
            opt.append(o)
    
    pool = Pool(processes=4)      # start 4 worker processes
    
    start_time = time.time()
    R = pool.map(f, opt)          # izvrsi f sa argumentima iz liste opt
    end_time = time.time()
    
    print 'Elapsed time: %.1f s' % (start_time - end_time)
    
    ID = 0
    opt=[]
    bestR = Inf
    bestSS = 0
    bestW = 0    
    for i in range(len(SS)):
        for j in range(len(W)):
            ID = ID + 1
            r = R[ID-1]
            print 'swarm_size: %d, \t w: %f \t eval_mean: %.1f \t success: %.2f%%' % (SS[i], W[j], r[1], r[4])
            
            if r[1] < bestR:
                bestR = r[1]
                bestSS = i
                bestW = j
            
            
    print 'best configuration:\n-swarm size: %d\n-w: %f\nfunc. calls: %f' % (SS[bestSS], W[bestW], bestR)
        
    RR=array(R)
    
    RR=RR.reshape([len(SS), len(W), -1])
    #RR.reshape([len(SS), len(W)])
    
    cl = linspace(20, 40, 21)*1000.0
    cs = contourf(W,SS,RR[:,:,1],cl)
    contour(W,SS,RR[:,:,1],cl, colors = 'k', lw=0.5)
    colorbar(cs)
    xlabel('w')
    ylabel('swarm size')
    grid()

    savefig('ss-w_test_rastringin_fidi.pdf')    
    
    show()