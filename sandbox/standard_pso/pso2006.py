# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 09:58:30 2013

@author: stefan
"""

from sys import platform
import os

import subprocess

def printOpt(options):
    
    print '#################################'
    print 'Func:\t%s' % options['func']
    print 'Dim:\t%d' % options['dim']
    print 'lb:\t%f' % options['lb']
    print 'ub:\t%f' % options['ub']
    print 'eps:\t%f' % options['eps']
    
    print 'runs:\t%d' % options['runs']
    print 'swarm size:\t%d' % options['swarmsize']
    print 'w:\t%.2f' % options['w']
    print 'c:\t%.2f' % options['c']
    print 'lbest:\t%d' % options['lbest']
    print 'fifi:\t%d' % options['inertblock']
    print 'maxeval:\t%d' % options['maxeval']
    print '---------------------------------'

def printRes(r):
    (total_clocks, eval_mean, eps_mean, variance, successful, best_val) = r
    
    print 'tot. FC:\t%.0f' % r[0]
    print 'mean FC:\t%.1f' % r[1]
    print 'eps mean:\t%e' % r[2]
    print 'var FC:\t%.1f' % r[3]
    print 'Succ.:\t%.2f %%' % r[4]
    print 'Best val.:\t%f' % r[5]
    print '#################################'
    print '\n\n'

def createParamFile(filename, options):
    
    f = open(filename, 'w')
    
    f.write('%s\n' % options['func'])
    f.write('%d\n' % options['dim'])
    f.write('%f\n' % options['lb'])
    f.write('%f\n' % options['ub'])
    f.write('%f\n' % options['eps'])
    
    f.write('%d\n' % options['runs'])
    f.write('%d\n' % options['swarmsize'])
    f.write('%.2f\n' % options['w'])
    f.write('%.2f\n' % options['c'])
    f.write('%d\n' % options['lbest'])
    f.write('%d\n' % options['inertblock'])
    f.write('%d\n' % options['maxeval'])
    
    f.close()

def run(options, uniqueId):

    exepath = ''
    if platform == "linux" or platform == "linux2":
        exepath = './pso_standard_2006'
    elif platform == "darwin":
        exepath = ''
    elif platform == "win32":
        exepath = './pso2006.exe'
        
    filename = str(uniqueId)+'in.txt'
    createParamFile(filename, options)
    proc = subprocess.Popen([exepath, '-p', filename], stdout=subprocess.PIPE)
    proc.wait()
    
    try:
        pass
        #os.remove(filename)
    except OSError:
        pass    
    
    #print proc.returncode
    lines = proc.stdout.readlines();
    #for l in lines:
    #    print l
    
    total_clocks = float(lines[-6])
    eval_mean = float(lines[-5])
    eps_mean = float(lines[-4])
    variance = float(lines[-3])
    successful = float(lines[-2])
    best_val = float(lines[-1])
    
    return (total_clocks, eval_mean, eps_mean, variance, successful, best_val)