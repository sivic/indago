#-------------------------------------------------
#
# Project created by QtCreator 2012-12-19T17:01:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = indago
TEMPLATE = app


SOURCES += main.cpp\
    gui/psowidget.cpp \
    gui/evaluatorwidget.cpp \
    gui/mainwindow.cpp \
    evaluator/general.cpp \
    evaluator/evaluator.cpp \
    evaluator/variabledefinition.cpp \
    pso/swarm.cpp \
    pso/psotesting.cpp \
    pso/pso.cpp \
    pso/particle.cpp \
    pso/psoparameters.cpp

HEADERS  += gui/psowidget.h \
    gui/mainwindow.h \
    gui/evaluatorwidget.h \
    evaluator/general.h \
    evaluator/evaluator.h \
    evaluator/variabledefinition.h \
    pso/swarm.h \
    pso/psotesting.h \
    pso/pso.h \
    pso/particle.h \
    pso/psoparameters.h

FORMS    +=  gui/psowidget.ui \
    gui/mainwindow.ui \
    gui/evaluatorwidget.ui
