#ifndef PSOSETTINGS_H
#define PSOSETTINGS_H

/**
 * @brief Container for Particle Swarm Optimization parameters.
 */
class PsoParameters
{

public:
    /**
     * @brief PsoParameters constructors. Sets parameters to default values.
     */
    PsoParameters();
        
    /**
     * @param weightInertia is a number which use for calculate inertia, one part of velocity. Default value is 0.9.
     */
    double weightInertia;
    /**
     * @param weightPersonalBest is a number which use for calculate personal influence, one part of velocity. Default value is 0.4.
     */
    double weightPersonalBest;
    /**
     * @param weightPersonalBest is a number which use for calculate social influence, one part of velocity. Default value is 0.5.
     */
    double weightGlobalBest;
    
    /**
     * @param swarmSize is size of swarm. Default vale is 50.
     */
    int swarmSize;
    
    /**
     * @param maxIterations is number of max number of iterations. Default value is 250.
     */
    int maxIterations;
    
    /**
     * @brief targetedFunctionValue is best particle value at which optimization stops. Default value is 10e-6
     */
    double targetedFunctionValue;

    /**
      * @brief targetedVelocityValue is best velocity value at which optimization stops. Default velocity value is 10e-2.
      */
    double targetedVelocityValue;

    /**
      * @param activeTestA is checking if "testA" is active or "PSO parameters testing"
      */
    bool activeTestA;
};

#endif // PSOSETTINGS_H
