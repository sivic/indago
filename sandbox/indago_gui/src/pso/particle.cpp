
#include "particle.h"

Particle::Particle()
{
}

Particle::Particle(QVector< VariableDefinition > _variableDefinition)
{
    m_variableDefinition = _variableDefinition;
    int dimensions = m_variableDefinition.size();
    for (int iDim=0; iDim< dimensions; iDim++)
    {
        double ub = m_variableDefinition[iDim].upperBound();
        double lb = m_variableDefinition[iDim].lowerBound();
        double rp = General::randomNumber();
        double pos = lb+rp*(ub - lb);
        
        //double rv = General::randomNumber();
        //double vel = (rv-0.5)*(ub - lb)*0.5;
        //double vel = (alea( xmin[d], xmax[d] ) - X[s].x[d])/2
        double vel = (General::randomNumber(m_variableDefinition[iDim].lowerBound(), m_variableDefinition[iDim].upperBound()) - pos) / 2.0;
        
        x.push_back(pos);
        m_velocity.push_back(vel);
        personalBestPosition.push_back(pos);
    }

    value = std::numeric_limits<double>::max( );
    old_value = std::numeric_limits<double>::max( );
    personalBestValue = std::numeric_limits<double>::max( );
}


Particle::~Particle()
{
    clear();
}


void Particle::clear(void )
{
    m_variableDefinition.clear();
    
    x.clear();
    m_velocity.clear();
    value = std::numeric_limits<double>::max( );

    personalBestPosition.clear();
    personalBestValue = std::numeric_limits<double>::max( );
}


Particle& Particle::operator = (const Particle &_particle)
{
    int numberOfDimensions = _particle.m_variableDefinition.count();
    x.resize(numberOfDimensions);
    m_velocity.resize(numberOfDimensions);
    for(int iDim=0; iDim<numberOfDimensions; iDim++)
    {
        x[iDim] = _particle.x[iDim];
        m_velocity[iDim] = _particle.m_velocity[iDim];
    }

    value = _particle.value;

    personalBestPosition = _particle.personalBestPosition;
    personalBestValue = _particle.personalBestValue;

    return *this;
}

QString Particle::toString(void)
{
    QStringList positions;

    for(int iDim=0; iDim<x.size(); iDim++)
    {
        positions.append(QString::number(m_position[iDim]));
    }

    QString output = positions.join(", ");
    output += ": " + QString::number(value);

    return output;
}

QVector< double > Particle::position(void)
{
    return x;
}

double Particle::position(int _dimension)
{
    if(_dimension >= 0 && _dimension<x.size())
        return x[_dimension];
    return NULL;
}

void Particle::setPosition(QVector<double> _position)
{
    if(_position.size() == m_variableDefinition.size())
    {
        for (int iDim = 0 ; iDim < m_variableDefinition.size(); iDim++)
        {
            setPosition(iDim,_position[iDim]);
        }
    }
        
    calculateVelocityMagnitude();
}

void Particle::setPosition(int _dimension, double _position)
{
    if(_dimension >= 0 && _dimension<x.size())
    {
        double newPos;
        if(m_variableDefinition[_dimension].type() == VariableType::bounded)
        {
            if(_position < m_variableDefinition[_dimension].lowerBound())
            {
                newPos = m_variableDefinition[_dimension].lowerBound();
            }
            else if(_position > m_variableDefinition[_dimension].upperBound())
            {
                newPos = m_variableDefinition[_dimension].upperBound();
            }
            else
            {
                newPos = _position;
            }
        }
        else if(m_variableDefinition[_dimension].type() == VariableType::discrete)
        {
            newPos = m_variableDefinition[_dimension].discreteValues().first();
            for(int iDiscVal=0; iDiscVal<m_variableDefinition[_dimension].discreteValues().size(); iDiscVal++)
            {
                if( qAbs(_position - m_variableDefinition[_dimension].discreteValues()[iDiscVal]) < qAbs(_position - newPos) )
                {
                    newPos = m_variableDefinition[_dimension].discreteValues()[iDiscVal];
                }
            }
        }
        double newVel = newPos - x[_dimension];
        x[_dimension] = newPos;
        m_velocity[_dimension] = newVel;
    }
}

QVector<double> Particle::velocity(void)
{
    return m_velocity;
}

double Particle::velocity(int _dimension)
{
    if(_dimension >= 0 && _dimension<m_velocity.size())
        return m_velocity[_dimension];
    return NULL;
}

void Particle::calculateVelocityMagnitude(void )
{
    double sum = 0.0;
    for(int iDim=0; iDim<m_variableDefinition.size(); iDim++)
    {
        sum += pow(m_velocity[iDim],2.0);
    }
    sum = sqrt(sum);
    m_velocityMagnitude = sum;
}

double Particle::velocityMagnitude(void )
{
    return m_velocityMagnitude;
}

