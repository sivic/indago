#ifndef PSO_H
#define PSO_H
#include <QVector>
#include <QObject>
#include "particle.h"
#include <QThread>
#include <QTime>
#include "../evaluator/evaluator.h"
#include "psoparameters.h"
#include "../evaluator/general.h"
#include <QFile>
#include <QTextStream>
#include <math.h>


struct OptimizationStatusEnum {
    enum type {
        notStarted,
        running,
        maximumNumberOfIterationsReached,
        targetedFunctionValueReached,
        targetedVelocityValueReached,
        negWeightInertia,
        negWeightGlobalBest,
        negweightPersonalBest,
        negmaxIterations,
        userRequestedStop
    };
};
typedef SafeEnum<OptimizationStatusEnum> OptimizationStatus;

/**
 * @brief Base PSO class.
 */
class Pso: public QThread
{
    Q_OBJECT
public:
    /**
     * @brief PSO constructor
     */
    Pso();

    /**
     * @brief PSO constructor with random seed.
     * @param _randomSeed integer value for random seed used in srand after run or start is called.
     */
    Pso ( int _randomSeed );


    /**
     * @brief PSO destructor
     */
    ~Pso();

    /**
     * @brief initializeSwarm is function which starting the workers to initialize the position and velocity
     */
    void initializeSwarm ( void );

    /**
     * @brief findBest is function which finding the best particle after every iteration
     */
    void findBest ( void );

    /**
     * @brief Clears all dynamically alocated memory in Pso.
     */
    void clear ( void );


    /**
    * @param globalBest is particle whith the best position (minimum value)
    */
    Particle globalBest;

    
    /**
     * @param swarm is vector of Particle
     */
    QVector<Particle> swarm;
    
    void optimize(void);
    void fly(void);
    void evaluate(void);

    PsoParameters psoParameters;


    //int numOfThreads;
    Evaluator* evaluator;
    int iteration;
    //int test;
    int numberOfRuns;
    

    OptimizationStatus status;
    
    QVector<double> correctionFactors;

public slots:
    void run ( void );
    void print ( QString _message ); // Funkcija koja printa poruku u konzolu. Moze se zamjeniti funkcijom koja pise u fajl ili u text box u GUI-u.
    void requestStop ( void );

signals:
    /**
     * @brief Signal for Pso messages output.
     * @param _message contains message text.
     */
    void textOutput ( QString _message );

    /**
     * @brief Signal emmited when an iteration ends.
     * @param _iteration Number of finished iteration.
     */
    void iterationFinished ( int _iteration );

    void runFinished( int _run);
    /**
     * @brief Signal that emits corresponding optimization varaibles and function value when finds a new best value.
     * @param _variables Vector containing optimization variables values.
     * @param _value Newly found best function value.
     */
    void foundNewBest ( QVector<double> _variables, double _value );

    /**
     * @brief Signal is emmited on optimization stopping.
     * @param _variables Vector containing optimization variables values.
     * @param _value Newly found best function value.
     * @param _status Stopping condition because of which optimization finished.
     */
    void optimizationFinished ( QVector<double> _variables, double _value, OptimizationStatus _status );

private:
    int m_randomSeed;
    int m_lastPendingIndex;
    bool m_stopRequested;
    int m_numberOfEvaluations;

    /**
     * @brief Function for dumping current positions and velocities of all particles into files "position.txt" and "velocity.txt".
     */
    void saveData ( void );

    /**
     * @brief Function that checks if one of all stopping criteria is reached.
     * @return Returns true if at least one stopping condition is satisfied. Else returns false.
     */
    bool stoppingCondition ( void );

    bool checkParameters ( void );

    /**
     * @param timer measure the time of the threads job
     */
    QTime timer;
};


#endif // PSO_H
