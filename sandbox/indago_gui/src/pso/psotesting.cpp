#include "psotesting.h"

PsoTesting::PsoTesting()
{

}

Pso* PsoTesting::initPsoInstance(void )
{
    Pso* pso = new Pso;


    Evaluator* mojaFun = new Evaluator(typeRastrigin);
    for(int iDim=0;iDim<10;iDim++)
    {
        mojaFun->variables.append(VariableDefinition(-50.0,50.0));
    }
	pso->evaluator=mojaFun;
    
	pso->psoParameters.swarmSize = 400;
    pso->psoParameters.weightInertia = 0.9;
    pso->psoParameters.weightGlobalBest = 0.4;
    pso->psoParameters.weightPersonalBest = 0.3;
    pso->psoParameters.maxIterations = 200;
    pso->psoParameters.targetedFunctionValue = 1e-5;
    //pso->numOfThreads = 1;
    pso->psoParameters.activeTestA=false;
    return pso;
}

void PsoTesting::run(void)
{

    emit textOutput("void PsoTesting::runTest(void)");
    stopRequested = false;

    numberOfPsoRuns = 40;

    parameter1.clear();
    parameter2.clear();

    // Global best vs Inertia
    parameter1 = General::uniformList(0.0, 3.01, 0.1);
    parameter2 = General::uniformList(0.3, 1.01, 0.05);

    // Dimension vs Swarm size
    /*
	parameter1 = General::uniformList(10.0, 20., 1.);
    parameter2 = General::uniformList(20., 200., 10.);
	*/

    emit progressMax(parameter1.size()*parameter2.size());

    results.clear();
    results.resize(parameter1.size());
    for (int i = 0; i < parameter1.size(); i++)
        results[i].resize(parameter2.size());

    testLoops();
    saveToFile();

    emit textOutput("void PsoTesting::run(void) finished!");

}

void PsoTesting::testLoops(void)
{
    textOutput("void PsoTesting::testLoops(void)");
    for (int iParam1 = 0; iParam1 < parameter1.size(); iParam1++)
    {
        for (int iParam2 = 0; iParam2 < parameter2.size(); iParam2++)
        {
            QVector<Pso*> psoInstances;
            int numberOfThreads = 8;
            double tmpValueSum = 0.0;

            int iRun=0;
            while(iRun<numberOfPsoRuns)
            {
                for(int iThread=0; iThread<numberOfThreads; iThread++)
                {
                    Pso* pso = initPsoInstance();
                    psoInstances.push_back(pso);

                    // Global best vs Inertia
                    pso->psoParameters.weightGlobalBest = parameter1[iParam1];
                    pso->psoParameters.weightInertia = parameter2[iParam2];
                    
                    
                    // Dimension vs Swarm size
                    /*
					int maxfuncalls = 100000;
                    pso->dimensions = parameter1[iParam1];
                    QVector<double> lb(pso->dimensions), ub(pso->dimensions);
                    lb.fill(-50.0);
                    ub.fill(50.0);
                    pso->lowerBound = lb;
                    pso->upperBound = ub;                                        
                    pso->psoParameters.swarmSize= parameter2[iParam2];
                    pso->psoParameters.maxIterations = int(double(maxfuncalls)/double(pso->psoParameters.swarmSize));
					*/
                    
                    pso->clear();
                    pso->start();
                }

                for(int iThread=0; iThread<numberOfThreads; iThread++)
                {
                    psoInstances[iThread]->wait();

                    //tmpValueSum += psoInstances[iRun]->evaluator->numberOfFunctionCalls;
                    tmpValueSum += psoInstances[iThread]->iteration * psoInstances[iThread]->psoParameters.swarmSize;


                    delete psoInstances[iThread]->evaluator;
                    delete psoInstances[iThread];
                }
                iRun += numberOfThreads;
                psoInstances.clear();
            }

            results[iParam1][iParam2] = tmpValueSum / double(iRun);
            textOutput(
                " p1:"+QString::number(parameter1[iParam1])+
                " p2:"+QString::number(parameter2[iParam2])+
                " FC: "+QString::number(tmpValueSum / double(iRun)));
            emit progress(iParam1*parameter2.size() + iParam2);

            if(stopRequested) return;
        }
    }
}

void PsoTesting::saveToFile(void)
{

    QFile file1("p1.txt");
    file1.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out1(&file1);
    for (int i1 = 0 ; i1 < parameter1.size(); i1++)
    {
        out1 << parameter1[i1] << " ";
    }
    file1.close();


    QFile file2("p2.txt");
    file2.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out2(&file2);
    for (int i2 = 0 ; i2 < parameter2.size(); i2++)
    {
        out2 << parameter2[i2] << " ";
    }
    file2.close();


    QFile file("results.txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);

    for (int i1 = 0 ; i1 < parameter1.size(); i1++)
    {
        for (int i2 = 0 ; i2 < parameter2.size(); i2++)
        {
            out << results[i1][i2] << " ";
        }
        out << "\n";
    }

    // optional, as QFile destructor will already do it:
    file.close();
}


void PsoTesting::requestStop(void)
{
    stopRequested = true;
}
