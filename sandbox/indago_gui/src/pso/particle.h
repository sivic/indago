#ifndef PARTICLE_H
#define PARTICLE_H

#include <QVector>
#include <QString>
#include <QStringList>

#include <iostream>
#include <limits>

#include "../evaluator/general.h"
#include "../evaluator/evaluator.h"


/**
 * @brief Particle class is a basic PSO class that contains particle data.
 */
class Particle
{
public:

    /**
     * @brief Basic Particle constructor.
     */
    Particle();

    
    /**
     * @brief particle constructos which initialize random position and veliocity according to VariableDefinition.
     */
    Particle(QVector<VariableDefinition> _variableDefinition);

    
    /**
     * @brief Particle destructor.
     */
    ~Particle();

    
    /**
     * @brief Clears all data stored in Particle class.
     */
    void clear(void);
    
    
    /**
     * @brief Gets current postion of particle.
     * @return Vector containing coordinate for each dimension.
     */
    QVector<double> position(void);

    
    /**
     * @brief Gets coordinate of particle current position for given dimension.
     * @param _position Dimension index.
     */
    double position(int _dimension);

    
    /**
     * @brief Sets current position of particle. New velocity is calculated based on new position and velocity magnitude is updated.
     * @param _position Vector containing coordinates for each dimension.
     */
    void setPosition(QVector<double> _position);

    
    /**
    * @brief Gets current velocity of particle.
    * @return Vector containing velocity components for each dimension.
    */
    QVector<double> velocity(void);

    
    /**
     * @brief Gets component of current velocity for given dimension.
     * @param _dimension Dimesnion index.
     */
    double velocity(int _dimension);

    
    /**
     * @brief Value of particle at current position.
     */
    double value;
    double old_value;

    
    /**
     * @brief Best position of particle containing coordinate for each dimension.
     */
    QVector <double> personalBestPosition;

    
    /**
     * @brief Best value of particle.
     */
    double personalBestValue;

    
    /**
     * @brief Assignment operator for Particle class.
     */
    Particle & operator=(const Particle &_particle);

    
    /**
     * @brief Generate string containing current position and value on current position.
     * @return Function returns particle info in a QString.
     */
    QString toString(void);
    
    
    /**
     * @brief Gets magnitude of current velocity.
     * @return Velocity magnitude.
     */
    double velocityMagnitude(void);
    
private:

    /**
     * @brief Vector of variables definitions.
     */
    QVector<VariableDefinition> m_variableDefinition;
    
    
    /**
     * @brief Particle current position. It contains coordinates for each dimension.
     */
    QVector<double> x;

    
    /**
     * @brief Particle current velocity. It contains components for each dimension.
     */
    QVector <double> m_velocity;
    
    
    /**
     * @brief Sets coordinate of current position of particle for given dimension. New velocity is calculated based on new position.
     * @param _dimension Dimension index.
     * @param _position Position coordinate value.
     */
    void setPosition(int _dimension, double _position);
    
    
    /**
     * @brief Calculates and updates velocity magnitude based on new velocity components.
     */
    void calculateVelocityMagnitude(void);
    
    
    /**
     * @brief Particle current velocity magnitude.
     */
    double m_velocityMagnitude;
};

#endif // PARTICLE_H
