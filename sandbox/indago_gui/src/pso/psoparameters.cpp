#include "psoparameters.h"

PsoParameters::PsoParameters()
{
    swarmSize = 10;
    weightInertia=0.73;
    weightPersonalBest=1.;
    weightGlobalBest=1.;
    targetedFunctionValue = 1e-6;
    targetedVelocityValue=1e-20;
    maxIterations = 100;
}

