#include "pso.h"
Pso::Pso(): QThread() , status(OptimizationStatus::notStarted)
{

    clear();
    m_randomSeed = qrand();
    numberOfRuns = 0;
}

Pso::Pso(int _randomSeed): QThread(),  status(OptimizationStatus::notStarted)
{
    clear();
    m_randomSeed = _randomSeed;
}


Pso::~Pso()
{
    clear();
}


void Pso::clear(void )
{
    /*
    for (int iWorker = 0; iWorker < swarms.count(); iWorker++)
    {
        delete swarms[iWorker];
    }
    swarms.clear();
    */
}


void Pso::run(void)
{
    if (checkParameters()==false)
    {
        if(QFile::exists("position.txt")) QFile::remove("position.txt");
        if(QFile::exists("velocity.txt")) QFile::remove("velocity.txt");

        status = OptimizationStatus::running;
        qsrand(m_randomSeed);


        m_stopRequested = false;
        timer.start();
        
        double fSum = 0.0;
        double eSum = 0.0;
        for(int iRun=0; iRun<numberOfRuns; iRun++)
        {
            optimize();
            emit runFinished(iRun+1);
            fSum += globalBest.value;
            eSum += double(m_numberOfEvaluations);
        }
        
        fSum = fSum/double(numberOfRuns);
        eSum = eSum/double(numberOfRuns);
        
        int msec=timer.elapsed();
        //print("Optimization finished!");
        print("");
        //print("<u>Best variables and value:<u/>");
        //print(globalBest.toString());

        print("Average fitness: "+QString::number(fSum));
        print("Average evaluations: "+QString::number(eSum));
        
        //emit optimizationFinished(globalBest.position(),globalBest.value,status);

        print("<U>Elapsed time: </u>" + QString::number(double(msec)/1000.0) + " sec");
    }
    exit();
}

void Pso::optimize(void )
{
    initializeSwarm();
    evaluate();

    globalBest = swarm.first();
    findBest();

    iteration = 1;

    do
    {
        fly();
        evaluate();

        findBest();

        //print("iteration: " + QString::number(iteration) + " \t " + QString::number(globalBest.value));

        //emit iterationFinished(iteration);
        iteration++;
    }
    while (!stoppingCondition());
}


void Pso::initializeSwarm(void)
{
    swarm.clear();
    
    for(int iParticle=0; iParticle<psoParameters.swarmSize; iParticle++)
    {
        Particle tempParticle(evaluator->variables);
        swarm.push_back(tempParticle);
    }
    m_numberOfEvaluations = 0;
}

void Pso::findBest(void)
{
    bool newBestFound{false};
    for(int iParticle=0; iParticle<swarm.size(); iParticle++)
    {
        if(swarm[iParticle].value < globalBest.value)
        {
            globalBest = swarm[iParticle];
            newBestFound = true;
        }
    }
    
    if(newBestFound)
    {
     //   emit foundNewBest(globalBest.position(),globalBest.value);
    }
    
}

void Pso::fly(void )
{
    for(int iParticle=0; iParticle<swarm.count(); iParticle++)
    {
        QVector<double> newPos;
        for(int iDim=0; iDim<evaluator->numberOfVariables(); iDim++)
        {
            double randomPersonal = General::randomNumber();
            double randomGlobal = General::randomNumber();

            double v1 = (psoParameters.weightInertia) *
                        swarm[iParticle].velocity(iDim);
            double v2 = (psoParameters.weightPersonalBest) *
                        randomPersonal*( swarm[iParticle].personalBestPosition[iDim] - swarm[iParticle].position(iDim));
            double v3 = (psoParameters.weightGlobalBest) *
                        randomGlobal*(globalBest.position(iDim) - swarm[iParticle].position(iDim));
            /*
            if(swarm[iParticle].value > swarm[iParticle].old_value)
                v1 = 0.0;
            */          
            double vel = v1 + v2 + v3;
            double newCoord = swarm[iParticle].position(iDim) + vel;
            newPos.push_back(newCoord);
        }
        swarm[iParticle].setPosition(newPos);
    }
}

void Pso::evaluate(void )
{
    for(int iParticle=0; iParticle<swarm.count(); iParticle++)
    {
        double value = evaluator->evaluate(swarm[iParticle].position());
        swarm[iParticle].value = value;
        swarm[iParticle].old_value = swarm[iParticle].value;
        
        if(swarm[iParticle].value <= swarm[iParticle].personalBestValue)
        {
            swarm[iParticle].personalBestPosition = swarm[iParticle].position();
            swarm[iParticle].personalBestValue = swarm[iParticle].value;
        }
        
        m_numberOfEvaluations ++;
    }
}


void Pso::print(QString _message)
{
    emit textOutput(_message);
}

void Pso::requestStop(void)
{
    m_stopRequested = true;
}

void Pso::saveData(void)
{
    QFile fileP("position.txt");
    fileP.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
    QTextStream outP(&fileP);

    QFile fileV("velocity.txt");
    fileV.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
    QTextStream outV(&fileV);

    /*
    for(int iSwarm=0; iSwarm<swarms.count(); iSwarm++)
    {
        for(int iParticle=0; iParticle<swarms[iSwarm]->swarm.count(); iParticle++)
        {
            for(int iDim=0; iDim<evaluator->numberOfVariables(); iDim++)
            {
                outP << " " << swarms[iSwarm]->swarm[iParticle].position(iDim);
                outV << " " << swarms[iSwarm]->swarm[iParticle].velocity(iDim);
            }
            outP << "\n";
            outV << "\n";
        }
    }
    */

    fileP.close();
    fileV.close();
}

bool Pso::stoppingCondition(void)
{
    bool stop=false;

    if(m_stopRequested)
    {
        status = OptimizationStatus::userRequestedStop;
        stop=true;
        //print("Optimization finished!");
        //print("User requested stop.");
    }

    if(iteration > psoParameters.maxIterations)
    {
        status = OptimizationStatus::maximumNumberOfIterationsReached;
        stop=true;
        //print("Optimization finished!");
        //print("Maximum number of iterations reached.");
    }

    if(globalBest.value <= psoParameters.targetedFunctionValue)
    {
        status = OptimizationStatus::targetedFunctionValueReached;
        stop = true;
        //print("Optimization finished!");
        //print("Targeted function value reached.");
    }

    double averageVelocityMagnitude = 0.0;
    for(int iParticle=0; iParticle<swarm.size(); iParticle++)
    {
        averageVelocityMagnitude += swarm[iParticle].velocityMagnitude();
    }
    averageVelocityMagnitude /= (psoParameters.swarmSize);

    if( averageVelocityMagnitude < psoParameters.targetedVelocityValue)
    {
        status = OptimizationStatus::targetedVelocityValueReached;
        stop = true;
        //print("Optimization finished!");
        //print("Targeted velocity value reached.");
    }

    return stop;
}

bool Pso::checkParameters (void)
{
    bool stop=false;

    if (  (psoParameters.weightInertia <= 0) || (psoParameters.weightInertia>3) )
    {
        status=OptimizationStatus::negWeightInertia;
        stop=true;
        print("Weight Inertia Factor must be in allowable interval [0 3]");
    }

    if ( (psoParameters.weightGlobalBest <=0) || (psoParameters.weightGlobalBest >3))
    {
        status=OptimizationStatus::negWeightGlobalBest;
        stop=true;
        print("Weight Global Best factor must be in allowable interval [0 3]");
    }

    if( (psoParameters.weightPersonalBest <= 0) || (psoParameters.weightPersonalBest>3) )
    {
        status=OptimizationStatus::negweightPersonalBest;
        stop=true;
        print("Weight Personal Best factor must be in allowable interval [0 3]");
    }

    if (psoParameters.maxIterations <=0)
    {
        status=OptimizationStatus::negmaxIterations;
        stop=true;
        print("Maximum iteration must be a positive number");
    }

    return stop;
}
