#ifndef PSOTESTING_H
#define PSOTESTING_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include "pso.h"

class PsoTesting : public QThread
{
    Q_OBJECT
public:
    PsoTesting();

    /**
      *@param numberOfPsoRuns is number how many times repeat test for same parameters.
      */
    int numberOfPsoRuns;

    /**
      *@param parameter1 is one of testing parameters.
      */
    QVector<double> parameter1;

    /**
      *@param parameter2 is second testing parameter.
      */
    QVector<double> parameter2;

    /**
      *@param results is parameter to save results of test.
      */
    QVector< QVector<double> > results;

    /**
      *@brief initPSoInstance set initialize parameters value.
      */
    Pso* initPsoInstance(void);
    
    void testLoops(void);

    /**
      *@brief saveToFile save to file parameter1, parameter2 and results.
      */
    void saveToFile(void);
    
signals:
    void textOutput(QString);
    void progress(int);
    void progressMax(int);

public slots:
    void run(void);
	void requestStop(void);

private:
    bool stopRequested;
    
};

#endif // PSOTESTING_H
