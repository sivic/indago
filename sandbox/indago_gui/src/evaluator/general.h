#ifndef GENERAL_H
#define GENERAL_H

#include <QtGlobal>
#include <QVector>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

template<typename def, typename inner = typename def::type>
class SafeEnum : public def
{
  typedef typename def::type type;
  inner val;

public:

  SafeEnum(type v) : val(v) {}
  inner underlying() const { return val; }

  bool operator == (const SafeEnum & s) const { return this->val == s.val; }
  bool operator != (const SafeEnum & s) const { return this->val != s.val; }
  bool operator <  (const SafeEnum & s) const { return this->val <  s.val; }
  bool operator <= (const SafeEnum & s) const { return this->val <= s.val; }
  bool operator >  (const SafeEnum & s) const { return this->val >  s.val; }
  bool operator >= (const SafeEnum & s) const { return this->val >= s.val; }
};


class General
{
public:
    General();

static double randomNumber(void)
{
    double randNumber = static_cast<double>(qrand()) / static_cast<double>( RAND_MAX );
    return randNumber;
}

static double randomNumber(double _min, double _max)
{
    return _min + (_max - _min)*randomNumber();
}

static QVector<double> uniformList(double _start, double _end, double _step)
{
    QVector<double> list;
    int numberOfItems = int((_end - _start)/_step)+1;
    for(int i=0; i<numberOfItems;i++)
    {
        list.append(_start + double(i)*_step);
    }
    
    return list;
};

};


#endif // GENERAL_H
