#ifndef VARIABLEDEFINITION_H
#define VARIABLEDEFINITION_H

#include <QVector>
#include "general.h"



struct VariableTypeEnum {
  enum type
  {
      undefined,
      bounded,
      discrete
  };
};
/**
 * @brief Enum for optimization variable type.
 */
typedef SafeEnum<VariableTypeEnum> VariableType;

/**
 * @brief Provides definition of an optimization variable.
 */
class VariableDefinition
{
public:
    /**
     * @brief Constructor of VariableDefinition that creates undefined optimization variable.
     */
    VariableDefinition(void);
    
    /**
     * @brief Constructor for bounded optimization variable.
     * @param _lowerBound Value of lower bound for optimization variable value.
     * @param _upperBound Value of upper bound for optimization variable value.
     */
    VariableDefinition(double _lowerBound, double _upperBound);
    
    /**
     * @brief Constructor for discretized optimization variable.
     * @param _discreteValues Vecotor of avaliable values for variable.
     */
    VariableDefinition(QVector<double> _discreteValues);

    /**
     * @brief Gets variable type.
     * @return Variable type.
     */    
    VariableType type(void);
    
    /**
     * @brief Sets lower and upper bounds for optimization variable value.
     * @param _lowerBound Value of lower bound for optimization variable value.
     * @param _upperBound Value of upper bound for optimization variable value.
     */
    void setBounds(double _lowerBound, double _upperBound);
    
    /**
     * @brief Gets lower bound for optimization variable value.
     * @return Value of lower bound for optimization variable value.
     */
    double lowerBound(void);

    /**
     * @brief Gets upper bound for optimization variable value.
     * @return Value of upper bound for optimization variable value.
     */
    double upperBound(void);
    
    /**
    * @brief Sets avaliable discrete values for optimization variable.
    * @param _discreteValues Vector of avaliable discrete values.
    */
    void setDiscreteValues(QVector<double> _discreteValues);
        
    /**
    * @brief Gets avaliable discrete values for optimization variable.
    * @return Vector of avaliable discrete values.
    */
    QVector<double> discreteValues(void);
    
private:    
    VariableType m_type;
    double m_lowerBound;
    double m_upperBound;
    QVector<double> m_discreteValues;

};

#endif // VARIABLEDEFINITION_H
