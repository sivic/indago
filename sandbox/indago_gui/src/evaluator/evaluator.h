#ifndef EVALUATOR_H
#define EVALUATOR_H

#include <QVector>

#include <math.h>

#include "variabledefinition.h"

enum BuiltInFunctions
{
    typeQuadratic,
    typeRosenbrock,
    typeRastrigin,
    typeDeJong
};

/**
  *@param pi use for calculate in test function.
  */
const double pi=3.141592653589793;

/**
 * @brief Evalueator class is used for evaluating set of optimization variables.
 */
class Evaluator
{
public:
    Evaluator();

    Evaluator(BuiltInFunctions _function);    

    /**
      *@brief evaluate return value of test function.
      *@param x iz vector of positions of one particle.
      */
    double evaluate(QVector<double> x);

    /**
      *@brief Custom is test function which create user.
      *@param x iz vector of positions of one particle.
      */
    double Custom (QVector<double> x);

    /**
      *@brief Quadratic is test function
      *@param x iz vector of positions of one particle.
      */
    double Quadratic (QVector<double> x);

    /**
      *@brief Rosenbrockc is test function
      *@param x iz vector of positions of one particle.
      */
    double Rosenbrock (QVector<double> x);

    /**
      *@brief Rastrigin is test function
      *@param x iz vector of positions of one particle.
      */
    double Rastrigin (QVector<double> x);

    /**
      *@brief DeJong is test function
      *@param x iz vector of positions of one particle.
      */
    double DeJong (QVector<double> x);

    BuiltInFunctions testFunction;

    QVector<VariableDefinition> variables;

    int numberOfVariables(void);
};

#endif // EVALUATOR_H
