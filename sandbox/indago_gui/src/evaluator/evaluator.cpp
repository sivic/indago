
#include "evaluator.h"
#include <iostream>

Evaluator::Evaluator()
{
    testFunction = typeDeJong;
    variables.clear();
    for(int i=0;i<10;i++)
        variables.push_back(VariableDefinition(-100.0,100.0));
}

int Evaluator::numberOfVariables(void)
{
    return variables.count();
}


Evaluator::Evaluator(BuiltInFunctions _function)
{
    testFunction = _function;
}

double Evaluator::Custom (QVector<double> /*x*/)
{
    return 0.0;
}

double Evaluator::Quadratic (QVector<double> x)
{
    double zbroj=0.0;
    for (int iDim=0; iDim< x.size(); iDim++ )
    {
        zbroj += pow(x[iDim],2.0);
    }

    return zbroj;
}

double Evaluator::Rosenbrock (QVector<double> x)
{
    double zbroj=0.0;
    for (int iDim=0; iDim< x.size()-1; iDim++ )
    {
        zbroj +=pow(1-x[iDim],2.0)+100*pow( x[iDim+1]-pow(x[iDim],2.0) ,2.0);
    }
    return zbroj;
}

double Evaluator::Rastrigin (QVector<double> x)
{
    double zbroj=0.0;
    double A=10.0;
    for (int iDim=0; iDim< x.size(); iDim++ )
    {
        zbroj += pow(x[iDim],2.0) - A*cos(2*pi*x[iDim]);
    }
    zbroj=zbroj+A*x.size();
    return zbroj;
}

double Evaluator::DeJong (QVector<double> x)
{
    double zbroj=0.0;
    for (int iDim=0; iDim< x.size(); iDim++ )
    {
        zbroj +=(iDim+1)*( pow(x[iDim],2.0) );
    }
    return zbroj;
}


double Evaluator::evaluate(QVector<double> x)
{
    if(testFunction == typeQuadratic)
    {
        return Quadratic(x);
    }
    else if(testFunction == typeRosenbrock)
    {
        return Rosenbrock(x);
    }
    else if(testFunction == typeRastrigin)
    {
        return Rastrigin(x);
    }
    else if(testFunction == typeDeJong)
    {
        return DeJong(x);
    }
    return 0.0;
}
