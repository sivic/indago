#include "variabledefinition.h"

VariableDefinition::VariableDefinition(void) : m_type(VariableType::undefined)
{
}

VariableDefinition::VariableDefinition(double _lowerBound, double _upperBound) : m_type(VariableType::bounded)
{
    m_lowerBound = std::min(_lowerBound,_upperBound);
    m_upperBound = std::max(_lowerBound,_upperBound);
}

VariableDefinition::VariableDefinition(QVector< double > _discreteValues) : m_type(VariableType::bounded)
{
    m_discreteValues = _discreteValues;
}

VariableType VariableDefinition::type(void )
{
    return m_type;
}


void VariableDefinition::setBounds(double _lowerBound, double _upperBound)
{
    m_lowerBound = _lowerBound;
    m_upperBound = _upperBound;
}

double VariableDefinition::lowerBound(void )
{
    return m_lowerBound;
}

double VariableDefinition::upperBound(void)
{
    return m_upperBound;
}

void VariableDefinition::setDiscreteValues(QVector< double > _discreteValues)
{
    m_discreteValues = _discreteValues;
}

QVector< double > VariableDefinition::discreteValues(void )
{
    return m_discreteValues;
}

