#ifndef PSOWIDGET_H
#define PSOWIDGET_H

#include <QWidget>

#include "../pso/psoparameters.h"

namespace Ui {
class PsoWidget;
}

class PsoWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit PsoWidget(PsoParameters* _parameters, QWidget* _parent = 0);
    ~PsoWidget();
    
private:
    Ui::PsoWidget *ui;
    PsoParameters* psoParameters;
    void refreshGui(void);
    
signals:
    void cancelClicked(void);
    void acceptClicked(void);
    
public slots:
    void on_butCancel_clicked(void);
    void on_butAccept_clicked(void);
};

#endif // PSOWIDGET_H
