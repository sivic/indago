#ifndef EVALUATORWIDGET_H
#define EVALUATORWIDGET_H

#include <QWidget>

#include "../evaluator/evaluator.h"

namespace Ui {
class EvaluatorWidget;
}

class EvaluatorWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit EvaluatorWidget(Evaluator* _evaluator, QWidget* _parent = 0);
    ~EvaluatorWidget();
    
private:
    Ui::EvaluatorWidget *ui;
    Evaluator* evaluator;
    void refreshGui(void);
    
signals:
    void cancelClicked(void);
    void acceptClicked(void);
    
public slots:
    void on_butCancel_clicked(void);
    void on_butAccept_clicked(void);
};

#endif // EVALUATORWIDGET_H
