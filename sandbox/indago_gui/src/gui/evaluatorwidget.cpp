#include "evaluatorwidget.h"
#include "ui_evaluatorwidget.h"

EvaluatorWidget::EvaluatorWidget(Evaluator* _evaluator, QWidget* _parent) :
    QWidget(_parent),
    ui(new Ui::EvaluatorWidget)
{
    ui->setupUi(this);

    evaluator = _evaluator;
    refreshGui();
}

EvaluatorWidget::~EvaluatorWidget()
{
    delete ui;
}

void EvaluatorWidget::refreshGui(void )
{
    ui->comboFunction->setCurrentIndex(evaluator->testFunction);
    ui->spinNumberOfDimensions->setValue(evaluator->variables.count());
    ui->txtLowerBound->setText(QString::number(evaluator->variables.first().lowerBound()));
    ui->txtUpperBound->setText(QString::number(evaluator->variables.first().upperBound()));
}


void EvaluatorWidget::on_butCancel_clicked(void )
{
    emit cancelClicked();
}

void EvaluatorWidget::on_butAccept_clicked(void )
{
    evaluator->testFunction = (BuiltInFunctions)ui->comboFunction->currentIndex();
    evaluator->variables.clear();
    int nDim = ui->spinNumberOfDimensions->value();
    double lb = ui->txtLowerBound->text().toDouble();
    double ub = ui->txtUpperBound->text().toDouble();
    for(int iDim=0; iDim<nDim; iDim++)
    {
        evaluator->variables.push_back(VariableDefinition(lb,ub));
    }
    emit acceptClicked();
}
