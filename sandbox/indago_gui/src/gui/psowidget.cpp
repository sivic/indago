#include "psowidget.h"
#include "ui_psowidget.h"

PsoWidget::PsoWidget(PsoParameters* _parameters, QWidget* _parent) :
    QWidget(_parent),
    ui(new Ui::PsoWidget)
{
    ui->setupUi(this);
    
    psoParameters = _parameters;
    refreshGui();
}

PsoWidget::~PsoWidget()
{
    delete ui;
}

void PsoWidget::refreshGui(void )
{
    ui->spinSwarmSize->setValue(psoParameters->swarmSize);
    ui->spinInertiaWeightFactor->setValue(psoParameters->weightInertia);
    ui->spinGlobalBestWeightFactor->setValue(psoParameters->weightGlobalBest);
    ui->spinPersonalBestWeightFactor->setValue(psoParameters->weightPersonalBest);
    ui->spinMaximumIterations->setValue(psoParameters->maxIterations);
    ui->txtTargetedFunctionValue->setText(QString::number(psoParameters->targetedFunctionValue));
    ui->txtTargetedVelocityValue->setText(QString::number(psoParameters->targetedVelocityValue ));
}


void PsoWidget::on_butCancel_clicked(void )
{
        emit cancelClicked();
}

void PsoWidget::on_butAccept_clicked(void )
{
    psoParameters->swarmSize = ui->spinSwarmSize->value();
    psoParameters->weightInertia = ui->spinInertiaWeightFactor->value();
    psoParameters->weightGlobalBest = ui->spinGlobalBestWeightFactor->value();
    psoParameters->weightPersonalBest = ui->spinPersonalBestWeightFactor->value();
    psoParameters->maxIterations = ui->spinMaximumIterations->value();
    psoParameters->targetedFunctionValue = ui->txtTargetedFunctionValue->text().toDouble();
    psoParameters->targetedVelocityValue=ui->txtTargetedVelocityValue->text().toDouble();

    emit acceptClicked();
}
