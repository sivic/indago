#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDateTime>
#include <QDialog>

#include <time.h>

#include "../pso/pso.h"
#include "psowidget.h"
#include "evaluatorwidget.h"
#include "../pso/psotesting.h"

namespace Ui {
class MainWindow;
}



class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int dimensionTestA;
public slots:
    // Automatic connection: void on_<widget name="">_<signal name="">(<signal parameters="">);

    // Indago menu
    void on_actionClearLog_triggered(void);
    void on_actionExit_triggered(void);

    // Evaluator menu
    void on_actionEvaluatorSettings_triggered(void);

    // PSO menu
    void on_actionPSOwidget_triggered(void);
    void on_actionTestA_triggered(void);
    void on_actionPsoParametersTesting_triggered(void);
    
    // Other slots
    void appendToLog(QString _message);
    void updateProgressbar(int _progress);
    void setProgressBarMaxValue(int _maxValue);
    
private:
    Ui::MainWindow *ui;
    PsoParameters psoParameters;
    Evaluator evaluator;
};

#endif // MAINWINDOW_H
