#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    qRegisterMetaType<QVector<double> >("QVector<double>");

    //QMetaObject::connectSlotsByName(this);
    on_actionClearLog_triggered();
    
    qsrand ( time(NULL) );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionExit_triggered(void)
{
    this->close();
}


void MainWindow::on_actionTestA_triggered(void)
{
    ui->txtLog->appendHtml("    ");
    ui->txtLog->appendHtml("<b>Test A</b> started");
    ui->txtLog->appendHtml("swarmSize: "+QString::number( psoParameters.swarmSize));
    ui->txtLog->appendHtml("targetedFunctionValue: "+QString::number( psoParameters.targetedFunctionValue));
    ui->txtLog->appendHtml("weightGlobalBest: "+QString::number( psoParameters.weightGlobalBest));
    ui->txtLog->appendHtml("weightPersonalBest: "+QString::number( psoParameters.weightPersonalBest));
    ui->txtLog->appendHtml("weightInertia: "+QString::number( psoParameters.weightInertia));
    ui->txtLog->appendHtml("Maximum iteration: "+QString::number( psoParameters.maxIterations));

    qsrand ( time(NULL) );
    Pso* newPSO = new Pso;
    newPSO->evaluator=&evaluator; // Upotreba evaluatora definiranog u GUI-u
   
    newPSO->psoParameters = psoParameters;
    newPSO->numberOfRuns = 100;

    psoParameters.activeTestA=true;

    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(newPSO->numberOfRuns);
    ui->progressBar->setValue(0);
    
    //connect(newPSO, SIGNAL(iterationFinished(int)), this, SLOT(updateProgressbar(int)),Qt::QueuedConnection);
    connect(newPSO, SIGNAL(runFinished(int)), this, SLOT(updateProgressbar(int)),Qt::QueuedConnection);
    
    connect(newPSO, SIGNAL(textOutput(QString)), this, SLOT(appendToLog(QString)),Qt::QueuedConnection);
    connect(ui->butStopOptimization, SIGNAL(clicked()),newPSO,SLOT(requestStop()),Qt::QueuedConnection);

    newPSO->start();
}

void MainWindow::on_actionClearLog_triggered(void )
{
    ui->txtLog->clear();
    
    ui->txtLog->appendHtml("");
    ui->txtLog->appendHtml("<span style=\"color:red;\"><b>Indago</b></span>");
    ui->txtLog->appendHtml("");
    ui->txtLog->appendHtml("<i>"+QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + "</i>");
    ui->txtLog->appendPlainText("-----------------------------------");
}


void MainWindow::appendToLog(QString _message)
{
    ui->txtLog->appendHtml("<i><span style=\"color:grey;\">"+QDateTime::currentDateTime().toString("hh:mm:ss")+"</span></i> "+_message);
}

void MainWindow::updateProgressbar(int _progress)
{
    ui->progressBar->setValue(_progress);
}

void MainWindow::setProgressBarMaxValue(int _maxValue)
{
    ui->progressBar->setMaximum(_maxValue);
}


void MainWindow::on_actionPSOwidget_triggered(void )
{
    QDialog* psoDialog = new QDialog;
    PsoWidget* psoWidget = new PsoWidget(&psoParameters);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(psoWidget);
    psoDialog->setLayout(layout);
    connect(psoWidget,SIGNAL(cancelClicked()),psoDialog,SLOT(close()));
    connect(psoWidget,SIGNAL(acceptClicked()),psoDialog,SLOT(close()));
    psoDialog->show();
}

void MainWindow::on_actionEvaluatorSettings_triggered(void )
{
    QDialog* evaluatorDialog = new QDialog;
    EvaluatorWidget* evaluatorWidget = new EvaluatorWidget(&evaluator);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(evaluatorWidget);
    evaluatorDialog->setLayout(layout);
    connect(evaluatorWidget,SIGNAL(cancelClicked()),evaluatorDialog,SLOT(close()));
    connect(evaluatorWidget,SIGNAL(acceptClicked()),evaluatorDialog,SLOT(close()));
    evaluatorDialog->show();
}


void MainWindow::on_actionPsoParametersTesting_triggered(void )
{
    PsoTesting* testing = new PsoTesting;
    connect(testing, SIGNAL(textOutput(QString)), this, SLOT(appendToLog(QString)),Qt::QueuedConnection);
    connect(testing, SIGNAL(progressMax(int)), this, SLOT(setProgressBarMaxValue(int)),Qt::QueuedConnection);
    connect(testing, SIGNAL(progress(int)), this, SLOT(updateProgressbar(int)),Qt::QueuedConnection);
    connect(ui->butStopOptimization, SIGNAL(clicked()),testing,SLOT(requestStop()),Qt::QueuedConnection);

    ui->progressBar->setMaximum(27*7);
    
    testing->start();
}

