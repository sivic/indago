
from numpy import *
from matplotlib.pyplot import *
#from matplotlib import rc

#rc('text', usetex=True)
#rc('font', family='sans-serif')

p1 = genfromtxt("p1.txt")
p2 = genfromtxt("p2.txt")
D = genfromtxt("results.txt").T

P1, P2 = meshgrid(p1, p2)

"""
print P1.shape
print P2.shape
print D.shape
"""
minvalue = D.min()
minindex = D.argmin()
minp1 = P1.ravel()[minindex]
minp2 = P2.ravel()[minindex]
print 'Minimum value D=',   minvalue, 'found at:'
print 'p1=',  minp1
print 'p2=',  minp2


figure(figsize=(12,8))
#axes()
lvls = np.array([15000,20000,25000,30000,40000,50000,60000,70000,80000])

c1 = contourf(P1, P2, D, lvls, alpha=.75, cmap=cm.hot)
c2 = contour(P1, P2, D, lvls, colors='black', linewidth=.5)
clabel(c2, inline=True, fontsize=12, rightside_up=True, fmt='%d', manual=False)

title('Average number of functions calls to find minimum of\n10 dimensional Rastrigin function to precission of 10e-5')
xlabel('Weight Global Best')
ylabel('Weight Inertia')
#xlabel(r'w_{GB}')
#ylabel(r'w_I')
colorbar(c1, orientation='vertical')
grid()
xlim(-0.1,3)

plot(minp1,minp2,'sy')
note = '%d at\n p1=%.2f,\n p2=%.2f' % (minvalue, minp1, minp2)

text(minp1+0.01, minp2-0.03,note,color='y')

savefig("tes01.png")
show()