from numpy import *
from matplotlib.pyplot import *

P = genfromtxt("position.txt")
V = genfromtxt("velocity.txt")
nDim = P.shape[1]
swarmSize = 50;
nIter =  P.shape[0]/swarmSize

print P.shape
print V.shape
print nDim
print nIter

v = empty([nIter,1])
for iIter in range(nIter):
  velSum = 0.0
  for iPartcle in range(swarmSize):
    vel = 0.0
    for iDim in range(nDim):
      vel = vel + abs(V[iIter*swarmSize+iPartcle,iDim])#**2
    #vel = sqrt(vel)
    vel = vel/nDim
    #print vel
    velSum = velSum + vel
  #print allPos/swarmSize
  v[iIter] = velSum/swarmSize
  
figure(figsize=(12,8))
plot(range(nIter),v,linewidth=2)
yscale('log')
xlabel('Iterations')
ylabel('Velocity')
grid('on')
show()