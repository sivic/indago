#include "PyMultithreaded.h"

#include <QTimer>
#include <iostream>

PyThread::PyThread(PyInterpreterState *_mainInterpreterState )
{
    PyEval_AcquireLock();
    myThreadState = PyThreadState_New(_mainInterpreterState);
    PyEval_ReleaseLock();
}

void PyThread::run(void )
{    
    PyEval_AcquireLock();
    PyThreadState_Swap(myThreadState);
    
    PyRun_SimpleString(
        "import time\n"
        "sum"+name.toAscii()+" = 0\n"
        "for i"+name.toAscii()+" in range(5):\n"   
        "    sum"+name.toAscii()+" = sum"+name.toAscii()+" +i"+name.toAscii()+"\n"
        "    time.sleep(1)\n"
        
        "print '"+name.toAscii()+"', sum"+name.toAscii()+"\n"
    );
    
    PyThreadState_Swap(NULL);
    PyEval_ReleaseLock();
}


PyMultithreaded::PyMultithreaded()
{
}

void PyMultithreaded::run(void )
{
    Py_Initialize();    
    PyEval_InitThreads();
    PyThreadState *mainThreadState = PyThreadState_Get();
    PyEval_ReleaseLock();
    PyInterpreterState *mainInterpreterState = mainThreadState->interp;
    /*
    PyRun_SimpleString(
        "print 'hello'\n"
        "import time\n"
    );
    */
    PyThread* threadA = new PyThread(mainInterpreterState);
    threadA->name="A";    
    PyThread* threadB = new PyThread(mainInterpreterState);
    threadB->name="B";    
    PyThread* threadC = new PyThread(mainInterpreterState);
    threadC->name="C";
    
    threadA->start();
    threadB->start();
    threadC->start();
    
    threadA->wait();
    threadB->wait();
    threadC->wait();
    
    PyEval_RestoreThread(mainThreadState);
    Py_Finalize();
}


#include "PyMultithreaded.moc"
