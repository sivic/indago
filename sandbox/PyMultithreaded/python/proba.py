'''proba.py - Python source designed to '''
'''demonstrate the use of python embedding'''
import numpy

def multiply(a,b):
    print "Will compute", a, "times", b
    c = 0
    for i in range(0, a):
        c = c + b
    return c

def function(x,y):
  s = 0.0
  for i in range(len(x)):
    s = s + x[i]*x[i]
  s = s + y.shape[0] + y.shape[1]
  return s