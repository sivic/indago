#ifndef PyMultithreaded_H
#define PyMultithreaded_H

#include <QtCore/QObject>
#include <QThread>
#include <QString>
#include <Python.h>

class PyThread: public QThread
{
    Q_OBJECT
public:
    PyThread(PyInterpreterState *_mainInterpreterState);
    PyThreadState *myThreadState;
    
    QString name;
    void run(void);
};

class PyMultithreaded : public QObject
{
Q_OBJECT
public:
    PyMultithreaded();
    
    void run(void);
};

#endif // PyMultithreaded_H
