#include <QCoreApplication>
#include "PyMultithreaded.h"


int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    PyMultithreaded foo;
    foo.run();
    return app.exec();
}
