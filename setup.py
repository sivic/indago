#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Indago
Python framework for numerical optimization
https://indago.readthedocs.io/
https://pypi.org/project/Indago/

Description: Indago contains several modern methods for real fitness function optimization over a real parameter domain
and supports multiple objectives and constraints. It was developed at the University of Rijeka, Faculty of Engineering.
Authors: Stefan Ivić, Siniša Družeta, Luka Grbčić
Contact: stefan.ivic@riteh.uniri.hr
License: MIT

File content: Indago PyPI setup script.
Usage: 

"""


from setuptools import setup, find_packages
# from distutils.core import setup
import os
import indago

print(f'Detected Indago version: {indago.__version__}')

with open('readme.md', 'r', encoding='utf-8') as fh:
    long_description = fh.read()
    
with open('docs/source/index.rst', 'r', encoding='utf-8') as fh:
    rst_lines = fh.readlines()
rst_lines[5] = f'Welcome to Indago {indago.__version__} documentation!\n'
rst_lines[6] = '=' * (len(rst_lines[5]) - 1) + '\n'

with open('docs/source/index.rst', 'w', encoding='utf-8') as fh:
    fh.write(''.join(rst_lines))

# # Iterate directory
# dir_path = 'indagobench/cec2014_data'
# data_files = []
# for path in os.listdir(dir_path):
#     # check if current path is a file
#     if os.path.isfile(os.path.join(dir_path, path)):
#         data_files.append(f'{dir_path}/{path}')
# #print(f'{data_files=}')

project_urls = {
  'Documentation': 'https://indago.readthedocs.io/',
  'GitLab': 'https://gitlab.com/sivic/indago'
}

setup(name='Indago',
      version=indago.__version__,
      description='Numerical optimization framework',
      author='Department of fluid mnechanics and computational engineering, Faculty of engineering, University of Rijeka',
      author_email='stefan.ivic@uniri.hr',
      project_urls=project_urls,
      py_modules=['indago',
                  #'indago.optimizer',
                  #'indago.pso',
                  #'indago.fwa',
                  #'indago.abca',
                  #'indago.ssa',
                  #'indago.direct_search',
                  #'indago.de',
                  #'indago.e3o',
                  #'indago.benchmarks',
                  ],
      setup_requires=['wheel'],
      install_requires=['numpy>=1.22.2',
                        'matplotlib>=3.6',
                        'scipy>=1.6.3',
                        'rich>=12.5'
                        ],
      long_description=long_description,
      long_description_content_type="text/markdown",
      classifiers=[
          "Programming Language :: Python :: 3",
          "License :: OSI Approved :: MIT License",
          "Operating System :: OS Independent",
          ],
      python_requires='>=3.9',
      #data_files=[('indago/benchmarks/cec2014_data', data_files)],
      packages=find_packages(exclude=["indagobench"]),
      #package_data={'indago': ['benchmarks/cec2014_data/*.txt']},
      #include_package_data=True,
)
