#! /bin/bash

echo "-------- Building Indago --------"
echo " Please choose target: TestPyPI/[PyPI]:"
read target

if [ -z "$target" ];
then
target="PyPI"
fi

if [[ $target != "TestPyPI" ]] && [[ $target != "PyPI" ]];
then
echo "Unsupported target: " $target
exit
fi
echo "Using target " $target


rm -r build dist Indago.egg-info 
python setup.py clean bdist_wheel

if [[ $target == "TestPyPI" ]];
then
python3 -m twine upload --repository testpypi dist/*
python3 -m pip install --upgrade --force-reinstall --index-url https://test.pypi.org/simple/ indago
fi

if [[ $target == "PyPI" ]];
then
#python3 -m twine upload --repository-url https://upload.pypi.org/legacy/ dist/*
twine upload --repository indago dist/*
fi
