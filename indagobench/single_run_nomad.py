import sys
import os
import pathlib


sys.path.append('..')
sys.path.append('../..')

import shutil
import glob
import indagobench.nomad_wrapper_parallel as nw
# import nomad_f_generator as n_gen
import indagobench
from indagobench import _local_paths


def init_nomad_evaluator(function_dict, instance_label):
    funcname = function_dict['label']
    gen_fun = f"""
import sys
sys.path.append('..')
sys.path.append('../..')
sys.path.append('{pathlib.Path(__file__).parent.resolve()}')

import numpy as np
import indagobench
function = indagobench.st24_function_dict_list    

    
indx = [i for i in range(len(function)) if function[i]['label'] == '{funcname}']
function_dict = function[indx[0]]
instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'], instance_label='instance_label')
x = np.loadtxt(sys.argv[1])
f = instance(x)
print (f)

with open("evaluations.log", "a") as log:
    log.write(sys.argv[1] + ' ' + str(x) + ' ' + str(f) + '\\n')
"""

    # f = self.dict_value['gen'](self.dict_value['label'])
    f = function_dict['class'](function_dict['problem'], function_dict['dimensions'], instance_label=instance_label)
    lb = f.lb
    ub = f.ub
    D = len(f.ub)
    evals = function_dict['max_evaluations']

    return gen_fun, int(evals), int(D), lb, ub
def single_nomad_batch_run(function_dict, optimizer_dict, instance_label):

    # print(function_dict)
    # print(instance_label)
    for case_dir in glob.glob('case_*'):
        shutil.rmtree(case_dir)

    f, evals, D, lb, ub = init_nomad_evaluator(function_dict, instance_label)

    nomad = nw.nomad_wrapper(f, D, lb, ub, function_dict['max_evaluations'],
                             nomad_path=_local_paths.nomad_binary_path,
                             instance_label=instance_label)
    f_best, best_X  = nomad.run_job(instance_label)

    if os.path.exists(nomad.working_dir):
        shutil.rmtree(nomad.working_dir)

    return f_best, best_X


if __name__ == '__main__':

    f, x_best = single_nomad_batch_run(indagobench.EmpReg_function_dict_list[0],
                           None, 'test_run')
    print(f)
    print(x_best)





