import numpy as np
import os
import shutil
import multiprocessing
import sys 
import subprocess
import re 
import inspect
import scipy.interpolate as interp
import string
import random
import sys

class nomad:
        
    def __init__(self, f, D, lb, ub, evals, label, nomad_path=None):
        
        self.f = f
        self.D = D
        self.lb = lb
        self.ub = ub
        self.evals = evals
        self.label = label
        self.nomad_path = nomad_path 
        
    def setup_case(self):
        
        rng =''.join(random.choices(string.ascii_letters, k=5))
        folder_name = f'{self.label}_{rng}'
        os.makedirs(folder_name, exist_ok=True)
        script_name = f'{folder_name}/run_{rng}.py'
        param_file_name = f'{folder_name}/parameters_{rng}.txt'
        
        return script_name, param_file_name, folder_name    
    
    
    def gen_f_script(self, script_name):
        
        # print (script_name)
        
        with open(f'{script_name}', 'w') as file:
            file.write(f'{self.f}')
   
        return script_name
        
    
    def gen_param_file(self, script_name, param_file_name):
        
        bb = self.gen_f_script(script_name)
        bb = os.path.basename(bb)
        x0 = '( ' + ' '.join(map(str, np.round(np.random.uniform(self.lb, self.ub), decimals=3))) + ' )'
        lb = '( ' + ' '.join(map(str, self.lb)) + ' )'
        ub = '( ' + ' '.join(map(str, self.ub)) + ' )'
        
        with open(f'{param_file_name}', 'w') as file:
            
            file.write(f'DIMENSION \t {self.D}\n')
            file.write(f'BB_EXE \t "$python {bb}"\n')
            file.write(f'BB_OUTPUT_TYPE \t OBJ \n')
            file.write(f'X0 \t {x0}\n')
            file.write(f'LOWER_BOUND \t {lb}\n')
            file.write(f'UPPER_BOUND \t {ub}\n')
            file.write(f'MAX_BB_EVAL \t {self.evals}\n')
            file.write(f'DISPLAY_DEGREE 2\n')
            file.write(f'DISPLAY_STATS BBE ( SOL ) OBJ\n')
            
    def run_nomad(self):
        
        script_name, param_file_name, folder_name = self.setup_case()
        self.gen_param_file(script_name, param_file_name)
        param_file_name = param_file_name.split('/')[1]
        cmd = f'cd {folder_name}; {self.nomad_path} {param_file_name}'
        result = subprocess.run(['bash', '-i', '-c', cmd], text=True, capture_output=True, check=True)

        pattern = r'(\d+)\t\(([\s\S]*?)\)\t\s*([-?\d.]+)'
        matches = re.findall(pattern, result.stdout)
        data = np.array([np.array(i) for i in matches])

        evals = np.array([int(data[i, 0]) for i in range(len(data))])
        f_ = np.array([float(data[i, -1]) for i in range(len(data))])
        evals_f = np.hstack((evals.reshape(-1,1), f_.reshape(-1,1)))
        max_eval = self.evals  
        filled_matrix = []
        last_num = int(evals_f[0][0])
        value_to_fill = evals_f[0][1]
        
        for row in evals_f:
            current_num = int(row[0])
            
            while last_num < current_num:
                filled_matrix.append([float(last_num), value_to_fill])
                last_num += 1
            
            value_to_fill = row[1]
            filled_matrix.append(row)
            last_num = current_num + 1
        
        while last_num <= max_eval:
            filled_matrix.append([float(last_num), value_to_fill])
            last_num += 1
            
        filled_matrix = np.array(filled_matrix)
        filled_matrix[:,0] = filled_matrix[:,0] - 1 #if evals from 1 to 100, remove this line
        
        best_X = data[-1, 1:-1][0].split()
        best_X = np.array([float(best_X[i]) for i in range(len(best_X))])
        
        interp_f = interp.interp1d(filled_matrix[:,0], 
                                    filled_matrix[:,1],
                                    bounds_error=False,
                                    fill_value=np.nan)
        
        f_best = interp_f(np.linspace(0, self.evals, 101))

        return f_best, best_X
    

        
        
