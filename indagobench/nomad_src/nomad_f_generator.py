import numpy as np

    
class nomad_f_generator:
    
    def __init__(self, dict_):
               
        self.dict_ = dict_
        
    def get_values(self):
       
        class_ = self.dict_ ['label'].split('_')[0]
        label = self.dict_ ['label']


        gen_fun = f"""
import sys
sys.path.append('../../../')
import indagobench as indago_benchmarks
import numpy as np


function_dict = indago_benchmarks.{class_}_function_dict_list
indx = [i for i in range(len(function_dict)) if function_dict[i]['label'] == '{label}'][0]
function_dict = indago_benchmarks.{class_}_function_dict_list[indx]
instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'],
                           instance_label=function_dict['label'])

print (instance(np.loadtxt(sys.argv[1])))

"""

        instance = self.dict_['class'](self.dict_['problem'], self.dict_['dimensions'],
                                   instance_label=self.dict_['label'])
        lb = instance.lb
        ub = instance.ub
        D = instance.dimensions
        evals = self.dict_['max_evaluations']
        label = self.dict_['label']

        return gen_fun, int(evals), int(D), lb, ub, label
        


