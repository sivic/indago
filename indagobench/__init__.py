# import sys
# sys.path.append('../indago')

from ._cec2014 import CEC2014, cec2014_10d_function_dict_list
from ._shortest_path import ShortestPath, shortest_path_function_dict_list
from ._hydraulic_network import HydraulicNetwork, hydraulic_network_function_dict_list
from ._analytical_engineering_problems import *
from ._empirical_regression import EmpReg, EmpReg_function_dict_list
from ._ill_posed_problems import IPP_all_function_dict_list
from ._structural_frame import StructuralFrame, structural_frame_function_dict_list
from ._ergodic import ergodic_function_dict_list
from ._airfoil_design import AirfoilDesign, AirfoilDesign_function_dict_list
from ._packing_problems import PP, PP_function_dict_list
from ._standard_test import StandardTest
#from ._standard_test_functions import st24_function_dict_list, shortest_path_function_dict_list
from ._standard_test_optimizers import st24_optimizers_list
from ._packing_problems import PP_function_dict_list
# st24_function_dict_list = []
# st24_function_dict_list.extend(_cec2014.cec2014_10d_function_dict_list)
# st24_function_dict_list.extend(AEP_function_dict_list)
# st24_function_dict_list.extend(EmpReg_function_dict_list)
# st24_function_dict_list.extend(IPP_all_function_dict_list)
# st24_function_dict_list.extend(shortest_path_function_dict_list)
# st24_function_dict_list.extend(hydraulic_network_function_dict_list)
# st24_function_dict_list.extend(structural_frame_function_dict_list)
# st24_function_dict_list.extend(AirfoilDesign_function_dict_list)
