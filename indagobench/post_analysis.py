import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os

# PATH = r"C:\Users\Sinisa\AOSeR Dropbox\Siniša Družeta\Research\indago_st24_paper\indagobench24_results\summary"  # sinisa laptop
PATH = r"C:\Users\user\AOSeR Dropbox\Siniša Družeta\Research\indago_st24_paper\indagobench24_results\summary"  # sinisa faks

METHODS = ['RS', 'L-BFGS-B', 'ZOOpt', 'DA', 'ESCH', 'CRS', 'STOGO', 'NM GaoHan', 'MSGD', 'PSO',
           'FWA', 'SSA', 'BA', 'GWO', 'EFO', 'MRFO', 'ABC', 'DE LSHADE', 'GA', 'CMAES']

M_NAME = {'RS': 'RS', 'L-BFGS-B': 'LBFGSB', 'ZOOpt': 'ZOOPT', 'DA': 'DA', 'ESCH': 'ESCH', 'CRS': 'CRS',
          'STOGO': 'STOGO', 'NM GaoHan': 'NM', 'MSGD': 'MSGD', 'PSO': 'PSO', 'FWA': 'FWA', 'SSA': 'SSA', 'BA': 'BA',
          'GWO': 'GWO', 'EFO': 'EFO', 'MRFO': 'MRFO', 'ABC': 'ABC', 'DE LSHADE': 'LSHADE', 'GA': 'GA', 'CMAES': 'CMAES'}

DATA = {fname: {'name': fname[8:-4], 'D': int(fname[:-5].split('_')[-1])} for fname in os.listdir(PATH)}


def G_of_D():

    print('\n*** G(D) plot')

    res = {m: {'G_100': [], 'D': []} for m in METHODS}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        for i in range(len(METHODS)):
            res[METHODS[i]]['G_100'].append(G_100[i])
            res[METHODS[i]]['D'].append(info['D'])

    x = np.min(res['RS']['D'])
    colors = plt.cm.jet(np.linspace(0, 1, len(METHODS)))
    plt.figure(figsize=(8, 16))
    for m, c in zip(METHODS, colors):
        pearson = np.corrcoef(res[m]['D'], res[m]['G_100'])[0, 1]
        reg = np.polynomial.Polynomial.fit(res[m]['D'], res[m]['G_100'], deg=1)
        # plt.plot(res[m]['D'], res[m]['G_100'], 'o', alpha=0.9, color=c)
        d = np.linspace(np.min(res[m]['D']), np.max(res[m]['D']), 50)
        plt.plot(d, reg(d), label=M_NAME[m], color=c)
        plt.text(x, reg(x), f'{M_NAME[m]} r={pearson:.2f}', color=c, fontsize=8,
                 bbox=dict(facecolor='white', edgecolor='none', alpha=0.8))
        x += (d[-1] - d[0]) / len(METHODS)
    # plt.legend()
    plt.xlabel(r'$D$')
    plt.ylabel(r'$G_{100}$')
    plt.title(r'$G_{100}$ vs. $D$')
    plt.tight_layout()
    plt.savefig('post_analysis_G(D).png')


def G_of_M():

    print('\n*** G(M) plot')

    res = {m: {'G_100': [], 'M': []} for m in METHODS}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        for i in range(len(METHODS)):
            res[METHODS[i]]['G_100'].append(G_100[i])
            res[METHODS[i]]['M'].append((1 - G_100[1]) / 2)

    colors = plt.cm.rainbow(np.linspace(0, 1, len(METHODS)))
    fig, axes = plt.subplots(ncols=5, nrows=4, figsize=(5 * 2.4, 4 * 2.4 + 1))
    for i in range(len(METHODS)):
        m = METHODS[i]
        c = colors[i]
        ax = axes.flat[i]
        pearson = np.corrcoef(res[m]['M'], res[m]['G_100'])[0, 1]
        reg = np.polynomial.Polynomial.fit(res[m]['M'], res[m]['G_100'], deg=1)
        ax.plot(res[m]['M'], res[m]['G_100'], 'o', alpha=0.4, color=c)
        m_ = np.linspace(np.min(res[m]['M']), np.max(res[m]['M']), 50)
        ax.plot(m_, reg(m_), label=fr'{M_NAME[m]}\n $r={pearson:.2f}$', color='black')
        ax.title.set_text(f'{M_NAME[m]}  $r={pearson:.2f}$')
        ax.set_ylim(-1, 1)
        ax.tick_params(bottom=False, labelbottom=False, left=False, labelleft=False)
    # fig.suptitle(r'$G_{100}$ vs. $M$')
    fig.tight_layout()
    fig.savefig('post_analysis_G(M).png')


def CEC_vs_nonCEC():

    print('\n*** CEC vs non-CEC histogram')

    res = {'G_100_cec': [], 'G_100_noncec': []}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        if info['name'].startswith('CEC2014'):
            res['G_100_cec'] += G_100[1:].tolist()  # include every result
            # res['G_100_cec'].append(np.average(G_100[1:]))  # take mean result
        else:
            res['G_100_noncec'] += G_100[1:].tolist()  # include every result
            # res['G_100_noncec'].append(np.average(G_100[1:]))  # take mean result

    plt.figure(figsize=(12, 8))
    plt.hist(res['G_100_cec'], bins=50, label='CEC', alpha=0.5, color='green')
    plt.hist(res['G_100_noncec'], bins=50, label='non-CEC', alpha=0.5, color='red')
    med_cec = np.median(res['G_100_cec'])
    med_noncec = np.median(res['G_100_noncec'])
    plt.axvline(med_cec, color='green', linestyle='--', linewidth=3, label='median CEC')
    plt.axvline(med_noncec, color='red', linestyle='--', linewidth=3, label='median non-CEC')
    plt.legend()
    plt.xlabel(r'$G_{100}$')
    plt.ylabel(r'f')
    plt.title(rf'CEC vs non-CEC ($\Delta G = {med_cec - med_noncec:.2f}$)')
    plt.tight_layout()
    plt.savefig('post_analysis_CEC.png')


def D_hist():

    print('\n*** D histogram')

    D = []
    for _, info in DATA.items():
        D.append(info['D'])

    plt.figure(figsize=(4, 2))
    plt.hist(D, bins=50, color='violet')
    plt.xlim(0, 60)
    plt.xlabel(r'$D$')
    plt.ylabel('number of functions')
    plt.tight_layout()
    plt.savefig('post_analysis_Dhist.png')


def H_hist():

    print('\n*** function hardness H = M*D histogram')

    D, M = [], []
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        D.append(info['D'])
        M.append((1 - G_100[1]) / 2)
    H = np.array(M) * np.array(D)

    plt.figure(figsize=(4, 2))
    plt.hist(H, bins=60, color='tomato')
    plt.xlim(0, 60)
    plt.xlabel(r'function hardness $H = M \cdot D$')
    plt.ylabel(r'number of functions')
    plt.tight_layout()
    plt.savefig('post_analysis_Hhist.png')


def G_of_H():

    print('\n*** G(H) plots')

    res = {m: {'G_100': [], 'H': []} for m in METHODS}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        for i in range(len(METHODS)):
            res[METHODS[i]]['G_100'].append(G_100[i])
            res[METHODS[i]]['H'].append(info['D'] * (1 - G_100[1]) / 2)
    colors = plt.cm.rainbow(np.linspace(0, 1, len(METHODS)))
    fig, axes = plt.subplots(ncols=5, nrows=4, figsize=(5 * 2.4, 4 * 2.4 + 1))
    for i in range(len(METHODS)):
        m = METHODS[i]
        c = colors[i]
        ax = axes.flat[i]
        pearson = np.corrcoef(res[m]['H'], res[m]['G_100'])[0, 1]
        reg = np.polynomial.Polynomial.fit(res[m]['H'], res[m]['G_100'], deg=1)
        ax.plot(res[m]['H'], res[m]['G_100'], 'o', alpha=0.4, color=c)
        m_ = np.linspace(np.min(res[m]['H']), np.max(res[m]['H']), 50)
        ax.plot(m_, reg(m_), label=fr'{M_NAME[m]}\n $r={pearson:.2f}$', color='black')
        ax.axhline(0, color='grey', linestyle=':')
        ax.title.set_text(f'{M_NAME[m]}  $r={pearson:.2f}$')
        ax.set_ylim(-1, 1)
        ax.tick_params(bottom=False, labelbottom=False, left=False, labelleft=False)
    # fig.suptitle(r'$G_{100}$ vs. $H$')
    fig.tight_layout()
    fig.savefig('post_analysis_G(H).png')


def method_performance():

    print('\n*** method performance bar plots')

    wins, succ, lasts, fail, wins90perc, succ90perc = {m: 0 for m in METHODS}, {m: 0 for m in METHODS}, \
        {m: 0 for m in METHODS}, {m: 0 for m in METHODS}, {m: 0 for m in METHODS}, {m: 0 for m in METHODS}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        wins[METHODS[np.argmax(G_100)]] += 1
        lasts[METHODS[np.argmin(G_100)]] += 1
        for i in range(len(METHODS)):
            if G_100[i] > 0.9:
                succ[METHODS[i]] += 1
            elif G_100[i] < 0:
                fail[METHODS[i]] += 1

    fig, ax = plt.subplots(ncols=2, nrows=1, figsize=(5.5, 5.5))
    fig.subplots_adjust(wspace=0)
    ax[0].barh(list(M_NAME.values()), (100 * np.array(list(fail.values())) / len(DATA)),
               color='pink',
               label=r'catastrophic ($G_{100} < 0$)')
    ax[0].barh(list(M_NAME.values()), (100 * np.array(list(lasts.values())) / len(DATA)),
               color='red',
               label=r'worst of all ($G_{100} = \min$)')
    ax[0].set_xlim(0, 60)
    ax[0].invert_xaxis()
    ax[0].invert_yaxis()
    ax[1].barh(list(M_NAME.values()), (100 * np.array(list(succ.values())) / len(DATA)),
               color='lightgreen',
               label=r'excellent ($G_{100} > 0.9$)')
    ax[1].barh(list(M_NAME.values()), (100 * np.array(list(wins.values())) / len(DATA)),
               color='green',
               label=r'best of all ($G_{100} = \max$)')
    ax[1].invert_yaxis()
    ax[1].set_xlim(0, 60)
    ax[1].set_yticks([])
    fig.legend(loc='upper center', ncols=2)
    fig.supxlabel(r'relative frequency [%]')
    fig.savefig('post_analysis_method_performance.png')


def method_sets():

    print('\n*** best sets of methods')

    from itertools import combinations

    exc = {m: [] for m in METHODS}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        for i in range(len(METHODS)):
            if G_100[i] > 0.9:
                exc[METHODS[i]].append(info['name'])

    best_scores = []
    for n in range(1, 6):
        combs = list(combinations(METHODS, n))
        exc_combs = {}
        for comb in combs:
            exc_combs[comb] = []
            for m in comb:
                exc_combs[comb] += exc[m]
            exc_combs[comb] = list(set(exc_combs[comb]))  # remove duplicates
        score_combs = {comb: len(funs) / len(DATA) for comb, funs in exc_combs.items()}
        best_score = 0
        best_comb = None
        for comb, score in score_combs.items():
            if score > best_score:
                best_score = score
                best_comb = comb
        print(f'{best_comb} -> G>0.9 in {best_score:.0%} test functions')
        best_scores.append(best_score)
    best_comb = best_comb[::-1]
    best_comb = ['+'+M_NAME[m] if m!=best_comb[0] else m for m in best_comb]

    fig, ax = plt.subplots(ncols=2, nrows=1, figsize=(8, 3))
    ax[0].bar(best_comb, best_scores, color='lightgreen')
    ax[0].set_ylabel('share of functions successfully\n' + r'solved ($G_{100} > 0.9$)')
    ax[0].set_ylim(0, 0.8)

    exc = {m: [] for m in METHODS}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(7,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        for i in range(len(METHODS)):
            if G_100[i] > 0.9:
                exc[METHODS[i]].append(info['name'])

    best_scores = []
    for n in range(1, 6):
        combs = list(combinations(METHODS, n))
        exc_combs = {}
        for comb in combs:
            exc_combs[comb] = []
            for m in comb:
                exc_combs[comb] += exc[m]
            exc_combs[comb] = list(set(exc_combs[comb]))  # remove duplicates
        score_combs = {comb: len(funs) / len(DATA) for comb, funs in exc_combs.items()}
        best_score = 0
        best_comb = None
        for comb, score in score_combs.items():
            if score > best_score:
                best_score = score
                best_comb = comb
        print(f'{best_comb} -> G90>0.9 in {best_score:.0%} test functions')
        best_scores.append(best_score)
    best_comb = best_comb[::-1]
    best_comb = ['+'+M_NAME[m] if m!=best_comb[0] else m for m in best_comb]

    ax[1].bar(best_comb, best_scores, color='pink')
    ax[1].set_ylabel('share of functions successfully\n' + r'solved given 10 runs ($G90_{100} > 0.9$)')
    ax[1].set_ylim(0, 0.8)
    fig.tight_layout()
    fig.savefig('post_analysis_method_sets.png')


def method_overlap_heatmap():

    print('\n*** method performance overlaps')

    exc = {m: [] for m in METHODS}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        for i in range(len(METHODS)):
            if G_100[i] > 0.9:
                exc[METHODS[i]].append(info['name'])

    overlaps = np.ones((len(METHODS), len(METHODS)))
    for i, m1 in enumerate(METHODS):
        for j, m2 in enumerate(METHODS):
            if m1 != m2:
                # compute the overlap of m1 in m2
                doubles = [f for f in exc[m1] if f in exc[m2]]
                if len(exc[m1]) > 0:
                    overlaps[i, j] = len(doubles) / len(exc[m1])
                else:
                    overlaps[i, j] = 1  # placeholder for 0/0

    import seaborn as sns
    plt.figure(figsize=(13, 10))
    heatmap = sns.heatmap(overlaps, cmap='hot_r', annot=overlaps,
                          linewidths=0.5, linecolor='k',
                          xticklabels=M_NAME.values(), yticklabels=M_NAME.values())
    for _, spine in heatmap.spines.items():
        spine.set_visible(True)
    heatmap.set_ylabel('how much is this method...')
    heatmap.set_xlabel('...superseded by this method')
    plt.tight_layout()
    plt.savefig('post_analysis_overlap_heatmap.png')


def method_features():

    print('\n*** method features')

    NCOLS = 5
    pi = -1  # counting ax
    fig, ax = plt.subplots(ncols=NCOLS, nrows=1, figsize=(NCOLS * 2, 4))
    fig.subplots_adjust(wspace=0.05)
    color = mpl.colormaps['Set2'](np.linspace(0,1, NCOLS))

    ### uniqueness
    exc = {m: [] for m in METHODS}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        for i in range(len(METHODS)):
            if G_100[i] > 0.9:
                exc[METHODS[i]].append(info['name'])
    overlaps = np.ones((len(METHODS), len(METHODS)))
    for i, m1 in enumerate(METHODS):
        for j, m2 in enumerate(METHODS):
            if m1 != m2:
                # compute the overlap of m1 in m2
                doubles = [f for f in exc[m1] if f in exc[m2]]
                if len(exc[m1]) > 0:
                    overlaps[i, j] = len(doubles) / len(exc[m1])
                else:
                    overlaps[i, j] = np.nan
    for i in range(len(METHODS)):  # cleanup ones
        overlaps[i, i] = np.nan
    uniq = 1 - np.nanmax(overlaps, axis=1)
    uniq = np.nan_to_num(uniq)

    pi += 1
    ax[pi].barh(list(M_NAME.values()), uniq,
               color=color[pi],
               label='unique specialization')
    ax[pi].set_xlabel('share of test functions', fontsize='small')
    ax[pi].spines['top'].set_visible(False)
    ax[pi].spines['right'].set_visible(False)
    ax[pi].invert_yaxis()
    ax[pi].set_title('unique\nspecialization', fontsize='small')

    ### sensitivity to problem hardness
    res = {m: {'G_100': [], 'H': []} for m in METHODS}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        for i in range(len(METHODS)):
            res[METHODS[i]]['G_100'].append(G_100[i])
            res[METHODS[i]]['H'].append(info['D'] * (1 - G_100[1]) / 2)
    pearson = np.full(len(METHODS), np.nan)
    for i in range(len(METHODS)):
        pearson[i] = np.corrcoef(res[METHODS[i]]['H'], res[METHODS[i]]['G_100'])[0, 1]
    pearson = np.nan_to_num(pearson)

    pi += 1
    ax[pi].barh(list(M_NAME.values()), pearson,
               color=color[pi],
               label='sensitivity to problem hardness')
    ax[pi].set_xlabel('correlation coefficient', fontsize='small')
    ax[pi].invert_yaxis()
    ax[pi].invert_xaxis()
    ax[pi].set_yticks([])
    ax[pi].spines['top'].set_visible(False)
    ax[pi].spines['right'].set_visible(False)
    ax[pi].spines['left'].set_visible(False)
    ax[pi].axvline(0, color='k', linewidth=2/3, linestyle='-')
    ax[pi].set_title('problem hardness\nsensitivity', fontsize='small')
    # fig.legend(loc='upper center', ncols=2)

    ### performance stability (1-std)
    stdG = {m: [] for m in METHODS}  # {m: std(G_100)}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        for i in range(len(METHODS)):
            stdG[METHODS[i]].append(G_100[i])
    res = np.array([1 - np.std(stdG[m]) for m in METHODS])

    pi += 1
    ax[pi].barh(list(M_NAME.values()), res,
               color=color[pi],
               label='performance stability')
    ax[pi].set_xlabel(r'1 - $\sigma (G)$', fontsize='small')
    ax[pi].invert_yaxis()
    ax[pi].set_yticks([])
    ax[pi].set_xlim(0, 1)
    ax[pi].spines['top'].set_visible(False)
    ax[pi].spines['right'].set_visible(False)
    ax[pi].spines['left'].set_visible(False)
    ax[pi].axvline(0, color='k', linewidth=2 / 3, linestyle='-')
    ax[pi].set_title('performance\nstability', fontsize='small')

    ### computational complexity
    Tdata = _extract_execution_times()
    T_m, T_f, D, factor = _decompose_execution_times(Tdata)

    max_T_m, max_C_m, min_C_m = -np.inf, 0, np.inf
    for m in METHODS:
        for fun in Tdata.keys():
            if T_m[m][fun] is not None:
                if T_m[m][fun] > max_T_m:
                    max_T_m = T_m[m][fun]
                if T_m[m][fun] / T_m['RS'][fun] > max_C_m:
                    max_C_m = T_m[m][fun] / T_m['RS'][fun]
                if T_m[m][fun] / T_m['RS'][fun] < min_C_m:
                    min_C_m = T_m[m][fun] / T_m['RS'][fun]

    from scipy.optimize import curve_fit
    def freg(x, k, e, C0):
        return k * x**e + C0
    k, e, C0 = np.full(len(METHODS), np.nan), np.full(len(METHODS), np.nan), np.full(len(METHODS), np.nan)
    for i in range(len(METHODS)):
        m = METHODS[i]
        Cm = [T_m[m][info['name']] / T_m['RS'][info['name']] for info in DATA.values() if info['name'] in factor]
        (k[i], e[i], C0[i]), _ = curve_fit(freg, D, Cm,
                                  bounds=([0, 0, 1], [max_C_m, 3, max_C_m]))
        if e[i] < 1e-3:
            C0[i] = k[i] + C0[i]
            k[i] = 0

    pi += 1
    ax[pi].barh(list(M_NAME.values()), C0,
               color=color[pi],
               label='baseline relative computational complexity')
    ax[pi].set_xlabel(r'$C_0$', fontsize='small')
    ax[pi].invert_yaxis()
    ax[pi].set_yticks([])
    ax[pi].set_xlim(1, np.max(C0))
    ax[pi].set_xscale('log')
    ax[pi].spines['top'].set_visible(False)
    ax[pi].spines['right'].set_visible(False)
    ax[pi].spines['left'].set_visible(False)
    ax[pi].axvline(1, color='k', linewidth=2 / 3, linestyle='-')
    ax[pi].set_title('baseline\nrelative complexity', fontsize='small')

    pi += 1
    ax[pi].barh(list(M_NAME.values()), k,
               color=color[pi],
               label='computational complexity increase with problem dimensionality')
    ax[pi].set_xlabel(r'power over $D$', fontsize='small')
    ax[pi].invert_yaxis()
    ax[pi].set_yticks([])
    ax[pi].set_xlim(0, 3)
    ax[pi].spines['top'].set_visible(False)
    ax[pi].spines['right'].set_visible(False)
    ax[pi].spines['left'].set_visible(False)
    ax[pi].axvline(0, color='k', linewidth=2 / 3, linestyle='-')
    ax[pi].axvline(1, color='grey', linewidth=2 / 3, linestyle=':')
    ax[pi].set_title('complexity increase with\nproblem dimensionality', fontsize='small')

    ### done
    fig.tight_layout()
    plt.savefig('post_analysis_method_features.png')


def method_clustering():

    print('\n*** method clustering')

    res = {m: [] for m in METHODS}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        for i in range(len(METHODS)):
            res[METHODS[i]].append(G_100[i])

    X = np.empty((len(METHODS), len(res['RS'])))
    for i in range(len(METHODS)):
        X[i, :] = res[METHODS[i]]
    X = X[1:,:]  # remove RS

    from sklearn.cluster import SpectralClustering
    N_CLUSTERS = 7
    clusters = SpectralClustering(n_clusters=N_CLUSTERS,
                                  assign_labels='kmeans').fit(X)
    for c in range(N_CLUSTERS):
        print(f'cluster #{c}')
        for i in range(0, len(METHODS)-1):
            if clusters.labels_[i] == c:
                print(f'  {METHODS[i+1]}')


def function_clustering():

    print('\n*** method clustering')

    res = {}
    funs = []
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        funs.append(info['name'])
        res[info['name']] = G_100

    X = np.empty((len(funs), len(METHODS)))
    for i, f in enumerate(funs):
        X[i, :] = res[f]

    from sklearn.cluster import SpectralClustering
    N_CLUSTERS = 20
    CLUST = [[] for _ in range(N_CLUSTERS)]
    clusters = SpectralClustering(n_clusters=N_CLUSTERS, random_state=1,
                                  assign_labels='discretize').fit(X)
    for c in range(N_CLUSTERS):
        print(f'cluster #{c}')
        for i in range(len(funs)):
            if clusters.labels_[i] == c:
                print(f'  {funs[i]}')
                CLUST[c].append(funs[i])

    TAKE_EVERY_NTH_FUN = 5
    picked_funs = [c[::TAKE_EVERY_NTH_FUN] for c in CLUST]
    picked_funs = [x for xx in picked_funs for x in xx]  # flatten list
    picked_funs.sort()
    resm = {m: [] for m in METHODS}
    resm_reduced = {m: [] for m in METHODS}
    for fname, info in DATA.items():
        try:  # not all results are complete
            G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
        except:
            print(f"...skipping {info['name']}")
            continue
        for i in range(len(METHODS)):
            resm[METHODS[i]].append(G_100[i])
            if info['name'] in picked_funs:
                resm_reduced[METHODS[i]].append(G_100[i])

    fig, axes = plt.subplots(ncols=5, nrows=4, figsize=(5 * 2.4, 4 * 2.4 + 1))
    err = np.empty(len(METHODS))
    for i in range(len(METHODS)):
        m = METHODS[i]
        ax = axes.flat[i]
        ax.hist(resm[m], weights=np.ones_like(resm[m]) / len(resm[m]), bins=np.linspace(-1, 1, 20),
                label=r'$G_{avg}$ = ' + f'{np.mean(resm[m]):.2f}',
                color='green', alpha=0.5)
        ax.hist(resm_reduced[m], weights=np.ones_like(resm_reduced[m]) / len(resm_reduced[m]),  bins=np.linspace(-1, 1, 20),
                label=r'$G_{avg}$ = ' + f'{np.mean(resm_reduced[m]):.2f}',
                color='crimson', alpha=0.5)
        ax.legend(loc='upper left')
        ax.axvline(0, color='k', linewidth=2/3, linestyle=':')
        ax.title.set_text(f'{M_NAME[m]}')
        ax.set_xlim(-1, 1)
        ax.set_ylim(0, 0.7)
        ax.tick_params(bottom=False, labelbottom=False, left=False, labelleft=False)
        err[i] = np.abs(np.mean(resm[m]) - np.mean(resm_reduced[m]))

    fig.suptitle(f'green - full test ({len(funs)} funs); red - mini test ({len(picked_funs)} funs from {N_CLUSTERS} clusters); avg discrepancy {np.mean(err):.2f}')
    fig.tight_layout()
    fig.savefig('post_analysis_test_reduction_hist.png')

    print('\nmini test functions:')
    print('\n'.join(picked_funs))
    print(f'\ndiscrepancy: avg = {np.mean(err):.2f}; max = {np.max(err):.2f}')


def _extract_execution_times():

    SIMULATION_BASED = 'AirfoilDesign HydraulicNetwork StructuralFrame'.split(' ')

    # Tdata format -> function: [maxevals, {method: [avg(T_t), cutoff]}]
    Tdata = {info['name']: [None, {m: [None, None] for m in METHODS}] for info in DATA.values()}

    # load T_t and maxevals
    from _standard_test_functions import st24_function_dict_list
    PATH_RAW = PATH[:-8] + r'\raw'
    for _, info in DATA.items():

        # PP is incomplete
        if info['name'].startswith('PP'):
            print(f"...skipping {info['name']}")
            del Tdata[info['name']]
            continue

        # exclude simulation based functions
        EXCLUDE = False
        for prefix in SIMULATION_BASED:
            if info['name'].startswith(prefix):
                EXCLUDE = True
        if EXCLUDE:
            print(f"...skipping {info['name']}")
            del Tdata[info['name']]
            continue

        try:  # not all results are complete
            NPZ = np.load(os.path.join(PATH_RAW, info['name'] + '.npz'))
        except:
            print(f"...skipping {info['name']}")
            del Tdata[info['name']]
            continue

        for key in NPZ.keys():
            met = key[2:]
            if met == 'MSGS':
                met = 'MSGD'

            if key.startswith('f '):
                # get evaluation cutoff percentage
                fit = NPZ[key]
                if met in 'L-BFGS-B MSGD'.split(' '):
                    for i in range(np.shape(fit)[0]):
                        for j in range(1, np.shape(fit)[1]):
                            if fit[i,j] == fit[i,-1]:
                                fit[i,j] = np.nan
                cutoffs = []
                for fitrow in fit:
                    cutoffs.append(min(np.size(fitrow[np.logical_not(np.isnan(fitrow))]), 100))
                cutoff = np.ceil(np.median(cutoffs))
                # if met == 'MSGD': print('MSGD', cutoff)
                if cutoff is not None:
                    Tdata[info['name']][1][met][1] = cutoff
                else:
                    print("...cutoff is None; removing {info['name']}")
                    del Tdata[info['name']]

            elif key.startswith('t '):
                # get computation times
                Tdata[info['name']][1][met][0] = np.median(NPZ[key])

        for fun_dict in st24_function_dict_list:
            if fun_dict['label'] == info['name']:
                if fun_dict['max_evaluations'] is None:
                    print("...max_evaluations is None; removing {info['name']}")
                    del Tdata[info['name']]
                Tdata[info['name']][0] = fun_dict['max_evaluations']
    return Tdata


def _decompose_execution_times(tdata):

    T_m = {m: {data['name']: None for data in DATA.values()} for m in METHODS}
    T_f = {data['name']: None for data in DATA.values()}
    factor = {}
    with open(PATH[:-7] + r'other\fun_time.txt', 'r') as file:
        for line in file.readlines()[1:]:
            line = line[:-1].split(' ')
            factor[line[0]] = float(line[-1])
    for fun, (maxevals, resdict) in tdata.items():
        if fun not in factor:
            continue
        T_f[fun] = factor[fun] * resdict['RS'][0] / maxevals
        for m in METHODS:
            T_t_avg, cutoff = resdict[m]
            if cutoff < 1:
                print(f'...ignoring {fun} due to cutoff = {cutoff} < 1')
                del factor[fun]
                break
            t = T_t_avg / (maxevals * cutoff / 100) - T_f[fun]
            if t < 0:
                print(f'...ignoring {fun} due to T_m({m}) = {t:.2e} < 0')
                del factor[fun]
                break
            T_m[m][fun] = t
    D = [info['D'] for info in DATA.values() if info['name'] in factor]
    return T_m, T_f, D, factor


def computational_complexity():

    print('\n*** computational complexity')

    Tdata = _extract_execution_times()

    # average T_t per function
    T_t_all_avg = np.empty([len(METHODS), len(Tdata)])
    for fi, (fun, (maxevals, resdict)) in enumerate(Tdata.items()):
        for i, m in enumerate(METHODS):
            T_t_avg, cutoff = resdict[m]
            if cutoff == 0:
                cutoff = np.nan
            T_t_all_avg[i, fi] = T_t_avg / (maxevals * cutoff / 100)
    T_t_all_perm = np.nanmedian(T_t_all_avg, axis=1)

    # execution time plot
    fig = plt.figure(figsize=[3, 4])
    fig.gca().barh(list(M_NAME.values()), T_t_all_perm * 1000, color='pink')
    fig.gca().axvline(T_t_all_perm[0] * 1000, linestyle=':', color='red')
    fig.gca().invert_yaxis()
    fig.gca().set_xlabel('median run time \nper one evaluation [ms]')
    fig.tight_layout()
    fig.savefig('post_analysis_computational_complexity.png')

    # # histogram of all avg(T_t)
    # T = T_t_all_avg.flatten()
    # plt.figure(figsize=(4, 2))
    # plt.hist(T, bins=np.logspace(np.log10(np.nanmin(T)), np.log10(np.nanmax(T)), 50),
    #          color='tomato')
    # plt.xscale('log')
    # plt.xlabel(r'run time per one evaluation [s]')
    # plt.ylabel(r'frequency')
    # plt.tight_layout()
    # plt.savefig('post_analysis_Thist.png')

    T_m, T_f, D, factor = _decompose_execution_times(Tdata)

    # limits for plotting
    max_T_m, max_C_m, min_C_m = -np.inf, 0, np.inf
    for m in METHODS:
        for fun in Tdata.keys():
            if T_m[m][fun] is not None:
                if T_m[m][fun] > max_T_m:
                    max_T_m = T_m[m][fun]
                if T_m[m][fun] / T_m['RS'][fun] > max_C_m:
                    max_C_m = T_m[m][fun] / T_m['RS'][fun]
                if T_m[m][fun] / T_m['RS'][fun] < min_C_m:
                    min_C_m = T_m[m][fun] / T_m['RS'][fun]
    # print(max_T_m, max_C_m, min_C_m)

    # plot T_m(D)
    colors = plt.cm.rainbow(np.linspace(0, 1, len(METHODS)))
    fig, axes = plt.subplots(ncols=5, nrows=4, figsize=(5 * 2.4, 4 * 2.4 + 1))
    for i in range(len(METHODS)):
        m = METHODS[i]
        c = colors[i]
        ax = axes.flat[i]
        Tm = [T_m[m][info['name']] for info in DATA.values() if info['name'] in factor]
        pearson = np.corrcoef(D, Tm)[0, 1]
        reg = np.polynomial.Polynomial.fit(D, Tm, deg=1)
        ax.plot(D, Tm, 'o', alpha=0.4, color=c)
        m_ = np.linspace(np.min(D), np.max(D), 50)
        ax.plot(m_, reg(m_), label=fr'{M_NAME[m]}\n $r={pearson:.2f}$', color='black')
        ax.set_ylim(0, max_T_m)
        ax.title.set_text(f'{M_NAME[m]}  $r={pearson:.2f}$')
        ax.tick_params(bottom=False, labelbottom=False, left=False, labelleft=False)
    fig.tight_layout()
    fig.savefig('post_analysis_Tm(D).png')

    # plot C_m(D)
    from scipy.optimize import curve_fit
    def freg(x, k, e, C0):
        return k * x**e + C0
    colors = plt.cm.rainbow(np.linspace(0, 1, len(METHODS)))
    fig, axes = plt.subplots(ncols=5, nrows=4, figsize=(5 * 2.4, 4 * 2.4 + 1))
    for i in range(len(METHODS)):
        m = METHODS[i]
        c = colors[i]
        ax = axes.flat[i]
        Cm = [T_m[m][info['name']] / T_m['RS'][info['name']] for info in DATA.values() if info['name'] in factor]
        # pearson = np.corrcoef(D, Cm)[0, 1]
        # reg = np.polynomial.Polynomial.fit(D, Cm, deg=1)
        (k, e, C0), _ = curve_fit(freg, D, Cm,
                                  bounds=([0, 0, 1], [max_C_m, 3, max_C_m]))
        if e < 1e-3:
            C0 = k + C0
            k = 0
        ax.plot(D, Cm, 'o', alpha=0.4, color=c)
        m_ = np.linspace(np.min(D), np.max(D), 50)
        ax.plot(m_, freg(m_, k, e, C0),
                label=f'$C_m = {k:.4f} \cdot D^{{{e:.2f}}} + {C0:.2f}$', color='black')
        ax.plot(m_, freg(m_, k, 1, C0), linestyle=':', linewidth=0.5, color='black')
        ax.legend(loc='lower left', fontsize='small', handletextpad=0, handlelength=0)
        ax.axhline(1, linestyle=':', color='grey')
        ax.set_ylim(min_C_m, max_C_m)
        ax.set_yscale('log')
        ax.title.set_text(f'{M_NAME[m]}')
        if i % 5 == 0:
            ax.tick_params(bottom=False, labelbottom=False)
        else:
            ax.tick_params(bottom=False, labelbottom=False, labelleft=False)  # add left=False for non-log-axis
    fig.tight_layout()
    fig.savefig('post_analysis_Cm(D).png')


def function_similarity():

    print('\n*** function similarity')

    import seaborn as sns
    PREFIXI = ['AEP', 'AirfoilDesign', 'EmpReg', 'Ergodic',
               'HydraulicNetwork', 'IPP', 'PP', 'ShortestPath', 'StructuralFrame']

    for pref in PREFIXI:
        print(f'...checking similarity of {pref} functions')
        res = {}

        for fname, info in DATA.items():
            funame = info['name']
            if not funame.startswith(pref):
                continue
            try:  # not all results are complete
                G_100 = np.loadtxt(os.path.join(PATH, fname), usecols=(4,), max_rows=len(METHODS))
            except:
                print(f"...skipping {funame}")
                continue
            res[funame] = G_100

        funs = list(res.keys())
        sol = np.array([G for G in res.values()])
        dist = np.full([len(funs), len(funs)], np.nan)
        for i in range(len(funs)):
            for j in range(i, len(funs)):
                dist[i,j] = np.linalg.norm(sol[i,:] - sol[j,:])
        dist_max = np.linalg.norm(np.full(len(METHODS), 2))
        simil = (dist_max - dist) / dist_max

        plt.figure(figsize=(13, 10))
        heatmap = sns.heatmap(simil, cmap='hot_r', annot=simil,
                              vmin=0.5, vmax=1,
                              linewidths=0.5, linecolor='k',
                              xticklabels=funs, yticklabels=funs)
        for _, spine in heatmap.spines.items():
            spine.set_visible(True)
        heatmap.set_ylabel('similarity of results for this function...')
        heatmap.set_xlabel('...with the results for this function')
        plt.tight_layout()
        plt.savefig(f'post_analysis_function_similarity_{pref}.png')


### MAIN

# G_of_D()
# G_of_M()
# CEC_vs_nonCEC()
# D_hist()
# H_hist()
# G_of_H()
# method_performance()
# method_sets()
# method_overlap_heatmap()
method_features()
# method_clustering()
# function_clustering()
# computational_complexity()
# function_similarity()