# import sys
# sys.path.append('../tests')

import numpy as np
import scipy as sp


def single_pymoo_run(fun_dict, optimizer_dict, instance_label):
    # print(optimizer_dict)

    from pymoo.core.problem import Problem
    from pymoo.algorithms.soo.nonconvex.ga import GA
    from pymoo.algorithms.soo.nonconvex.cmaes import CMAES
    from pymoo.optimize import minimize
    from pymoo.termination import get_termination
    from pymoo.core.callback import Callback

    class PymooIndagoProblemWrapper(Problem):
        def __init__(self, fun_dict):
            self.f = fun_dict['class'](fun_dict['problem'], fun_dict['dimensions'], instance_label)
            super().__init__(n_var=self.f.lb.size, n_obj=1, n_ieq_constr=0, xl=self.f.lb, xu=self.f.ub)

        def _evaluate(self, x, out, *args, **kwargs):
            n = x.shape[0]
            F = np.full(n, np.nan)
            for i in range(n):
                F[i] = self.f(x[i, :])
            out["F"] = F

    class MyCallback(Callback):

        def __init__(self) -> None:
            super().__init__()
            self.evaluation = []
            self.fitness = []

        def notify(self, algorithm):
            self.evaluation.append(algorithm.evaluator.n_eval)
            self.fitness.append(algorithm.opt[0].F[0])

    # if 'forward_unique_str' in function_dict.keys():
    #     opt.forward_unique_str = function_dict['forward_unique_str']
    # if 'eval_fail_behavior' in function_dict.keys():
    #     opt.eval_fail_behavior = function_dict['eval_fail_behavior']
    problem = PymooIndagoProblemWrapper(fun_dict)
    callback = MyCallback()
    """
    Algorithm
    """
    algorithm = None
    if optimizer_dict['label'] == 'GA':
        algorithm = GA(
            pop_size=problem.f.lb.size * 2,
            eliminate_duplicates=True)
    if optimizer_dict['label'] == 'CMAES':
        algorithm = CMAES(
            # pop_size=problem.f.lb.size * 2,
            )

    termination = get_termination("n_eval", fun_dict['max_evaluations'])
    res = minimize(problem,
                   algorithm,
                   termination=termination,
                   # seed=1,
                   verbose=False,
                   # save_history=True,
                   callback=callback)
    # print("Best solution found: \nX = %s\nF = %s" % (res.X, res.F))

    # evals = np.array([e.evaluator.n_eval for e in res.history])
    # fitness = np.array([e.opt[0].F for e in res.history]).reshape(-1)
    evals = np.array(callback.evaluation)
    fitness = np.array(callback.fitness)
    # print(f'{evals.shape=}')
    # print(f'{fitness.shape=}')

    if evals[-1] < fun_dict['max_evaluations']:
        evals = np.append(evals, fun_dict['max_evaluations'])
        fitness = np.append(fitness, fitness[-1])

    # if os.path.exists(instance_label):
    #     shutil.rmtree(instance_label)

    f_interp = sp.interpolate.interp1d(evals, fitness,
                                       bounds_error=False,
                                       fill_value=np.nan)
    evals = np.linspace(0, 1, 101) * fun_dict['max_evaluations']
    fit = f_interp(evals).reshape((1, -1))
    return fit, res.X

pymoo_optimizers = []
pymoo_optimizers.append({'run': single_pymoo_run,
                         'label': f'GA',
                         })
pymoo_optimizers.append({'run': single_pymoo_run,
                         'label': f'CMAES',
                         })

if __name__ == '__main__':

    import indagobench
    import _local_paths

    supertest = indagobench.StandardTest(_local_paths.st24_results_dir)
    supertest.optimizers = indagobench.st24_optimizers_list[-2:]
    # supertest.optimizers.extend(pymoo_optimizers)
    # supertest.optimizers = pymoo_optimizers
    supertest.benchmarks = indagobench.EmpReg_function_dict_list
    # supertest.benchmarks = indagobench._cec2014.cec2014_50d_function_dict_list[2:]
    # # supertest.single_run(supertest.optimizers[0], supertest.benchmarks[0])
    print(supertest.benchmarks)
    supertest.run_all()
