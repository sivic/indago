import numpy as np
import sys
sys.path.append('..')
from _standard_test_functions import st24_function_dict_list
import indago
import time
import shutil, os


# PATH = r"C:\Users\Sinisa\AOSeR Dropbox\Siniša Družeta\Research\indago_st24_paper\indagobench24_results\other"  # sinisa laptop
PATH = r"C:\Users\user\AOSeR Dropbox\Siniša Družeta\Research\indago_st24_paper\indagobench24_results\other"  # sinisa faks

METHODS = ['RS', 'L-BFGS-B', 'ZOOpt', 'DA', 'ESCH', 'CRS', 'STOGO', 'NM GaoHan', 'MSGD', 'PSO',
           'FWA', 'SSA', 'BA', 'GWO', 'EFO', 'MRFO', 'ABC', 'DE LSHADE', 'GA', 'CMAES']
M_NAME = {'RS': 'RS', 'L-BFGS-B': 'LBFGSB', 'ZOOpt': 'ZOOPT', 'DA': 'DA', 'ESCH': 'ESCH', 'CRS': 'CRS',
          'STOGO': 'STOGO', 'NM GaoHan': 'NM', 'MSGD': 'MSGD', 'PSO': 'PSO', 'FWA': 'FWA', 'SSA': 'SSA', 'BA': 'BA',
          'GWO': 'GWO', 'EFO': 'EFO', 'MRFO': 'MRFO', 'ABC': 'ABC', 'DE LSHADE': 'LSHADE', 'GA': 'GA', 'CMAES': 'CMAES'}
SIMULATION_BASED = 'AirfoilDesign HydraulicNetwork StructuralFrame'.split(' ')

N_FUN_RUNS = 1000
N_OPT_RUNS = 10


DONE = {}

if os.path.isfile(PATH + r'\fun_time.txt'):
    shutil.copy(PATH + r'\fun_time.txt', PATH + r'\fun_time_backup.txt')
    with open(PATH + r'\fun_time.txt', 'r') as file:
        for line in file.readlines()[1:]:
            line = line[:-1].split(' ')
            DONE[line[0]] = {'tf': float(line[1]), 'to': float(line[2]), 'factor': float(line[3])}

with open(PATH + r'\fun_time.txt', 'w') as file:
    file.write('# function_name evaluation_time RS_time_per_evaluation evaluation_time_share\n')

for fun_dict in st24_function_dict_list:

    funame = fun_dict['label']

    if funame in DONE:
        print(f"{funame} already done")
        row = DONE[funame]
        with open(PATH + r'\fun_time.txt', 'a') as file:
            file.write(f"{funame} {row['tf']} {row['to']} {row['factor']}\n")
        continue

    SKIP = False
    for prefix in SIMULATION_BASED:
        if funame.startswith(prefix):
            SKIP = True
    if SKIP:
        continue

    f = fun_dict['class'](fun_dict['problem'], fun_dict['dimensions'])

    print(f"{funame} timing evaluation ")
    times = []
    for i in range(N_OPT_RUNS):
        print(f'... evaluation batch #{i+1}')
        tf_0 = time.perf_counter()
        for _ in range(N_FUN_RUNS):
            f(np.random.uniform(f.lb, f.ub))
        tf_1 = time.perf_counter()
        times.append((tf_1 - tf_0) / N_FUN_RUNS)
    tf = np.min(times)

    print(f"{funame} timing RS optimization")
    times = []
    for i in range(N_OPT_RUNS):
        print(f'... optimization run #{i+1}')
        to_0 = time.perf_counter()
        indago.minimize(f, f.lb, f.ub, 'RS',
                        max_evaluations=fun_dict['max_evaluations'])
        to_1 = time.perf_counter()
        times.append((to_1 - to_0) / fun_dict['max_evaluations'])
    to = np.min(times)

    print(f"{funame} result: {tf} {to} {tf/to}")

    with open(PATH + r'\fun_time.txt', 'a') as file:
        file.write(f"{funame} {tf} {to} {tf/to}\n")
