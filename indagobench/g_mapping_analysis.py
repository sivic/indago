import numpy as np

import _local_paths
import indagobench
import matplotlib.pyplot as plt

standard_test = indagobench.StandardTest(_local_paths.st24_results_dir,
                                         convergence_window=10, eps_max=0.1, runs_min=10, )

f_rel = np.linspace(-0.01, 1.01, 102*10+1)
f_rs_med = 0.13
alpha = standard_test.calc_alpha(f_rs_med)



fig, axes = plt.subplots(nrows=2, ncols=1,
                         figsize=(8, 6))

ax_g = axes[0]
for f in f_rel[3:-3]:
    a = standard_test.calc_alpha(f)
    g = standard_test.calc_g(f_rel=f_rel, alpha=a)
    if np.any(np.isnan(g)):
        print(f'g is nan, {a=}, f_rs_med={f}, {f_rel[np.where(np.isnan(g))]}')
    ax_g.plot(f_rel, g,
              lw=1, ls='-', color='grey')
g = standard_test.calc_g(f_rel=f_rel, alpha=alpha)
ax_g.plot(f_rel, g, lw=2, c='r')
ax_g.plot([0, f_rs_med, 1], [1, 0, -1], 'go')


gbox = dict(boxstyle='square,pad=0.3', facecolor='olivedrab', alpha=0.6, edgecolor='none')
rbox = dict(boxstyle='square,pad=0.3', facecolor='indianred', alpha=0.6, edgecolor='none')
pbox = dict(boxstyle='square,pad=0.3', facecolor='purple', alpha=0.6, edgecolor='none')

fig, (ax1, ax2) = plt.subplots(figsize=(12, 12), nrows=2, constrained_layout=True)
ax1.set_xlabel('Normed linear grade, $\\rho(f)$\n"less is better" $\\Longleftarrow$')
ax1.set_ylabel('Normed logarithmic grade, $\\mathcal{G}(f)$\n"more is better" $\\Longrightarrow$')
ax1.set_xticks([0, 0.1, 1])
ax1.set_yticks([-1, 0, 1])
ax1.axvline(0, c='olivedrab', lw=2)
ax1.axvline(1, c='indianred', lw=2)
ax1.axvline(f_rs_med, c='purple', lw=2)

dx, dy = 0.01, 0.05
ax1.plot(0, 1, 'o', c='olivedrab')
ax1.text(0 + dx, 1 - dy, 'Best solution $f^-$',
        rotation=0, color='k', va='top', ha='left', bbox=gbox)

ax1.plot(f_rs_med, 0, 'o', c='purple')
if f_rs_med < 0.5:
    ax1.text(f_rs_med + dx, 0 + dy, 'Median of random search $f^\circ$',
        rotation=0, color='k', va='bottom', ha='left', bbox=rbox)
else:
    ax1.text(f_rs_med - dx, 0 + dy, 'Median of random search $f^\circ$',
        rotation=0, color='k', va='bottom', ha='right', bbox=rbox)

ax1.plot(1, -1, 'o', c='indianred')
ax1.text(1 - dx, -1 + dy, 'Median of uniform sampling $f^+$',
        rotation=0, color='k', va='bottom', ha='right', bbox=pbox)
ax1.axhline(1, c='olivedrab', lw=2)
ax1.axhline(-1, c='indianred', lw=2)
ax1.axhline(0, c='purple', lw=2)
# ax1.text(0.5, 1.015, 'Best ever solution',
#         rotation=0, color='k', va='bottom', bbox=gbox)
# ax1.text(f_rs_med, 0.015, 'Median of random search', rotation=0, color='k', bbox=rbox)

f_rel = np.linspace(0, 1, 501)
alpha = standard_test.calc_alpha(f_rs_med)
g = standard_test.calc_g(f_rel=f_rel, alpha=alpha)
ax1.plot(f_rel, g, lw=3, c='k')

f_rs_range = np.linspace(0, 1, 21)
alpha = standard_test.calc_alpha(f_rs_range)
for a in alpha:
    g = standard_test.calc_g(f_rel=f_rel, alpha=a)
    ax1.plot(f_rel, g, lw=1, c='grey', zorder=-2)

ax2.plot(f_rel, standard_test.calc_alpha(f_rel), lw=2, c='r')
ax2.set_yscale('log')
ax2.set_xlabel(r'$\rho^\circ$')
ax2.set_ylabel(r'$\alpha$')

plt.savefig('normed_log_grade.png')


plt.show()