import indagobench as indago_benchmarks

import sys
sys.path.append(0, 'nomad_src')


import nomad_f_generator as n_gen
"""nomad f generator has a relative path in the string, maybe this needs to be
specific to the user"""
import nomad_wrapper as nw

def single_mads_run(function_dict, optimizer_dict, instance_label):
    
    if instance_label == None:
        """generates the problem label"""
        f, evals, D, lb, ub, instance_label = n_gen.nomad_f_generator(function_dict).get_values()
    else:
        f, evals, D, lb, ub, _ = n_gen.nomad_f_generator(function_dict).get_values()
        
    test = nw.nomad_wrapper(f, D, lb, ub, evals, instance_label, nomad_path='/SET/NOMAD/PATH/HERE')
    f, X = test.run_nomad()

    return f, X

mads = {'run': single_mads_run,
        'label': f'MADS'}






