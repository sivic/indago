import shutil
import numpy as np
import scipy as sp
import os
import sys
import subprocess
import re 
import scipy.interpolate as interp
from indagobench import _local_paths

class nomad_wrapper:
    
    """nomad wrapper
    
    inputs:
    Important:
        
    Takes in f as an inspected object since it generates a run script on the fly for each function.
    Must be synced with function dict somehow (TODO)
    
    D - dimension
    lb - lower boundary
    ub - upper boundary
    evals - maximum evalualtions
    nomad_path - path of the binary file if not setup in .bashrc
    
    outputs:
        
    f - array of f_best values per evaluations
    best design vector (X_best)
    
    """
    
    def __init__(self, f, D, lb, ub, evals, nomad_path=None, instance_label=None):
        
        self.f = f
        self.D = D
        self.lb = lb
        self.ub = ub
        self.evals = evals
        self.nomad_path = nomad_path
        self.working_dir = f'{_local_paths.nomad_tmp_dir}/nomad_{instance_label}'
        if os.path.exists(self.working_dir):
            shutil.rmtree(self.working_dir)
        os.makedirs(self.working_dir, exist_ok=True)
        self.script_name = f'{self.working_dir}/run.py'
        self.param_file_name = f'{self.working_dir}/parameters.txt'
        
    # def setup_case(self, job_id):
    #
    #     folder_name = f'case_{job_id}'
    #     os.makedirs(folder_name, exist_ok=True)
    #     script_name = f'{folder_name}/run_{job_id}.py'
    #     param_file_name = f'{folder_name}/parameters_{job_id}.txt'
    #
    #     return script_name, param_file_name, folder_name
    
    
    def gen_f_script(self, script_name):
        
        
        with open(f'{script_name}', 'w') as file:
            file.write(f'{self.f}')
   
        return script_name
        
    
    def gen_param_file(self):
        
        bb = self.gen_f_script(self.script_name)
        bb = os.path.basename(bb)
        x0 = '( ' + ' '.join(map(str, np.random.uniform(self.lb, self.ub))) + ' )'
        lb = '( ' + ' '.join(map(str, self.lb)) + ' )'
        ub = '( ' + ' '.join(map(str, self.ub)) + ' )'
        
        with open(f'{self.param_file_name}', 'w') as file:
            
            file.write(f'DIMENSION \t {self.D}\n')
            #file.write(f'BB_EXE \t bb.exe\n')            
            #file.write(f'BB_EXE \t "$/home/luka/Documents/programs/anaconda3/bin/python {bb}"\n')
            file.write(f'BB_EXE \t "${sys.executable} {bb}"\n')
            file.write(f'BB_OUTPUT_TYPE \t OBJ \n')
            file.write(f'X0 \t {x0}\n')
            file.write(f'LOWER_BOUND \t {lb}\n')
            file.write(f'UPPER_BOUND \t {ub}\n')
            file.write(f'MAX_BB_EVAL \t {self.evals}\n')
            file.write(f'DISPLAY_DEGREE 2\n')
            file.write(f'DISPLAY_STATS BBE ( SOL ) OBJ\n')
            
    def run_job(self, job_id):
        
        # script_name, param_file_name, folder_name = self.setup_case(job_id)
        self.gen_param_file()

        if self.nomad_path == None:
            cmd = f'nomad {self.param_file_name}'
            result = subprocess.run(['bash', '-i', '-c', cmd], capture_output=True, text=True)
        else:
            cmd = f'{self.nomad_path} {self.param_file_name}'
            result = subprocess.run(cmd, capture_output=True, text=True, shell=True, cwd=self.working_dir)

        pattern = r'(\d+)\t\(([\s\S]*?)\)\t\s*([-?\d.]+)'
        matches = re.findall(pattern, result.stdout)
        data = np.array([np.array(i) for i in matches])

        evaluations = np.array([int(data[i, 0]) for i in range(len(data))])
        fitness = np.array([float(data[i, -1]) for i in range(len(data))])

        # print(f'{evaluations=}')
        # print(f'{fitness=}')

        f_interp = sp.interpolate.interp1d(evaluations, fitness,
                                           bounds_error=False,
                                           fill_value=(fitness[0], fitness[-1]),
                                           )
        evals = np.linspace(0, 1, 101) * self.evals
        fit = f_interp(evals).reshape((1, -1))

        x_best = np.array([float(xi) for xi in data[-1, 1:-1][0].split()])
        # print(f'{x_best=}')
        return fit, x_best

    # def run(self):
    #     with multiprocessing.Pool(processes=self.n_jobs) as pool:
    #         results = pool.map(self.run_job, range(self.n_jobs))
    #
    #     return results
            
# test = nomad(f, 5, np.ones((5,))*-6, np.array([5, 6, 7, 1, 1]), 100) 
#              #nomad_path='/inser/nomad/bin/path/here')
# test.run()

        
        
