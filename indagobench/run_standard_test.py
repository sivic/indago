import sys
sys.path.append('..')

import indagobench
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':



    standard_test = indagobench.StandardTest(indagobench._local_paths.st24_results_dir,
                                             convergence_window=10, eps_max=0.1, runs_min=10,
                                             )
    standard_test.optimizers = indagobench.st24_optimizers_list

    standard_test.benchmarks = []
    # standard_test.benchmarks.extend(indagobench._cec2014.cec2014_all_function_dict_list)
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_10d_function_dict_list)
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_20d_function_dict_list)
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_50d_function_dict_list)
    standard_test.benchmarks.extend(indagobench.AEP_function_dict_list)
    standard_test.benchmarks.extend(indagobench._empirical_regression.EmpReg_function_dict_list)
    standard_test.benchmarks.extend(indagobench._ill_posed_problems.IPP_all_function_dict_list)
    standard_test.benchmarks.extend(indagobench._shortest_path.shortest_path_function_dict_list)
    standard_test.benchmarks.extend(indagobench._hydraulic_network.hydraulic_network_function_dict_list)
    standard_test.benchmarks.extend(indagobench._structural_frame.structural_frame_function_dict_list)
    standard_test.benchmarks.extend(indagobench._ergodic.ergodic_function_dict_list)
    standard_test.benchmarks.extend(indagobench._airfoil_design.AirfoilDesign_function_dict_list)

    #standard_test.single_run(standard_test.benchmarks[0], standard_test.optimizers[0], 'ST_test')
    # standard_test.delete_optimizer('HS')
    # standard_test.delete_optimizer('HS (fixed)')
    # standard_test.delete_optimizer('HALO')
    # standard_test.delete_optimizer('GS')
    # standard_test.delete_optimizer('GD')
    # standard_test.delete_optimizer('MSGD')
    # standard_test.run_all()

    # for opt_dict in standard_test.optimizers:
    #     standard_test.optimizer_profile(opt_dict)

    # for i, b in enumerate(standard_test.benchmarks):
    #     print(f'{i + 1:03d} {b["label"]}')
    # input(' > Press return to continue.')

    props = dict(boxstyle='square,pad=0.3', facecolor='silver', alpha=0.6, edgecolor='none')
    # fig, ax = plt.subplots(figsize=(4, 4))
    # standard_test.prepare_radar_plot(ax)
    # for opt_dict in np.array(standard_test.optimizers)[[2, 3, 8, 11]]:
    #     standard_test.optimizer_profile(opt_dict, ax)
    # ax.legend(fontsize=6, facecolor='silver', fancybox=False, borderpad=0.3)
    # plt.savefig(f'{standard_test.results_dir}/optimizer_comparison.png', dpi=300)

    ncols = 5
    nrows = 4
    fig, axes = plt.subplots(ncols=ncols, nrows=nrows,
                             figsize=(ncols * 2.4, nrows * 2.4 + 1),
                             )
    axes = np.ravel(axes)
    print(f'\n\nNumber of functions: {len(standard_test.benchmarks)}')
    print(f'{"Optimizer":20s} {"G10%50%100% avg.":>15s} {"G100%":>12s}')
    for i, opt_dict in enumerate(np.array(standard_test.optimizers)):
        standard_test.prepare_radar_plot(axes[i], cat_labels=True)
        standard_test.optimizer_profile(opt_dict,
                                        ax_radar=axes[i],
                                        axes_hist=None,
                                        optimizer_label=True)
    for ax in axes[len(standard_test.optimizers):]:
        ax.axis('off')
        ax.set_facecolor("whitesmoke")
        for spine in ax.spines.values():
            spine.set_visible(False)
    fig.suptitle(f'IndagoBench standard test 2024 ({len(standard_test.benchmarks)} functions)', bbox=props, fontsize=12)
    plt.subplots_adjust(bottom=0, top=0.95, left=0, right=1)
    plt.savefig(f'{standard_test.results_dir}/other/all_optimizers.png', dpi=300)