import sys
import time

sys.path.append('../indago')
sys.path.append('../indagobench')
import indago
import scipy as sp
import numpy as np
# from pybads import BADS
import zoopt
# import _standard_test_mads

st24_optimizers_list = []

st24_optimizers_list.append({'optimizer_class': indago.RS,
                             'variant': 'Vanilla',
                             'params': None,  # Default parameters
                             'label': f'RS'},
                            )

st24_optimizers_list.append({'optimizer_class': indago.NM,
                             'variant': 'GaoHan',
                             'params': None,
                             'label': 'NM GaoHan'})

# st24_optimizers_list.append({'optimizer_class': indago.MSGS,
#                              'variant': 'Vanilla',
#                              'params': None,
#                              'label': 'MSGS'})

st24_optimizers_list.append({'optimizer_class': indago.MSGD,
                             'variant': 'Vanilla',
                             'params': None,
                             'label': 'MSGD'})

st24_optimizers_list.append({'optimizer_class': indago.PSO,
                             'variant': 'Vanilla',
                             'params': None,  # Default parameters
                             'label': 'PSO',
                             # 'old_label': 'PSO Vanilla',  # For changing optimizer label and keeping existing results
                             })

# st24_optimizers_list.append({'optimizer_class': indago.PSO,
#                              'variant': 'Vanilla',
#                              'params': {'inertia': 'anakatabatic'},
#                              'label': 'PSO Vanilla anakatabatic'})

# st24_optimizers_list.append({'optimizer_class': indago.PSO,
#                              'variant': 'Vanilla',
#                              'params': {'swarm_size': 20},
#                              'label': 'PSO s=20'})

st24_optimizers_list.append({'optimizer_class': indago.FWA,
                             'variant': 'Vanilla',
                             'params': None,  # Default parameters
                             'label': 'FWA',
                             # 'old_label': 'FWA Vanilla',  # For changing optimizer label and keeping existing results)
                             })

# st24_optimizers_list.append({'optimizer_class': indago.FWA,
#                              'variant': 'Rank',
#                              'params': None,  # Default parameters
#                              'label': 'FWA Rank'})

st24_optimizers_list.append({'optimizer_class': indago.SSA,
                             'variant': 'Vanilla',
                             'params': None,  # Default parameters
                             # 'old_label': 'SSA Vanilla',
                             'label': 'SSA'})

st24_optimizers_list.append({'optimizer_class': indago.BA,
                             'variant': 'Vanilla',
                             'params': None,  # Default parameters
                             # 'old_label': 'BA Vanilla',
                             'label': 'BA'})

st24_optimizers_list.append({'optimizer_class': indago.GWO,
                             'variant': 'Vanilla',
                             'params': None,
                             'label': 'GWO'})

st24_optimizers_list.append({'optimizer_class': indago.EFO,
                             'variant': 'Vanilla',
                             'params': None,
                             'label': 'EFO'})

st24_optimizers_list.append({'optimizer_class': indago.MRFO,
                             'variant': 'Vanilla',
                             'params': None,  # Default parameters
                             # 'old_label': 'MRFO Vanilla',
                             'label': 'MRFO'})

st24_optimizers_list.append({'optimizer_class': indago.ABC,
                             'variant': 'Vanilla',
                             'params': None,  # Default parameters
                             # 'old_label': 'ABC Vanilla',
                             'label': 'ABC'})

st24_optimizers_list.append({'optimizer_class': indago.DE,
                             'variant': 'LSHADE',
                             'params': None,
                             'label': 'DE LSHADE'})

# st24_optimizers_list.append({'optimizer_class': indago.CMAES,
#                              'variant': 'Vanilla',
#                              'params': None,
#                              'label': 'CMAES Indago v2'})

import indagobench
from indagobench._standard_test_pymoo import pymoo_optimizers

st24_optimizers_list.extend(pymoo_optimizers)


def single_scipy_run(function_dict, optimizer_dict, instance_label):
    if 'class' in function_dict.keys():
        instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'],
                                          instance_label=instance_label)

    fitness = []

    def f_log(x, uniqe_str=None, fitness=fitness):
        fit = instance(x)
        fitness.append(fit)
        return fit

    np.random.seed()
    x0 = np.random.uniform(instance.lb, instance.ub)

    if optimizer_dict['label'] == 'L-BFGS-B':
        res = sp.optimize.minimize(f_log, x0,
                                   method='L-BFGS-B',
                                   bounds=np.array([instance.lb, instance.ub]).T,
                                   options={'maxfun': function_dict['max_evaluations'],
                                            'tol': 0,
                                            'ftol': 0,
                                            'gtol': 0,
                                            'eps': 1e-20,
                                            },
                                   )
    if optimizer_dict['label'] == 'DA':
        res = sp.optimize.dual_annealing(f_log, x0=x0,
                                         bounds=list(zip(instance.lb, instance.ub)),
                                         maxfun=function_dict['max_evaluations'],
                                         maxiter=function_dict['max_evaluations'],
                                         )

    # print(res.success, res.status, res.message)
    # print(f'{len(fitness)=}, {res.nfev}, {res.fun}, {res.x}')
    for i in range(len(fitness)):
        fitness[i] = np.min(fitness[:i + 1])

    evals = np.arange(len(fitness)) + 1
    # fitness = np.array([e.opt[0].F for e in res.history]).reshape(-1)

    # if os.path.exists(instance_label):
    #     shutil.rmtree(instance_label)

    f_interp = sp.interpolate.interp1d(evals, fitness,
                                       bounds_error=False,
                                       fill_value=(fitness[0], fitness[-1]),
                                       )
    evals = np.linspace(0, 1, 101) * function_dict['max_evaluations']
    fit = f_interp(evals).reshape((1, -1))
    return fit, res.x


scipy_lbfgsb = {'run': single_scipy_run,
                'label': f'L-BFGS-B',
                }
st24_optimizers_list.insert(1, scipy_lbfgsb)
scipy_dual_annealing = {'run': single_scipy_run,
                'label': f'DA',
                }
st24_optimizers_list.insert(2, scipy_dual_annealing)


def single_zoopt_run(function_dict, optimizer_dict, instance_label):
    if 'class' in function_dict.keys():
        instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'],
                                          instance_label=instance_label)

    def f_wrap(solution):
        x = np.array(solution.get_x())
        value = instance(x)
        return value

    np.random.seed()
    from zoopt import Dimension, ValueType, Dimension2, Objective, Parameter, Opt, ExpOpt
    import time
    bounds = np.append([instance.lb], [instance.ub], axis=0).T
    dim = Dimension(instance.dimensions, bounds, [True] * instance.dimensions)
    # dim = Dimension2([(ValueType.CONTINUOUS, [-1, 1], 1e-6)]*dim_size)
    obj = Objective(f_wrap, dim)
    solution = Opt.min(obj, Parameter(budget=function_dict['max_evaluations'],
                                      seed=np.random.randint(0, 1e6)))
                                      # int(time.time_ns() + np.random.randint(2**32)) % (2**32-1)))
    # print(type(solution.get_x()), solution.get_value())
    # parallel optimization for time-consuming tasks
    # solution = Opt.min(obj, Parameter(budget=100 * dimensions**2, parallel=True, server_num=3))

    fitness = np.array(obj.get_history())
    for i in range(fitness.size):
        fitness[i] = np.min(fitness[:i + 1])
    evals = np.arange(len(fitness)) + 1

    f_interp = sp.interpolate.interp1d(evals, fitness,
                                       bounds_error=False,
                                       fill_value=(fitness[0], fitness[-1]),
                                       )
    evals = np.linspace(0, 1, 101) * function_dict['max_evaluations']
    fit = f_interp(evals).reshape((1, -1))
    return fit, np.array(solution.get_x())


zoopt_dict = {'run': single_zoopt_run,
              'label': f'ZOOpt',
              }
st24_optimizers_list.insert(2, zoopt_dict)

# st24_optimizers_list.insert(_standard_test_mads.)

# def single_bads_run(function_dict, optimizer_dict, instance_label):
#
#     if 'class' in function_dict.keys():
#         instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'], instance_label=instance_label)
#
#     fitness = []
#     def f_log(x, uniqe_str=None, fitness=fitness):
#         fit = instance(x)
#         fitness.append(fit)
#         return fit
#
#     options = {'max_fun_evals': function_dict['max_evaluations'],
#                # 'max_iter': function_dict['max_evaluations'],
#                # 'tol_fun': 1e-10,
#                # 'tol_mesh': 1e-10,
#                # 'tol_stall_iters': function_dict['max_evaluations'],
#                'display': 'off',
#                }
#     bads = BADS(f_log,
#                 x0=np.random.uniform(instance.lb, instance.ub),
#                 lower_bounds=instance.lb,
#                 upper_bounds=instance.ub,
#                 options=options)
#
#     res = bads.optimize()
#
#     for i in range(len(fitness)):
#         fitness[i] = np.min(fitness[:i + 1])
#
#     evals = np.arange(len(fitness)) + 1
#     # fitness = np.array([e.opt[0].F for e in res.history]).reshape(-1)
#
#     # if os.path.exists(instance_label):
#     #     shutil.rmtree(instance_label)
#
#     f_interp = sp.interpolate.interp1d(evals, fitness,
#                                        bounds_error=False,
#                                        fill_value=(fitness[0], fitness[-1]),
#                                        )
#     evals = np.linspace(0, 1, 101) * function_dict['max_evaluations']
#     fit = f_interp(evals).reshape((1, -1))
#     return fit, res.x
#
# bads = {'run': single_bads_run,
#         'label': f'BADS',
#         }
# st24_optimizers_list.insert(2, bads)

def single_nomad_lib_run(function_dict, optimizer_dict, instance_label):
    if 'class' in function_dict.keys():
        instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'],
                                          instance_label=instance_label)
    import sys
    from pathlib import Path
    sys.path.append(Path(__file__).parent.absolute())
    from indagobench import indagobenchnomad
    import importlib
    importlib.reload(indagobenchnomad)

    np.random.seed()
    # print(f'{x0=}')

    succes_run = False
    while not succes_run:
        try:
            x0 = np.random.uniform(instance.lb, instance.ub)
            fitness, x_best, cnt = indagobenchnomad.optimize(instance, x0, instance.lb, instance.ub, function_dict['max_evaluations'],
                                      'ORTHO_NP1_QUAD', instance_label)
            if len(fitness) > 0:
                succes_run = True
        except:
            print('*** Repeating NOMAD optimization')
            pass

    evaluations = np.arange(len(fitness)) + 1
    if len(fitness) > 1:
        f_interp = sp.interpolate.interp1d(evaluations, fitness,
                                           bounds_error=False,
                                           fill_value=(fitness[0], fitness[-1]),
                                           )
    else:
        f_interp = sp.interpolate.interp1d([1, 2], [fitness[0], fitness[0]],
                                           bounds_error=False,
                                           fill_value=(fitness[0], fitness[-1]),
                                           )
    evals = np.linspace(0, 1, 101) * function_dict['max_evaluations']
    fit = f_interp(evals).reshape((1, -1))
    return fit, x_best


nomadlib_dict = {'run': single_nomad_lib_run,
                 'label': f'NOMAD lib',
                 }
# st24_optimizers_list.insert(3, nomadlib_dict)




def single_mads_run(function_dict, optimizer_dict, instance_label):
    if 'class' in function_dict.keys():
        instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'],
                                          instance_label=instance_label)
    import sys
    from pathlib import Path
    sys.path.append(Path(__file__).parent.absolute().__str__())
    import mads

    np.random.seed()
    x0 = np.random.uniform(instance.lb, instance.ub)

    dp_tol = 1e-20  # Minimum poll size stopping criteria
    max_evals = function_dict['max_evaluations']  # Maximum objective function evaluations
    dp = 0.1  # Initial poll size as percent of bounds
    dm = 0.01  # Initial mesh size as percent of bounds

    # Run the optimizer
    f_best, x_best, fitness = mads.orthomads(x0, instance.ub, instance.lb, instance,
                       dp, dm, dp_tol, max_evals,
                       displog=False, savelog=False)

    evaluations = np.arange(len(fitness)) + 1
    if len(fitness) > 1:
        f_interp = sp.interpolate.interp1d(evaluations, fitness,
                                           bounds_error=False,
                                           fill_value=(fitness[0], fitness[-1]),
                                           )
    else:
        f_interp = sp.interpolate.interp1d([1, 2], [fitness[0], fitness[0]],
                                           bounds_error=False,
                                           fill_value=(fitness[0], fitness[-1]),
                                           )
    evals = np.linspace(0, 1, 101) * function_dict['max_evaluations']
    fit = f_interp(evals).reshape((1, -1))
    return fit, x_best


mads_dict = {'run': single_mads_run,
              'label': f'MADS',
              }
# st24_optimizers_list.insert(4, mads_dict)



def single_hs_run(function_dict, optimizer_dict, instance_label):
    if 'class' in function_dict.keys():
        instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'],
                                          instance_label=instance_label)

    from mealpy import FloatVar, HS

    # print(f'{instance.dimensions=}')
    # print(f'{instance.lb=}')
    # print(f'{instance.ub=}')

    problem_dict = {
                    "obj_func": instance,
                    "bounds": FloatVar(lb=instance.lb.tolist(), ub=instance.ub.tolist()),
                    # "bounds": FloatVar(n_vars=instance.dimensions, lb=instance.lb.tolist(), ub=instance.ub.tolist(), name="delta"),
                    "minmax": "min",
                    "log_to": None,
                    }
    
    term_dict = {"max_fe": function_dict['max_evaluations']}

    pop_size = max(20, instance.dimensions)
    optimizer = HS.OriginalHS(epoch=100_000, pop_size=pop_size, c_r = 0.15, pa_r = 0.5, save_convergence=True, verbose=False)
    optimizer.solve(problem_dict, termination=term_dict)
    
    x_best = optimizer.g_best.solution
    fitness = optimizer.history.list_global_best_fit
    evaluations = (np.arange(len(fitness)) + 2) * pop_size
    # print(f'{evaluations=}')
    # print(f'{fitness=}')
    # print(f'{optimizer.nfe_counter=}')
    # input()

    f_interp = sp.interpolate.interp1d(evaluations, fitness,
                                       bounds_error=False,
                                       fill_value=(fitness[0], fitness[-1]),
                                       )
    evals = np.linspace(0, 1, 101) * function_dict['max_evaluations']
    fit = f_interp(evals).reshape((1, -1))

    return fit, x_best

hs_dict = {'run': single_hs_run,
              'label': f'HS',
              }
# st24_optimizers_list.insert(5, hs_dict)
hs_dict = {'run': single_hs_run,
              'label': f'HS (fixed)',
              }
# st24_optimizers_list.insert(5, hs_dict)


from indagobench.single_run_nomad import single_nomad_batch_run
nomadbatch_dict = {'run': single_nomad_batch_run,
                   'label': f'NOMAD batch',
                   }
# st24_optimizers_list.insert(4, nomadbatch_dict)


def single_halo_run(function_dict, optimizer_dict, instance_label):
    if 'class' in function_dict.keys():
        instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'],
                                          instance_label=instance_label)
    from halo import HALO
    bounds = np.vstack((instance.lb, instance.ub)).T
    halo = HALO(f=instance,
                bounds=bounds,
                max_feval=function_dict['max_evaluations'],
                max_iter=function_dict['max_evaluations'],
                beta=1e-3,
                local_optimizer='L-BFGS-B',
                verbose=0)
    results = halo.minimize()
    x_best, best_f, f_history = results['best_x'],  results['best_f'], results['F_history']
    f_history = np.array([np.min(f_history[:i + 1]) for i in range(len(f_history))])
    x_best = np.array(x_best)

    f_interp = sp.interpolate.interp1d(np.arange(f_history.size) + 1, f_history,
                                       bounds_error=False,
                                       fill_value=(f_history[0], f_history[-1]),
                                       )
    evals = np.linspace(0, 1, 101) * function_dict['max_evaluations']
    fit = f_interp(evals).reshape((1, -1))

    return fit, x_best

halo_dict = {'run': single_halo_run,
                   'label': f'HALO',
                   }
# st24_optimizers_list.insert(4, halo_dict)


def single_nlopt_run(function_dict, optimizer_dict, instance_label):
    if 'class' in function_dict.keys():
        instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'],
                                          instance_label=instance_label)
    fitness = []
    def f_log(x, grad):
        fit = float(instance(x))
        fitness.append(fit)
        return fit

    import nlopt

    if optimizer_dict['label'] == 'STOGO':
        opt = nlopt.opt(nlopt.GD_STOGO_RAND, instance.dimensions)
    if optimizer_dict['label'] == 'CRS':
        opt = nlopt.opt(nlopt.GN_CRS2_LM, instance.dimensions)
    if optimizer_dict['label'] == 'ISRES':
        opt = nlopt.opt(nlopt.GN_ISRES, instance.dimensions)
    if optimizer_dict['label'] == 'ESCH':
        opt = nlopt.opt(nlopt.GN_ESCH, instance.dimensions)

    opt.set_lower_bounds(instance.lb)
    opt.set_upper_bounds(instance.ub)
    x0 = np.random.uniform(instance.lb, instance.ub)
    opt.set_min_objective(f_log)
    opt.set_maxeval(function_dict['max_evaluations'])

    x_best = opt.optimize(x0)

    for i in range(len(fitness)):
        fitness[i] = np.min(fitness[:i + 1])

    evals = np.arange(len(fitness)) + 1
    f_interp = sp.interpolate.interp1d(evals, fitness,
                                       bounds_error=False,
                                       fill_value=(fitness[0], fitness[-1]),
                                       )
    evals = np.linspace(0, 1, 101) * function_dict['max_evaluations']
    fit = f_interp(evals).reshape((1, -1))
    return fit, x_best

nlopt_stogo_dict = {'run': single_nlopt_run,
                   'label': f'STOGO',
                   }
st24_optimizers_list.insert(4, nlopt_stogo_dict)

nlopt_crs_dict = {'run': single_nlopt_run,
                   'label': f'CRS',
                   }
st24_optimizers_list.insert(4, nlopt_crs_dict)

# nlopt_isres_dict = {'run': single_nlopt_run,
#                    'label': f'ISRES',
#                    }
# st24_optimizers_list.insert(4, nlopt_isres_dict)

nlopt_esch_dict = {'run': single_nlopt_run,
                   'label': f'ESCH',
                   }
st24_optimizers_list.insert(4, nlopt_esch_dict)