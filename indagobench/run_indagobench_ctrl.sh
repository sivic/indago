echo "Indagobench control panel"
if [ $CONDA_DEFAULT_ENV ]; then
    echo "Active conda environment: $CONDA_DEFAULT_ENV ($CONDA_PREFIX)"
else
    echo "No active conda environment. Exiting"
    exit 1
fi

$CONDA_PREFIX/bin/python _control_indagobench.py
