import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import image
import sys
sys.path.append('..')
from indagobench import _local_paths


class Ergodic():

    @staticmethod
    def problem_cases_list():
        # problem, dimensions, max_evaluations
        return [('gaussian1', 20, 8_000),
                ('gaussian1', 30, 12_000),
                ('gaussian1', 50, 25_000),
                ('gaussian4', 50, 25_000),
                ('phi', 50, 20_000),
                ('beta', 50, 15_000),
                ('epsilon', 50, 20_000),
                ]

    def __call__(self, design, label=None):
        return self.evaluate(design)

    def __init__(self, problem, dimensions=None, instance_label=None):

        self.problem = problem
        self.dimensions = dimensions
        if problem == 'gaussian1':
            self.x_min = -10
            self.x_max = 10
            self.y_min = -10
            self.y_max = 10
            self.x = np.linspace(self.x_min, self.x_max, 101)
            self.y = np.linspace(self.y_min, self.y_max, 101)
            self.x, self.y = np.meshgrid(self.x, self.y)
            self.xy0 = [-5, -5]
            # self.m = np.zeros_like(self.x)
            self.m = self.gauss2d(self.x, self.y, 0, 0, 5, 5)
            self.m /= np.sum(self.m)

            self.lb = np.deg2rad([0] + [-20] * (dimensions - 1))
            self.ub = np.deg2rad([360] + [20] * (dimensions - 1))
            self.dx = 0.5

        elif problem == 'gaussian4':
            self.x_min = -10
            self.x_max = 10
            self.y_min = -10
            self.y_max = 10
            self.x = np.linspace(self.x_min, self.x_max, 101)
            self.y = np.linspace(self.y_min, self.y_max, 101)
            self.x, self.y = np.meshgrid(self.x, self.y)
            self.xy0 = [-7, 2]
            self.m = np.zeros_like(self.x)
            xc = [3, -4, 7, -1, 1]
            yc = [4, 0, -2, -5, -1]
            sx = [2, 3, 2, 4, 5]
            sy = [3, 2, 3, 2, 5]
            for i in range(5):
                self.m += self.gauss2d(self.x, self.y, xc[i], yc[i], sx[i], sy[i])
            self.m /= np.sum(self.m)

            self.lb = np.deg2rad([0] + [-20] * (dimensions - 1))
            self.ub = np.deg2rad([360] + [20] * (dimensions - 1))
            self.dx = 1

        elif problem in 'phi beta epsilon'.split():
            self.m = image.imread(f'{os.path.dirname(os.path.abspath(__file__))}/ergodic_data/{problem}.png')[::-1, :]
            self.m = 1 - np.mean(self.m, axis=2)
            # print(f'{np.min(self.m)=}, {np.max(self.m)=}')
            self.m /= np.sum(self.m)

            self.x_min = 0
            self.x_max = self.m.shape[1]
            self.y_min = 0
            self.y_max = self.m.shape[0]
            self.x = np.arange(self.x_max)
            self.y = np.arange(self.y_max)
            self.x, self.y = np.meshgrid(self.x, self.y)
            if problem == 'phi':
                self.xy0 = [57, 13]
                self.dx = 6.6
            if problem == 'beta':
                self.xy0 = [12, 9]
                self.dx = 6.5
            if problem == 'epsilon':
                self.xy0 = [84, 45]
                self.dx = 6

            self.lb = np.deg2rad([0] + [-30] * (dimensions - 1))
            self.ub = np.deg2rad([360] + [30] * (dimensions - 1))


    @staticmethod
    # define normalized 2D gaussian
    def gauss2d(x=0, y=0, mx=0, my=0, sx=1, sy=1):
        return 1. / (2. * np.pi * sx * sy) * np.exp(
            -((x - mx) ** 2. / (2. * sx ** 2.) + (y - my) ** 2. / (2. * sy ** 2.)))

    def evaluate(self, design, plot=False):
        path = self.path(design)
        c = self.calc_c(path)
        o = np.sum(np.abs(c - self.m))
        # o = np.sum((c - self.m) ** 2)

        p = 0
        c1 = self.x_min - path[:, 0]
        c1[c1 < 0] = 0
        c2 = path[:, 0] - self.x_max
        c2[c2 < 0] = 0
        c3 = self.y_min - path[:, 1]
        c3[c3 < 0] = 0
        c4 = path[:, 1] - self.y_max
        c4[c4 < 0] = 0

        for cp in [c1, c2, c3, c4]:
            p += np.sum(cp ** 2)

        if plot:
            print(f'{o=}, {p=}')
            self.plot(design)

        if p > 0:
            return 2 + p * 1e-2

        return o

    def calc_c(self, path):
        c = np.zeros_like(self.m)
        for i in range(path.shape[0]):
            c += self.gauss2d(self.x, self.y, path[i, 0], path[i, 1], self.dx, self.dx)

        c /= np.sum(c)
        return c

    def path(self, design):
        phi0, *theta = design
        phi = np.cumsum(np.append(0, theta)) + phi0

        path = np.array([self.xy0])
        for p in phi:
            xy = path[-1, :] + np.array([np.cos(p), np.sin(p)]) * self.dx
            path = np.append(path, [xy], axis=0)

        return path

    def plot(self, design, filename=None):

        path = self.path(design)
        c = self.calc_c(path)
        #c[c < 1e-5] = np.nan

        fig, axes = plt.subplots(ncols=3, figsize=(30, 10))
        _m = axes[0].contourf(self.x, self.y, self.m, 20)
        plt.colorbar(_m, ax=axes[0])
        _c = axes[1].contourf(self.x, self.y, c, 20)
        plt.colorbar(_c, ax=axes[1])
        _d = axes[2].contourf(self.x, self.y, self.m - c, 20)
        plt.colorbar(_d, ax=axes[2])

        axes[0].plot(path[:, 0], path[:, 1], 'k', lw=2)
        for ax in axes:
            ax.axis('equal')

        if filename is not None:
            fig.savefig(filename)
            plt.close(fig)


ergodic_function_dict_list = []
for problem, dimensions, max_evals in Ergodic.problem_cases_list():
    f_dict = {'label': f'Ergodic_{problem}_{int(dimensions)}D',
              'class': Ergodic,
              'problem': problem,
              'dimensions': dimensions,
              'max_evaluations': max_evals,
              'max_runs': 1000,
              'forward_unique_str': True,
              }
    ergodic_function_dict_list.append(f_dict)

if __name__ == '__main__':
    # Testing
    # erg = Ergodic(problem='beta', dimensions=50)
    # design0 = np.deg2rad(np.append(np.random.uniform(0, 360, 1),
    #                                np.random.uniform(-20, 20, erg.dimensions - 1)
    #                                ))
    # erg.evaluate(design0, plot=True)
    # erg.plot(design0, f'test_Ergodic_{erg.problem}_{erg.dimensions}D.png')
    #
    # import indago
    #
    # optimizer = indago.DE()
    # optimizer.evaluation_function = erg
    # optimizer.monitoring = 'dashboard'
    # optimizer.max_evaluations = 25_000
    # optimizer.processes = 'max'
    # optimizer.optimize()
    # erg.plot(optimizer.best.X, f'best_Ergodic_{erg.problem}_{erg.dimensions}D.png')
    # optimizer.plot_history(f'conv_Ergodic_{erg.problem}_{erg.dimensions}D.png')

    # Run standard test
    import indagobench
    standard_test = indagobench.StandardTest(_local_paths.st24_results_dir,
                                             # convergence_window=20, eps_max=0.05, runs_min=20,
                                             convergence_window=50, eps_max=0.01, runs_min=100,
                                             )
    standard_test.optimizers = indagobench.st24_optimizers_list
    standard_test.benchmarks = ergodic_function_dict_list#[4:]
    standard_test.run_all()

    # Results postprocessing and visualization
    if not os.path.exists(f'{_local_paths.st24_results_dir}/Ergodic/'):
        os.mkdir(f'{_local_paths.st24_results_dir}/Ergodic/')

    for fun_dict in standard_test.benchmarks:
        erg = Ergodic(fun_dict['problem'], fun_dict['dimensions'])
        results = standard_test.read_results(fun_dict)

        case_name = f'{fun_dict["label"]}_best'
        x_best = results['x_best']
        erg.plot(x_best, f'{_local_paths.st24_results_dir}/Ergodic/{case_name}.png')

        for optimizer in standard_test.optimizers:
            f = results['f ' + optimizer['label']]
            i = np.argmin(np.abs(np.median(f[:, -1]) - f[:, -1]))
            x = results[f'x ' + optimizer['label']][i, :]
            case_name = f'{fun_dict["label"]}_{optimizer["label"].replace(" ", "-")}_median'
            erg.plot(x, f'{_local_paths.st24_results_dir}/Ergodic/{case_name}.png')
