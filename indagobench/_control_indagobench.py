import pip

def run_function(f_dict):
    import subprocess

    sh_path = f'/tmp/run_{f_dict["label"]}.sh'
    py_path = f'run_{f_dict["label"]}.py'
    with open(sh_path, 'w') as bash_script, open(py_path, 'w') as py_script:

        py_script.write(f'import sys\n')
        py_script.write(f'sys.path.append("{"/".join(__file__.split("/"))}")\n')
        py_script.write(f'import indagobench\n')
        py_script.write(f'print("yes")\n')

        bash_script.writelines(f'{sys.executable} run_{f_dict["label"]}.py > run_{f_dict["label"]}.log &')

    print(subprocess.run(["bash", sh_path],
                         capture_output=True))
    # rc = call(f'{sh_path}')

if __name__ == '__main__':
    print(f'Running python script: {__file__}')
    import sys
    sys.path.append('..')
    import indago
    import numpy
    import indagobench

    print('Pyhon executable:', sys.executable)
    print('Python version:', sys.version)
    print('Indago version:', indago.__version__)
    print('Numpy version:', numpy.__version__)


    print('\nPlease select action:')
    actions = {'1': 'Run optimization',
               '2': 'Make convergence plots',
               '3': 'Make optimizer profile',
               'x': 'Exit',
               }
    for k, v in actions.items():
        print(f' [{k}] {v}')

    while True:
        cmd = input('> [' + '/'.join(actions.keys()) + '] : ')
        if cmd == '1':
            from indagobench._standard_test_functions import st24_function_dict_list
            class_functions = {}
            for f_dict in st24_function_dict_list:
                k = f_dict['class'].__name__
                if k not in class_functions.keys():
                    class_functions[k] = [f_dict]
                else:
                    class_functions[k].append(f_dict)

            class_selections = [f'{i + 1:d}' for i in range(len(class_functions))] + ['a', 'c']
            while True:
                print('\nPlease select a class:')
                for i, k in enumerate(class_functions):
                    print(f' [{i + 1}] {k} ({len(class_functions[k])} functions)')
                print(f' [a] All classes')
                print(f' [c] Cancel')

                class_action = input('> [' + '/'.join(class_selections) + '] : ')
                if class_action not in class_selections:
                    print('Wrong selection')
                    continue

                if class_action == 'c':
                    print('Canceling')
                    break

                if class_action == 'a':
                    print('Run all classes')
                    break

                else:
                    k = int(class_action) - 1
                    class_name = list(class_functions.keys())[k]
                    function_selecions = [f'{i + 1:d}' for i in range(len(class_functions[class_name]))] + ['a', 'c']
                    while True:
                        print('\nPlease select a function:')
                        for i, k in enumerate(class_functions[class_name]):
                            print(f' [{i + 1}] {k["label"]}')
                        print(f' [a] All functions')
                        print(f' [c] Cancel')

                        function_action = input('> [' + '/'.join(function_selecions) + '] : ')
                        if function_action not in function_selecions:
                            print('Wrong selection')
                            continue

                        if function_action == 'c':
                            print('Canceling')
                            break

                        if function_action == 'a':
                            print('Run all functions')
                            break

                        else:
                            f_dict = class_functions[class_name][int(function_action) - 1]
                            print(f'Running function {f_dict["label"]}')
                            run_function(f_dict)
                            break
                    break

            break
        elif cmd == '2':
            break
        elif cmd == '3':
            break
        elif cmd == 'x':
            print('Goodbye')
            break
        else:
            print(f'Invalid selection: {cmd=}')



