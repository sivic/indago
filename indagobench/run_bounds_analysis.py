import sys

from commonmark.common import replace_unsafe_char

from tryouts.sinisa.anakatabatic_metaopt_interp_par_shortestpath import runsperproc

sys.path.append('..')
import os
import indagobench
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':



    standard_test = indagobench.StandardTest(indagobench._local_paths.st24_results_dir,
                                             convergence_window=10, eps_max=0.1, runs_min=10,
                                             )
    standard_test.optimizers = indagobench.st24_optimizers_list

    standard_test.benchmarks = []
    # standard_test.benchmarks.extend(indagobench._cec2014.cec2014_all_function_dict_list)
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_10d_function_dict_list)
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_20d_function_dict_list)
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_50d_function_dict_list)
    standard_test.benchmarks.extend(indagobench.AEP_function_dict_list)
    standard_test.benchmarks.extend(indagobench._empirical_regression.EmpReg_function_dict_list)
    standard_test.benchmarks.extend(indagobench._ill_posed_problems.IPP_all_function_dict_list)
    standard_test.benchmarks.extend(indagobench._shortest_path.shortest_path_function_dict_list)
    standard_test.benchmarks.extend(indagobench._hydraulic_network.hydraulic_network_function_dict_list)
    standard_test.benchmarks.extend(indagobench._structural_frame.structural_frame_function_dict_list)
    standard_test.benchmarks.extend(indagobench._ergodic.ergodic_function_dict_list)


    failed_methods = {}
    for fun_dict in standard_test.benchmarks:
        print('\n' + fun_dict['label'])
        results = standard_test.read_results(fun_dict)
        f = fun_dict['class'](fun_dict['problem'], fun_dict['dimensions'])
        for k in results.keys():
            if k[:2] == 'x ':
                x_best = results[k]
                runs_to_remove = []
                print(f'{k}: {results[k].shape=}')
                for i in range(x_best.shape[0]):
                    x = x_best[i, :]
                    if np.any(f.lb > x) or np.any(f.ub < x):
                        print(f'BOUNDS ERROR in {k[2:]}, run {i}')
                        runs_to_remove.append(i)
                        if k[2:] in failed_methods.keys():
                            failed_methods[k[2:]] += 1
                        else:
                            failed_methods[k[2:]] = 1
                # Remove row
                print(f'{k}: {results[k].shape=}, {runs_to_remove=}')
                for r in sorted(runs_to_remove, reverse=True):
                    results[k] = np.delete(results[k], r, 0)
                # results[k] = np.delete(results[k], run_to_remove, 0)
                if len(runs_to_remove) > 0:
                    input('Press Enter to continue...')
        results_filename = os.path.normpath(f'{standard_test.results_dir}/raw/{fun_dict["label"]}.npz')
        np.savez_compressed(results_filename, **results)
        # input('Press Enter to continue...')

    print(failed_methods)