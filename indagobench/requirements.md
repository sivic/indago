mamba create -n indagobench -c conda-forge python=3.10 numpy=1.26 matplotlib=3.9 scipy=1.14 cmasher rich nlopt pymoo wntr
pip install zoopt


Download and compile code for Epanet 2.2 (Linux) from https://github.com/OpenWaterAnalytics/EPANET, or download precompiled executable (Windows) from https://www.epa.gov/water-research/epanet.
