# import sys
# sys.path.append('..')

import sys


sys.path.append('..')
import indago
import indagobench
import numpy as np
import scipy as sp
import os
import shutil
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib as mpl
import multiprocessing
import time
from datetime import timedelta, datetime
from matplotlib.collections import LineCollection
from indagobench import _local_paths


class StandardTest():

    def __init__(self, results_dir, batch_size=10, convergence_window=50, eps_max=0.005, runs_min=200):
        self.results_dir = results_dir
        if not os.path.exists(results_dir):
            os.mkdir(results_dir)
        for subdir in 'raw summary convergence other'.split():
            if not os.path.exists(f'{results_dir}/{subdir}'):
                os.mkdir(f'{results_dir}/{subdir}')
        self._batch_size = batch_size
        self._convergence_window = convergence_window
        self._eps_max = eps_max
        self._runs_min = runs_min

        self.n_samples = 10_000

        self.optimizers = []
        self.benchmarks = []
        self.history_snapshots = np.linspace(0, 1, 101)
        self.selected_optimizers = []
        self.pool = multiprocessing.Pool(10)

        self._g_formulation = 'logb' # 'exp

    def __getstate__(self):
        self_dict = self.__dict__.copy()
        del self_dict['pool']  # Why do I need to do that?
        return self_dict

    @staticmethod
    def calc_alpha(f_rs_med_rel):
        """Calculates the alpha parameter for fitted G"""
        f = np.array(f_rs_med_rel)
        # a = f ** 2
        # b = -2 * f ** 2 + 2 * f - 1
        # c = f ** 2 - 2 * f + 1
        # alpha = (-b - np.sqrt(b ** 2 - 4 * a * c)) / (2 * a)
        alpha = (1 + np.sqrt(1 + 4 * f * (f - 1))) / (2 * f)
        alpha = np.array(alpha)

        cond = f < 0.5
        # alpha[cond] = (-b[cond] + np.sqrt(b[cond] ** 2 - 4 * a[cond] * c[cond])) / (2 * a[cond])
        alpha[cond] = (1 + np.sqrt(1 + 4 * f[cond] * (f[cond] - 1))) / (2 * f[cond])

        cond = f > 0.5
        # alpha[cond] = (-b[cond] - np.sqrt(b[cond] ** 2 - 4 * a[cond] * c[cond])) / (2 * a[cond])
        alpha[cond] = (1 - np.sqrt(1 + 4 * f[cond] * (f[cond] - 1))) / (2 * f[cond])

        alpha = alpha ** 2
        return alpha

        f_rs_med_rel = np.array(f_rs_med_rel)
        alpha = np.full(np.shape(f_rs_med_rel), np.nan)

        cond = f_rs_med_rel < 0.5
        alpha[cond] = (1 + np.sqrt(1 - 4 * f_rs_med_rel[cond] * (1 - f_rs_med_rel[cond]))) / (2 * f_rs_med_rel[cond])

        cond = f_rs_med_rel > 0.5
        alpha[cond] = (1 + np.sqrt(1 + 4 * f_rs_med_rel[cond] * (1 - f_rs_med_rel[cond]))) / (2 * f_rs_med_rel[cond])

        alpha = alpha ** 2
        return alpha

    @staticmethod
    def calc_g(f_rel, alpha):
        if np.abs(alpha - 1) < 1e-3:
            return 1 - 2 * f_rel
        g = 1 - 2 * np.log(f_rel * (alpha - 1) + 1) / np.log(alpha)
        return g

    @staticmethod
    def calc_f_rel(results, f):
        return (f - results['f_best']) / (results['f_med'] - results['f_best'])


    def calc_g_convergence(self, results, optimizer, ax_run_conv=None):

        # Get all f's at 100% evaluations
        f_end = results['f ' + optimizer['label']][:, -1]
        runs = np.arange(f_end.size) + 1

        f_end_med = np.array([np.median(f_end[:r + 1]) for r in range(f_end.shape[0])])

        f_best = results['f_best']
        f_med = np.median(results['f_sampling'])
        f_rs_med = results['f_rs_med']
        f_end_rel = (f_end_med - f_best) / (f_med - f_best)
        f_rs_med_rel = (f_rs_med - f_best) / (f_med - f_best)

        alpha = self.calc_alpha(f_rs_med_rel)
        g = self.calc_g(f_end_rel, alpha)

        window = self._convergence_window  # runs
        eps = np.max(g[-window:]) - np.min(g[-window:])

        if ax_run_conv:
            ax_run_conv.axhline(-1, c='indigo', lw=1)
            ax_run_conv.axhline(0, c='indianred', lw=1)
            ax_run_conv.axhline(1, c='olivedrab', lw=1)
            ax_run_conv.axvline(runs[-1] + ax_run_conv.get_xlim()[1] * 0.01, c='silver', lw=0.5)
            ax_run_conv.plot(runs, g, 'k-', lw=1.5, zorder=10)
            props = dict(boxstyle='square,pad=0.3', facecolor='silver', alpha=0.6, edgecolor='none')
            if runs[-1] < 0.7 * ax_run_conv.get_xlim()[1]:
                ax_run_conv.text(runs[-1] + ax_run_conv.get_xlim()[1] * 0.02, -0.4, f'{runs[-1]}',
                                 fontsize=5, color='k', ha='left', va='bottom', bbox=props)
            else:
                ax_run_conv.text(runs[-1] - ax_run_conv.get_xlim()[1] * 0.02, -0.4, f'{runs[-1]}',
                                 fontsize=5, color='k', ha='right', va='bottom', bbox=props)

        else:
            print(f' - Finished runs: {f_end.size}, ' +
                  f'f_med: {f_end_med[-1]:.6e}, ' +
                  f'f_best: {results["f_best"]:.6e}, ' +
                  f'f_rs_med: {results["f_rs_med"]:.6e}, ' +
                  f'G_med: {g[-1]:.3f}, ' +
                  f'eps: {eps:.6f}'
                  )

        return eps, g[-1]

    def calc_milestones(self, results):

        # if 'f_best' not in results.keys():
        #     results['f_best'] = np.inf
        #     results['x_best'] = None
        results['f_best'] = np.inf
        results['x_best'] = None

        # print(f'>> {results["f_best"]=}')
        if 'f_sampling' in results.keys():
            f_min = np.min(results['f_sampling'])
            if f_min <= results['f_best']:
                results['f_best'] = f_min


        # print(f'{results["f_best"]=}')
        for k in list(results):  #.keys():
            if k[:2] == 'f ':
                i_best = np.argmin(results[k][:, -1])
                f_min = results[k][i_best, -1]
                # print(f'  {k}={f_min}')

                if f_min < results['f_best']:
                    results['f_best'] = f_min
                    results['x_best'] = results['x ' + k[2:]][i_best, :]
                    # print(k)

        # print(f'{results["f_best"]=}')
        if 'f RS' in results.keys():
            results['f_rs_med'] = np.median(results['f RS'][:, -1])
            i_med = np.argmin(np.abs(results['f RS'][:, -1] - results['f_rs_med']))
            results['x_rs_med'] = results['x RS'][i_med, :]
        else:
            results['f_rs_med'] = np.inf

        if 'f L-BFGS-B' in results.keys():
            results['f_bfgs_med'] = np.median(results['f L-BFGS-B'][:, -1])
            i_med = np.argmin(np.abs(results['f L-BFGS-B'][:, -1] - results['f_bfgs_med']))
            results['x_bfgs_med'] = results['x L-BFGS-B'][i_med, :]
        else:
            results['f_bfgs_med'] = np.inf

        results['alpha'] = self.calc_alpha(results['f_rs_med'])

        # print(f'{np.min(results["f_sampling"])=}')
        # print(f'{results["f_rs_med"]}')
        # print(f'{results["x_rs_med"]}')
        # print(f'{results["f_best"]=}')
        # print(f'{results["x_best"]=}')

    def function_profile(self, function_dict, ax_function_profile, ax_function_info):
        print(f'Profiling function')
        results = self.read_results(function_dict)

        instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'],
                                          instance_label='sampling')

        n_samples = self.n_samples
        f_sampling = np.full([0], 0.0)
        if 'f_sampling' in results.keys():
            f_sampling = results['f_sampling']
            n_samples -= results['f_sampling'].size
            print(f' - Available samples: {results["f_sampling"].size}')

        if n_samples > 0:
            print(f'Running uniform random sampling ({n_samples} samples)...')

            # New (parallel) code
            # pool = multiprocessing.Pool(processes=10)
            # f_sampling = pool.starmap(instance,
            #                        [(np.random.uniform(instance.lb, instance.ub),) for sample in range(n_samples)])
            #
            #
            # results['f_sampling'] = np.array(f_sampling)
            #
            # results_filename = os.path.normpath(f'{self.results_dir}/raw/{function_dict["label"]}.npz')
            # np.savez_compressed(results_filename, **results)
            # pool.terminate()
            # print(f' Evaluation done..')

            # Old (serial) code
            for i in range(n_samples):
                f_sampling = np.append(f_sampling,
                                       instance(np.random.uniform(instance.lb, instance.ub)))

                results['f_sampling'] = f_sampling
                # print(f'{f_sampling.shape=}')

                results_filename = os.path.normpath(f'{self.results_dir}/raw/{function_dict["label"]}.npz')
            np.savez_compressed(results_filename, **results)
            print(f' Evaluation done..')
            # input('Press Enter to continue...')

        self.calc_milestones(results)

        f_best = float(results['f_best'])
        f_rs_med = results['f_rs_med']
        f_sampling = results['f_sampling']
        f_med = np.median(f_sampling)
        f_sampling_rel = (f_sampling - f_best) / (f_med - f_best)
        df = (f_med - f_best) * 0.01
        f = np.linspace(f_best - df, f_med + df, 201)

        f_rel = (f - f_best) / (f_med - f_best)
        f_rs_med_rel = (f_rs_med - f_best) / (f_med - f_best) if not np.isinf(f_rs_med) else 0.4

        alpha = self.calc_alpha(f_rs_med_rel)


        ax_function_profile.yaxis.set_tick_params(labelleft=False)
        ax_function_profile.xaxis.set_tick_params(labelbottom=False)
        ax_function_profile.tick_params(#bottom=False, labelbottom=False,
                                        left=False, labelleft=False)
        ax_function_info.yaxis.set_tick_params(labelleft=False)
        ax_function_info.xaxis.set_tick_params(labelbottom=False)
        ax_function_info.tick_params(bottom=False, labelbottom=False, left=False, labelleft=False)

        props = dict(boxstyle='square,pad=0.3', facecolor='silver', alpha=0.6, edgecolor='none')
        ax_function_profile.text(0.006, 1.05, 'Function profile',
                                 fontsize=8,  # fontweight='bold',
                                 horizontalalignment='left',
                                 verticalalignment='bottom',
                                 transform=ax_function_profile.transAxes,
                                 bbox=props,
                                 zorder=-10)

        ax_function_profile.axhline(-1, c='indigo', lw=1, zorder=-1)
        ax_function_profile.axhline(0, c='indianred', lw=1, zorder=-1)
        ax_function_profile.axhline(1, c='olivedrab', lw=1, zorder=-1)

        for ax in [ax_function_profile, ax_function_info]:
            for spine in ax.spines.values():
                spine.set_visible(False)
            ax.set_facecolor("whitesmoke")

        g_sampling = self.calc_g(f_sampling_rel, alpha)

        n_bins = 30
        g_bins = np.linspace(-2, 1, n_bins + 1)
        g_hist, _ = np.histogram(g_sampling, bins=g_bins)

        samples_ticks = np.log10(np.array([10, 100, 1_000, 10_000]))
        samples_minor_ticks = np.log10(np.concatenate([np.arange(1, 10, 1),
                                                       np.arange(20, 100, 10),
                                                       np.arange(200, 1_000, 100),
                                                       np.arange(2000, 10_000, 1_000)]))
        ax_function_profile.set_xticks(samples_minor_ticks, minor=True)
        ax_function_profile.set_xticks(samples_ticks, minor=False)
        ax_function_profile.set_xticklabels(
            [f'{10.0 ** _} %' if _<0 else f'{10 ** _} %' for _ in np.array([1, 2, 3, 4])-2],
            fontsize=5, rotation=90)

        ax_function_profile.xaxis.set_ticks_position('bottom')

        ax_function_profile.tick_params(axis='x', which='minor', bottom=True,
                                                  width=.5, length=2, color='darkgrey')
        ax_function_profile.tick_params(axis='x', which='major', bottom=True,
                                                  width=.5, length=2, color='darkgrey')
        g_hist[g_hist<=1] = 1
        ax_function_profile.barh(g_bins[:-1],
                                 np.log10(g_hist),
                                 align='edge', color='gray', alpha=0.5, height=3/n_bins)

        ax_function_profile.set_xlim(0, 4)
        ax_function_profile.set_ylim(-2.05, 1.05)
        # ax_function_profile.invert_yaxis()
        ax_function_profile.grid(ls='--', lw=0.5, c='thistle', axis='x')
        # print(f'{g_hist=}')

        self.calc_milestones(results)
        alpha = self.calc_alpha(f_rs_med_rel)
        g = self.calc_g(f_rel, alpha)
        i = np.max([1, np.sum(np.isnan(g))]) # at least one is changed
        ix = np.arange(i)
        g[ix] = 1 + ix / i


        ax_function_info.plot(f, g, lw=2, c='k', zorder=5)
        ax_function_info.set_ylim(-2.05, 1.05)
        f_span = f_med - f_best
        # print(f'{f_best=}')
        # print(f'{f_med=}')
        # print(f'{f_span=}')
        # input('Press Enter to continue...')
        ax_function_info.set_xlim(f_best - 0.01 * f_span, f_med + 0.01 * f_span)
        ax_function_info.set_xlabel('$f$', fontsize=6)

        # Best
        ax_function_info.axhline(1, c='olivedrab', lw=1, zorder=-1)
        ax_function_info.plot(f_best, 1, '+', c='olivedrab', ms=10, zorder=10)
        ax_function_info.text(f_best, 1, f'$f_{{best}}={f_best:.6e}$',
                              fontsize=5,  # fontweight='bold',
                              horizontalalignment='left',
                              verticalalignment='top',
                              # transform=ax_function_info.transAxes,
                              bbox=props)
        # RS med
        ax_function_info.axhline(0, c='indianred', lw=1, zorder=-1)
        ax_function_info.plot(f_rs_med, 0, '+', c='indianred', ms=10, zorder=10)
        ax_function_info.text(f_rs_med, 0, f'$f_{{RS}}={f_rs_med:.6e}$',
                              fontsize=5,  # fontweight='bold',
                              horizontalalignment='left',
                              verticalalignment='center',
                              # transform=ax_function_info.transAxes,
                              bbox=props)
        # f med
        ax_function_info.axhline(-1, c='indigo', lw=1, zorder=-1)
        ax_function_info.plot(f_med, -1, '+', c='indigo', ms=10, zorder=10)
        ax_function_info.text(f_med, -1, f'$f_{{med}}={f_med:.6e}$',
                              fontsize=5,  # fontweight='bold',
                              horizontalalignment='right',
                              verticalalignment='bottom',
                              # transform=ax_function_info.transAxes,
                              bbox=props)

        ax_function_info.text(0.9, 0.9, f'$\\alpha={alpha:.2e}$',
                              fontsize=5,  # fontweight='bold',
                              horizontalalignment='right',
                              verticalalignment='top',
                              transform=ax_function_info.transAxes,
                              bbox=props)



    def run_indago_optimization(self, function_dict, optimizer_dict, instance_label):

        start = time.process_time()
        opt = optimizer_dict['optimizer_class']()
        opt.variant = optimizer_dict['variant']
        if not optimizer_dict['params'] is None:
            opt.params = optimizer_dict['params']

        if 'class' in function_dict.keys():
            instance = function_dict['class'](function_dict['problem'], function_dict['dimensions'],
                                              instance_label=instance_label)
            opt.evaluation_function = instance
            opt.dimensions = instance.lb.size
            opt.lb = instance.lb
            opt.ub = instance.ub
        opt.monitoring = 'none'
        opt.processes = 1

        opt.max_evaluations = function_dict['max_evaluations']

        if 'forward_unique_str' in function_dict.keys():
            opt.forward_unique_str = function_dict['forward_unique_str']
        if 'eval_fail_behavior' in function_dict.keys():
            opt.eval_fail_behavior = function_dict['eval_fail_behavior']

        best = opt.optimize()

        # if os.path.exists(instance_label):
        #     shutil.rmtree(instance_label)

        _evals = opt.history['eval'][:, 0]
        _f = opt.history['f'][:, 0]
        if _evals[-1] < function_dict['max_evaluations']:
            _evals = np.append(_evals, function_dict['max_evaluations'])
            _f = np.append(_f, _f[-1])
        f_interp = sp.interpolate.interp1d(_evals,
                                           _f,
                                           bounds_error=False,
                                           fill_value=np.nan)
        evals = self.history_snapshots * function_dict['max_evaluations']
        f = f_interp(evals).reshape((1, -1))

        t_cpu = time.process_time() - start
        return f, best.X, t_cpu

    def read_results(self, fun_dict):
        results_filename = os.path.normpath(f'{self.results_dir}/raw/{fun_dict["label"]}.npz')
        results = {'f_best': np.inf,
                   'f_rs_med': np.inf,
                   'f_bfgs_med': np.inf,
                   }
        if os.path.exists(results_filename):
            print(f' - Checking for existing results in {results_filename}')
            results = dict(np.load(results_filename))
            self.calc_milestones(results)
            # np.savez_compressed(results_filename, **results)
        return results

    def rename_optimizer(self, old_label, new_label):
        for function_dict in self.benchmarks:
            results = self.read_results(function_dict)
            for prefix in 'xft':
                old_var = f'{prefix} {old_label}'
                new_var = f'{prefix} {new_label}'
                if old_var in results.keys():
                    results[new_var] = results[old_var]
                    del results[old_var]
            results_filename = os.path.normpath(f'{self.results_dir}/raw/{function_dict["label"]}.npz')
            np.savez_compressed(results_filename, **results)

    def delete_optimizer(self, label):
        for function_dict in self.benchmarks:
            results = self.read_results(function_dict)
            for prefix in 'xft':
                var = f'{prefix} {label}'
                if var in results.keys():
                    del results[var]
            results_filename = os.path.normpath(f'{self.results_dir}/raw/{function_dict["label"]}.npz')
            np.savez_compressed(results_filename, **results)

    def analyze_function(self, optimizer_dict, fun_dict, axes=None):

        print()
        print(f'Running {optimizer_dict["label"]} on {fun_dict["label"]}')
        results = self.read_results(fun_dict)
        results_filename = os.path.normpath(f'{self.results_dir}/raw/{fun_dict["label"]}.npz')

        r0 = 0
        if 'f ' + optimizer_dict['label'] in results.keys():
            r0 = results['f ' + optimizer_dict['label']].shape[0]
        print(f' - Available finished runs: {r0}')

        if not axes is None:
            ax_run_conv, ax_f_conv, ax_f_hist, ax_percentile = axes
        else:
            assert False, 'Unexpected number of axis'

        run = r0
        batch_size = self._batch_size
        # if optimizer_dict['label'] == 'NOMAD Ortho-MADS':
        #    batch_size = 1
        eps = 1e20
        if run > 1:
            self.calc_milestones(results)
            eps, g_fin = self.calc_g_convergence(results, optimizer_dict)

        while True:
            if eps < self._eps_max and run >= self._runs_min:
                print(f' - Median converged ({g_fin=:.3f}, {eps=:.6f}, runs={run}, ' +
                      f'f_med: {np.median(results["f " + optimizer_dict["label"]][:, -1]):.6e})')
                break

            if run >= fun_dict['max_runs']:
                print(f' - Maximal number of runs reached ({g_fin=:.3f}, {eps=:.6f}, runs={run}, '
                      + f'f_med: {np.median(results["f " + optimizer_dict["label"]][:, -1]):.6e})')
                break

            start = time.time()

            pool_runs = batch_size - run % batch_size
            if 'run' in optimizer_dict.keys():
                pool_results = self.pool.starmap(self.run_3rdparty_optimization,
                                                 [(fun_dict,
                                                   optimizer_dict,
                                                   f'{fun_dict["label"]}_run{run + r:05d}') for r in range(pool_runs)])

            else:
                pool_results = self.pool.starmap(self.run_indago_optimization,
                                                 [(fun_dict,
                                                   optimizer_dict,
                                                   f'{fun_dict["label"]}_run{run + r:05d}') for r in range(pool_runs)])
            run += pool_runs

            for r, (f, x, t_cpu) in zip(range(pool_runs), pool_results):

                if ('f ' + optimizer_dict['label']) in results.keys():
                    results['f ' + optimizer_dict['label']] = np.append(results['f ' + optimizer_dict['label']],
                                                                        f.reshape((1, -1)),
                                                                        axis=0)
                else:
                    results['f ' + optimizer_dict['label']] = f.reshape((1, -1))

                if ('x ' + optimizer_dict['label']) in results.keys():
                    results['x ' + optimizer_dict['label']] = np.append(results['x ' + optimizer_dict['label']],
                                                                        x.reshape((1, -1)), axis=0)
                else:
                    results['x ' + optimizer_dict['label']] = x.reshape((1, -1))

                if ('t ' + optimizer_dict['label']) in results.keys():
                    results['t ' + optimizer_dict['label']] = np.append(results['t ' + optimizer_dict['label']],
                                                                        np.array([t_cpu]), axis=0)
                else:
                    results['t ' + optimizer_dict['label']] = np.array([t_cpu])

                all_evals_db_filename = f'{self.results_dir}/{fun_dict["label"]}_RS_evals_f.npy'

            self.calc_milestones(results)
            eps, g_fin = self.calc_g_convergence(results, optimizer_dict)
            np.savez_compressed(results_filename, **results)

            t10 = int(time.time() - start)
            runs_done = np.array([])
            for opt in self.optimizers[:self.optimizers.index(optimizer_dict)]:
                if 'f ' + opt['label'] in results:
                    runs_done = np.append(runs_done, np.shape(results['f ' + opt['label']])[0])
            if runs_done.size > 0:
                total_runs = int(np.average(runs_done) * len(self.optimizers))
            else:
                total_runs = 300 * len(self.optimizers)

            runs_done = int(np.sum(runs_done) + run)
            remaining_runs = int(total_runs - runs_done)
            remaining_time = int((t10 / batch_size) * remaining_runs)

            print(f'   {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}, t{batch_size}: {str(timedelta(seconds=t10))}, ' +
                  f'estimated remaining runs: {remaining_runs}/{total_runs}, ' +
                  f'estimated remaining time: {str(timedelta(seconds=remaining_time))}')

        f_all = results['f ' + optimizer_dict['label']].T
        evals = self.history_snapshots * fun_dict['max_evaluations']
        f_rel = (f_all - results['f_best']) / (np.median(results['f_sampling']) - results['f_best'])



        f_best = results['f_best']
        f_med = np.median(results['f_sampling'])
        f_rs_med = results['f_rs_med']
        # f_end_rel = (f_end_med - f_best) / (f_med - f_best)
        f_rs_med_rel = (f_rs_med - f_best) / (f_med - f_best)

        alpha = self.calc_alpha(f_rs_med_rel)
        g_all = self.calc_g(f_rel, alpha)

        f_all = results['f ' + optimizer_dict['label']].T
        _f_med = np.median(f_all, axis=1)
        f_med_rel = (_f_med - f_best) / (f_med - f_best)

        g_med = self.calc_g(f_med_rel, alpha)
        i = int(np.max([1, np.sum(np.isnan(g_med))])) # at least one is changed
        ix = np.arange(i)
        g_med[ix] = -1 + ix / i * (g_med[i] + 1)

        # Relative g
        f_rs_med = np.median(results['f RS'], axis=0)
        f_rs_med_rel = (f_rs_med - f_best) / (f_med - f_best)
        alpha = self.calc_alpha(f_rs_med_rel)
        g_rel_med = np.array([
            self.calc_g(f, a) for f, a in zip(f_med_rel, alpha)])
        i = np.max([1, np.sum(np.isnan(g_rel_med))]) # at least one is changed
        ix = np.arange(i)
        g_rel_med[ix] = 0 + ix / i


        if ax_f_conv:
            t = np.log10(1 + evals / evals[-1] * 9)
            t = t / t[-1] * evals[-1]
            ax_f_conv.plot(t, g_all, c='grey', lw=0.3, alpha=0.1)
            ax_f_conv.axhline(-1, c='indigo', lw=1)
            ax_f_conv.axhline(0, c='indianred', lw=1)
            ax_f_conv.axhline(1, c='olivedrab', lw=1)
            ax_f_conv.plot(t, g_rel_med, c='navy', lw=1.5)
            ax_f_conv.plot(t, g_med, c='magenta', lw=1.5)

            for i, c in zip([10, 50, 100], 'tan mediumpurple royalblue'.split()):
                props = dict(boxstyle='square,pad=0.3', facecolor=c, alpha=0.4,
                             edgecolor='none')
                t[i] = np.max([t[i], 0.0])
                ax_f_conv.text(t[i] - 0.015 * fun_dict['max_evaluations'],
                               g_rel_med[i] + (0.3 if g_rel_med[i] < 0 else -0.3),
                               f'{g_rel_med[i]:.2f}',
                               fontsize=5, color='k',
                               ha='right',
                               va='center', bbox=props)


        if ax_f_hist:
            g_fin = g_all[-1, :]
            g_fin[g_fin < -1] = -1 + 1e-6
            bins = np.linspace(-1, 1, 21)
            bins[-1] = 1 + 1e-6
            ax_f_hist.hist(g_fin, bins=bins, color='gray', alpha=0.5, density=True)
            ax_f_hist.axvline(-1, c='indigo', lw=1)
            ax_f_hist.axvline(0, c='indianred', lw=1)
            ax_f_hist.axvline(1, c='olivedrab', lw=1)
            # dy = 0

            for i, c, y in zip([10, 50, 100], 'tan mediumpurple royalblue'.split(), [0.2, 0.4, 0.6]):
                # ax_f_hist.axvline(g_med[i], c=c, lw=1.5)
                ax_f_hist.plot([g_med[i], g_med[i]],
                               [ax_f_hist.get_ylim()[0], y * ax_f_hist.get_ylim()[1]], c=c, lw=1.5)

                props = dict(boxstyle='square,pad=0.3', facecolor=c, alpha=0.4,
                             edgecolor='none')

                ax_f_hist.text(g_med[i] + (- 0.03 if g_med[i] > -0.5 else 0.03),
                               y * ax_f_hist.get_ylim()[1],
                               f'{g_med[i]:.2f}',
                               fontsize=5, color='k',
                               ha='right' if g_med[i] > -0.5 else 'left',
                               va='top', bbox=props)
                # dy += 0.25

        if ax_run_conv:
            self.calc_milestones(results)
            self.calc_g_convergence(results, optimizer_dict, ax_run_conv)


        if ax_percentile:
            ax_percentile.axhline(-1, c='indigo', lw=1)
            ax_percentile.axhline(0, c='indianred', lw=1)
            ax_percentile.axhline(1, c='olivedrab', lw=1)

            ax_percentile.axhline(np.mean(g_fin), c='dimgrey', lw=.5, ls='--')
            g_perc = np.sort(g_fin)
            perc = np.linspace(0, 100, g_perc.size)
            # perc = np.linspace(0, 100, 101)
            # g_perc = np.percentile(g_fin, perc)
            ax_percentile.plot(perc, g_perc, c='k', lw=1.5)
            # ax_percentile.fill_between(perc, g_perc, color='gray')
            ax_percentile.fill_between(perc, np.clip(g_perc, -1, 0), color='indigo', alpha=0.5, zorder=-1)
            ax_percentile.fill_between(perc, np.clip(g_perc, 0, 1), color='olivedrab', alpha=0.5, zorder=-1)

            g_interp = sp.interpolate.interp1d(perc, g_perc)
            NN = np.arange(1, 11, 1)
            GS = np.zeros_like(NN, dtype=float)
            for i, N in enumerate(NN):
                n = np.arange(1, N)
                p = 100 * 0.5 ** (1. / n)
                g = g_interp(p)
                w = 1 / (1 + np.arange(g.size))
                g_sum = np.sum(g * w) / np.sum(w)
                # print(f'\n{N=}')
                # print(f'{p=}')
                # print(f'{g=}')
                # print(f'{w=}')
                # print(f'{g_sum=}')
                GS[i] = g_sum
            # ax_percentile.plot(NN, GS, c='orange', ls='-', lw=1.5)

            # print(f'{GS=}')
            # input('Press enter to continue')

            ax_percentile.plot(p, g, '.', c='orange', ms=2, zorder=10)

            if ax_f_hist:
                props = dict(boxstyle='square,pad=0.3', facecolor='dimgray', alpha=0.4,
                             edgecolor='none')
                ax_f_hist.axvline(g_sum, c='k', lw=1.5, ls='--')

                ax_f_hist.text(g_sum + (- 0.03 if g_sum > -0.5 else 0.03),
                               (0.9) * ax_f_hist.get_ylim()[1],
                               f'{g_sum:.2f}',
                               fontsize=5, color='k',
                               ha='right' if g_sum > -0.5 else 'left',
                               va='top', bbox=props)

                # ax_f_hist.text(0, 0, f'$\mathbb{{G}}_{{combined}}={g_sum:.2f}$')

            ax_percentile.plot(50, g_interp(50), marker='+', color='royalblue', ms=6)

            if np.any(np.isnan(g_perc)):
                print(f'{g_perc=}')
                print(f'{g_fin=}')
                np.savetxt('f_all.txt', f_all)
                np.savetxt('g_all.txt', g_all)
                input('Press Enter to continue...')


        f_med = np.median(results['f ' + optimizer_dict['label']][:, -1])
        runs = np.shape(results['f ' + optimizer_dict['label']])[0]
        # G10 = g_med[10]
        # G50 = g_med[50]
        # G100 = g_med[100]
        G10 = g_rel_med[10]
        G50 = g_rel_med[50]
        G100 = g_rel_med[100]
        # input(f'{g_med.shape=}')
        # input(f'{g_all.shape=}')
        G_90perc_10 = np.percentile(g_all[10, :], 90)
        G_90perc_50 = np.percentile(g_all[50, :], 90)
        G_90perc_100 = np.percentile(g_all[100, :], 90)

        return f_med, runs, G10, G50, G100, G_90perc_10, G_90perc_50, G_90perc_100

    def matrix_plots(self, var='t_cpu'):
        n_opt = len(self.optimizers)
        n_fun = len(self.benchmarks)
        fig = plt.figure(figsize=(3 + 0.5 * n_opt, 3 + 0.5 * n_fun), dpi=100)

        x1 = 3 / (3 + 0.5 * n_opt)
        y2 = 3 / (3 + 0.5 * n_fun)
        # ax = fig.add_axes([0.22, 0.01, 0.8, 0.92])
        ax = fig.add_axes([x1, 0.01, 1 - x1, 1 - y2 - 0.01])
        t_cpu = np.full([n_fun, n_opt], np.nan)
        runs = np.full([n_fun, n_opt], np.nan)

        for i_fun, function_dict in enumerate(self.benchmarks):
            results = self.read_results(function_dict)
            for i_opt, optimizer_dict in enumerate(self.optimizers):
                if ('t ' + optimizer_dict['label']) in results.keys():
                    t_cpu[i_fun, i_opt] = np.nanmean(results['t ' + optimizer_dict['label']])
                    #print(f'{optimizer_dict["label"]} t_cpu: {t_cpu[i_fun, i_opt]}')
                if ('f ' + optimizer_dict['label']) in results.keys():
                    runs[i_fun, i_opt] = np.shape(results['f ' + optimizer_dict['label']])[0]
                # else:
                #     print(f'{function_dict["label"]} -- {optimizer_dict["label"]}')
                #     input('?')

        if var == 't_cpu':
            im = ax.matshow(t_cpu, norm=mpl.colors.LogNorm(), cmap=plt.cm.cividis)
        elif var == 'runs':
            im = ax.matshow(runs,
                            norm=mpl.colors.Normalize(vmin=0, vmax=500, clip=False),
                            cmap=plt.cm.magma)
        ax.set_xticks(np.arange(n_opt), labels=[o['label'] for o in self.optimizers], rotation=90)
        ax.set_yticks(np.arange(n_fun), labels=[f['label'] for f in self.benchmarks])
        ax.set_yticks(np.arange(n_fun), labels=[f['label'] + f' ({np.mean(t_cpu[i_fun, :]):.1f} s)' for i_fun, f in enumerate(self.benchmarks)])
        ax.axis('image')

        ax.tick_params(axis=u'both', which=u'both', length=0)

        for spine in ax.spines.values():
            spine.set_visible(False)
        # ax.axis('off')

        ax.tick_params(top=True, labeltop=True, bottom=False, labelbottom=False)
        ax.set_xticks(np.arange(-.5, n_opt, 1), minor=True)
        ax.set_yticks(np.arange(-.5, n_fun, 1), minor=True)

        # Gridlines based on minor ticks
        ax.grid(which='minor', color='w', linestyle='-', linewidth=1)
        # Add color bar below chart
        cax = fig.add_axes((0.25, 0.986, 0.7, 0.003))

        cpu_total_time = np.array([np.nanmean(t_cpu[i_fun, :]) for i_fun, f in enumerate(self.benchmarks)]) * n_opt
        print(f'{100/10*np.sum(cpu_total_time)/3600:.1f} h')
        fig.text(0.25, 1. - .4 / n_fun, f'Number of optimizers: {n_opt}'
                             f'\nNumber of functions: {n_fun}'
                             f'\nEstimated total calculation time (100 runs  on 10 threads): {100/10*np.sum(cpu_total_time)/3600/24:.1f} days',
                 va='top', ha='left',
                 size='x-large')


        if var == 't_cpu':
            fig.colorbar(im, cax=cax, orientation='horizontal', label='CPU time [s]')
            plt.savefig(f'{self.results_dir}/other/cpu_time.png')
        elif var == 'runs':
            fig.colorbar(im, cax=cax, orientation='horizontal', label='Number of finished optimizations')
            plt.savefig(f'{self.results_dir}/other/runs.png')

    def run_3rdparty_optimization(self, function_dict, optimizer_dict, instance_label):

        start = time.process_time()
        fit, x_best = optimizer_dict['run'](function_dict, optimizer_dict, instance_label)

        t_cpu = time.process_time() - start

        return fit, x_best, t_cpu

    def _benchmark_convx_clustering(self, fun_dict):
        print()
        print(f'Convergence clustering for {fun_dict["label"]}')

        fig, axes = plt.subplots(figsize=(10, 14), nrows=3, height_ratios=[1, 5, 1])
        X = np.full([0, fun_dict['dimensions']], np.nan)
        results_filename = os.path.normpath(f'{self.results_dir}/raw/{fun_dict["label"]}.npz')
        I = []
        if os.path.exists(results_filename):
            print(f' - Reading results in {results_filename}')
            results = dict(np.load(results_filename))

            for optimizer in self.optimizers[1:]:
                x = results['x ' + optimizer['label']]
                X = np.append(X, x, axis=0)
                I.append(x.shape[0])

        print(f' - {X.shape=}')

        U, S, V = np.linalg.svd(X, full_matrices=False)
        print(f' - {U.shape=}, {S.shape=}, {V.shape=}')

        axes[0].bar(np.arange(fun_dict['dimensions']) + 0.8, S / np.sum(S), width=0.4)
        axes[0].bar(np.arange(fun_dict['dimensions']) + 1.2,
                    np.cumsum(S) / np.sum(S), width=0.4)
        axes[0].set_title('SVD\'s singular values')
        axes[0].set_xticks(np.arange(S.size) + 1)

        I = np.cumsum([0] + I)
        # for i, optimizer in enumerate(self.optimizers[1:]):
        #     axes[1].scatter(U[I[i]:I[i+1], 0], U[I[i]:I[i+1], 1], marker='o',
        #                     s=10, alpha=0.3, label=optimizer['label'])
        # axes[1].legend()
        axes[1].set_title(f'{X.shape[0]} converged {fun_dict["dimensions"]}D solutions $X$ reduced to 2D space')

        gm_aic = np.array([])
        gm_bic = np.array([])
        sc_ss = np.array([])
        sc_bic = np.array([])
        clusters = np.arange(10) + 1
        # for n_clusters in clusters:
        #     print(f' - Clustering ({n_clusters=}) ... ', end='')
        #     # define the model
        #     model = GaussianMixture(n_components=n_clusters, init_params='k-means++')
        #     # fit the model
        #     fit = model.fit(X)
        #     gm_aic = np.append(gm_aic, fit.aic(X))
        #     gm_bic = np.append(gm_bic, fit.bic(X))
        #     print(f'aic={gm_aic[-1]}, bic={gm_bic[-1]}')

        # db = skl_cluster.SpectralClustering(n_clusters=3, eigen_solver="amg").fit(X)

        # db = skl_cluster.DBSCAN(eps=0.3, min_samples=int(X.shape[0] * 0.05)).fit(X)
        # db = skl_cluster.HDBSCAN(min_cluster_size=int(X.shape[0] * 0.05),
        #                      allow_single_cluster=True,
        #                      alpha=10).fit(X)
        # db = skl_cluster.OPTICS(min_samples=0.05).fit(X)
        # labels = db.labels_

        SHS = []
        for n_clusters in range(2, 11):
            db = skl_cluster.AgglomerativeClustering(n_clusters=n_clusters,
                                                     # distance_threshold=1,
                                                     linkage="complete",  # "ward", "average", "complete", "single",
                                                     ).fit(X)
            #
            #     db = skl_cluster.Birch(n_clusters=n_clusters).fit(X)
            labels = db.labels_
            ss = skl_metrics.silhouette_score(X, labels, metric='euclidean')
            shs = skl_metrics.calinski_harabasz_score(X, labels)
            print(f'{n_clusters=}, {ss=}, {shs=}')
            SHS.append(shs)
        #
        n_clusters = np.argmax(SHS) + 2

        db = skl_cluster.AgglomerativeClustering(n_clusters=n_clusters,
                                                 # distance_threshold=1,
                                                 linkage="complete",  # "ward", "average", "complete", "single",
                                                 ).fit(X)

        labels = db.labels_

        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        n_noise_ = list(labels).count(-1)

        print("Estimated number of clusters: %d" % n_clusters_)
        print("Estimated number of noise points: %d" % n_noise_)

        for lbl in np.unique(labels):
            print(f' - Cluster {lbl}: {np.sum(labels == lbl)} points')
            axes[1].scatter(U[labels == lbl, 0], U[labels == lbl, 1], marker='o',
                            s=10, alpha=0.3, label=f'Cluster {lbl} ({np.sum(labels == lbl)} points)')
        axes[1].legend()

        # print(f'{aic=}')
        # print(f'{bic=}')
        # axes[2].plot(clusters, gm_aic, 'o--', label='gm_aic')
        # axes[2].plot(clusters, gm_bic, '*:', label='gm_bic')
        # axes[2].plot(clusters, sc_ss, '*:', label='sc_ss')
        # axes[2].legend()
        # axes[2].set_ylim(-1, 2)
        axes[2].set_xlabel('No. of clusters')
        axes[2].set_title('Clustering of solutions X')
        axes[2].set_xticks(clusters)

        fig.savefig(f'{self.results_dir}/other/clstrX_{fun_dict["label"]}.png', dpi=300)
        plt.close(fig)

    def run_all(self):
        print(f'selected_optimizers: {self.selected_optimizers}')
        for benchmark in self.benchmarks:
            print(benchmark['label'])
            optimizers = self.optimizers
            if benchmark['label'] in self.selected_optimizers:
                optimizers = []
                for optimizer_dict in self.optimizers:
                    if optimizer_dict['label'] in self.selected_optimizers[benchmark['label']]:
                        optimizers.append(optimizer_dict)
                print(f'Selected optimizers: {self.selected_optimizers[benchmark["label"]]}')

            nrows = 1 + len(optimizers)
            fig = plt.figure(figsize=(6, 0.5 + 0.8 * (nrows)),
                             constrained_layout=True)
            gs = fig.add_gridspec(nrows=nrows,
                                  ncols=4,
                                  wspace=0, hspace=0,
                                  )

            ax_function_profile = fig.add_subplot(gs[0, 0])
            ax_function_info = fig.add_subplot(gs[0, 1:3])

            self.function_profile(benchmark, ax_function_profile, ax_function_info)
            fig.savefig(f'{self.results_dir}/convergence/full_conv_{benchmark["label"]}.png', dpi=300)

            ax_optimizers = []
            for i, _ in enumerate(optimizers):
                ax_r = fig.add_subplot(gs[i + 1, 0])
                ax_f = fig.add_subplot(gs[i + 1, 1])
                ax_d = fig.add_subplot(gs[i + 1, 2])
                ax_p = fig.add_subplot(gs[i + 1, 3])
                ax_optimizers.append([ax_r, ax_f, ax_d, ax_p])
            ax_optimizers = np.array(ax_optimizers)

            h = 0.5 / (0.5 + 0.8 * nrows)
            # fig.subplots_adjust(left=0.0, right=1,
            #                     bottom=0 + 0.3 * h, top=1 - 0.3 * h,
            #                     hspace=0.24, wspace=0.05)

            props = dict(boxstyle='square,pad=0.3', facecolor='silver', alpha=0.6, edgecolor='none')
            fig.suptitle(benchmark['label'], bbox=props)
            for ax in np.ravel(ax_optimizers[:-1, :]):
                # Hide X and Y axes label marks
                ax.xaxis.set_tick_params(labelbottom=False)
                ax.tick_params(bottom=False, labelbottom=False)
            for ax in np.ravel(ax_optimizers):
                ax.tick_params(left=False, labelleft=False)
                # ax.yaxis.set_tick_params(labelleft=False)
                # ax.get_xaxis().set_visible(False)
                # ax.get_yaxis().set_visible(False)
                for spine in ax.spines.values():
                    spine.set_visible(False)

                # ax.axis('off')
                ax.set_facecolor("whitesmoke")
                ax.grid(ls='--', lw=0.5, c='thistle'),

            fig.align_ylabels(ax_optimizers[:, 0])

            for i_optimizer, optimizer in enumerate(optimizers):
                props = dict(boxstyle='square,pad=0.3', facecolor='silver', alpha=0.6, edgecolor='none')
                ax_optimizers[i_optimizer, 0].text(0.02, 1.01, optimizer['label'],
                                                   fontsize=8,  # fontweight='bold',
                                                   horizontalalignment='left',
                                                   verticalalignment='bottom',
                                                   transform=ax_optimizers[i_optimizer, 0].transAxes,
                                                   bbox=props)

                runs_ticks = np.linspace(0, benchmark['max_runs'], 3)
                ax_optimizers[i_optimizer, 0].set_xticks(runs_ticks)
                ax_optimizers[i_optimizer, 0].set_xticklabels([f'{_ / 1000:.1f}k' for _ in runs_ticks],
                                                              fontsize=6, rotation=90)
                ax_optimizers[i_optimizer, 0].set_xlim(np.array([-0.03, 1.03]) * benchmark['max_runs'])
                ax_optimizers[i_optimizer, 0].set_ylim(-1.05, 1.05)

                # evals_ticks = np.array([0, 0.1, 0.5, 1]) * benchmark['max_evaluations']
                evals_ticks = np.log10(1 + np.array([0, 0.1, 0.5, 1]) * 9) * benchmark['max_evaluations']
                evals_minor_ticks = np.log10(1 + np.linspace(0, 1, 11) * 9) * benchmark['max_evaluations']
                ax_optimizers[i_optimizer, 1].set_xticks(evals_minor_ticks, minor=True)
                ax_optimizers[i_optimizer, 1].set_xticks(evals_ticks, minor=False)
                ax_optimizers[i_optimizer, 1].set_xticklabels([f'{_ / 1000:.1f}k' for _ in (np.array([0, 0.1, 0.5, 1]) * benchmark['max_evaluations'])],
                                                              fontsize=6, rotation=90)
                # ax_optimizers[i_optimizer, 1].set_xlim(np.array([-0.03, 1.03]) * benchmark['max_evaluations'])
                # ax_optimizers[i_optimizer, 1].set_xscale('symlog', linthresh=0.01*benchmark['max_evaluations'], linscale=0.2)
                ax_optimizers[i_optimizer, 1].set_xlim(-0.01 * benchmark['max_evaluations'], 1.01 * benchmark['max_evaluations'])
                ax_optimizers[i_optimizer, 1].set_ylim(-1.05, 1.05)
                ax_optimizers[i_optimizer, 1].tick_params(axis='x', which='minor', bottom=True,
                                                          width=.5, length=2, color='darkgrey')
                ax_optimizers[i_optimizer, 1].tick_params(axis='x', which='major', bottom=True,
                                                          width=.5, length=2, color='darkgrey')

                sg_ticks = np.array([-1, 0, 1])
                ax_optimizers[i_optimizer, 2].set_xticks(sg_ticks)
                ax_optimizers[i_optimizer, 2].set_xticklabels([f'{_:.1f}' for _ in sg_ticks],
                                                              fontsize=6, rotation=90)
                ax_optimizers[i_optimizer, 2].set_yticks([])
                ax_optimizers[i_optimizer, 2].set_xlim(-1.05, 1.05)
                ax_optimizers[i_optimizer, 2].set_ylim(0, 10)

                perc_ticks = np.linspace(0, 100, 6)
                ax_optimizers[i_optimizer, 3].set_xticks(perc_ticks)
                ax_optimizers[i_optimizer, 3].set_xticklabels([f'{_:.0f}%' for _ in perc_ticks],
                                                              fontsize=6, rotation=90)
                ax_optimizers[i_optimizer, 3].set_yticks([])
                ax_optimizers[i_optimizer, 3].set_xlim(-2.5, 102.5)
                ax_optimizers[i_optimizer, 3].set_ylim(-1.05, 1.05)

            ax_optimizers[0, 0].set_title('$\mathbb{G}_{FIN}(r)$', fontsize=10, x=0.5, y=1.2)
            ax_optimizers[0, 1].set_title('$\mathbb{G}(t)$', fontsize=10, x=0.5, y=1.2)
            ax_optimizers[0, 2].set_title('$\mathbb{G}_{FIN}$ distribution', fontsize=10, x=0.5, y=1.2)
            ax_optimizers[0, 3].set_title('$\mathbb{G}_{FIN}$ percentiles', fontsize=10, x=0.5, y=1.2)

            ax_optimizers[-1, 0].set_xlabel('Runs', fontsize=6)
            ax_optimizers[-1, 1].set_xlabel('Evaluations', fontsize=6)
            ax_optimizers[-1, 2].set_xlabel('$\mathbb{G}$', fontsize=6)

            f_med, Gr_10, Gr_50, Gr_100, G_90perc_10, G_90perc_50, G_90perc_100 = [np.full(len(optimizers), np.nan) for i in range(7)]
            runs = np.full(len(optimizers), 0)
            for i_optimizer, optimizer in enumerate(optimizers):
                r = self.analyze_function(optimizer,
                                          benchmark,
                                            ax_optimizers[i_optimizer, :])

                (f_med[i_optimizer], runs[i_optimizer],
                 Gr_10[i_optimizer], Gr_50[i_optimizer], Gr_100[i_optimizer],
                 G_90perc_10[i_optimizer], G_90perc_50[i_optimizer], G_90perc_100[i_optimizer]) = r

                fig.align_ylabels(ax_optimizers[:, 0])

                if benchmark['label'] in self.selected_optimizers:
                    fig.savefig(f'{self.results_dir}/convergence/selected_conv_{benchmark["label"]}.png', dpi=300)
                else:
                    fig.savefig(f'{self.results_dir}/convergence/full_conv_{benchmark["label"]}.png', dpi=300)

            if benchmark['label'] in self.selected_optimizers:
                fig.savefig(f'{self.results_dir}/convergence/selected_conv_{benchmark["label"]}.png', dpi=300)
            else:
                fig.savefig(f'{self.results_dir}/convergence/full_conv_{benchmark["label"]}.png', dpi=300)
            plt.close(fig)

            results = self.read_results(benchmark)

            lines = []
            lines.append('# ' + benchmark['label'])
            lines.append('# ' + '-' * 90)
            lines.append(
                f'# {"Optimizer":30s} {"f_med":>15s}'
                f' {"Gr_med 10%":>15s} {"Gr_med 50%":>15s} {"Gr_med 100%":>15s}'
                f' {"G_90perc10%":>15s} {"G_90perc50%":>15s} {"G_90perc100%":>15s} {"runs":>10s}')
            for i_optimizer, optimizer in enumerate(optimizers):
                lines.append(f'  {optimizer["label"]:30s} {f_med[i_optimizer]:15.6e}'
                             f' {Gr_10[i_optimizer]:15.3f} {Gr_50[i_optimizer]:15.3f} {Gr_100[i_optimizer]:15.3f}'
                             f' {G_90perc_10[i_optimizer]:15.3f} {G_90perc_50[i_optimizer]:15.3f} {G_90perc_100[i_optimizer]:15.3f}'
                             f' {runs[i_optimizer]:10d}')
            lines.append('# ' + '-' * 90)
            lines.append(f'  f_best={results["f_best"]}')
            lines.append(f'  x_best={results["x_best"]}')
            lines.append(f'  f_rs_med={results["f_rs_med"]}')
            lines.append(f'  x_rs_med={results["x_rs_med"]}')
            lines.append(f'  f_bfgs_med={results["f_bfgs_med"]}')
            lines.append(f'  x_bfgs_med={results["x_bfgs_med"]}')
            lines.append('# ' + '-' * 90)

            summary_filename = f'{self.results_dir}/summary/summary_{benchmark["label"]}.txt'
            if os.path.exists(summary_filename):
                os.remove(summary_filename)
            print()
            with open(summary_filename, 'w') as summary_file:
                for line in lines:
                    summary_file.write(f'{line}\n')
                    print(line)
            # input('?')

    def read_summary(self, fun_dict):
        summary_filename = f'{self.results_dir}/summary/summary_{fun_dict["label"]}.txt'
        # print(f'Reading {summary_filename}')
        G = {}
        with (open(summary_filename, 'r') as summary_file):
            lines = summary_file.readlines()
            for line in lines[3:]:
                if line[0] == '#':
                    break
                else:
                    optimizer_label = line[:34].strip()
                    G[optimizer_label] = np.array([float(g) for g in line[30:].strip().split()[1:7]])
                    if np.any(np.isnan(G[optimizer_label])):
                        print('Unexpected NaN in summary file')
                        print(summary_filename)
                        print(G[optimizer_label])
                        # input('?')

        return G

    def prepare_radar_plot(self, ax=None, cat_labels=True):
        g_offset = 0
        if ax is None:
            fig, ax = plt.subplots(figsize=(4, 4))

        for spine in ax.spines.values():
            spine.set_visible(False)

        ax.set_xlim(-1, 1)
        ax.set_ylim(-1, 1)
        ax.axis('equal')
        ax.axis('off')
        ax.set_facecolor("whitesmoke")

        categories = ['High dim.', 'Exhaustive', 'Local', 'Low dim.', 'Fast', 'Global']
        angles = np.linspace(0, 2 * np.pi, 7)[:-1] + np.pi / 6 + 2 * np.pi / 3
        angles_all = np.append(angles, angles[0])

        # axis.plot((1 + g_offset) * np.cos(angles_all),
        #           (1 + g_offset) * np.sin(angles_all),
        #           c='olivedrab', lw=0.2, ls='-', alpha=1, zorder=10)
        # axis.plot((g_offset) * np.cos(angles_all),
        #           (g_offset) * np.sin(angles_all),
        #           c='indianred', lw=0.5, ls='-', alpha=1, zorder=10)
        for g_levl in np.arange(0, 5) * 0.25:
            ax.plot((g_levl + g_offset) * np.cos(angles_all),
                    (g_levl + g_offset) * np.sin(angles_all),
                    c='k', lw=0.1, ls='-', alpha=1, zorder=10)

        props = dict(boxstyle='square,pad=0.3', facecolor='silver', alpha=1, edgecolor='none')
        for a, c in zip(angles, categories):
            x = (1.05 + g_offset) * np.cos(a)
            y = (1.05 + g_offset) * np.sin(a)
            if cat_labels:
                ax.text(x, y, c, color='k', size=6,
                        # bbox=props,
                        horizontalalignment='center',
                        verticalalignment='center',
                        zorder=2)
            ax.plot((np.array([0, 0.05]) + g_offset) * np.cos(a),
                    (np.array([0, 0.05]) + g_offset) * np.sin(a),
                    c='k', lw=0.5, ls='-')
            ax.plot(x, y, c='thistle', lw=0.5, ls='-')

        plt.subplots_adjust(left=0, right=1,
                            bottom=0, top=1)

    #        return fig, ax

    def optimizer_profile(self, optimizer_dict, ax_radar=None, axes_hist=None, optimizer_label=False):

        g_offset = 0.0
        print(f'{optimizer_dict["label"]:20s} ', end='')
        n_functions = len(self.benchmarks)
        w_lowd = sp.interpolate.interp1d([0, 10, 30, 1_000], [1, 1, 0, 0])

        G_all = np.full((n_functions, 3), np.nan)
        G_all_90perc = np.full((n_functions, 3), np.nan)
        # G_fast, G_exh, G_loc, G_glob, G_lowd, G_highd = [np.full((n_functions, 3), np.nan) for _ in range(6)]
        W_fast, W_exh, W_loc, W_glob, W_lowd, W_highd = [np.full((n_functions, 3), np.nan) for _ in range(6)]

        for i_case, case_dict in enumerate(self.benchmarks):

            # print(case_dict['label'])
            instance = case_dict['class'](case_dict['problem'], case_dict['dimensions'], instance_label=None)

            G_case = self.read_summary(case_dict)
            # print(f'{G_case=}')

            # Fast-exhaustive weights
            W_fast[i_case, :] = [1, 0.5, 0]  # G 10%, G 50%, G 100%
            W_exh[i_case, :] = 1 - W_fast[i_case, :]

            # Local-global weights
            G_bfgs = G_case['L-BFGS-B'][:3]
            positive_G_bfgs = np.copy(G_bfgs)
            positive_G_bfgs[G_bfgs < 0] = 0
            W_loc[i_case, :] = positive_G_bfgs[2]
            W_glob[i_case, :] = 1 - positive_G_bfgs[2]

            # Low-high dimensions weights
            W_lowd[i_case, :] = w_lowd(instance.lb.size)
            W_highd[i_case, :] = 1 - w_lowd(instance.lb.size)

            if optimizer_dict['label'] in G_case.keys():
                G_all[i_case, :] = G_case[optimizer_dict['label']][0:3]
                G_all_90perc[i_case, :] = G_case[optimizer_dict['label']][3:6]

                # if np.any(G_all_90perc[i_case, :] < G_all[i_case, :]):
                #     print(G_all_90perc[i_case, :])
                #     print(G_all[i_case, :])
                #     print(G_case)
                #     print('ERROR: 90-percentile worse than median (50-percentile): ' + optimizer_dict['label'] + ' on ' + case_dict['label'])
                #     # input('Press Enter to continue...')
            else:
                pass
                # print(f'{optimizer_dict["label"]} missing')

        if not axes_hist is None:
            for i in range(3):
                axes_hist[i].hist(G_all[:, i], bins=np.linspace(-1, 1, 21),
                                  density=True,
                                  color='silver')
                axes_hist[i].set_ylim(0, 10)

        # print(G_all)
        G_values = np.full(6, np.nan)
        G_90perc_values = np.full(6, np.nan)

        G_values[4] = np.sum(G_all * W_fast) / np.sum(W_fast)
        G_values[1] = np.sum(G_all * W_exh) / np.sum(W_exh)
        G_90perc_values[1] = np.sum(G_all_90perc * W_exh) / np.sum(W_exh)
        G_90perc_values[4] = np.sum(G_all_90perc * W_fast) / np.sum(W_fast)

        # G_values[0] = np.sum(np.mean(G_all, axis=1) * W_highd[:, 0]) / np.sum(W_highd[:, 0])
        # G_values[3] = np.sum(np.mean(G_all, axis=1) * W_lowd[:, 0]) / np.sum(W_lowd[:, 0])
        G_values[0] = np.sum(G_all[:, 2] * W_highd[:, 2]) / np.sum(W_highd[:, 2])
        G_values[3] = np.sum(G_all[:, 2] * W_lowd[:, 2]) / np.sum(W_lowd[:, 2])
        G_90perc_values[0] = np.sum(G_all_90perc[:, 2] * W_highd[:, 2]) / np.sum(W_highd[:, 2])
        G_90perc_values[3] = np.sum(G_all_90perc[:, 2] * W_lowd[:, 2]) / np.sum(W_lowd[:, 2])

        # G_values[2] = np.sum(np.mean(G_all[:, 0], axis=1) * W_loc[:, 0]) / np.sum(W_loc[:, 0])
        # G_values[5] = np.sum(np.mean(G_all[:, 0], axis=1) * W_glob[:, 0]) / np.sum(W_glob[:, 0])
        G_values[2] = np.sum(G_all[:, 2] * W_loc[:, 2]) / np.sum(W_loc[:, 2])
        G_values[5] = np.sum(G_all[:, 2] * W_glob[:, 2]) / np.sum(W_glob[:, 2])
        G_90perc_values[2] = np.sum(G_all_90perc[:, 2] * W_loc[:, 2]) / np.sum(W_loc[:, 2])
        G_90perc_values[5] = np.sum(G_all_90perc[:, 2] * W_glob[:, 2]) / np.sum(W_glob[:, 2])

        with open(f'{self.results_dir}/other/G_{optimizer_dict["label"]}.txt', 'w') as tbl:
            for i, f in enumerate(self.benchmarks):
                tbl.write(f'{f["label"]:50s}')
                tbl.write(f'{G_all[i, 0]:10.2f}')
                tbl.write(f'{G_all[i, 1]:10.2f}')
                tbl.write(f'{G_all[i, 2]:10.2f}\n')
        # print(G_values)
        # print(G_90perc_values)
        # print(G_all_90perc)
        print(f'{np.nanmean(G_all):15.3f} {np.nanmean(G_all[:, 2]):12.3f}')


        props = dict(boxstyle='square,pad=0.3', facecolor='silver', alpha=1, edgecolor='none')
        if ax_radar is None:
            fig, ax_radar = self.prepare_radar_plot()
        savefig = ax_radar is None
        if optimizer_label:
            ax_radar.text(0.01, 0.99, optimizer_dict['label'],
                          fontsize=10,  # fontweight='bold',
                          horizontalalignment='left',
                          verticalalignment='top',
                          transform=ax_radar.transAxes,
                          bbox=props)

        angles = np.linspace(0, 2 * np.pi, 7)[:-1] + np.pi / 6 + 2 * np.pi / 3
        angles_all = np.append(angles, angles[0])
        G_sorted = np.append(G_values, G_values[0])

        # Plot data
        x_all = (G_sorted + g_offset) * np.cos(angles_all)
        y_all = (G_sorted + g_offset) * np.sin(angles_all)

        x_dense = x_all[0:1]
        y_dense = y_all[0:1]
        g_dense = G_sorted[0:1]
        for _x, _y, _g in zip(x_all[1:], y_all[1:], G_sorted[1:]):
            # print(f'{x_dense=}')
            x_dense = np.append(x_dense, np.linspace(x_dense[-1], _x, 15)[1:])
            y_dense = np.append(y_dense, np.linspace(y_dense[-1], _y, 15)[1:])
            g_dense = np.append(g_dense, np.linspace(g_dense[-1], _g, 15)[1:])

            # x_dense = np.append(x_dense, _x)
            # y_dense = np.append(y_dense, _y)
            # g_dense = np.append(g_dense, _g)
            # print(f'>>{g_dense.shape=}')

        g_dense = 0.5 * (g_dense[:-1] + g_dense[1:])[:x_dense.size - 1]

        points = np.array([x_all, y_all]).T.reshape(-1, 1, 2)
        points = np.array([x_dense, y_dense]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)

        #print(f'{segments=}')
        # print(f'{g_dense.shape=}, {segments.shape=}, {G_all.shape=}, {x_all.shape=}')
        # Create a continuous norm to map from data points to colors
        # norm = plt.Normalize(G_all.min(), G_all.max())
        norm = plt.Normalize(0, 1)
        lc = LineCollection(segments, cmap='plasma', norm=norm, zorder=50)
        # Set the values used for colormapping
        lc.set_array(g_dense)
        lc.set_linewidth(2 + g_dense)
        line = ax_radar.add_collection(lc)
        #fig.colorbar(line, ax=axs[0])

        # Old constant color plot
        # L = ax_radar.plot(x_all, y_all, c='grey',
        #                   marker='s', ms=3, zorder=50,
        #                   linewidth=0, linestyle='solid',
        #                   label=optimizer_dict['label'])
        ax_radar.scatter(x_all, y_all, 2 + 5 * G_sorted,
                         marker='s', color='k', zorder=50,
                         label=optimizer_dict['label'])
        for a, g in zip(angles, G_values):
            _g = np.array([g_offset, g_offset + g])
            # axis.plot(_g * np.cos(a), _g * np.sin(a), linewidth=1, linestyle='solid',
            #           alpha=1, color='k')#L[0].get_color())

        def reorder(poly, cw=True):
            """Reorders the polygon to run clockwise or counter-clockwise
            according to the value of cw. It calculates whether a polygon is
            cw or ccw by summing (x2-x1)*(y2+y1) for all edges of the polygon,
            see https://stackoverflow.com/a/1165943/898213.
            """
            # Close polygon if not closed
            if not np.allclose(poly[:, 0], poly[:, -1]):
                poly = np.c_[poly, poly[:, 0]]
            direction = ((poly[0] - np.roll(poly[0], 1)) *
                         (poly[1] + np.roll(poly[1], 1))).sum() < 0
            if direction == cw:
                return poly
            else:
                return poly[:, ::-1]

        def ring_coding(n):
            """Returns a list of len(n) of this format:
            [MOVETO, LINETO, LINETO, ..., LINETO, LINETO CLOSEPOLY]
            """
            codes = [Path.LINETO] * n
            codes[0] = Path.MOVETO
            codes[-1] = Path.CLOSEPOLY
            return codes

        x = (G_90perc_values + g_offset) * np.cos(angles)
        y = (G_90perc_values + g_offset) * np.sin(angles)
        ext = np.array([x, y])
        t = -np.linspace(0, 2 * np.pi)
        hole = np.array([g_offset * np.cos(angles), g_offset * np.sin(angles)])
        polys = [ext, hole]
        ccw = [True] + ([False] * (len(polys) - 1))
        polys = [reorder(poly, c) for poly, c in zip(polys, ccw)]
        codes = np.concatenate([ring_coding(p.shape[1]) for p in polys])
        vertices = np.concatenate(polys, axis=1)

        patch = PathPatch(Path(vertices.T, codes), alpha=0.3, color='grey')

        ax_radar.add_patch(patch)

        if savefig:
            plt.savefig(f'{self.results_dir}/other/profile_{optimizer_dict["label"]}.png', dpi=300)
            plt.close()

        fig, ax = plt.subplots(figsize=(10, 5))
        ax.hist(G_all[:, 2], bins=np.linspace(-2, 1, 31))
        ax.set_ylim(0, 60)
        plt.savefig(f'{self.results_dir}/profile_{optimizer_dict["label"]}.png', dpi=300)
        plt.close()
        #print(f'{G_all.shape=}')
        # input('?')

    def compare_cpu_time(self,):
        n_opt = len(self.optimizers)
        n_fun = len(self.benchmarks)
        fig = plt.figure(figsize=(3 + 0.5 * n_opt, 3 + 0.5 * n_fun), dpi=100)

        x1 = 3 / (3 + 0.5 * n_opt)
        y2 = 3 / (3 + 0.5 * n_fun)
        # ax = fig.add_axes([0.22, 0.01, 0.8, 0.92])
        ax = fig.add_axes([x1, 0.01, 1 - x1, 1 - y2 - 0.01])
        t1 = np.full([n_fun, n_opt], np.nan)
        t2 = np.full([n_fun, n_opt], np.nan)

        for i_fun, function_dict in enumerate(self.benchmarks):
            results = self.read_results(function_dict)
            for i_opt, optimizer_dict in enumerate(self.optimizers):
                if ('t ' + optimizer_dict['label']) in results.keys():
                    t_cpu = results['t ' + optimizer_dict['label']]
                    if t_cpu.size >= 100:
                        t1[i_fun, i_opt] = np.median(t_cpu[:50])
                        t2[i_fun, i_opt] = np.median(t_cpu[50:])
                        # print(optimizer_dict['label'] + f' {t_cpu=}')
                        # print(f'{t1[i_fun, i_opt]} {t2[i_fun, i_opt]}')
                        # input('?')

        for i_opt, optimizer_dict in enumerate(self.optimizers):
            print(f'{optimizer_dict["label"]} t_cpu factor: {np.nanmean(t2[:, i_opt] / t1[:, i_opt])}')

if __name__ == '__main__':
    standard_test = indagobench.StandardTest(_local_paths.st24_results_dir,
                                             convergence_window=50, eps_max=0.01, runs_min=100,
                                             )
    standard_test.optimizers = indagobench.st24_optimizers_list

    standard_test.benchmarks = []
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_10d_function_dict_list)
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_20d_function_dict_list)
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_50d_function_dict_list)
    standard_test.benchmarks.extend(indagobench.AEP_function_dict_list)
    standard_test.benchmarks.extend(indagobench._empirical_regression.EmpReg_function_dict_list)
    standard_test.benchmarks.extend(indagobench._ill_posed_problems.IPP_all_function_dict_list)
    standard_test.benchmarks.extend(indagobench._shortest_path.shortest_path_function_dict_list)
    standard_test.benchmarks.extend(indagobench._hydraulic_network.hydraulic_network_function_dict_list)
    standard_test.benchmarks.extend(indagobench._structural_frame.structural_frame_function_dict_list)
    standard_test.benchmarks.extend(indagobench._ergodic.ergodic_function_dict_list)
    standard_test.benchmarks.extend(indagobench._airfoil_design.AirfoilDesign_function_dict_list)

    # standard_test.rename_optimizer('ABC Vanilla', 'ABC')
    # standard_test.rename_optimizer('BA Vanilla', 'BA')
    # standard_test.rename_optimizer('MRFO Vanilla', 'MRFO')
    # standard_test.rename_optimizer('SSA Vanilla', 'SSA')
    # standard_test.optimizers = [indagobench._standard_test_optimizers.nomadlib_dict,
    #                             indagobench._standard_test_optimizers.mads_dict,
    #                             indagobench._standard_test_optimizers.nomadbatch_dict,
    #                             ]

    standard_test.matrix_plots(var='t_cpu')
    standard_test.matrix_plots(var='runs')
    # standard_test.compare_cpu_time()
    # for fun_dict in standard_test.benchmarks:
    #     print('\n' + fun_dict['label'])
    #     results = standard_test.read_results(fun_dict)
    #     f = results['f L-BFGS-B'][:, 0]
    #     print(f)

