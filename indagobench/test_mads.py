#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 24 14:26:01 2024

@author: stefan
"""

import numpy as np
import matplotlib.pyplot as plt
import mads

def f(x):
    ff = np.sum((x - np.arange(x.size)**2) ** 2 * (2 + np.cos(x)))
    return ff

d = 12
# Provide the problem configuration
lb = np.full(d, -100)             # Lower bounds for design variables
ub = np.full(d, 100)             # Upper bounds for design variables
x0 = np.random.uniform(lb, ub)   # Initial design variables
dp_tol = 1e-20                               # Minimum poll size stopping criteria
max_evals = 1_000                             # Maximum objective function evaluations
dp = 0.1                                    # Initial poll size as percent of bounds
dm = 0.01                                   # Initial mesh size as percent of bounds

# Run the optimizer
r = mads.orthomads(x0, ub, lb, f, 
                   dp, dm, dp_tol, max_evals, 
                   displog=False, savelog=False)

print(f'Evaluations: {r[2].shape[0]}')
print(f'f_best: {r[0]}')
print(f'x_best: {r[1]}')

plt.plot(r[2])
