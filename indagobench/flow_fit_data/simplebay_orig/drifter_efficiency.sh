#!/bin/bash

export PATH=$PATH:/usr/lib64/mpi/gcc/openmpi/bin/
source /opt/openfoam/OpenFOAM-v2306/etc/bashrc

postProcess -func writeCellCentres > log.cell_centers
postProcess -func writeCellVolumes > log.cell_volumes
