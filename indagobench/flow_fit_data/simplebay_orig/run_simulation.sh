#!/bin/bash

export PATH=$PATH:/usr/lib64/mpi/gcc/openmpi/bin/
source /opt/openfoam/OpenFOAM-v2306/etc/bashrc


# NPROC=$(nproc)
# cores=4
# sed -i "s/numberOfSubdomains.*[0-9][0-9]*;/numberOfSubdomains $cores;/g" system/decomposeParDict
# decomposePar > log.decomposePar
# 
# mpirun -np $cores simpleFoam -parallel -fileHandler collated > log.simpleFoam 2>&1
# 
# reconstructPar -latestTime > log.reconstructPar 2>&1

simpleFoam > log.simpleFoam
