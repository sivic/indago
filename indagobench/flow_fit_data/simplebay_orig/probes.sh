#!/bin/bash

export PATH=$PATH:/usr/lib64/mpi/gcc/openmpi/bin/
source /opt/openfoam/OpenFOAM-v2306/etc/bashrc

postProcess -func drifter_probes > log.drifter_probes
postProcess -func field_probes > log.field_probes
