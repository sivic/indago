import os.path
import sys
sys.path.append('..')

import indagobench
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import numpy as np

if __name__ == '__main__':

    standard_test = indagobench.StandardTest(indagobench._local_paths.st24_results_dir,
                                             convergence_window=10, eps_max=0.1, runs_min=10,
                                             )
    standard_test.optimizers = indagobench.st24_optimizers_list

    standard_test.benchmarks = []
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_10d_function_dict_list)
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_20d_function_dict_list)
    standard_test.benchmarks.extend(indagobench._cec2014.cec2014_50d_function_dict_list)
    standard_test.benchmarks.extend(indagobench.AEP_function_dict_list)
    standard_test.benchmarks.extend(indagobench._empirical_regression.EmpReg_function_dict_list)
    standard_test.benchmarks.extend(indagobench._ill_posed_problems.IPP_all_function_dict_list)
    standard_test.benchmarks.extend(indagobench._shortest_path.shortest_path_function_dict_list)
    standard_test.benchmarks.extend(indagobench._hydraulic_network.hydraulic_network_function_dict_list)
    standard_test.benchmarks.extend(indagobench._structural_frame.structural_frame_function_dict_list)
    standard_test.benchmarks.extend(indagobench._ergodic.ergodic_function_dict_list)

    n_functions = len(standard_test.benchmarks)
    n_optimizers = len(standard_test.optimizers)
    optimizer_labels = [optimizer_dict['label'] for optimizer_dict in standard_test.optimizers]
    function_labels = [function_dict['label'] for function_dict in standard_test.benchmarks]

    npz_filename = indagobench._local_paths.st24_results_dir + '/svd.npz'
    if os.path.exists(npz_filename):
        G = np.load(npz_filename)['G']
    else:
        G = np.full([n_functions, n_optimizers], np.nan)
        for i_function, function_dict in enumerate(standard_test.benchmarks):
            results = standard_test.read_results(function_dict)
            # print(results.keys())
            for i_optimizer, optimizer_dict in enumerate(standard_test.optimizers):
                f = results['f ' + optimizer_dict['label']][:, -1]
                f = f[np.logical_not(np.isnan(f))]
                f_rel = standard_test.calc_f_rel(results, f)
                alpha = standard_test.calc_alpha(standard_test.calc_f_rel(results, results['f_rs_med']))
                g = standard_test.calc_g(f_rel, alpha)

                g = g[np.logical_not(np.isnan(g))]
                # if np.any(np.isnan(f)):
                #     print(f'{optimizer_dict["label"]=}')
                #     input('Press Enter to continue...')
                # if np.any(np.isnan(g)):
                #     print(f'{optimizer_dict["label"]=}')
                #     print(f'{f.shape=}')
                #     print(f'{g.shape=}')
                #     for _f, _g in zip(f, g):
                #         print(_f, _g)
                #     input('Press Enter to continue...')
                G[i_function, i_optimizer] = np.median(g)
        np.savez_compressed(npz_filename, G=G)

    print(f'{G.shape=}')
    print(f'{np.any(np.isnan(G))=}')
    print(f'{np.min(G)=}')
    print(f'{np.max(G)=}')

    # SVD
    U, S, V = np.linalg.svd(G, full_matrices=False)

    k = 5

    fig = plt.figure(figsize=(24, (k + 1) * 1.2), layout="constrained")
    gs = GridSpec(nrows=k+1, ncols=2, figure=fig,
                  width_ratios=[1, 8])

    ax_s = fig.add_subplot(gs[0, 0])
    ax_s.bar(np.arange(k) + 1, np.cumsum(S[:k] / np.sum(S)), color='grey')
    ax_s.bar(np.arange(k) + 1, S[:k] / np.sum(S), color=[f'C{i}' for i in range(k)])
    # ax_s.axis('off')
    # ax_s.get_xaxis().set_visible(False)
    # ax_s.get_yaxis().set_visible(False)
    ax_s.spines['top'].set_visible(False)
    ax_s.set_ylim(0, 1)
    ax_s.tick_params(top='off', bottom='off', left='off', right='off', labelleft='on', labelbottom='on')
    ax_s.tick_params(labelsize=4)
    ax_s.grid(ls=':', c='lightgray', lw=0.3)

    ax_s.set_frame_on(False)
    ax_s.set_facecolor("whitesmoke")

    ax_g = fig.add_subplot(gs[0, 1])
    ax_g.matshow(G.T)
    ax_g.axis('off')
    ax_g.set_facecolor("whitesmoke")

    print(f'{V.shape=}')
    print(f'{U.shape=}')
    for i in range(k):
        if np.sum(V[i, :]) < 0:
            U[:, i] *= -1
            V[i, :] *= -1
        v = V[i, :]
        u = U[:, i]

        ax_v = fig.add_subplot(gs[1 + i, 0])
        ax_v.set_xlim(-0.5, n_optimizers - 0.5)
        ax_v.set_frame_on(False)
        ax_v.set_facecolor("whitesmoke")
        ax_v.axhline(lw=0.3, c='k')
        ax_v.bar(optimizer_labels, v, color=f'C{i}')
        if i + 1 == k:
            ax_v.tick_params(axis='x', rotation=90, labelsize=4)
            ax_v.set_yticks([])
        else:
            ax_v.set_xticks([])
            ax_v.axis('off')

        ax_u = fig.add_subplot(gs[1 + i, 1])
        ax_u.set_xlim(-0.5, n_functions - 0.5)
        ax_u.set_frame_on(False)
        ax_u.set_facecolor("whitesmoke")
        ax_u.axhline(lw=0.3, c='k')
        ax_u.bar(function_labels, u, color=f'C{i}')
        if i + 1 == k:
            ax_u.tick_params(axis='x', rotation=90, labelsize=4)
            ax_u.set_yticks([])
        else:
            ax_u.set_xticks([])
            ax_u.axis('off')

    plt.savefig(indagobench._local_paths.st24_results_dir + '/indagobench_pca.pdf')
    plt.show()