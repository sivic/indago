# problem_label         dimensions  max_evals
corner3                 4           2_000
corner5                 8           4_000
cantileverbeam8         11          7_000
cantileverbeam14        22          10_000
archbridgei             4           2_000
archbridgev             4           1_500
archbridgem             4           1_500
suspensionbridgei       5           2_500
suspensionbridgeii      5           2_500
box3x3                  12          10_000
box4x3                  17          20_000
box3x4                  17          20_000
box4x4                  24          30_000
box5x4                  31          50_000
box4x5                  31          50_000
box5x5                  40          80_000