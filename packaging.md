# Packaging instructions

Do the following **in given order**:

1. Change the Indago version in `indago/__init__.py`.
2. Run `build.sh`, select target (default PyPI), wait while building, provide login credentials, wait while uploading.
3. Commit changed documentation (only version is changed in `index.rst`) to `master` branch. 
4. Merge `master` to `release` branch in order to update documentation on readthedocs.org.



Manual commands for step 3 (called in `build.sh`) are given below.

Remove old files:

```bash
rm -r build dist Indago.egg-info
```

Build:
```bash
/opt/anaconda3/bin/python setup.py clean sdist bdist_wheel
```

Upload to TestPyPI:
```bash
twine upload --repository-url https://test.pypi.org/legacy/ dist/*
```

Upload to PyPI:
```bash
twine upload --repository-url https://upload.pypi.org/legacy/ dist/*
```
